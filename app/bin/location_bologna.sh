#!/bin/sh

# bologna
ARGS="11.3316855 44.4991182"

PORT=`adb devices | grep emulator | awk '{print $1}' | cut -d"-" -f2`

SEND_FIX="geo fix "

if [ $# -eq 2 ]
then
	ARGS="$1 $2"
fi

for p in $PORT; do

echo sending to "emulator-${p}"
   telnet localhost $p << EOF 
$SEND_FIX $ARGS
EOF
   echo
done
