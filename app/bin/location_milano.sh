#!/bin/sh

# milano
ARGS="9.1777323 45.4627338"

PORT=`adb devices | grep emulator | awk '{print $1}' | cut -d"-" -f2`

SEND_FIX="geo fix "

if [ $# -eq 2 ]
then
	ARGS="$1 $2"
fi

for p in $PORT; do

echo sending to "emulator-${p}"
   telnet localhost $p << EOF 
$SEND_FIX $ARGS
EOF
   echo
done
