package io.nlopez.smartlocation.geocoding;

import io.nlopez.smartlocation.OnGeocodingListener;
import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.utils.Logger;
import android.app.IntentService;
import android.content.Context;
import android.location.Location;

/**
 * Created by mrm on 20/12/14.
 */
public interface GeocodingProvider {
    void init(Context context, Logger logger);

    void addName(String name, int maxResults);

    void addLocation(Location location, int maxResults);

    void start(OnGeocodingListener geocodingListener, OnReverseGeocodingListener reverseGeocodingListener);
    
    Class<? extends IntentService> getGeocodingServiceClass();

    void stop();

}
