package io.nlopez.smartlocation.geocoding.providers;

import java.util.Locale;

import android.app.IntentService;

/**
 * Geocoding provider based on Android's Geocoder class.
 */
public class DefaultAndroidGeocodingProvider extends AbstractAndroidGeocodingProvider {

    public DefaultAndroidGeocodingProvider() {
        super(Locale.getDefault());
    }

    public DefaultAndroidGeocodingProvider(Locale locale) {
        super(locale);
    }
    
    @Override
    public Class<? extends IntentService> getGeocodingServiceClass() {
    	return SLAndroidGeocodingService.class;
    }
}
