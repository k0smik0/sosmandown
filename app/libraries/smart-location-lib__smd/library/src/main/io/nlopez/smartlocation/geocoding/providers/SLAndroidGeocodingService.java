package io.nlopez.smartlocation.geocoding.providers;

import io.nlopez.smartlocation.geocoding.utils.LocationAddress;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

public class SLAndroidGeocodingService extends IntentService {

	private static final String TAG = "SLAndroidGeocodingService";
	private Geocoder geocoder;

	public SLAndroidGeocodingService() {
		super(SLAndroidGeocodingService.class.getSimpleName());
		Log.d(TAG, "" + hashCode());
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		Log.d(TAG, "started");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Locale locale = (Locale) intent.getSerializableExtra(AbstractAndroidGeocodingProvider.LOCALE_ID);
		geocoder = new Geocoder(this, locale);

		Log.d(TAG, "handling intent:" + intent);

		if (intent.hasExtra(AbstractAndroidGeocodingProvider.DIRECT_GEOCODING_ID)) {
			@SuppressWarnings("unchecked")
			HashMap<String, Integer> nameList = (HashMap<String, Integer>) intent.getSerializableExtra(AbstractAndroidGeocodingProvider.DIRECT_GEOCODING_ID);
			for (String name : nameList.keySet()) {
				int maxResults = nameList.get(name);
				ArrayList<LocationAddress> response = addressFromName(name, maxResults);
				sendDirectGeocodingBroadcast(name, response);
			}
		}

		if (intent.hasExtra(AbstractAndroidGeocodingProvider.REVERSE_GEOCODING_ID)) {
			@SuppressWarnings("unchecked")
			HashMap<Location, Integer> locationList = (HashMap<Location, Integer>) intent.getSerializableExtra(AbstractAndroidGeocodingProvider.REVERSE_GEOCODING_ID);
			for (Location location : locationList.keySet()) {
				int maxResults = locationList.get(location);
				ArrayList<Address> response = addressFromLocation(location, maxResults);
				Log.d("AndroidGeocodingService:onHandleIntent:172", "response: " + response.size());
				sendReverseGeocodingBroadcast(location, response);
			}
		}
	}

	private void sendDirectGeocodingBroadcast(String name, ArrayList<LocationAddress> results) {
		Intent directIntent = new Intent(AbstractAndroidGeocodingProvider.BROADCAST_DIRECT_GEOCODING_ACTION);
		directIntent.putExtra(AbstractAndroidGeocodingProvider.NAME_ID, name);
		directIntent.putExtra(AbstractAndroidGeocodingProvider.RESULT_ID, results);
		sendBroadcast(directIntent);
	}

	private void sendReverseGeocodingBroadcast(Location location, ArrayList<Address> results) {
		Intent reverseIntent = new Intent( AbstractAndroidGeocodingProvider.BROADCAST_REVERSE_GEOCODING_ACTION);
		reverseIntent.putExtra(AbstractAndroidGeocodingProvider.LOCATION_ID, location);
		reverseIntent.putExtra(AbstractAndroidGeocodingProvider.RESULT_ID, results);
		sendBroadcast(reverseIntent);
	}

	private ArrayList<Address> addressFromLocation(Location location, int maxResults) {
		try {
			return new ArrayList<>(geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), maxResults));
		} catch (IOException ignored) {}
		return new ArrayList<>();
	}

	private ArrayList<LocationAddress> addressFromName(String name, int maxResults) {
		try {
			List<Address> addresses = geocoder.getFromLocationName(name, maxResults);
			ArrayList<LocationAddress> result = new ArrayList<>();
			for (Address address : addresses) {
				result.add(new LocationAddress(address));
			}
			return result;
		} catch (IOException ignored) {}
		return new ArrayList<>();
	}
}