package io.nlopez.smartlocation.geocoding.providers;

import io.nlopez.smartlocation.OnGeocodingListener;
import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.geocoding.GeocodingProvider;
import io.nlopez.smartlocation.geocoding.utils.LocationAddress;
import io.nlopez.smartlocation.utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Set;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Location;
import android.util.Log;

/**
 * Geocoding provider based on Android's Geocoder class.
 */
public abstract class AbstractAndroidGeocodingProvider implements GeocodingProvider {
    public static final String BROADCAST_DIRECT_GEOCODING_ACTION = AbstractAndroidGeocodingProvider.class.getCanonicalName() + ".DIRECT_GEOCODE_ACTION";
    public static final String BROADCAST_REVERSE_GEOCODING_ACTION = AbstractAndroidGeocodingProvider.class.getCanonicalName() + ".REVERSE_GEOCODE_ACTION";
    public static final String DIRECT_GEOCODING_ID = "direct";
    public static final String REVERSE_GEOCODING_ID = "reverse";
    public static final String LOCALE_ID = "locale";
    public static final String NAME_ID = "name";
    public static final String LOCATION_ID = "location";
    public static final String RESULT_ID = "result";


    private Locale locale;
    private OnGeocodingListener geocodingListener;
    private OnReverseGeocodingListener reverseGeocodingListener;
    private HashMap<String, Integer> fromNameList;
    private HashMap<Location, Integer> fromLocationList;
    private Context context;
    private Logger logger;

    public AbstractAndroidGeocodingProvider() {
        this(Locale.getDefault());
    }

    public AbstractAndroidGeocodingProvider(Locale locale) {
        this.locale = locale;
        fromNameList = new HashMap<>();
        fromLocationList = new HashMap<>();
    }

    @Override
    public void init(Context context, Logger logger) {
        this.logger = logger;
        this.context = context;
    }

    @Override
    public void addName(String name, int maxResults) {
        fromNameList.put(name, maxResults);
    }

    @Override
    public void addLocation(Location location, int maxResults) {
        fromLocationList.put(location, maxResults);
    }

    @Override
    public void start(OnGeocodingListener geocodingListener, OnReverseGeocodingListener reverseGeocodingListener) {
Log.d("AndroidGeocodingProvider","starting");    	
        this.geocodingListener = geocodingListener;
        this.reverseGeocodingListener = reverseGeocodingListener;

        if (fromNameList.isEmpty() && fromLocationList.isEmpty()) {
            logger.w("No direct geocoding or reverse geocoding points added");
Log.d("AndroidGeocodingProvider","No direct geocoding or reverse geocoding points added");
        } else {
            // Registering receivers for both possibilities
            IntentFilter directFilter = new IntentFilter(BROADCAST_DIRECT_GEOCODING_ACTION);
            IntentFilter reverseFilter = new IntentFilter(BROADCAST_REVERSE_GEOCODING_ACTION);

            // Launch service for processing the geocoder stuff in a background thread
//            Intent serviceIntent = new Intent(context, io.nlopez.smartlocation.geocoding.providers.AndroidGeocodingProvider.AndroidGeocodingService
//.class);
            Intent serviceIntent = new Intent(context, getGeocodingServiceClass());
Log.d("AndroidGeocodingProvider","starting "+serviceIntent);
            serviceIntent.putExtra(LOCALE_ID, locale);
            if (!fromNameList.isEmpty()) {
                context.registerReceiver(directReceiver, directFilter);
                serviceIntent.putExtra(DIRECT_GEOCODING_ID, fromNameList);
            }
            if (!fromLocationList.isEmpty()) {
                context.registerReceiver(reverseReceiver, reverseFilter);
                serviceIntent.putExtra(REVERSE_GEOCODING_ID, fromLocationList);
Set<Entry<Location, Integer>> entrySet = fromLocationList.entrySet();
String s = "";
for (Entry<Location, Integer> entry : entrySet) {
	s+=", "+entry.getKey()+":"+entry.getValue();
}
Log.d("AndroidGeocodingProvider.start:98","prepared "+reverseReceiver+" with "+reverseFilter+" and "+s.replaceFirst(",",""));
            }
            context.startService(serviceIntent);
        }
Log.d("AndroidGeocodingProvider","fromName size: "+fromNameList.size());
Log.d("AndroidGeocodingProvider","fromLocation size: "+fromLocationList.size());
    }

    @Override
    public void stop() {
        try {
            context.unregisterReceiver(directReceiver);
        } catch (IllegalArgumentException e) {
            logger.d("Silenced 'receiver not registered' stuff (calling stop more times than necessary did this)");
        }

        try {
            context.unregisterReceiver(reverseReceiver);
        } catch (IllegalArgumentException e) {
            logger.d("Silenced 'receiver not registered' stuff (calling stop more times than necessary did this)");
        }
    }

    private BroadcastReceiver directReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BROADCAST_DIRECT_GEOCODING_ACTION.equals(intent.getAction())) {
                logger.d("sending new direct geocoding response");
                if (geocodingListener != null) {
                    String name = intent.getStringExtra(NAME_ID);
                    ArrayList<LocationAddress> results = intent.getParcelableArrayListExtra(RESULT_ID);
                    geocodingListener.onLocationResolved(name, results);
                }
            }
        }
    };

    private BroadcastReceiver reverseReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BROADCAST_REVERSE_GEOCODING_ACTION.equals(intent.getAction())) {
                logger.d("sending new reverse geocoding response");
                if (reverseGeocodingListener != null) {
                    Location location = intent.getParcelableExtra(LOCATION_ID);
                    @SuppressWarnings("unchecked")
					ArrayList<Address> results = (ArrayList<Address>) intent.getSerializableExtra(RESULT_ID);
                    reverseGeocodingListener.onAddressResolved(location, results);
                }
            }
        }
    };
}
