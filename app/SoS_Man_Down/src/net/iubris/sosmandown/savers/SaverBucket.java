/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SaverBucket<D> {

	private final List<D> dataList;
	private boolean locked;
	
	public SaverBucket(/*Application application*/) {
//		Printor.whoIAm(this,activity);
		this.dataList = new CopyOnWriteArrayList<>();
	}
	
	public void add(D data) {
		dataList.add(data);
	}
	
	public List<D> getAll() {
		return dataList;
	}
	public void resetCollection() {
		dataList.clear();
	}

	public boolean isLocked() {
		return locked;
	}
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	
	private int lastSavedCounter;
	public void setLastSavedCounter(int lastSavedCounter) {
		this.lastSavedCounter = lastSavedCounter;
	}
	public int getLastSavedCounter() {
		return lastSavedCounter;
	}
}
