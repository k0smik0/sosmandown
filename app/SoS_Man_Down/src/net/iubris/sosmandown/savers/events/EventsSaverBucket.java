/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.events;

import javax.inject.Inject;

import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.savers.SaverBucket;
import net.iubris.sosmandown.utils.Printor;
import android.content.Context;

//@Singleton
public class EventsSaverBucket extends SaverBucket<ComputedResult> {

	@Inject
	public EventsSaverBucket(Context context) {
Printor.whoIAm(this,context);
//		super(application);
	}
}
