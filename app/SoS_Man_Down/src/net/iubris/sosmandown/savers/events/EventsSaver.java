/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.events;

import net.iubris.sosmandown.savers.Saver;


public interface EventsSaver extends Saver {

	static final String COLUMN_X = "X";
	static final String COLUMN_Y = "Y";
	static final String COLUMN_Z = "Z";
	static final String COLUMN_RESULTANT = "Resultant";
	static final String COLUMN_MAJORSTATE = "MajorState";
	static final String COLUMN_MAJORSTATEWEIGHT = "MajorStateWeight";


//	EventsSaver setLatch(CountDownLatch latch);
//	void exportEvents(List<ResultEvent> events/*, String formattedDatestarted, String formattedDatenow*/) /*throws EventsSaverException*/;
		
}
