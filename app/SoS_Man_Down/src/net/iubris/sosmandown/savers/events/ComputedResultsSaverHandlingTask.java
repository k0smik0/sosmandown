/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.events;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.savers._handler.CSVFileSaverHandlingTask;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.utils.StringsUtils;

public abstract class ComputedResultsSaverHandlingTask extends CSVFileSaverHandlingTask<ComputedResult> {
	public ComputedResultsSaverHandlingTask(List<ComputedResult> computedResultsToSave, String filename, ProgressDialogHandler progressDialogHandler /*, CountDownLatch latch*/) {
		super(computedResultsToSave, filename, progressDialogHandler);
	}
	
	@Override
	protected String buildHeader() {
		StringBuilder sbF = new StringBuilder();
        sbF
//        .append("#")
//        .append( EventsSaver.COLUMN_ANDROID_ID).append(CSV_SEPARATOR)
        .append( EventsSaver.COLUMN_X ).append(CSV_SEPARATOR)
    	.append( EventsSaver.COLUMN_Y ).append(CSV_SEPARATOR)
    	.append( EventsSaver.COLUMN_Z ).append(CSV_SEPARATOR)
    	.append( EventsSaver.COLUMN_RESULTANT ).append(CSV_SEPARATOR) //  "accelerationResultant" 
    	.append( EventsSaver.COLUMN_MAJORSTATE).append(CSV_SEPARATOR) // major
    	.append( EventsSaver.COLUMN_MAJORSTATEWEIGHT ).append(CSV_SEPARATOR) // "weight"
    	.append( EventsSaver.COLUMN_TIMESTAMP ) //  "timestamp"
    	.append(StringsUtils.NEW_LINE);
        
        return sbF.toString();
	}
	
	@Override
	protected void handleDataItem(ComputedResult dataItem, FileWriter fileWriter) throws IOException {
		String s = buildStringToWrite(dataItem);
		fileWriter.append( s );
	}
	
	@Override
	protected String buildStringToWrite(ComputedResult computedResult) {
		SensorEventDataHolder sensorEventDataHolder = computedResult.getSensorEventDataHolder();
		WorkingMajorState major = computedResult.getEventState().getMajor();
    	StringBuilder stringBuilderMajorState = buildStringBuilderMajor(sensorEventDataHolder, major);
		return stringBuilderMajorState.toString();
	}
	
	private static StringBuilder buildStringBuilderMajor(SensorEventDataHolder sensorEventDataHolder, WorkingMajorState major) {
		StringBuilder sb = new StringBuilder();
    	sb
//        	.append(androidID).append(CSV_SEPARATOR)
        	.append( sensorEventDataHolder.x ).append(CSV_SEPARATOR)
        	.append( sensorEventDataHolder.y ).append(CSV_SEPARATOR)
        	.append( sensorEventDataHolder.z ).append(CSV_SEPARATOR)
        	.append( sensorEventDataHolder.resultant ).append(CSV_SEPARATOR)
        	.append( major.name()).append(CSV_SEPARATOR)
        	.append( major.getWeight() ).append(CSV_SEPARATOR)
        	.append( sensorEventDataHolder.timestamp )
        	.append(StringsUtils.NEW_LINE);
    	return sb;
	}
	
	@Override
	protected void handleException(Exception e) {
		e.printStackTrace();			
	}
}
