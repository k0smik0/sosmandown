/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.events.csv.drive;

import javax.inject.Inject;

import net.iubris.sosmandown._di.annotations.AndroidID;
import net.iubris.sosmandown._di.annotations.DeviceName;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.savers.events.EventsSaverBucket;
import net.iubris.sosmandown.savers.events.csv.CSVFileEventsSaver;
import net.iubris.sosmandown.savers.utils.GoogleDriveUploaderTask;
import net.iubris.sosmandown.utils.Printor;
import net.iubris.sosmandown.utils.ThreadsUtil;
import roboguice.inject.ContextSingleton;
import roboguice.util.Ln;
import android.app.Activity;

@ContextSingleton
//@Singleton
public class GoogleDriveCSVFileEventsSaver extends CSVFileEventsSaver {
	
	private final Activity activity;
	private GoogleDriveUploaderTask googleDriveUploaderTask;
	
	private final String driveEventsFolderDriveID = Config.Saver.Detection.DRIVE_DETECTIONS_FOLDER_DRIVE_ID_CLEAR;
		
	@Inject
	public GoogleDriveCSVFileEventsSaver(Activity activity, 
			EventsSaverBucket eventsSaverBucket,
			ProgressDialogHandler progressDialogHandler,
			@DeviceName String deviceName,
			@AndroidID String androidID, ThreadsUtil threadsUtil ) {
		super(activity, eventsSaverBucket, progressDialogHandler, deviceName, androidID, threadsUtil);
		Printor.whoIAm(this, activity);
		this.activity = activity;
	}
	
	@Override
	protected void doSomethingBeforePersist(String folderName, String filename) {
		Ln.d("activity:"+activity);
//		Ln.d("applicationContext:"+activity.getApplicationContext());
		googleDriveUploaderTask = new GoogleDriveUploaderTask(activity, folderName, filename, progressDialogHandler, driveEventsFolderDriveID);
		Ln.d("googleDriveUploaderTask:"+googleDriveUploaderTask);
	}

	
	@Override
	protected void doSomethingPostSave() {
		contextedSaverHandler.post(new Runnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
//				GoogleDriveUploaderTask googleDriveUploaderTask = new GoogleDriveUploaderTask(activity, resources, folderName, filename, progressDialogHandler);
				googleDriveUploaderTask.execute();
			}
        });
	}
}
