/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.events;

public class EventsSaverException extends Exception {

	private static final long serialVersionUID = -1684335093941486366L;

	public EventsSaverException() {
		super();
	}

	public EventsSaverException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public EventsSaverException(String detailMessage) {
		super(detailMessage);
	}

	public EventsSaverException(Throwable throwable) {
		super(throwable);
	}

	
}
