/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.events.csv;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

import net.iubris.sosmandown._di.annotations.AndroidID;
import net.iubris.sosmandown._di.annotations.DeviceName;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.engine.computing_state.ComputedState;
import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.savers.Exceptionable;
import net.iubris.sosmandown.savers._handler.CSVFileSaverHandlingTask;
import net.iubris.sosmandown.savers._handler.ContextedSaverHandler;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.savers.events.AbstractEventsSaver;
import net.iubris.sosmandown.savers.events.ComputedResultsSaverHandlingTask;
import net.iubris.sosmandown.savers.events.EventsSaverBucket;
import net.iubris.sosmandown.savers.utils.PersistDelegate;
import net.iubris.sosmandown.utils.StringsUtils;
import net.iubris.sosmandown.utils.ThreadsUtil;
import roboguice.util.Ln;
import android.app.Activity;

//@ContextSingleton
//@Singleton
public class CSVFileEventsSaver extends AbstractEventsSaver implements Exceptionable {
	
//	private final static String CSV_SEPARATOR = StringsUtils.PIPE;
	private final static String FILENAME_PREFIX = Config.APP_NAME_INTERNAL.toLowerCase()+"__events__";
//	private final static String FILENAME_EXTENSION = ".csv";
	private final static String CSV_FOLDER_NAME = "events";
	
//	private final AtomicReference<Boolean> persistExceptionOccurredReference = new AtomicReference<>( Boolean.valueOf(false) );
//	private final AtomicReference<String> persistExceptionMessageReference = new AtomicReference<>(StringsUtils.EMPTY);
	
	protected final ContextedSaverHandler<CSVFileEventsSaver> contextedSaverHandler;
	
	private final PersistDelegate<ComputedResult, CSVFileSaverHandlingTask<ComputedResult>> persistDelegate;
	
	@Inject
	public CSVFileEventsSaver(Activity activity, 
			EventsSaverBucket eventsSaverBucket,
			ProgressDialogHandler progressDialogHandler,
			@DeviceName final String deviceName,
			@AndroidID final String androidID, 
			ThreadsUtil threadsUtil ) {
		super(eventsSaverBucket, progressDialogHandler, deviceName, androidID);
		this.contextedSaverHandler = new ContextedSaverHandler<>(activity, this);
		
		this.persistDelegate = new PersistDelegate<ComputedResult, CSVFileSaverHandlingTask<ComputedResult>>(threadsUtil) {
			@Override
			protected String getFileName(List<ComputedResult> data, int numEvents, String padding) {
				long fromMS = data.get(0).getSensorEventDataHolder().timestamp;
		        long toMS = data.get( numEvents-1 ).getSensorEventDataHolder().timestamp;
		        String filename = 
						buildFileName(buildFileNamePrefix(FILENAME_PREFIX, deviceName, androidID, Config.Detection.Impact.getImpactThresholdForGravityComplementaryMultiplier()), 
						getFilenameTimeStampsPart(fromMS, toMS), 
						CSVFileSaverHandlingTask.FILENAME_EXTENSION)
						.replace(StringsUtils.SPACE, StringsUtils.UNDERSCORE);
				return filename;
			}
			@Override
			protected CSVFileSaverHandlingTask<ComputedResult> getCSVFileSaverHandlingTask(List<ComputedResult> data, String filenameWithFullPath, ProgressDialogHandler progressDialogHandler) {
				return new ContextedHandledComputedResultsSaverHandlingTask(data, filenameWithFullPath, progressDialogHandler);
			}
			@Override
			protected String getCSVFolderName() {
				return CSV_FOLDER_NAME;
			}
			@Override
			protected void doSomethingBeforePersist(String folderName, String filename) {
				CSVFileEventsSaver.this.doSomethingBeforePersist(folderName, filename);
			}
		};
	}
	
	@Override
	protected String getDestinationSaving() {
		return "csv file...";
	}
	
	@Override
	protected void persist(final List<ComputedResult> eventsToSave) {
		// reset before writing starts
//		persistExceptionOccurredReference.set( Boolean.valueOf(false) );
		super.persist(eventsToSave);
		
		persistDelegate.persist(eventsToSave, progressDialogHandler);
    }
		
	@SuppressWarnings("unused")
	protected void doSomethingBeforePersist(String folderName, String filename) {}

	protected void doSomethingPostSave() {
		saverBucket.setLocked(false);
	}

	/*@Override
	public boolean hasExceptionOccurredInTask() {
		return persistExceptionOccurredReference.get().booleanValue();
	}
    @Override
	public String getExceptionMessageFromTask() {
		return persistExceptionMessageReference.get();
	}*/
    
	protected class ContextedHandledComputedResultsSaverHandlingTask extends ComputedResultsSaverHandlingTask {
		
		protected ContextedHandledComputedResultsSaverHandlingTask(List<ComputedResult> dataItemsToSave, String filename, ProgressDialogHandler progressDialogHandler /*, CountDownLatch latch*/) {
			super(dataItemsToSave, filename, progressDialogHandler);
		}
						
		@Override
		protected boolean preHandleDataItem(ComputedResult computedResult, AtomicInteger numDataItemsToSave) {
			ComputedState state = computedResult.getEventState();
        	WorkingMajorState major = state.getMajor();
        	if (major.equals(WorkingMajorStateEnum.INIT)) {
        		int decrementedNumDataItemsToSave= numDataItemsToSave.decrementAndGet();
        		
        		String message = getMessagePrefix(decrementedNumDataItemsToSave)+getDestinationSaving();
        		this.progressDialogHandler.setProgressDialogMessage(message);                		
        		this.progressDialogHandler.setProgressDialogMax(decrementedNumDataItemsToSave);
        		// was: continue
        		return false;
        	}
        	return true;
		}
		
		@Override
		protected void postHandleDataItem(final int dataItemHandledIndex) {
        	if (dataItemHandledIndex%250==0) {
        		contextedSaverHandler.post(new Runnable() {
					@Override
					public void run() {
Ln.d("sending "+dataItemHandledIndex+" to progressdialog");
						ContextedHandledComputedResultsSaverHandlingTask.this
							.progressDialogHandler.updateProgressDialog(dataItemHandledIndex);
					}
            	});
        	}
		}

		@Override
		public void run() {
			super.run();
			contextedSaverHandler.sendEmptyMessage(0);
		}
		
		@Override
		protected void handleIOException(IOException e) {
			String msg = e.getCause()+StringsUtils.SPACE+e.getMessage();
        	setPersistExceptionOccurredReference(msg);
Ln.d("IOException: "+msg);
		}

		@Override
		protected void doSomethingPostSave() {
			CSVFileEventsSaver.this.doSomethingPostSave();			
		}		
	}
}
