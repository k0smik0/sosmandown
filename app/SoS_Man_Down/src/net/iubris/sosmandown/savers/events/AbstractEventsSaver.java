/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.events;

import java.util.List;

import net.iubris.sosmandown.config.Config.Saver.Events;
import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.savers.AbstractSaver;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import roboguice.util.Ln;

public abstract class AbstractEventsSaver extends AbstractSaver<ComputedResult> implements EventsSaver {
	
	public AbstractEventsSaver(EventsSaverBucket eventsSaverBucket, ProgressDialogHandler progressDialogHandler, String deviceName, String androidID) {
		super(eventsSaverBucket, progressDialogHandler, deviceName, androidID);
	}
	
	@Override
	public final void save() {
		saverBucket.setLocked(true);
		final List<ComputedResult> lastNAccelerationEvents = getLastNAccelerationEvents(saverBucket.getAll());
		final int size = lastNAccelerationEvents.size();
		saverBucket.setLastSavedCounter(size);		
Ln.d("saving "+size+" events");
		String message = getMessagePrefix(size)+getDestinationSaving();
		progressDialogHandler.setProgressDialogMessage(message);
		persist(lastNAccelerationEvents);
	}
	protected List<ComputedResult> getLastNAccelerationEvents(List<ComputedResult> events) {		
		List<ComputedResult> eventsToSave;
		int numEvents = events.size();
Ln.d("found "+numEvents+" events");
		String message = getMessagePrefix(numEvents)+getDestinationSaving();
		progressDialogHandler.setProgressDialogMessage(message);
		if (numEvents>Events.EVENTS_TO_SAVE_FOR_EXTERNAL_ANALYSIS_MAX_NUM) {
			eventsToSave = events.subList(events.size()-Events.EVENTS_TO_SAVE_FOR_EXTERNAL_ANALYSIS_MAX_NUM, events.size()-1);
		} else {
			eventsToSave = events;
		}
Ln.d("saving "+eventsToSave.size()+" events");
		return eventsToSave;
	}
	
//	@Override
//	protected abstract void persist(List<ComputedResult> events);
	
	@Override
	protected String getMessagePrefix(int numDetections) {
		return "saving "+numDetections+" detections on ";
	}
}
