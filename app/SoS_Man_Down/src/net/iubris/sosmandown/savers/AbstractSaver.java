/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import net.iubris.sosmandown.savers._handler.ProgressDialogDismissable;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.utils.StringsUtils;

public abstract class AbstractSaver<D> implements Saver, ProgressDialogDismissable {
	
//	protected final DateFormat dateFormat_yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
	
	protected final SaverBucket<D> saverBucket;
	protected final ProgressDialogHandler progressDialogHandler;
	
	protected final String androidID;
	protected final String deviceName;
	
	private final AtomicReference<Boolean> persistExceptionOccurredReference = new AtomicReference<>( Boolean.valueOf(false) );
	private final AtomicReference<String> persistExceptionMessageReference = new AtomicReference<>(StringsUtils.EMPTY);
	
	public AbstractSaver(SaverBucket<D> saverBucket, ProgressDialogHandler progressDialogHandler, String deviceName, String androidID) {
		this.saverBucket = saverBucket;
//Ln.d("saverBucket: "+saverBucket);
		this.progressDialogHandler = progressDialogHandler;

		this.deviceName = deviceName;
		this.androidID = androidID;
	}

	@Override
	public void progressDialogDismiss() {
		progressDialogHandler.dismissProgressDialog();		
	}
	
	@Override
	public void reset() {
		saverBucket.resetCollection();
	}
	
	
	protected abstract String getMessagePrefix(int numDetections);
	protected abstract String getDestinationSaving();
	
	@SuppressWarnings("unused")
	protected void persist(List<D> events) {
		resetPersistExceptionOccurredReference();
	}
	protected void setPersistExceptionOccurredReference(String optionallyExceptionMessage) {
		persistExceptionOccurredReference.set(true);
		persistExceptionMessageReference.set(optionallyExceptionMessage);
	}
	protected void resetPersistExceptionOccurredReference() {
		persistExceptionOccurredReference.set(false);
		persistExceptionMessageReference.set(StringsUtils.EMPTY);
	}
//	@Override
	public boolean hasExceptionOccurredInTask() {
		return persistExceptionOccurredReference.get().booleanValue();
	}
//    @Override
	public String getExceptionMessageFromTask() {
		return persistExceptionMessageReference.get();
	}
}
