/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.utils;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.savers._handler.CSVFileSaverHandlingTask;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.utils.StringsUtils;
import net.iubris.sosmandown.utils.ThreadsUtil;
import roboguice.util.Ln;
import android.os.Environment;

public abstract class PersistDelegate<D, T extends CSVFileSaverHandlingTask<D>> {
	
	private final ThreadsUtil threadsUtil;
	
	public PersistDelegate(ThreadsUtil threadsUtil) {
		this.threadsUtil = threadsUtil;
	}
	
	private File getFolder() {
		File folder = new File(Environment.getExternalStorageDirectory()+File.separator
        		+Config.APP_NAME_INTERNAL+File.separator
        		+getCSVFolderName());
		if (!folder.exists()) {
            folder.mkdirs();
        }
		return folder;
	}
			
	
	public void persist(List<D> data, ProgressDialogHandler progressDialogHandler) {
		// reset before writing starts
//		exceptionOccurredReference.set( Boolean.valueOf(false) );

		final int size = data.size();
        if (checkDataSize(size, progressDialogHandler)) {
        	return;
        }
        
        final File folder = getFolder();
		String filename = getFileName(data, size, StringsUtils.EMPTY);
    	final String filenameWithFullPath = folder.toString() + File.separator +filename;
    	final String folderName = folder.toString();
    	
    	doSomethingBeforePersist(folderName, filename);
    	
    	final CSVFileSaverHandlingTask<D> fileSavingTask = getCSVFileSaverHandlingTask(data, filenameWithFullPath, progressDialogHandler); 
        threadsUtil.execOnTmp(fileSavingTask);
	}
	
	public void persist(List<D> data, int realFallDetectedOccurrence, ProgressDialogHandler progressDialogHandler) {
		final int size = data.size();
        if (!checkDataSize(size, progressDialogHandler)) {
        	return;
        }
        
        final File folder = getFolder();
		String filename = getFileName(data, size, "__real_falls_"+realFallDetectedOccurrence+"__");
    	final String filenameWithFullPath = folder.toString() + File.separator +filename;
    	final String folderName = folder.toString();
    	
    	doSomethingBeforePersist(folderName, filename);
    	
    	Ln.d("saving on: "+filenameWithFullPath);
    	
    	final CSVFileSaverHandlingTask<D> fileSavingTask = getCSVFileSaverHandlingTask(data, filenameWithFullPath, progressDialogHandler); 
        threadsUtil.execOnTmp(fileSavingTask);
	}
	
//	protected String getFilenameWithFullPath() {}
	
	protected static boolean checkDataSize(int size, ProgressDialogHandler progressDialogHandler) {
		Ln.d("size:"+size);
        if (size==0) {
Ln.d("zero items to save, exiting...");
			progressDialogHandler.dismissProgressDialog();
        	return false;
        }
        return true;
	}
	
	protected final DateFormat dateFormat_yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
	protected String getFilenameTimeStampsPart(long timestampFromPrefix, long timestampFromTo) {
		Ln.d("fromMS:"+timestampFromPrefix+" toMS:"+timestampFromTo);
        Date dateFrom = new Date( timestampFromPrefix );
        Date dateTo = new Date( timestampFromTo );
Ln.d("dateFrom:"+dateFrom+" dateTo:"+dateTo);
		String dateFromString = dateFormat_yyyyMMddHHmmss.format(  dateFrom );
		String dateToString = dateFormat_yyyyMMddHHmmss.format( dateTo );
		String filename = dateFromString+StringsUtils.UNDERSCORE+dateToString;
		return filename;
	}
	
	protected abstract String getFileName(List<D> data, int numEvents, String padding);
//	protected abstract String getFileName(D firstItem, D lastItem);
	
	protected static String buildFileNamePrefix(String filenamePrefix, String deviceName, String androidID, double gravityComplementaryMultiplier) {
		return filenamePrefix+deviceName+StringsUtils.UNDERSCORE+androidID+StringsUtils.UNDERSCORE+StringsUtils.UNDERSCORE+"m"+gravityComplementaryMultiplier+StringsUtils.UNDERSCORE;		
	}	
	protected static String buildFileName(String filenamePrefix, String filenameContent, String filenameExtension) {
		String filename = filenamePrefix
				+filenameContent
				+"."+filenameExtension;
		return filename;
	}
	
	protected abstract CSVFileSaverHandlingTask<D> getCSVFileSaverHandlingTask(List<D> data, String filenameWithFullPath, ProgressDialogHandler progressDialogHandler);
	
	@SuppressWarnings("unused")
	protected void doSomethingBeforePersist(String folderName, String filename) {}
	
	protected abstract String getCSVFolderName();
}
