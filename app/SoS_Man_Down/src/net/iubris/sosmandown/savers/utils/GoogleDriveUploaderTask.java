/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.savers.utils.GoogleDriveUploaderTask.DriveResult;
import net.iubris.sosmandown.ui.activities.main.google.GoogleApiClientDelegateProvider;
import net.iubris.sosmandown.ui.activities.main.google.GoogleApiClientProvider;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.StringsUtils;
import net.iubris.sosmandown.utils.ThreadsUtil;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import roboguice.util.RoboAsyncTask;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.IntentSender.SendIntentException;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveFolder.DriveFileResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;

public class GoogleDriveUploaderTask extends RoboAsyncTask/*AndroidCallable*/<DriveResult> {

//	private GoogleApiClient googleApiClient;
//	protected DriveId folderDriveId;
	
//	private final FragmentActivity activity;
	private final String filename;
	private final ProgressDialogHandler progressDialogHandler;
//	private final Handler mainThreadHandler;
	
//	@Inject @AndroidID 
//	private final String androidID;
	
	
//	private final Random randomClientIdGenerator;
//	@SuppressWarnings("unused")
//	private int clientId;
	private final static int COUNTDOWNLATCH_COUNTER = 1;
	private CountDownLatch latchGoogleapiConnection;
//	private boolean googleapiConnected;

//	https://drive.google.com/folderview?id=0B9oS3H2pbafWcGV4emtUV3hUeFU&usp=sharing
//	private static final String DRIVE_FOLDER_RESOURCE_ID = "0B9oS3H2pbafWcGV4emtUV3hUeFU";
	// to obtain this below: new DriveId(DRIVE_FOLDER_RESOURCE_ID,0,0,0).encodeToString();
//	private static final String DRIVE_EVENTS_FOLDER_DRIVE_ID = "DriveId:CAESHDBCOW9TM0gycGJhZldjR1Y0ZW10VVYzaFVlRlUYACAAKAA=";

	protected static final int REQUEST_CODE_RESOLUTION = 1;
	private DriveId fileDriveId;
	private CountDownLatch latchExecution;
	
	private final String dialogButtonCancel;
	
	
	private final AtomicBoolean sending = new AtomicBoolean(true);
	private final OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
//			sendingTask.stop();
			sending.set(false);
			dialog.dismiss();
		}
	};
	private String folderName;
	private GoogleApiClientProvider googleApiClientProvider;
	
	private final String saverMessageQueuePrefix;
	private final String saverMessageQueueSuffix;
	
	@InjectView(R.id.textviewEventsSaverNotificationBottomBarContent)
	protected TextView textviewEventssaveContent;
	
	@InjectResource(R.string.saver__error__no_network_available)
	private String errorNoNetworkAvailable;

	
	@Inject 
	private ThreadsUtil threadsUtil;
	
//	private static final String oa = "972886220911-vv3bo3486228naogm4vo2l1uo7fk6ihi.apps.googleusercontent.com";
	
//	private final Activity context;
	
	private final String driveEventsFolderDriveId;

	public GoogleDriveUploaderTask(Context context, /*Resources resources,*/ String folderName, String filename, ProgressDialogHandler progressDialogHandler, String driveEventsFolderDriveId) {
		super(context);
//		this.context = activity;
		this.driveEventsFolderDriveId = driveEventsFolderDriveId;
		
//		textviewEventssaveContent = (TextView) activity.findViewById(R.id.textviewEventsSaverNotificationBottomBarContent);
		
		googleApiClientProvider = ((GoogleApiClientDelegateProvider) context).getGoogleApiClientDelegate();
Ln.d("googleApiClientProvider:"+googleApiClientProvider);
		this.folderName = folderName;
//		this.activity = activity;
//		Ln.d(this);
//		this.externalLatch = latch;
		this.filename = filename;
Ln.d("acting on filename: "+filename);
//		dateFormat = new SimpleDateFormat(DATE_PATTERN);
		this.progressDialogHandler = progressDialogHandler;
		
//		this.mainThreadHandler = new Handler();

//		Ln.d(Config.APP_NAME+"_"+androidID);
//		this.randomClientIdGenerator = new Random( Long.valueOf(Config.APP_NAME/*+"_"+androidID*/, Character.MAX_RADIX) );
		
//		callbackLatch = new CountDownLatch(1);
		
		Resources resources = context.getResources();
		
		dialogButtonCancel = resources.getString(R.string.saver_dialog_button_cancel);
		saverMessageQueuePrefix = resources.getString(R.string.saver_message_queue_prefix);
		saverMessageQueueSuffix = resources.getString(R.string.saver_message_queue_suffix);
		
		Ln.d(this);
	}

//	private static final String MAIL = "sosmandownapp@gmail.com";
	/*private void googleapiConnect() {
		Ln.d("connect - begin");
//		latchGoogleapiConnection = new CountDownLatch(COUNTDOWNLATCH_COUNTER);
//		googleApiClientProvider.getGoogleApiClient(connectionCallbacks, onConnectionFailedListener);
//		Drive.DriveApi.
		// usaew quello della guida ufficiale!
		Ln.d("connect - end");
	}*/
	
	private final ConnectionCallbacks connectionCallbacks = new ConnectionCallbacks() {
		@Override
		public void onConnected(Bundle arg0) {
Ln.d("onConnected");
//			try {
//				googleapiConnected = true;
			if (latchGoogleapiConnection!=null) {
				latchGoogleapiConnection.countDown();
			}
//				doPostConnect();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
		}
		@Override
		public void onConnectionSuspended(int arg0) {
Ln.d("onConnectionSuspended");
//			googleapiConnected = false;
//			googleapiConnectionLatch.countDown();
		}
	};
	
	private final OnConnectionFailedListener onConnectionFailedListener = new OnConnectionFailedListener() {
		@Override
		public void onConnectionFailed(ConnectionResult connectionResult) {
Ln.d("onConnectionFailed");
			int errorCode = connectionResult.getErrorCode();
			String string = connectionresultError.get(errorCode);
Ln.d("error code:"+errorCode+" = "+string);
			
			if (!connectionResult.hasResolution()) {
				googleApiClientProvider.getGoogleApiClient().stopAutoManage((FragmentActivity) context);
//				googleapiConnected = false;
				latchGoogleapiConnection.countDown();
				SToast.with(context).showLong(errorNoNetworkAvailable);
				return;
			}
			try {
	            connectionResult.startResolutionForResult((Activity) context, REQUEST_CODE_RESOLUTION);
	        } catch (SendIntentException e) {
Ln.d("Exception while starting resolution activity", e);
	        }
		}
	};
	
    private void showMessage(String string) {
    	SToast.with(context).showLong(string);
    }

    @Override
	public void onPreExecute() throws Exception {
Ln.d("onPreExecute");
		progressDialogHandler.setProgressDialogButton(Dialog.BUTTON_NEGATIVE, dialogButtonCancel, onClickListener);
Ln.d("progressDialogHandler:"+progressDialogHandler);
		GoogleApiClient googleApiClient = googleApiClientProvider.getGoogleApiClient();
Ln.d("googleApiClient:"+googleApiClient);
		if (googleApiClient==null || !googleApiClient.isConnected()) {
			latchGoogleapiConnection = new CountDownLatch(COUNTDOWNLATCH_COUNTER);
Ln.d("latchGoogleapiConnection:"+latchGoogleapiConnection);
			googleApiClientProvider.buildGoogleApiClient(connectionCallbacks, onConnectionFailedListener);
			googleApiClient = googleApiClientProvider.getGoogleApiClient();
			googleApiClient.connect();
		} 
	}
    
    @Override
	public DriveResult call() throws Exception {
Ln.d("call");
//		googleapiConnect();
		if (latchGoogleapiConnection!=null) {
			latchGoogleapiConnection.await();
		}
		if (!googleApiClientProvider.getGoogleApiClient().isConnected()) {
			throw new Exception("not connected");
		}
		latchExecution = new CountDownLatch(COUNTDOWNLATCH_COUNTER);
Ln.d("latchExecution:"+latchExecution);
		doPostConnect(googleApiClientProvider.getGoogleApiClient());
		latchExecution.await();
		if (fileDriveId!=null) {
Ln.d("returning:"+fileDriveId);
			return new DriveResult(filename, fileDriveId);
		}
		throw new DriveException("fileDriveId NULL!");
	}
    
    @Override
	public void onSuccess(DriveResult t) /*throws Exception*/ {
//		latchExecution.await();
Ln.d("driveId: "+t.driveId+" for "+t.filename);
//		showMessage("Created a file: " + t);
		showMessage(saverMessageQueuePrefix+StringsUtils.EMPTY+t.filename+StringsUtils.SPACE+saverMessageQueueSuffix);
		progressDialogHandler.dismissProgressDialog();
	}
	
	@Override
	public void onException(Exception e) throws RuntimeException {
//		try {
//			latchExecution.await();
			
//			getGoogleApiClient().stopAutoManage((FragmentActivity) context);
//			getGoogleApiClient().disconnect();
//			googleApiClientProvider.stop();
//			googleApiClientProvider.disconnect();
			
			e.printStackTrace();
			showMessage(e.getMessage());

			if (latchExecution!=null) {
				latchExecution.countDown();
			}
			progressDialogHandler.dismissProgressDialog();
		/*} catch (InterruptedException e1) {
			e1.printStackTrace();
		}*/
	}
	
//	@SuppressWarnings("resource")
	private void doPostConnect(final GoogleApiClient googleApiClient) throws Exception {
Ln.d("doPostConnect");
		DriveId folderDriveId = DriveId.decodeFromString(driveEventsFolderDriveId);
Ln.d("folderDriveId: '"+folderDriveId+"' from "+driveEventsFolderDriveId);
		
//		new RoboAsyncTask<Void>(context) {
//			
//			private OutputStream outputStream;
//
//			@Override
//			public Void call() throws Exception {
//				String folderDriveIdString = "";
//				DriveId folderDriveId = Drive.DriveApi.fetchDriveId(googleApiClient, folderDriveIdString).await().getDriveId();
				
Ln.d("folderDriveId: '"+folderDriveId+"' from "+Config.Saver.Detection.DRIVE_DETECTIONS_FOLDER_DRIVE_ID_CLEAR);
				
		        DriveFolder folder = Drive.DriveApi.getFolder(googleApiClient, folderDriveId);
Ln.d("folder.driveId:"+folder.getDriveId());

		        String filenameWithFullPath = folderName + File.separator +filename;
		        final long filesizeKB = getFileSize();

				/*mainThreadHandler.post(*/
				threadsUtil.execOnUI(
						new Runnable() {
					@Override
					public void run() {
						progressDialogHandler.setProgressDialogMax((int)filesizeKB);
//				        progressDialogHandler.progressDialogSetMessage("sendind data to developer ("+String.format("%2f", filesizeKB)+"kb)");
				        progressDialogHandler.setProgressDialogMessage("sendind data to developer ("+filesizeKB+"kb)");
					}
				});
				
				
				DriveContentsResult driveContentsResult = Drive.DriveApi.newDriveContents(googleApiClient).await();
				Status driveContentsResultStatus = driveContentsResult.getStatus();
Ln.d("driveContentsResultStatus status success:"+driveContentsResultStatus.isSuccess()
						+" code:"+driveContentsResultStatus.getStatusCode()
						+" message:"+driveContentsResultStatus.getStatusMessage() );		
				if (!driveContentsResultStatus.isSuccess()) {
					latchExecution.countDown();
//					dismissProgressOnUiThread();
					throw new DriveException("Error while trying to create new file contents");
		        }
				DriveContents driveContents = driveContentsResult.getDriveContents();
Ln.d("driveContents.mode:"+driveContents.getMode());
				@SuppressWarnings("resource")
				OutputStream outputStream = driveContents.getOutputStream();
		        addContentToFile(outputStream, filenameWithFullPath, /*fileSize,*/ sending);
		        
		        
		        MetadataChangeSet changeSet = buildChangeSet(filename);
		        DriveFileResult driveFileResult = folder.createFile(googleApiClient, changeSet, driveContents).await();
		        
		        Status driveFileResultStatus = driveFileResult.getStatus();
Ln.d("status:"+driveFileResultStatus);
		        if (!driveFileResult.getStatus().isSuccess()) {
Ln.d(driveFileResultStatus.describeContents()+" "+driveFileResultStatus.getStatusMessage()+" "+driveFileResultStatus.getStatus().getStatusCode());
		        	try { 
		        		outputStream.close();
		        	} catch(NullPointerException e) {}
//		        	progressDialogHandler.dismissProgressDialog();
//		        	dismissProgressOnUiThread();
		        	latchExecution.countDown();
		            throw new DriveException("Error creating the file");
		        }
		        final DriveId fileDriveId = driveFileResult.getDriveFile().getDriveId();
Ln.d("driveFileResult.driveFile.driveId:"+driveFileResult.getDriveFile().getDriveId());
		        
		        try {
		        	Status status = driveContents.commit(googleApiClient, changeSet).await();
Ln.d("commit:"+ status.isSuccess());
		        } catch(IllegalStateException e) {
		        	// catch "DriveContents already closed"
		        	Ln.d(e.getMessage());
//		        	throw e;        	
		        }
//		        mainThreadHandler.post(
		        dismissProgressOnUiThread();
		        
		        
		        GoogleDriveUploaderTask.this.fileDriveId = fileDriveId;
Ln.d("assigning fileDriveId:" +fileDriveId );
		        
		        latchExecution.countDown();
				
			/*	return null;
			}
			@Override
			protected void onException(Exception e) throws RuntimeException {
				if (outputStream!=null) {
					try {
						outputStream.close();
					} catch (IOException e1) {}
				}
			}
		}.execute();*/
	}
	private static MetadataChangeSet buildChangeSet(String filename) {
		MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
        .setTitle(filename)
        .setMimeType("text/csv")
        .setStarred(true)
        .setLastViewedByMeDate(new Date())
        .build();
		return changeSet;
	}
	
	private long getFileSize() {
		String filenameWithFullPath = folderName + File.separator +filename;
        long fileSize = new File(filenameWithFullPath).length();
        final long filesizeKB = fileSize/1024;
Ln.d("start sending file of "+fileSize+" (="+filesizeKB+"kb)");
		return filesizeKB;
	}

	private void addContentToFile(OutputStream outputStream, String filenameWithFullPath, /*long fileSize,*/ AtomicBoolean sending) {
Ln.d("adding text file to outputstream...");
		byte[] buffer = new byte[1024];
		int bytesRead;
		final AtomicInteger bytesReadSum = new AtomicInteger();
		@SuppressWarnings("resource")
		BufferedInputStream inputStream = null;
		try {
			inputStream = new BufferedInputStream(new FileInputStream(filenameWithFullPath));
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				if (!sending.get()) {
Ln.d("sending:"+sending.get());
					break;
				}
				outputStream.write(buffer, 0, bytesRead);
				bytesReadSum.addAndGet(bytesRead);
				final long sentInKb = bytesReadSum.get()/1024;
				if (sentInKb % 30 == 0) {
					updateProgress(sentInKb);
				}
			}
Ln.d("red and wrote: "+bytesReadSum.get()+ "="+bytesReadSum.get()/1024);
		} catch (IOException e) {
Ln.d("problem converting input stream to output stream: " + e);
			e.printStackTrace();
		} finally {
			closeStream(inputStream);
			closeStream(outputStream);
		}
		final long sentInKb = bytesReadSum.get()/1024;
		updateProgress(sentInKb);
	}
	private void updateProgress(final long sentInKb) {
Ln.d(/*"bytesReadSum:" + bytesReadSum + " | " +*/ "sentInKB:" + sentInKb);
			threadsUtil.execOnUI(new Runnable() {
				@Override
				public void run() {
Ln.d("updating dialog to:" + sentInKb);
					progressDialogHandler.updateProgressDialog((int) sentInKb);
				}
			});
	}
	private void dismissProgressOnUiThread() {
		threadsUtil.execOnUI(
        		new Runnable() {
			@Override
			public void run() {
				progressDialogHandler.dismissProgressDialog();
			}
		});		
	}
	private static void closeStream(InputStream inputStream) {
		if (inputStream!=null) {
			try {
				inputStream.close();
Ln.d("closed inputStream "+inputStream.hashCode());
			} catch (IOException e) {
Ln.d("error closing inputstream "+inputStream.hashCode());
			}
		}
	}
	private static void closeStream(OutputStream outputStream) {
		if (outputStream!=null) {
			try {
				outputStream.close();
				Ln.d("closed outputStream:"+outputStream.hashCode());
			} catch (IOException e) {
				Ln.d("error closing outputstream:"+outputStream.hashCode());
			}
		}
	}
	
//	public static class MyUploadProgressListener implements MediaHttpUploaderProgressListener {
//
//    public void progressChanged(MediaHttpUploader uploader) throws IOException {
//      switch (uploader.getUploadState()) {
//        case INITIATION_STARTED:
//          System.out.println("Initiation Started");
//          break;
//        case INITIATION_COMPLETE:
//          System.out.println("Initiation Completed");
//          break;
//        case MEDIA_IN_PROGRESS:
//          System.out.println("Upload in progress");
//          System.out.println("Upload percentage: " + uploader.getProgress());
//          break;
//        case MEDIA_COMPLETE:
//          System.out.println("Upload Completed!");
//          break;
//      }
//    }
//  }
	
	public static class DriveResult {
		private String filename;
		private DriveId driveId;
		public DriveResult(String filename, DriveId driveId) {
			this.filename = filename;
			this.driveId = driveId;
		}
	}
	
	public static class DriveException extends Exception {
		private static final long serialVersionUID = 1914478540934911648L;
		public DriveException() {
			super();
		}
		public DriveException(String detailMessage, Throwable throwable) {
			super(detailMessage, throwable);
		}
		public DriveException(String detailMessage) {
			super(detailMessage);
		}
		public DriveException(Throwable throwable) {
			super(throwable);
		}
	}
	
	
	private static Map<Integer,String> connectionresultError = new HashMap<>();
	static {
		connectionresultError.put(ConnectionResult.SUCCESS,"SUCCESS"); // 0
		connectionresultError.put(ConnectionResult.SERVICE_MISSING,"SERVICE_MISSING"); // 1
		connectionresultError.put(ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED,"SERVICE_VERSION_UPDATE_REQUIRED"); // 2 
		connectionresultError.put(ConnectionResult.SERVICE_DISABLED,"SERVICE_DISABLED"); // 3
		connectionresultError.put(ConnectionResult.SIGN_IN_REQUIRED, "SIGN_IN_REQUIRED"); // 4
		connectionresultError.put(ConnectionResult.INVALID_ACCOUNT,"INVALID_ACCOUNT"); // 5
		connectionresultError.put(ConnectionResult.RESOLUTION_REQUIRED,"RESOLUTION_REQUIRED"); // 6
		connectionresultError.put(ConnectionResult.NETWORK_ERROR,"NETWORK_ERROR"); // 7
		connectionresultError.put(ConnectionResult.INTERNAL_ERROR,"INTERNAL_ERROR"); // 8
		connectionresultError.put(ConnectionResult.SERVICE_INVALID,"SERVICE_INVALID"); // 9
		connectionresultError.put(ConnectionResult.DEVELOPER_ERROR,"DEVELOPER_ERROR"); // 10
		connectionresultError.put(ConnectionResult.LICENSE_CHECK_FAILED,"LICENSE_CHECK_FAILED"); // 11
		connectionresultError.put(ConnectionResult.CANCELED,"CANCELED"); // 13
		connectionresultError.put(ConnectionResult.TIMEOUT,"TIMEOUT"); // 14
		connectionresultError.put(ConnectionResult.INTERRUPTED,"INTERRUPTED"); // 15
		connectionresultError.put(ConnectionResult.API_UNAVAILABLE,"API_UNAVAILABLE"); // 16
		connectionresultError.put(ConnectionResult.SIGN_IN_FAILED, "SIGN_IN_FAILED"); // 17
		connectionresultError.put(ConnectionResult.SERVICE_UPDATING,"SERVICE_UPDATING"); // 18
		connectionresultError.put(ConnectionResult.DRIVE_EXTERNAL_STORAGE_REQUIRED,"DRIVE_EXTERNAL_STORAGE_REQUIRED"); // 1500
	}
	
}
