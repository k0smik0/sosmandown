/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.detections.csv;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import net.iubris.sosmandown._di.annotations.AndroidID;
import net.iubris.sosmandown._di.annotations.DeviceName;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.savers.Exceptionable;
import net.iubris.sosmandown.savers.SaverBucket;
import net.iubris.sosmandown.savers._handler.CSVFileSaverHandlingTask;
import net.iubris.sosmandown.savers._handler.ContextedSaverHandler;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.savers.detections.AbstractDetectionSaver;
import net.iubris.sosmandown.savers.detections.DetectionsSaverBucket.DetectionResult;
import net.iubris.sosmandown.savers.detections.DetectionsSaverHandlingTask;
import net.iubris.sosmandown.savers.utils.PersistDelegate;
import net.iubris.sosmandown.utils.StringsUtils;
import net.iubris.sosmandown.utils.ThreadsUtil;
import roboguice.util.Ln;
import android.annotation.SuppressLint;
import android.app.Activity;

@SuppressLint("DefaultLocale")
public class CSVFileDetectionsSaver extends AbstractDetectionSaver implements Exceptionable {
	
	private static final String CSV_FOLDER = "detections";

	protected static final String FILENAME_PREFIX = Config.APP_NAME_INTERNAL.toLowerCase()+"__detections__";
	
	protected final ContextedSaverHandler<CSVFileDetectionsSaver> contextedSaverHandler;

	private PersistDelegate<DetectionResult, CSVFileSaverHandlingTask<DetectionResult>> persistDelegate;

	private int realFallDetectedOccurrence;

	public CSVFileDetectionsSaver(Activity activity, 
			SaverBucket<DetectionResult> saverBucket, 
			ProgressDialogHandler progressDialogHandler, 
			@DeviceName final String deviceName,
			@AndroidID final String androidID, 
			ThreadsUtil threadsUtil ) {
		super(saverBucket, progressDialogHandler, deviceName, androidID);
		
		this.contextedSaverHandler = new ContextedSaverHandler<>(activity, this);
		
		persistDelegate = new PersistDelegate<DetectionResult, CSVFileSaverHandlingTask<DetectionResult>>(threadsUtil) {
			@Override
			protected String getFileName(List<DetectionResult> data, int numEvents, String padding) {
				long fromMS = data.get(0).timestamp;
		        long toMS = data.get( numEvents-1 ).timestamp;
		        String filename = 
						buildFileName(buildFileNamePrefix(FILENAME_PREFIX, deviceName, androidID, Config.Detection.Impact.getImpactThresholdForGravityComplementaryMultiplier()), 
								getFilenameTimeStampsPart(fromMS, toMS), 
								CSVFileSaverHandlingTask.FILENAME_EXTENSION);
		        
		        String ext = StringsUtils.DOT+CSVFileSaverHandlingTask.FILENAME_EXTENSION;
		        filename = filename.replace(ext, padding+ext).replace(StringsUtils.SPACE, StringsUtils.UNDERSCORE);
				return filename;
			}
			@Override
			protected CSVFileSaverHandlingTask<DetectionResult> getCSVFileSaverHandlingTask(List<DetectionResult> data, String filenameWithFullPath, ProgressDialogHandler progressDialogHandler) {
				return new ContextedHandledDetectionResultsSaverHandlingTask(data, filenameWithFullPath, progressDialogHandler);
			}
			@Override
			protected String getCSVFolderName() {
				return CSV_FOLDER;
			}
			@Override
			protected void doSomethingBeforePersist(String folderName, String filename) {
				CSVFileDetectionsSaver.this.doSomethingBeforePersist(folderName, filename);
			}
			
		};
	}

	@Override
	protected String getDestinationSaving() {
		return "csv file...";
	}
	
	@Override
	public void setRealFallDetectedOccurrence(int realFallDetectedOccurrence) {
		this.realFallDetectedOccurrence = realFallDetectedOccurrence;
	}

	@Override
	protected void persist(List<DetectionResult> detections) {
		super.persist(detections);
        persistDelegate.persist(detections, realFallDetectedOccurrence, progressDialogHandler);
	}
	
	@SuppressWarnings("unused")
	protected void doSomethingBeforePersist(String folderName, String filename) {}
	
	protected void doSomethingPostSave() {
		saverBucket.setLocked(false);
	}
	
	protected class ContextedHandledDetectionResultsSaverHandlingTask extends DetectionsSaverHandlingTask {
		
		protected ContextedHandledDetectionResultsSaverHandlingTask(List<DetectionResult> dataItemsToSave, String filename, ProgressDialogHandler progressDialogHandler /*, CountDownLatch latch*/) {
			super(dataItemsToSave, filename, progressDialogHandler);
		}
						
		@Override
		protected boolean preHandleDataItem(DetectionResult detectionResult, AtomicInteger numDataItemsToSave) {
			/*ComputedState state = computedResult.getEventState();
        	WorkingMajorState major = state.getMajor();
        	if (major.equals(WorkingMajorStateEnum.INIT)) {
        		int decrementedNumDataItemsToSave= numDataItemsToSave.decrementAndGet();
        		
        		String message = getMessagePrefix(decrementedNumDataItemsToSave)+getDestinationSaving();
        		this.progressDialogHandler.progressDialogSetMessage(message);                		
        		this.progressDialogHandler.progressDialogSetMax(decrementedNumDataItemsToSave);
        		// was: continue
        		return false;
        	}*/
        	return true;
		}
		
		@Override
		protected void postHandleDataItem(final int dataItemHandledIndex) {
        	if (dataItemHandledIndex%250==0) {
        		contextedSaverHandler.post(new Runnable() {
					@Override
					public void run() {
Ln.d("sending "+dataItemHandledIndex+" to progressdialog");
						ContextedHandledDetectionResultsSaverHandlingTask.this
							.progressDialogHandler.updateProgressDialog(dataItemHandledIndex);
					}
            	});
        	}
		}

		@Override
		public void run() {
			super.run();
			contextedSaverHandler.sendEmptyMessage(0);
		}
		
		@Override
		protected void handleIOException(IOException e) {
			String msg = e.getCause()+StringsUtils.SPACE+e.getMessage();
        	setPersistExceptionOccurredReference(msg);
Ln.d("IOException: "+msg);
		}
		@Override
		protected void doSomethingPostSave() {
			CSVFileDetectionsSaver.this.doSomethingPostSave();			
		}
	}
}
