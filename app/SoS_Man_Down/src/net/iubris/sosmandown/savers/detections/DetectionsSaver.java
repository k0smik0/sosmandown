/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.detections;

import net.iubris.sosmandown.savers.Saver;

public interface DetectionsSaver extends Saver {

	static final String COLUMN_WORKING_MAJOR_STATE_RESULT = "working_major_state_result";  
	static final String COLUMN_COUNTER_POSTURE_WALKING = "counter_posture_walking";
	static final String COLUMN_COUNTER_ACTIVITY_WALKING = "counter_activity_walking";
	static final String COLUMN_COUNTER_ACTIVITY_RUN = "counter_activity_run"; 
	static final String COLUMN_COUNTER_ACTIVITY_SHAKE = "counter_activity_shake";
	static final String COLUMN_COUNTER_BIG_MOVEMENTS = "counter_big_movements";
	
	void setRealFallDetectedOccurrence(int realFallDetectedOccurrence);
}
