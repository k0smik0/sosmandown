/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.detections.csv.drive;

import javax.inject.Inject;

import net.iubris.sosmandown._di.annotations.AndroidID;
import net.iubris.sosmandown._di.annotations.DeviceName;
import net.iubris.sosmandown.config.Config.Saver.Detection;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.savers.detections.DetectionsSaverBucket;
import net.iubris.sosmandown.savers.detections.csv.CSVFileDetectionsSaver;
import net.iubris.sosmandown.savers.utils.GoogleDriveUploaderTask;
import net.iubris.sosmandown.utils.ThreadsUtil;
import roboguice.inject.ContextSingleton;
import android.app.Activity;

@ContextSingleton
public class GoogleDriveCSVFileDetectionsSaver extends CSVFileDetectionsSaver {
	
	private final Activity activity;
	private GoogleDriveUploaderTask googleDriveUploaderTask;
	
	private static final String DRIVE_DETECTIONS_FOLDER_DRIVE_ID = Detection.DRIVE_DETECTIONS_FOLDER_DRIVE_ID_ENCODED;
		
	@Inject
	public GoogleDriveCSVFileDetectionsSaver(Activity activity, 
			DetectionsSaverBucket detectionsSaverBucket,
			ProgressDialogHandler progressDialogHandler,
			@DeviceName String deviceName,
			@AndroidID String androidID, ThreadsUtil threadsUtil ) {
		super(activity, detectionsSaverBucket, progressDialogHandler, deviceName, androidID, threadsUtil);
//Ln.d(this+", context:"+activity);
		this.activity = activity;
	}
	
	@Override
	protected void doSomethingBeforePersist(String folderName, String filename) {
//Ln.d("activity:"+activity);
//		Ln.d("applicationContext:"+activity.getApplicationContext());
		googleDriveUploaderTask = new GoogleDriveUploaderTask(activity, folderName, filename, progressDialogHandler, DRIVE_DETECTIONS_FOLDER_DRIVE_ID);
//Ln.d("googleDriveUploaderTask:"+googleDriveUploaderTask);
	}

	
	@Override
	protected void doSomethingPostSave() {
		contextedSaverHandler.post(new Runnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
//				GoogleDriveUploaderTask googleDriveUploaderTask = new GoogleDriveUploaderTask(activity, resources, folderName, filename, progressDialogHandler);
				googleDriveUploaderTask.execute();
			}
        });
	}
}
