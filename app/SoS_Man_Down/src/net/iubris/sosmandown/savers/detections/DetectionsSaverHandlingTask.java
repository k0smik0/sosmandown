/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.detections;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import net.iubris.sosmandown.savers._handler.CSVFileSaverHandlingTask;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.savers.detections.DetectionsSaverBucket.DetectionResult;
import net.iubris.sosmandown.utils.StringsUtils;

public abstract class DetectionsSaverHandlingTask extends CSVFileSaverHandlingTask<DetectionResult> {
	public DetectionsSaverHandlingTask(List<DetectionResult> detectionsToSave, String filename, ProgressDialogHandler progressDialogHandler) {
		super(detectionsToSave, filename, progressDialogHandler);
	}
	
	@Override
	protected String buildHeader() {
		StringBuilder sbF = new StringBuilder();
        sbF
//        .append("#")
//        .append( EventsSaver.COLUMN_ANDROID_ID).append(CSV_SEPARATOR)
        .append( DetectionsSaver.COLUMN_COUNTER_ACTIVITY_RUN).append(CSV_SEPARATOR)
        .append( DetectionsSaver.COLUMN_COUNTER_ACTIVITY_WALKING).append(CSV_SEPARATOR)
        .append( DetectionsSaver.COLUMN_COUNTER_ACTIVITY_SHAKE).append(CSV_SEPARATOR)
        .append( DetectionsSaver.COLUMN_COUNTER_POSTURE_WALKING).append(CSV_SEPARATOR)
        .append( DetectionsSaver.COLUMN_COUNTER_BIG_MOVEMENTS).append(CSV_SEPARATOR)
        .append( DetectionsSaver.COLUMN_WORKING_MAJOR_STATE_RESULT).append(CSV_SEPARATOR)
    	.append( DetectionsSaver.COLUMN_TIMESTAMP ) //  "timestamp"
    	.append(StringsUtils.NEW_LINE);
        
        return sbF.toString();
	}
	
	@Override
	protected void handleDataItem(DetectionResult dataItem, FileWriter fileWriter) throws IOException {
		String s = buildStringToWrite(dataItem);
		fileWriter.append( s );
	}
	
	@Override
	protected String buildStringToWrite(DetectionResult  detectionResult) {
//		SensorEventDataHolder sensorEventDataHolder = computedResult.getSensorEventDataHolder();
//		WorkingMajorState major = computedResult.getEventState().getMajor();
    	StringBuilder stringBuilder = buildStringBuilder(detectionResult);
		return stringBuilder.toString();
	}
	
	private static StringBuilder buildStringBuilder(DetectionResult detectionResult) {
		StringBuilder sb = new StringBuilder();
    	sb
//        	.append(androidID).append(CSV_SEPARATOR)
    	.append( detectionResult.counterActivityRun ).append(CSV_SEPARATOR)
    	.append( detectionResult.counterActivityWalking ).append(CSV_SEPARATOR)
    	.append( detectionResult.counterActivityShake ).append(CSV_SEPARATOR)
    	.append( detectionResult.counterPostureWalking ).append(CSV_SEPARATOR)
    	.append( detectionResult.workingMajorStateEnum ).append(CSV_SEPARATOR)
        	.append(StringsUtils.NEW_LINE);
    	return sb;
	}
	
	@Override
	protected void handleException(Exception e) {
		e.printStackTrace();			
	}
}
