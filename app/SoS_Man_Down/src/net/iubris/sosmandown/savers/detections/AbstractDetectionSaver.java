/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.detections;

import java.util.List;

import net.iubris.sosmandown.savers.AbstractSaver;
import net.iubris.sosmandown.savers.SaverBucket;
import net.iubris.sosmandown.savers._handler.ProgressDialogHandler;
import net.iubris.sosmandown.savers.detections.DetectionsSaverBucket.DetectionResult;
import roboguice.util.Ln;

public abstract class AbstractDetectionSaver extends AbstractSaver<DetectionResult> implements DetectionsSaver {

	public AbstractDetectionSaver(SaverBucket<DetectionResult> saverBucket, 
			ProgressDialogHandler progressDialogHandler, 
			String deviceName, String androidID) {
		super(saverBucket, progressDialogHandler, deviceName, androidID);
	}
	
	@Override
	public void save() {
		saverBucket.setLocked(true);
		
		List<DetectionResult> detectionResults = saverBucket.getAll();
		int size = detectionResults.size();
		saverBucket.setLastSavedCounter(size);		
Ln.d("saving "+size+" detections");

		String message = getMessagePrefix(size)+getDestinationSaving();
		progressDialogHandler.setProgressDialogMessage(message);
		persist(detectionResults);
	}
	
	@Override
	protected String getMessagePrefix(int numDetections) {
		return "saving "+numDetections+" detections on ";
	}
}
