/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.detections;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.savers.SaverBucket;
import net.iubris.sosmandown.savers.detections.DetectionsSaverBucket.DetectionResult;
import net.iubris.sosmandown.utils.Printor;
import android.content.Context;

//@ContextSingleton
@Singleton
public class DetectionsSaverBucket extends SaverBucket<DetectionResult> {
	
	@Inject
	public DetectionsSaverBucket(Context context) {
Printor.whoIAm(this,context);
	}
	
	public static class DetectionResult {
		public final WorkingMajorStateEnum workingMajorStateEnum;
		public final int counterActivityRun;
		public final int counterActivityWalking;
		public final int counterActivityShake; 
		public final int counterPostureWalking; 
		public final int bigMovementsCounter;
		public final long timestamp;
		public DetectionResult(WorkingMajorStateEnum workingMajorStateEnum, int counterActivityRun, int counterActivityWalking,
				int counterActivityShake, int counterPostureWalking, int bigMovementsCounter, long timestamp) {
			this.workingMajorStateEnum = workingMajorStateEnum;
			this.counterActivityRun = counterActivityRun;
			this.counterActivityWalking = counterActivityWalking;
			this.counterActivityShake = counterActivityShake;
			this.counterPostureWalking = counterPostureWalking;
			this.bigMovementsCounter = bigMovementsCounter;
			this.timestamp = timestamp;
		}
	}
}
