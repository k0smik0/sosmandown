/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers.detections;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.RoboGuice;
import roboguice.util.Ln;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class DetectionsSaverDialoger {
	
//	private Activity activity;
//	private Builder alertDialogBuilder;
//	private final OKAlertDialogListener okAlertDialogListener = new OKAlertDialogListener();
//	private EditText detectionSaverPromptEditText;
	protected boolean canSave;
	protected int realFallDetected;
	private AlertDialog alertDialog;
//	private OnOkCallback onOkCallback;
	
	
	public int getRealFallDetected() {
		return realFallDetected;
	}
	public void showPromptDialog() {
		alertDialog.show();
	}
	/*public void setOnOkCallback(OnOkCallback onOkCallback) {
		this.onOkCallback = onOkCallback;
	}*/
	
	@Inject
	private DetectionsSaver detectionsSaver;
//	private RoboInjector injector;

	@Inject
	public DetectionsSaverDialoger(final Activity activity) {
//		this.activity = activity;
		
		RoboGuice.injectMembers(activity,this);
		
		View view = LayoutInflater.from(activity).inflate(R.layout.detections_saver_prompt,null);
		
		final EditText detectionSaverPromptEditText = (EditText) view.findViewById(R.id.detections_saver_prompt_edit_text);

		Builder alertDialogBuilder = new AlertDialog.Builder( activity );
		alertDialogBuilder.setView( view );
		alertDialogBuilder.setTitle( activity.getString(R.string.saver__detections_saver__title) );
		
		alertDialog = alertDialogBuilder
		.setPositiveButton( activity.getString(R.string.preference_dialog_button_ok), 
				/*okAlertDialogListener*/
				new AlertDialogListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Ln.d("saverDialog: OK");
						
						String text = StringsUtils.EMPTY + detectionSaverPromptEditText.getText();
						try {
							int value = Integer.parseInt(text);
Ln.d("value:" + value);
							if (value > -1) {
								realFallDetected = value;
Ln.d("saverDialog: saving!");
								detectionsSaver.setRealFallDetectedOccurrence(realFallDetected);
								detectionsSaver.save();
							} else {
								realFallDetected = -1;
								String prefix = activity.getString(R.string.saver__detections_saver__question__error_value);
								SToast.with(activity).showShort(prefix+StringsUtils.SPACE+value);
							}
						} catch(NumberFormatException e) {
							realFallDetected = -1;
							String msg = activity.getString(R.string.saver__detections_saver__question__error_value_not_number);
							SToast.with(activity).showShort(msg);
						}						
					}
				})
		.setNegativeButton(activity.getString(R.string.preference_dialog_button_cancel), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
Ln.d("saverDialog: CANCEL");
					alertDialog.dismiss();
				}
			})
		.create();
		
		
		
//		Ln.d("detectionSaverPromptEditText"+detectionSaverPromptEditText);
		
//		if (detectionSaverPromptEditText != null) {
			/*detectionSaverPromptEditText.setOnEditorActionListener(new OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					String text = StringsUtils.EMPTY + v.getText();
					int value = Integer.parseInt(text);
					Ln.d("value:" + value);
					if (value > -1) {
						canSave = true;
						realFallDetected = value;
 
//						onOkCallback.doSomething();
						
						detectionsSaver.setRealFallDetectedOccurrence(value);
						Ln.d("detectionsSaver.setRealFallDetectedOccurrence: "+value);

						return true;
					}
					SToast.INSTANCE.show("wrong value: " + value);
					realFallDetected = -1;
					canSave = false;
					return false;
				}
			});*/

		alertDialog.setOnShowListener( new OnShowListener() {
			
			@Override
			public void onShow(DialogInterface dialog) {
				detectionSaverPromptEditText.requestFocus();
			}
		});
	}
	
	private abstract static class AlertDialogListener implements DialogInterface.OnClickListener {
//		protected Preference preferenceToEdit;
//		protected String key;
//		protected String valueToSet;
		
		/*public Preference getPreferenceToEdit() {
			return preferenceToEdit;
		}*/
		/*public void setPreference(Preference preferenceToEdit) {
			this.preferenceToEdit = preferenceToEdit;
		}*/
		/*public String getValueToSet() {
			return valueToSet;
		}
		public void setValueToSet(String valueToSet) {
			this.valueToSet = valueToSet;
		}*/
		/*public void setPreferenceKey(String key) {
			this.key = key;
		}
		public String getPreferenceKey() {
			return key;
		}*/
		@Override
		public abstract void onClick(DialogInterface dialog, int which);
	}
	/*private static class OKAlertDialogListener extends AlertDialogListener {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			Ln.d("saverDialog: OK");
			detectionsSaver.save();
		}
	}*/
	
	public static interface OnOkCallback {
		void doSomething();
	}
	
	
//	wss_wpapskbuilder.setMessage(sWpaAlert);
}
