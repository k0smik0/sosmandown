/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers._handler;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.util.Ln;

public abstract class CSVFileSaverHandlingTask<D> extends SaverHandlingTask<D> {
	
	protected final static String CSV_SEPARATOR = StringsUtils.PIPE;
	public final static String FILENAME_EXTENSION = "csv";
	
	protected final String filename;
	protected final ProgressDialogHandler progressDialogHandler;
	
	protected CSVFileSaverHandlingTask(List<D> dataItemsToSave, String filename, ProgressDialogHandler progressDialogHandler /*, CountDownLatch latch*/) {
		super(dataItemsToSave);
		this.filename = filename;
		this.progressDialogHandler = progressDialogHandler /*, CountDownLatch latch*/;
	}
	
	protected abstract String buildHeader();
	
	@Override
	public void run() {
		super.run();
		try {
			AtomicInteger numDataItemsToSave = new AtomicInteger( dataItemsToSave.size() );
			FileWriter fileWriter = preHandleData(numDataItemsToSave.get());
			int dataItemHandledIndex = 0;
            for (D dataItemToSave : dataItemsToSave) {
            	if (!preHandleDataItem(dataItemToSave, numDataItemsToSave)) {
            		continue;
            	}

            	handleDataItem(dataItemToSave, fileWriter);
            	
            	final int valueToDraw = (dataItemHandledIndex++);
            	postHandleDataItem(valueToDraw);
			}
            fileWriter.flush();
            fileWriter.close();
            
Ln.d("really saved: "+numDataItemsToSave);
            
			doSomethingPostSave();                
        } catch(IOException e) {
        	handleIOException(e);
        } catch(Exception e) {
        	handleException(e);
        }
	}
	
	protected abstract void doSomethingPostSave();

	protected FileWriter preHandleData(int numDataItemsToSave) throws IOException {
        FileWriter fileWriter = new FileWriter(filename);
Ln.d("saving "+numDataItemsToSave+" dataItems on csv file");
        progressDialogHandler.setProgressDialogMax(numDataItemsToSave);
        String header = buildHeader();
        fileWriter.append( header );
        return fileWriter;
	}
	
	protected abstract boolean preHandleDataItem(D dataItemToSave, AtomicInteger numDataItemsToSave);
	
	
	protected void handleDataItem(D dataItem, FileWriter fileWriter) throws IOException {		
		String s = buildStringToWrite(dataItem);
		fileWriter.append( s );
	}
	protected abstract String buildStringToWrite(D dataItem);
	protected abstract void postHandleDataItem(final int dataItemHandledIndex);
	
	@SuppressWarnings("unused")
	protected void handleIOException(IOException e) {}
	@SuppressWarnings("unused")
	protected void handleException(Exception e) {}
}
