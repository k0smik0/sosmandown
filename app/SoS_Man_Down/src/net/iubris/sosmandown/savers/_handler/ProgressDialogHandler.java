/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers._handler;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.utils.Printor;
import roboguice.inject.ContextSingleton;
import roboguice.util.Ln;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;

@ContextSingleton
public class ProgressDialogHandler {

	private final Context context;
	
	private ProgressDialog progressDialog;

	@Inject
	public ProgressDialogHandler(Context context) {
		Printor.whoIAm(this,context);
		this.context = context;
	}

	private ProgressDialog getProgressDialog() {
		if (progressDialog==null) {
			progressDialog = new ProgressDialog(context);
			CharSequence title = context.getString(R.string.app_name);
			progressDialog.setTitle(title);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressDialog.setIndeterminate(false);
			progressDialog.setCancelable(true);
		}
		return progressDialog;
	}
	
	/*public void progressDialogSetTitle(String title) {
		progressDialog.setTitle(title);
	}*/
	
	public void setProgressDialogMax(final int max) {
		final ProgressDialog progressDialog = getProgressDialog();
		((Activity)context).runOnUiThread( new Runnable() {
			@Override
			public void run() {
				progressDialog.setMax(max);
			}
		});
		
	}
	
	public void setProgressDialogMessage(final String message) {
		final ProgressDialog progressDialog = getProgressDialog();
		if (context instanceof Activity && !((Activity)context).isFinishing()) {
			((Activity)context).runOnUiThread( new Runnable() {
				@Override
				public void run() {
					progressDialog.setMessage(message);
					progressDialog.show();
				}
			});
			
		}
	}
	
//	@Override
	public void dismissProgressDialog() {
		ProgressDialog progressDialog = getProgressDialog();
		if (progressDialog!=null) {
			progressDialog.dismiss();
		} else {
			Ln.d("progressDialog: "+null);
		}
	}


	public void updateProgressDialog(int progress) {
		progressDialog.setProgress(progress);
	}


	public void setProgressDialogButton(int whichButton, String string, OnClickListener onClickListener) {
		progressDialog.setButton(whichButton, string, onClickListener);
	}
	
	public Context getContext() {
		return context;
	}
}
