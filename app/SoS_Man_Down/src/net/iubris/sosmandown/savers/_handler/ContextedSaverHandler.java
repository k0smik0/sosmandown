/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers._handler;


import java.lang.ref.WeakReference;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.savers.Exceptionable;
import net.iubris.sosmandown.savers.Saver;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.StringsUtils;
import android.app.Activity;
import android.os.Message;

public class ContextedSaverHandler<ES extends Exceptionable & Saver & ProgressDialogDismissable> extends SaverHandler<ES> {
    	
	private final WeakReference<Activity> activityWeakreference;
	private String saverErrorPrefix;
	
	public ContextedSaverHandler(Activity activity, ES exceptionableSaver) {
		super(exceptionableSaver);
		this.activityWeakreference = new WeakReference<>(activity);
		saverErrorPrefix = activity.getResources().getString(R.string.saver__csv_writing_error_prefix);
	}

	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		
		final ES exceptionableSaver = saverWeakreference.get();
		
    	if (exceptionableSaver.hasExceptionOccurredInTask()) {
    		String toastMsg = saverErrorPrefix+StringsUtils.SPACE+exceptionableSaver.getExceptionMessageFromTask();
//    			Toast.makeText(activityWeakreference.get(), toastMsg, Toast.LENGTH_LONG).show();
			SToast.with(activityWeakreference.get()).showLong(toastMsg);
    	}
	}
}
