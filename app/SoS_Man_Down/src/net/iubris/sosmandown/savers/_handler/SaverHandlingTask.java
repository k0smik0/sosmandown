/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers._handler;

import java.util.List;

public class SaverHandlingTask<D> implements Runnable {

	protected volatile boolean working = false;
	protected List<D> dataItemsToSave;

	public SaverHandlingTask(List<D> dataItemsToSave) {
		this.dataItemsToSave = dataItemsToSave;
	}

	@Override
	public void run() {
		working = true;
	}

	public void stop() {
		working = false;
	}
}
