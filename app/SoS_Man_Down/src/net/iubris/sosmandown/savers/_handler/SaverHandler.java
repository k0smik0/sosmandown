/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.savers._handler;

import java.lang.ref.WeakReference;

import net.iubris.sosmandown.savers.Saver;
import android.os.Handler;
import android.os.Message;

public class SaverHandler<S extends Saver & ProgressDialogDismissable> extends Handler {
	protected final WeakReference<S> saverWeakreference;    	
	public SaverHandler(S saver) {
		this.saverWeakreference = new WeakReference<>(saver);
	}
	@Override
	public void handleMessage(Message msg) {
		S saver = saverWeakreference.get();
		saver.progressDialogDismiss();
	}
}
