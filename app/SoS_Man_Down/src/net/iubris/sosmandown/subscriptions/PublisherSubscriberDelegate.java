/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.subscriptions;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.utils.Printor;

import org.greenrobot.eventbus.EventBus;

import android.content.Context;

@Singleton
//@ContextSingleton
public class PublisherSubscriberDelegate {

	private final EventBus eventBus;
	
	@Inject
	public PublisherSubscriberDelegate(EventBus eventBus, Context context) {
		Printor.whoIAm(this,context);
		this.eventBus = eventBus;
	}

	public void registerSubscription(Subscriber subscriber) {
		if (!eventBus.isRegistered(subscriber)) {
			eventBus.register(subscriber);
		}
	}
	public void unregisterSubscription(Subscriber subscriber) {
		if (eventBus.isRegistered(subscriber)) {
			eventBus.unregister(subscriber);
		}
	}
	
	public void post(Object event) {
		eventBus.post(event);
	}
}
