/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.model.result;

import java.io.Serializable;

import net.iubris.sosmandown.model.state.phase.PhaseState;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;


public class LightComputedResult implements Serializable {

	private static final long serialVersionUID = 5574458478962060282L;
	
	private final PhaseState phaseState;
	private final double accelerationResultant;
	private final WorkingMajorState majorState;
	
	public LightComputedResult(PhaseState phaseState, WorkingMajorState majorState, double accelerationResultant) {
		this.phaseState = phaseState;
		this.majorState = majorState;
		this.accelerationResultant = accelerationResultant;
	}
	public PhaseState getPhaseState() {
		return phaseState;
	}
	public WorkingMajorState getMajorState() {
		return majorState;
	}
	public double getAccelerationResultant() {
		return accelerationResultant;
	}
}
