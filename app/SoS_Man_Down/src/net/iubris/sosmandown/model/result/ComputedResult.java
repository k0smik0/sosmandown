/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.model.result;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import net.iubris.sosmandown.engine.computing_state.ComputedState;

public class ComputedResult implements Serializable {
	
	private static final long serialVersionUID = 3978871152312289537L;
	
	private final ComputedState computedState;

	private final SensorEventDataHolder sensorEventDataHolder;

	public ComputedResult(SensorEventDataHolder sensorEventDataHolder, ComputedState computedState) {
		this.sensorEventDataHolder = sensorEventDataHolder;
		this.computedState = computedState;
	}
	
	public ComputedState getEventState() {
		return computedState;
	}
	
	public SensorEventDataHolder getSensorEventDataHolder() {
		return sensorEventDataHolder;
	}
	/*public void setSensorEventDataHolder(SensorEventDataHolder sensorEventDataHolder) {
		this.sensorEventDataHolder = sensorEventDataHolder;
	}*/
	
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd.HH:MM:ss.SSS", Locale.US);
	
	@Override
	public String toString() {
		return sensorEventDataHolder.resultant+" "
				+sensorEventDataHolder.timestamp
				+sdf.format(new Date(sensorEventDataHolder.timestamp)
				+" "+computedState.getPhase()+" "+computedState.getMajor()+" "+computedState.getMinor());
	}
}
