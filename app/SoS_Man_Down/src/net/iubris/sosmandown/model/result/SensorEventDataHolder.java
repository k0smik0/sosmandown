/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.model.result;

public class SensorEventDataHolder {
	public final float x;
	public final float y;
	public final float z;
	public final long timestamp;
	public final double resultant;
	public SensorEventDataHolder(float x, float y, float z, double resultant, long timestamp) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.resultant = resultant;
		this.timestamp = timestamp;
	}
}
