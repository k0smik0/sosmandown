/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.model.state.working.major.majors.impl;

import net.iubris.sosmandown.engine.computing_state.fsm.working.states.DetectingFallWorkingFSMComputingState;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.FallWorkingFSMComputingState;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.FreeFallWorkingFSMComputingState;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.GoingToImpactWorkingFSMComputingState;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.ImpactWorkingFSMComputingState;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.RelaxWorkingFSMComputingState;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.WorkingFSMComputingState;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.phase.PhaseState;
import net.iubris.sosmandown.model.state.phase.PhaseStateE;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.major.majors.WorkingMajorStateDetectingFall;
import net.iubris.sosmandown.model.state.working.major.majors.WorkingMajorStateFall;
import net.iubris.sosmandown.model.state.working.major.majors.WorkingMajorStateFreeFall;
import net.iubris.sosmandown.model.state.working.major.majors.WorkingMajorStateGoingToImpact;
import net.iubris.sosmandown.model.state.working.major.majors.WorkingMajorStateImpact;
import net.iubris.sosmandown.model.state.working.major.majors.WorkingMajorStateInit;
import net.iubris.sosmandown.model.state.working.major.majors.WorkingMajorStateNull;
import net.iubris.sosmandown.model.state.working.major.majors.WorkingMajorStateRelax;
import net.iubris.sosmandown.model.state.working.major.majors.utils.WorkingMajorStateRelaxVisitor;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorState;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorStateEnum;
import roboguice.util.Ln;

public enum WorkingMajorStateEnum implements WorkingMajorStateDetectingFall, WorkingMajorStateFreeFall, WorkingMajorStateGoingToImpact, WorkingMajorStateFall, WorkingMajorStateImpact, WorkingMajorStateInit, WorkingMajorStateNull, WorkingMajorStateRelax {

	// STARTED(0),
	NULL(-50) {
		@Override
		public Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass() {
			return WorkingFSMComputingStateDummy.class;
		}
	},
	INIT(0) {
		@Override
		public Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass() {
			return WorkingFSMComputingStateDummy.class;
		}
	},
	RELAX(1) {
		@Override
		public Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass() {
			return RelaxWorkingFSMComputingState.class;
		}
		@Override
		public void accept(WorkingMajorStateRelaxVisitor workingMajorStateRelaxVisitor) {
			workingMajorStateRelaxVisitor.visit(this);
		}
	},

	FREE_FALL(3) {
		@Override
		public Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass() {
			return FreeFallWorkingFSMComputingState.class;
		}
	}, // major

	GOING_TO_IMPACT(7)  {
		@Override
		public Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass() {
			return GoingToImpactWorkingFSMComputingState.class;
		}
	},// major

	// ANALYSIS_PHASE_2_UNDER_G,
	// ANALYSIS_PHASE_2_OVER_G,

	IMPACT(10) {
		@Override
		public void doOnState(/*Context context*/) {
			Ln.d(name() + ": "/* +getResult() */);
		}
		
		@Override
		public Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass() {
			return ImpactWorkingFSMComputingState.class;
		}
	},

	// TRANSIENT_AFTER_IMPACT,
	DETECTING_FALL(15) {
		@Override
		public Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass() {
			return DetectingFallWorkingFSMComputingState.class;
		}
	},
	/*PROBABLY_FALL(18) {
		@Override
		public Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass() {
			return ProbablyFallWorkingFSMComputingState.class;
		}
	},*/
	FALL(20) {
		@Override
		public Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass() {
			return FallWorkingFSMComputingState.class;
		}
	};
	// RESET,
	// UNKNOWN(-5),
	

	private final int weight;

	private WorkingMajorStateEnum(int weight) {
		this.weight = weight;
	}

	public static int GRAVITY_APPROXIMATED = 10;

	@Override
	public int getWeight() {
		return weight + GRAVITY_APPROXIMATED;
	}

	
	public static int getLogicZero() {
		return GRAVITY_APPROXIMATED;
	}

	public void doOnState( /*Context context*/) {}

	@Override
	public void accept(WorkingMajorStateRelaxVisitor workingMajorStateDetectingFallVisitor) {}


	public abstract Class<? extends WorkingFSMComputingState> getWorkingFSMComputedStateClass();
	private static class WorkingFSMComputingStateDummy implements WorkingFSMComputingState {
		private static final long serialVersionUID = -484977608444431539L;
		@Override
		public WorkingMajorState getMajor() {
			return WorkingMajorStateEnum.NULL;
		}
		@Override
		public WorkingMinorState getMinor() {
			return WorkingMinorStateEnum._NULL;
		}
		@Override
		public PhaseState getPhase() {
			return PhaseStateE.NULL;
		}
		@Override
		public void handle(SensorEventDataHolder sensorEventDataHolder) {}
		@Override
		public void reset() {}
		@Override
		public void prepare() {}
		@Override
		public void exec() {}
		@Override
		public void releaseResources() {}
	}
}
