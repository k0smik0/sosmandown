/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.model.state.working.minor;

public enum WorkingMinorStateEnum implements WorkingMinorState {
	
	// r: current result
	// rp: previous result
	
	_UNKNOWN(-4),
	_NULL(-40),
	
	ANY(0),
	
	_RELAX(1),
	RELAX_MOVEMENT(1),
	
	FREE_FALL_PHASE_0(3), // arrived here
	FREE_FALL_PHASE_1_DESCENT(3), // r < G && r < rp  
	FREE_FALL_PHASE_2_TRANSIENT(3), // r < G && r == rp
	FREE_FALL_PHASE_3_RAISING(3), // r < G && r > rp
	
	RAISING_PHASE_0(7), // arrived here
	RAISING_PHASE_1_DESCENT(7), // r > G && r < rp
	RAISING_PHASE_2_TRANSIENT(7), // r > G && r == rp (: r?=G)
	RAISING_PHASE_3_RAISING(7), // r > G && r > rp
	
	IMPACT_INTERRUPT(10),
	// FSM = finite state machine = we are inside State Machine
	IMPACT_FSM(10),
	
	DETECTING_FALL_START(15),
	DETECTING_FALL_TRANSIENT_NOT_ELAPSED(15), 
	DETECTING_FALL_CHECKER(15), 
	
	FALL_FSM(20);
	
	private final int weight;
	private WorkingMinorStateEnum(int weight) {
		this.weight = weight;
	}
	@Override
	public int getWeight() {
		return weight;
	}
}
