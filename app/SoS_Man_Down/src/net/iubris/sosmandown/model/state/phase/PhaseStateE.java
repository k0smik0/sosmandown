/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.model.state.phase;

import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;


public enum PhaseStateE implements PhaseStateInitializing, PhaseStateInitialized, PhaseStateStarting, PhaseStateStarted {
	INITIALIZING(0) {
		/*@Override
		public String getLabel(WorkingMajorState majorState) {
			String s = name()+" "+getStep()+"%";
			return s;
		}*/
	},
	INITIALIZED(1),
	STARTING(2),
	STARTED(3),
	NULL(-50);

	private final int weight;
	private PhaseStateE(int weight) {
		this.weight = weight;
	}
	@Override
	public int getWeight() {
		return weight;
	}
	
	private static final int INITIAL_STEP = 0;  
	private int step = INITIAL_STEP;
	@Override
	public int getStep() {
		return step;
	}
	public void setStep(int step) {
		this.step = step;
	}
	@Override
	public String getLabel(WorkingMajorState majorState) {
		return majorState.name();
	}
}
