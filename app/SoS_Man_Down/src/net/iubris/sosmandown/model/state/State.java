/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.model.state;

import java.io.Serializable;

public interface State extends Serializable {
	public int getWeight();
	public String name();
	
	

	
	/*public static class MediaPlayerProvider {
		private static MediaPlayer soundFall;
		@Inject
		public MediaPlayerProvider(Context context) {
			soundFall = MediaPlayer.create(context, R.raw.fall);
			soundFall.setVolume(10, 10);
		}
		public static MediaPlayer getSoundFall() {
			return soundFall;
		}
	}*/
}
