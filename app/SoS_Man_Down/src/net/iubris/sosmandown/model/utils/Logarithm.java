/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.model.utils;

public class Logarithm {
	
	public static double log2 = Math.log(2);

	public static double log_base(double x, double base) {
		return Math.log(x) / Math.log(base);
	}

	public static double log_base2(double x) {
//		return log_base(x, 2);
		return Math.log(x) / log2;
	}
}
