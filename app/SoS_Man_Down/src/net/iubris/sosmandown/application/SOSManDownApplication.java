/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.application;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.application.crashreport.SMDCrashReportDialog;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.utils.StringsUtils;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender.Method;
import org.acra.sender.HttpSender.Type;
import org.greenrobot.eventbus.EventBus;

import roboguice.RoboGuice;
import roboguice.util.Ln;
import android.app.Activity;
import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import frenchtoast.FrenchToast;
//import net.iubris.sosmandown.saver.parse.ParsecomEventsSaver;

@ReportsCrashes(
		httpMethod = Method.PUT,
	    reportType = Type.JSON,
		formUri = "http://sosmandownapp.cloudant.com/acra-sosmandownapp/_design/acra-storage/_update/report",
		formUriBasicAuthLogin = "anytorgereddrownsinkster",
		formUriBasicAuthPassword = "1cc99861fd59213b932a7f79ad1867a3abc3fd8c",
		
		// toast
//		mode = ReportingInteractionMode.TOAST,
//      forceCloseDialogAfterToast = false,
//      resToastText = R.string.crash_toast_text
		
		// dialog
		mode = ReportingInteractionMode.DIALOG,
		// optional, displayed as soon as the crash occurs, before collecting data which can take a few seconds
		resToastText = R.string.crash_toast_text, 
        resDialogText = R.string.crash_dialog_text,
        resDialogIcon = android.R.drawable.ic_dialog_info, //optional. default is a warning sign
        resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
   		// optional. When defined, adds a user text field input with this text resource as a label
        resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, 
        // optional. When defined, adds a user email text entry with this text resource as label. 
        // The email address will be populated from SharedPreferences and will be provided as an ACRA field if configured.
        resDialogEmailPrompt = R.string.crash_user_email_label, 
        resDialogOkToast = R.string.crash_dialog_ok_toast // optional. displays a Toast message when the user accepts to send a report.
        
//        , forceCloseDialogAfterToast = false
        
        // log report
        , logcatArguments = { "-t", "250", "-v", "long" }        
)
public class SOSManDownApplication extends /*MultiDex*/Application {
	
//	private final Random random = new Random();
	protected Activity currentActivity;
//	private boolean reportSendingAttempt;
	
	@Override
	public void onCreate() {
//		Ln.d("random: "+random.nextInt());
		super.onCreate();
		
		// ACRA zone
		ACRA.init(this);
		ACRA.getConfig().setReportDialogClass(SMDCrashReportDialog.class);

		// RoboGuice zone
		RoboGuice.setUseAnnotationDatabases(false);
		
		// Logger zone
//		setLogLevel();
		
		// EventBus zone
		EventBus.builder()
		.sendNoSubscriberEvent(false)
		.sendSubscriberExceptionEvent(false)
		.logNoSubscriberMessages(false)
		.logSubscriberExceptions(false)
		.throwSubscriberException(false)
		.strictMethodVerification(false)
		.installDefaultEventBus();
//		.build();
//		Ln.d("eventBus:"+EventBus.getDefault().hashCode());
		
		FrenchToast.install(this);

		
		// Parse.com zone
//		Parse.enableLocalDatastore(this);
////		Parse.initialize(this);
////		Parse.initialize(this, "MZQdm0ZZ0EaTYYE8GnuWlqWu2fzgwscDVtIU5cWs", "3LWN9ZFWmoqEzzkzStLbJzrxLXmSUosz54xrMfoD");
//		Parse.initialize(this, "2o1lvkrGwJqwmFueO6oiF1qVPeYQkenMmpeMUrqq","muQAhsAWnQvboWtIYO0Exl7bpScQZQCFCPOTuPgg");
	
	
		registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {			
			@Override
			public void onActivityStopped(Activity activity) {}
			@Override
			public void onActivityStarted(Activity activity) {}
			@Override
			public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}
			@Override
			public void onActivityResumed(Activity activity) {
				currentActivity = activity;
			}
			@Override
			public void onActivityPaused(Activity activity) {}
			@Override
			public void onActivityDestroyed(Activity activity) {}
			@Override
			public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}
		});
		
		Config.BUILD_VERSION = getBuildVersion();
		Config.BUILD_TIMESTAMP = getBuildTimestamp();
		Ln.d("BUILD_VERSION:"+Config.BUILD_VERSION+StringsUtils.PIPE+Config.BUILD_TIMESTAMP);
	}
	
	public Activity getCurrentActivity() {
		return currentActivity;
	}
	
	public static void setLogLevel() {
		// Logger zone
		if (Config.DEV) {
//			Ln.i(Ln.getLoggingLevel());
			Ln.setLoggingLevel(Log.DEBUG);
//			Ln.i(Ln.getLoggingLevel());			
		} else if (Config.TEST) {
			if (Config.LOG_IN_TEST) {
				Ln.setLoggingLevel(Log.DEBUG);
			} else {
				Ln.setLoggingLevel(Log.INFO);
			}
		}
	}

//	public boolean getTried() {
//		return reportSendingAttempt;
//	}
	
//	protected void attachBaseContext(Context base) {
//		super.attachBaseContext(base);
//		MultiDex.install(this);
//	}
	
	private String getBuildVersion() {
		String s = StringsUtils.PIPE;
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String version = pInfo.versionName;
			int verCode = pInfo.versionCode;
			String v = version + StringsUtils.DOT + verCode;
			s = v;
		} catch (NameNotFoundException e) {
//			e.printStackTrace();
		}
		return s;
	}
//	private static final String DATE_FORMAT_PATTERN = "yyyyMMdd:HHmm";
	private static final String DATE_FORMAT_PATTERN = "yyyyMMdd";
	private static final String MANIFEST_NODE = "META-INF/MANIFEST.MF";
	private String getBuildTimestamp() {
		String s = StringsUtils.EMPTY;
		ZipFile zf = null;
		try {
			ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), 0);
			zf = new ZipFile(ai.sourceDir);
			//	     ZipEntry ze = zf.getEntry("classes.dex");
			ZipEntry ze = zf.getEntry(MANIFEST_NODE);
			long time = ze.getTime();
			Date date = new java.util.Date(time);
			s = new SimpleDateFormat(DATE_FORMAT_PATTERN, Locale.getDefault()).format(date);
		} catch(NameNotFoundException e) {
			Ln.e(e.getClass().getSimpleName()+" "+e.getCause());
		} catch (IOException e) {
			Ln.e(e.getClass().getSimpleName()+" "+e.getCause());
		} finally {
			if (zf!=null) {
				try {
					zf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	     
	     
	     
	     
//	     float t = (date.getHours()/24) + (date.getMinutes()/60) + (date.getSeconds()/60);
//	     float t = time*100/MAX_TIME_TO_EPOCH;
	     
//	     float minorVersion = 
	    		 //(t*100/MAX_DAY_TIME);
//	    		 t;
//	     Ln.d(time+" "+minorVersion);
	     
//	     String mvs = new DecimalFormat("####.##").format(minorVersion);
	     return s;
	}
	
}
