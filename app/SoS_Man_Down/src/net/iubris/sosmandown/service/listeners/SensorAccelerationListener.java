/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.service.listeners;

import javax.inject.Inject;

import net.iubris.sosmandown.engine.computing_state.ComputedState;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessor;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.subscriptions.computed_result.ComputedResultPublisher;
import net.iubris.sosmandown.utils.ThreadsUtil;

import org.greenrobot.eventbus.EventBus;

import roboguice.inject.ContextSingleton;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

@ContextSingleton
public class SensorAccelerationListener implements SensorEventListener, ComputedResultPublisher {
	
//	private final ExecutorService executor;
	private final EventBus eventBus;


	private final SensorEventProcessor sensorEventProcessor;
	private final SensorAccelerationListenerEventSavingProcessorDelegate sensorAccelerationListenerEventProcessorDelegate;


	private final ThreadsUtil threadsUtil;

	@Inject
	public SensorAccelerationListener(SensorEventProcessor sensorEventProcessor, 
			SensorAccelerationListenerEventSavingProcessorDelegate sensorAccelerationListenerEventProcessorDelegate, ThreadsUtil threadsUtil, EventBus eventBus) {
//		this.eventProcessor = new EventProcessorFSM(context/*,this*/);
		this.sensorEventProcessor = sensorEventProcessor;
		this.threadsUtil = threadsUtil;
		this.sensorEventProcessor.init();
		this.sensorAccelerationListenerEventProcessorDelegate = sensorAccelerationListenerEventProcessorDelegate;
//		this.executor = Executors.newSingleThreadExecutor();
		this.eventBus = eventBus; 
	}
	
	private class EventProcessorRunnable implements Runnable {
		private final SensorEvent sensorEvent;
		public EventProcessorRunnable(SensorEvent sensorEvent) {
			this.sensorEvent = sensorEvent;
		}
		@Override
		public void run() {
			processEvent(sensorEvent);
		}
	}

	@Override
	public void onSensorChanged(SensorEvent sensorEvent) {
		if (sensorEvent.sensor.getType()==Sensor.TYPE_ACCELEROMETER) {
//			incrementalTime++;
			// using thread
			EventProcessorRunnable eventProcessorRunnable = new EventProcessorRunnable(sensorEvent);
//			executor.execute(eventProcessorRunnable);
			threadsUtil.execOnWorker(eventProcessorRunnable);
		}
	}
	
	private void processEvent(SensorEvent sensorEvent) {
//		double accelerationResultant = EventProcessorUtils.computeAccelerationResultant(sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
		final SensorEventDataHolder sensorEventDataHolder = buildSensorEventDataHolder(sensorEvent);
		ComputedResult computedResult = sensorEventProcessor.elaborate(sensorEventDataHolder);
		ComputedState computedState = computedResult.getEventState();
		computedState.exec();
		
		// no use event.timestamp
//		ResultEvent resultEvent = new ResultEvent(x, y, z, accelerationResultant, combinedState/*,System.currentTimeMillis()*/);
		// TODO only for dev - however, to improve
		sensorAccelerationListenerEventProcessorDelegate.processForSaving(computedResult);
		
//		Ln.d("accelerationEvent: "+accelerationEvent+" "+accelerationEvent.getCombinedState().getInit().name());
//		broadcastResultEventForChartDetails(resultEvent);
		publish(computedResult);
	}
	private static SensorEventDataHolder buildSensorEventDataHolder(SensorEvent sensorEvent) {
		float 	x = sensorEvent.values[0], 
				y = sensorEvent.values[1], 
				z = sensorEvent.values[2];
		double accelerationResultant = SensorEventProcessorUtils.computeAccelerationResultant(x, y, z);
		// use currentTime instead event.timestamp because.. ?
		long timestamp = System.currentTimeMillis();
		SensorEventDataHolder sensorEventDataHolder = new SensorEventDataHolder(x, y, z, accelerationResultant, timestamp);		
		return sensorEventDataHolder;
	}
	// TODO
	// now we broadcast to chartfragment and mainactivity, but we would distinct:
	// - all (each N seconds) to chartfragment, if they are working type
	// - onchangestate to mainactivity
	@Override
	public void publish(final ComputedResult resultEvent) {
		long timestampInMilliseconds = resultEvent.getSensorEventDataHolder().timestamp;
		// use only 2 per second -- 2/s -> 500
		long t = timestampInMilliseconds / 100;
		// use only ones each 3 second
		if (t % 2 != 0) {
			return;
		}
		
		eventBus.post(resultEvent);
	}
	

	// only for dev -- begin
	/*@Override
	public List<ResultEvent> getEvents() {
		return events;
	}
	@Override
	public void resetEvents() {
		events.clear();
	}*/
	// only for dev -- end
	
	/*public int getIncrementalTime() {
		return incrementalTime;
	}*/
		
//	@Override
//	public void onNewWorkingCombinedState(FSMWorkingCombinedState workingCombinedState) {
//		Intent intent = new Intent(SensorsService.ACTION);
//		intent.putExtra(EventsBroadcastReceiver.INTENT_EXTRA_ACCELERATION_EVENT, 
//				new WorkingEvent(workingCombinedState.getMajor() ));
//		context.sendBroadcast( intent );
//	}
//	public class WorkingEvent implements Serializable {
//		private static final long serialVersionUID = -4388222236162356971L;
//		public MajorState majorState;
//		private WorkingEvent(MajorState majorState) {
//			this.majorState = majorState;
//		}
//	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}
