/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.service.listeners;


import javax.inject.Inject;

import net.iubris.sosmandown.engine.computing_state.fsm.working.states.RelaxWorkingFSMComputingState;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.SeparatorWorkingFSMComputingState;
import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.savers.events.EventsSaverBucket;
import roboguice.util.Ln;

public class SensorAccelerationListenerEventSavingProcessorDelegateDev 
implements SensorAccelerationListenerEventSavingProcessorDelegate {

	private static final int MAX_RELAX_TO_ADD = 50;
	
	
	private static final float EVENT_VALUE_FOR_X_Y_Z_SEPARATOR = -50; /* MajorStateE.NULL.getWeight()-10; */ 
	private static final float EVENT_VALUE_FOR_RESULTANT_SEPARATOR = 50;

	private static final int MAX_RELAX_PADDING_AFTER_SEPARATORS_TO_ADD = 20;
	private static final float EVENT_VALUE_FOR_X_Y_Z_RELAX_PADDING = 10;
	private static final float EVENT_VALUE_FOR_RESULTANT_RELAX_PADDING = 10;
	
	private int processed = 0;
	
	private static final int MIN_START = 30;


	private static final boolean DEBUG = false;
	
	private int relaxeventCounter = 0;
	private boolean isPreviousEventRelax;
	
	private boolean isSeparator = false;
	private EventsSaverBucket eventsSaverBucket;
	
	@Inject
	public SensorAccelerationListenerEventSavingProcessorDelegateDev(EventsSaverBucket eventsSaverBucket) {
		this.eventsSaverBucket = eventsSaverBucket;
	}
	
	@Override
	public void processForSaving(ComputedResult currentResultEvent/*, List<ResultEvent> eventsToSave*/) {
		// old, adding all
		/*events.add(resultEvent);
		*/
		
		if (eventsSaverBucket.isLocked()) {
			return;
		}
		
		final WorkingMajorState major = currentResultEvent.getEventState().getMajor();
		boolean isCurrentEventRelax = WorkingMajorStateEnum.RELAX.equals(major);
		if (isCurrentEventRelax) {
			relaxeventCounter++;
		}
		if (processed<MIN_START) {
			conditionalPrint(6, "processed:"+processed+"<MIN_START[="+MIN_START+"],adding all");
			// add as well all events under #MIN_START=30
			eventsSaverBucket.add(currentResultEvent);
		} else {
			// adding if last 200 are not RELAX
			if (isPreviousEventRelax && isCurrentEventRelax) {				
				if (relaxeventCounter<MAX_RELAX_TO_ADD) {
					conditionalPrint(25, "relaxeventCounter:"+relaxeventCounter+"<MAX_RELAX_TO_ADD[="+MAX_RELAX_TO_ADD+"] -> adding relax");
					eventsSaverBucket.add(currentResultEvent);
				} else {
					if (!isSeparator) {
						conditionalPrint(25, "relaxeventCounter:"+relaxeventCounter+">=MAX_RELAX_TO_ADD[="+MAX_RELAX_TO_ADD+"] && !isSeparator -> adding separator+padding");
						long timestamp = System.currentTimeMillis();
						eventsSaverBucket.add( buildSeparatorEvent(timestamp) );
						for (int i=0; i<MAX_RELAX_PADDING_AFTER_SEPARATORS_TO_ADD;i++){
							eventsSaverBucket.add(buildRelaxPaddingEvent(timestamp));
						}
						isSeparator = true;
					}
				}
			} else if (!isPreviousEventRelax && isCurrentEventRelax) {
				print("adding new relax");
				// save new relax, different from previous
				eventsSaverBucket.add(currentResultEvent);
				
				// resetting blocks
				relaxeventCounter = 0;
				isSeparator = false;
			} else if (!isCurrentEventRelax) {
				conditionalPrint(50, "adding not relax (processed:"+processed+")");
				// of course save not relax
				eventsSaverBucket.add(currentResultEvent);
				
				// resetting blocks
				relaxeventCounter = 0;
				isSeparator = false;
			}
			else {
				print("AAH! isCurrentEventRelax:"+isCurrentEventRelax+" "+"isPreviousEventRelax:"+isPreviousEventRelax+" - "+currentResultEvent.getEventState().getMajor().name());
			}
		}

		if (isCurrentEventRelax) {
			isPreviousEventRelax = true;
		} else {
			isPreviousEventRelax = false;
		}
		isPreviousEventRelax = isCurrentEventRelax;
		
		// just for debug
		conditionalPrint(700, "events:reached:"+processed+","+"bucket:"+eventsSaverBucket.getAll().size()+",saved:"+eventsSaverBucket.getLastSavedCounter());
		
		processed++;
	}
	
	
	private static ComputedResult buildRelaxPaddingEvent(long timestamp) {
		return new ComputedResult(
				new SensorEventDataHolder(EVENT_VALUE_FOR_X_Y_Z_RELAX_PADDING,
						EVENT_VALUE_FOR_X_Y_Z_RELAX_PADDING,
						EVENT_VALUE_FOR_X_Y_Z_RELAX_PADDING,
						EVENT_VALUE_FOR_RESULTANT_RELAX_PADDING,
						timestamp),
				new RelaxWorkingFSMComputingState(null,null,null/*, null*/));
	}

	private static ComputedResult buildSeparatorEvent(long timestamp) {
		return new ComputedResult(
				new SensorEventDataHolder(
						EVENT_VALUE_FOR_X_Y_Z_SEPARATOR,
						EVENT_VALUE_FOR_X_Y_Z_SEPARATOR,
						EVENT_VALUE_FOR_X_Y_Z_SEPARATOR,
						EVENT_VALUE_FOR_RESULTANT_SEPARATOR,
						timestamp),
				new SeparatorWorkingFSMComputingState());
	}
	
	private void conditionalPrint(int divisor, String msg) {
		if (processed%divisor==0) {
			print(msg);
		}
	}
	
	private static void print(String msg) {
		if (DEBUG) {
			Ln.d(msg);
		}
	}
}
