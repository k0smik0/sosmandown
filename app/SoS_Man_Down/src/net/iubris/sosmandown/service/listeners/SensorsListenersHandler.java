/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.service.listeners;


import javax.inject.Inject;

import net.iubris.sosmandown._di.annotations.SensorAcceleration;
import net.iubris.sosmandown._di.providers.sensors.SensorSignificantMotionProvider;
import net.iubris.sosmandown._di.providers.sensors.SensorStepDetectionProvider;
import net.iubris.sosmandown.config.Config.Detection;
import roboguice.inject.ContextSingleton;
import android.annotation.TargetApi;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Build;

@ContextSingleton
public class SensorsListenersHandler {
	
	public static final int SENSOR_DELAY = Detection.SENSOR_SENSITIVITY;
	
	private final SensorManager sensorManager;
	
	private final Sensor sensorAcceleration;
	private final SensorAccelerationListener sensorAccelerationListener;
	
//	private final Sensor sensorSignificantMotion;
	private final SensorSignificantMotionProvider sensorSignificantMotionProvider;
	private final SensorSignificantMotionListener sensorSignificantMotionListener;
	
//	private final Sensor sensorStepDetection;
	private final SensorStepDetectionProvider sensorStepDetectionProvider;
	private final SensorStepDetectionListener sensorStepDetectionListener;
	
	@Inject
	public SensorsListenersHandler(SensorManager sensorManager,
			@SensorAcceleration Sensor sensorAcceleration, 
			SensorAccelerationListener sensorAccelerationListener, 
			
//			@Nullable @SensorSignificantMotion Sensor sensorSignificantMotion,
			SensorSignificantMotionProvider sensorSignificantMotionProvider,
			SensorSignificantMotionListener sensorSignificantMotionListener,
			
//			@Nullable @SensorStepDetection Sensor sensorStepDetection, 
			SensorStepDetectionProvider sensorStepDetectionProvider,
			SensorStepDetectionListener sensorStepDetectionListener) {
		this.sensorManager = sensorManager;
		
		this.sensorAcceleration = sensorAcceleration;
		this.sensorAccelerationListener = sensorAccelerationListener;
		
//		this.sensorSignificantMotion = sensorSignificantMotion;
		this.sensorSignificantMotionProvider = sensorSignificantMotionProvider;
		this.sensorSignificantMotionListener = sensorSignificantMotionListener;
		
//		this.sensorStepDetection = sensorStepDetection;
		this.sensorStepDetectionProvider = sensorStepDetectionProvider;
		this.sensorStepDetectionListener = sensorStepDetectionListener;
		
//		Ln.d(SensorsListenersHandler.class.getSimpleName()+":"+this.hashCode());
	}
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
	public void register() {
		sensorManager.registerListener(sensorAccelerationListener, sensorAcceleration, SENSOR_DELAY);

		Sensor sensorSignificantMotion = sensorSignificantMotionProvider.get();
			if (sensorSignificantMotion!=null && Build.VERSION.SDK_INT>=16) {
	//		sensorManager.registerListener(sensorSignificantMotionListener, sensorSignificantMotion, SENSOR_DELAY);
			// TODO use triggerSensor - the right way - but implements as external class, in order to use elsewhere
			sensorManager.requestTriggerSensor(new TriggerEventListener() {
				@Override
				public void onTrigger(TriggerEvent event) {
					
				}
			}, sensorSignificantMotion);
		}
		
		Sensor sensorStepDetection = sensorStepDetectionProvider.get();
		if (sensorStepDetection!=null) {
			sensorManager.registerListener(sensorStepDetectionListener, sensorStepDetection, SENSOR_DELAY);
		}
	}
	public void unregister() {
		sensorManager.unregisterListener(sensorAccelerationListener);
		sensorManager.unregisterListener(sensorSignificantMotionListener);
		sensorManager.unregisterListener(sensorStepDetectionListener);
	}
	
	// TODO only for dev ?
	public SensorAccelerationListener getSensorAccelerationListener() {
		return sensorAccelerationListener;
	}
}
