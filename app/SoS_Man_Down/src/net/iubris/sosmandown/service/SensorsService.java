/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.service;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.model.result.LightComputedResult;
import net.iubris.sosmandown.service.listeners.SensorsListenersHandler;
import net.iubris.sosmandown.subscriptions.PublisherSubscriberDelegate;
import net.iubris.sosmandown.subscriptions.lightevents.LightComputedResultSubscriber;
import net.iubris.sosmandown.ui.activities.main.MainActivity;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.SubscriberExceptionEvent;
import org.greenrobot.eventbus.ThreadMode;

import roboguice.inject.InjectResource;
import roboguice.service.RoboService;
import roboguice.util.Ln;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

public class SensorsService extends RoboService implements LightComputedResultSubscriber {
	
//	private static final String NOTIFICATION_TITLE = Config.APP_NAME;
	@InjectResource(R.string.app_name)
	private String NOTIFICATION_TITLE;
	@InjectResource(R.string.notification__text_prefix)
	private String notificationText;
	private static final int notificationId = 505118911; // "sos" "118" "911"
	
	@Inject
	private NotificationManager notificationManager;
	private PendingIntent mainActivityPendingIntent;
	
	@Inject
	private PublisherSubscriberDelegate subscriberDelegate;
	
	@Inject
	private SensorsListenersHandler sensorsListenersHandler;
	@Inject
	private FSMComputingStatesLocator fsmComputedStatesLocator;
	
	
	// we would singleton, thread-safe - but lock ?
	private static boolean started = false;
	
	@Override
	public void onCreate() {
		super.onCreate();
//		Ln.d("sensorsListenersHandler:"+sensorsListenersHandler.hashCode());
	}
	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
//		super.onStartCommand(intent, flags, startId);
		
		sensorsListenersHandler.register();
//Ln.d("service "+hashCode()+" started");
		// TODO restore - why ?
//		stateActionFall.test();
		
		
		// TODO only for dev
		/*if (Config.DEV) {
//			sensorsListenersHandler.getSensorAccelerationListener().resetEvents();
			eventsBucket.resetEvents();
			Ln.d("starting with "+
//					sensorsListenersHandler.getSensorAccelerationListener().getEvents().size()
					eventsBucket.getEvents().size()
					+" events");
		}*/
//		eventsSaverProvider.get().reset();

		subscriberDelegate.registerSubscription(this);
		
		initPendingIntentForMainActivity();
		Notification notification = buildNotification(notificationText);
		startForeground(notificationId, notification);
		
		started = true;
		
		return Service.START_STICKY;
	}
	@Override
	public void onDestroy() {
		sensorsListenersHandler.unregister();
Ln.d("service destroyed");

		// use enum
//		State.FALL.resourcesRelease();
		
		// use class ?
//		stateActionCommand.releaseResources();
		fsmComputedStatesLocator.releaseResources();

		
//		List<ResultEvent> events = eventsBucket.getEvents();
//		saveEvents();
		
//		getApplication().unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
//		releasePartialWakeLock();
		
		subscriberDelegate.unregisterSubscription(this);
		
		stopForeground(true);
		started = false;
	}
	/*public Collection<ResultEvent> getEvents() {
//		return sensorsListenersHandler.getSensorAccelerationListener().getEvents();
		return eventsBucket.getEvents();
	}*/
	
	public static boolean isStarted() {
		return started;
	}
	public static void start(Activity activity) {
//		SensorsService.activity = activity;
		activity.startService( new Intent(activity, SensorsService.class) );
	}
	public static void stop(Activity activity) {
//		_activity = activity;
		activity.stopService( new Intent(activity, SensorsService.class) );
//		activity = null;
	}

	@Override
	public IBinder onBind(Intent intent) {
//		return null;
		return binder;
	}
	private final IBinder binder = new SensorsServiceBinder();
	
	public class SensorsServiceBinder extends Binder {
		public SensorsService getService() {
			return SensorsService.this;
		}
	}
	
	
//	private void startNotification() {
//	}
//	private void stopNotification() {
//	}
	
//	public void saveEvents(/*List<ResultEvent> events,*/ /*Activity activity*/) {
//		/*if (Config.DEV) {
//				ProgressDialogHandler progressDialogHandler = new ProgressDialogHandler(activity);
//				String androidId = Secure.getString(activity.getContentResolver(), Secure.ANDROID_ID);
//				eventsSaverFactory.createWith(activity, progressDialogHandler, androidId)
////				eventsSaver
//				.exportEvents(eventsBucket.getEvents() , sdf.format(dateStart), sdf.format(new Date()));
//		}*/
////		eventsSaverProvider.get().save();
//	}

	/*public static void setSaveEvents(boolean saveEventsPreference) {
		SensorsService.saveEventsPreference = saveEventsPreference;
	}

	public static boolean isSaveEventsPreference() {
		return saveEventsPreference;
	}*/

	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedLightComputedResult(LightComputedResult lightEvent) {
		String majorStateName = lightEvent.getMajorState().name();
		Notification notification = buildNotification(notificationText+": "+majorStateName);
		notificationManager.notify(notificationId, notification);
	}
	@Subscribe
	@Override
	public void onEvent(SubscriberExceptionEvent subscriberExceptionEvent) {
		Ln.d("subscriberExceptionEvent: event:"+subscriberExceptionEvent.causingEvent+",subscriber:"+subscriberExceptionEvent.causingSubscriber+",eventBus:"+subscriberExceptionEvent.eventBus);
	}
	/*@Override
	public void registerSubscription() {
		if (!eventBus.isRegistered(this)) {
			eventBus.register(this);
			Ln.d("register to event subscription");
		}
	}
	@Override
	public void unregisterSubscription() {
		if (eventBus.isRegistered(this)) {
			eventBus.unregister(this);
			Ln.d("unregister to event subscription");
		}		
	}*/
	

	private void initPendingIntentForMainActivity() {
		Intent mainActivityIntent = new Intent(this, MainActivity.class);
        mainActivityPendingIntent = PendingIntent.getActivity(getBaseContext(), 0, mainActivityIntent, 
        		Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
	}
	private Notification buildNotification(String text) {
        Notification notification = new NotificationCompat.Builder(this)
        	.setContentTitle(NOTIFICATION_TITLE)
        	.setContentText(text)
        	.setSmallIcon(R.drawable.ic_notification_fall)
        	.setContentIntent(mainActivityPendingIntent)
        	.build();
        notification.flags = notification.flags | Notification.FLAG_FOREGROUND_SERVICE;
        
        return notification;
	}
	/*private int getNotificationIcon() {
	    boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
	    return useWhiteIcon ? R.drawable.icon_silhouette : R.drawable.ic_launcher;
	}*/


//	public static void requestCurrentEvent() {
//		
//	}
}
