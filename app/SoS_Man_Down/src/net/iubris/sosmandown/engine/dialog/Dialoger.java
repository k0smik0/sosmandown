/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.dialog;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.ThreadsUtil;
import roboguice.inject.ContextSingleton;
import roboguice.util.Ln;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;

@ContextSingleton
public class Dialoger {

//	@Inject
//	private Context activity;
	
	private final Context context;
	private final ThreadsUtil threadsUtil;
	
	private final AlertDialog alert;
	
//	private final Queue<MessageHolder> messagesQueue = new LinkedList<>();
	
	private final AtomicBoolean dialogShowed = new AtomicBoolean(false);
	
	@Inject
	public Dialoger(Context context, final ThreadsUtil threadsUtil) {
//		Printor.whoIAm(this,context);
		this.context = context;
Ln.d("context:"+context);
		this.threadsUtil = threadsUtil;
		
		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
	//		.setTitle("Leaving launcher")
//			.setMessage(msg);
		dialogBuilder.setPositiveButton(context.getString(R.string.button_ok), new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int whichButton) {
	//	        exitLauncher();
		    }
		});		
		this.alert = dialogBuilder.create();
		this.alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
		    @Override
		    public void onDismiss(DialogInterface dialog) {
//		        handler.removeCallbacks(runnable);
		        threadsUtil.removeCallbackFromUIThread(dismissRunnable);
		        dialogShowed.set(false);
		    }
		});
		this.alert.setOnShowListener( new OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				dialogShowed.set(true);
			}
		});
	}

	public void show(String title, String message) {
		// dummy implementation - a better would be using queue...
		
		if (dialogShowed.get()) {
			SToast.with(context).showLong(title.toUpperCase(Locale.getDefault())+"\n\n\n"+message);
		} else {
			alert.setTitle(title);
			alert.setMessage(message);
			alert.show();
		}
	}

	public void showAndDiseapperAfterAMaxWhile(String title, String message) {
		/*if (messagesQueue.size()==0) {
			show(title, message);
			threadsUtil.execOnUIWithDelay(dismissRunnable, Config.Dialoger.MAX_DIALOG_APPEARS);
		} else {
			MessageHolder messageHolder = new MessageHolder(title, message);
			messagesQueue.add(messageHolder);
		}*/
		
		show(title, message);
		threadsUtil.execOnUIWithDelay(dismissRunnable, Config.Dialog.MAX_DIALOG_APPEAR);
	}
	
	// Hide after some seconds
	private final Runnable dismissRunnable = new Runnable() {
	    @Override
	    public void run() {
//	        if (alert.isShowing()) {
	            alert.dismiss();
//	        }
	    }
	};
	
	/*static class MessageHolder {
		private final Date date;
		private final String title;
		private final String message;

		public MessageHolder(String title, String message) {
			this.date = new Date();
			this.title = title;
			this.message = message;
		}
	}*/
	
}