/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.sms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.utils.Printor;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.inject.InjectResource;
import roboguice.util.Ln;
import roboguice.util.Strings;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.widget.Toast;

public class SmsSender {
	
	@InjectResource(R.string.sms__message_alert)
	private String smsMessageAlert;
//	@InjectResource(R.string.message_location_conjunction)
//	private String messageLocationConjunction;
	
//	@InjectPreference("user_name") 
	private final String userName;
//	@InjectPreference("user_phone_number") 
//	private String myName;
//	@InjectPreference("contact_1_phone_number") 
//	private String contact1PhoneNumber;
//	@InjectPreference("contact_2_phone_number") 
//	private String contact2PhoneNumber;
//	@InjectPreference("contact_3_phone_number") 
//	private String contact3PhoneNumber;
	
	@InjectResource(R.string.sms__sending_error)
	private String smsSendingError;
	
	private final Context context;
	
	@InjectResource(R.string.sms__sending_error_to_prefix)
	private String smsSendingErrorToPrefix;
	
	private final SharedPreferences sharedPreferences;
	
	@Inject	
	public SmsSender(Context context, SharedPreferences sharedPreferences) {
Printor.whoIAm(this,context);
		this.context = context;
		this.sharedPreferences = sharedPreferences;
		this.userName = sharedPreferences.getString("user_name", StringsUtils.EMPTY);
	}
	
	private List<String> numbersSentTo = new ArrayList<>();

	private static final String CONTACT_PHONE_NUMBER_PREFIX = "contact";
	private static final String CONTACT_PHONE_NUMBER_SUFFIX = StringsUtils.UNDERSCORE+"phone_number";
	public List<String> send(String locationWithEventuallyAddress) {
		SmsManager smsManager = SmsManager.getDefault();
		
		String message = smsMessageAlert+StringsUtils.SPACE
				+userName+StringsUtils.DOT /*StringsUtils.SPACE*/
//				+"("+myPhoneNumber+")"+StringsUtils.SPACE
				/*+messageLocationConjunction*/+StringsUtils.SPACE
				+locationWithEventuallyAddress;
		
		numbersSentTo.clear();
		
		List<String> numbersNotValid = new ArrayList<>();
		/*Map<String, ?> all = sharedPreferences.getAll();
		for (Entry<String, ?> entry : all.entrySet()) {
			Ln.d("key:"+entry.getKey()+",value:"+entry.getValue());
		}*/
		
		ArrayList<PendingIntent> toSentPendingIntents = new ArrayList<>();
		toSentPendingIntents.add(sentPI);
		
		ArrayList<PendingIntent> toSentDeliveryIntents = new ArrayList<>();
		toSentDeliveryIntents.add(deliverPI);
		
		ArrayList<String> messageParts = smsManager.divideMessage(message);
		
		createIntents();
		for (int i=0;i<3;i++) {
			String phoneNumber = sharedPreferences.getString(CONTACT_PHONE_NUMBER_PREFIX+i+CONTACT_PHONE_NUMBER_SUFFIX, "");
//			Ln.d("phoneNumber:"+phoneNumber);
			if ( isNumberValid(phoneNumber)) {
//				sm.sendTextMessage(phoneNumber, null, message, sentPI, deliverPI);
				if (!Config.TEST_or_DEV) {
					smsManager.sendMultipartTextMessage(phoneNumber, null, messageParts, toSentPendingIntents, toSentDeliveryIntents);
				}
				numbersSentTo.add(phoneNumber);
				Ln.d("added "+phoneNumber+" to numbersSentTo");
			} else {
				if (!phoneNumber.isEmpty()) {
					numbersNotValid.add(phoneNumber);
					Ln.d("added "+phoneNumber+" to numbersNotValid");
				}
			}
		}
		
		/*if ( isNumberValid(contact1PhoneNumber)) {
			sm.sendTextMessage(contact1PhoneNumber, null, message, null, null);
		} else {
			numbersNotValid.add(contact1PhoneNumber);
		}
		if ( isNumberValid(contact2PhoneNumber)) {
			sm.sendTextMessage(contact2PhoneNumber, null, message, null, null);
		} else {
			numbersNotValid.add(contact2PhoneNumber);
		}
		if ( isNumberValid(contact3PhoneNumber)) {
			sm.sendTextMessage(contact3PhoneNumber, null, message, null, null);
		} else {
			numbersNotValid.add(contact3PhoneNumber);
		}*/
//		for (String nnv: numbersNotValid) {
		if (numbersNotValid.size()>0) {
			showErrorSendingToAnyNumber(numbersNotValid);
		}
//		}
			
		return numbersSentTo;
	}
	
	
	private static final String SENT = "sent";
	private static final String DELIVERED = "delivered";

//    Intent sentIntent;
    private PendingIntent sentPI;
    private PendingIntent deliverPI;

	private void createIntents() {
		if (sentPI == null || deliverPI == null) {
			
			Ln.d("creating intents and register them with receivers");
			
			final Context applicationContext = context.getApplicationContext();

			Intent sentIntent = new Intent(SENT);
			sentPI = PendingIntent.getBroadcast(context.getApplicationContext(), 0, sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			Intent deliveryIntent = new Intent(DELIVERED);
			deliverPI = PendingIntent.getBroadcast(applicationContext, 0, deliveryIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			/* Register for SMS send action */
			context.registerReceiver(sentReceiver, new IntentFilter(SENT));
			/* Register for Delivery event */
			context.registerReceiver(new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					Ln.d(DELIVERED);
//					SToast.with(applicationContext).show(DELIVERED, true);
					Toast.makeText(applicationContext, DELIVERED, Toast.LENGTH_LONG).show();
				}
			}, new IntentFilter(DELIVERED));
		}
	}
	private final BroadcastReceiver sentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			/*String result = "unknown";

			switch (getResultCode()) {

			case Activity.RESULT_OK:
				result = "Transmission successful";
				break;
			case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				result = "Transmission failed";
				break;
			case SmsManager.RESULT_ERROR_RADIO_OFF:
				result = "Radio off";
				break;
			case SmsManager.RESULT_ERROR_NULL_PDU:
				result = "No PDU defined";
				break;
			case SmsManager.RESULT_ERROR_NO_SERVICE:
				result = "No service";
				break;
			default:
				result = ""+getResultCode();
				break;
			}*/
			
			int resultCode = getResultCode();			
			String resultMessage = smsManagerResultMap.get(resultCode);
			if (resultMessage == null) {
				resultMessage = "unknown:"+resultCode;
			}			 


			Ln.d("sentReceiver.resultCode:"+resultMessage);
			Toast.makeText(context.getApplicationContext(), resultMessage, Toast.LENGTH_LONG).show();
		}
	};
	private static final Map<Integer,String> smsManagerResultMap = new HashMap<>();
	static {
		smsManagerResultMap.put(Activity.RESULT_OK, "Transmission successful");
		smsManagerResultMap.put(SmsManager.RESULT_ERROR_GENERIC_FAILURE, "Transmission failed");
		smsManagerResultMap.put(SmsManager.RESULT_ERROR_RADIO_OFF, "Radio off");
		smsManagerResultMap.put(SmsManager.RESULT_ERROR_NULL_PDU, "No PDU defined");
		smsManagerResultMap.put(SmsManager.RESULT_ERROR_NO_SERVICE, "No service");
		
	}
	
//	private PendingIntent smsPendingIntent = PendingIntent.
	
	private static boolean isNumberValid(String s) {
		// TODO add number validation using regexp
		if (s==null || s.isEmpty()) {
			return false;
		}		
		return true;
	}
	
	/*public String getPhoneNumbersAsString() {
		String joined = Strings.join(StringsUtils.COMMA, numbersSentTo);
		return joined;
	}*/
	
	private void showErrorSendingToAnyNumber(List<String> numbersNotValid) {
		String joined = Strings.join(StringsUtils.COMMA, numbersNotValid);
		Ln.d("joined: "+joined);
//		Toast.makeText(context, smsSendingError+StringsUtils.SPACE+smsSendingErrorToPrefix+StringsUtils.SPACE+joined, Toast.LENGTH_LONG).show();
		String msg = smsSendingError+StringsUtils.SPACE+smsSendingErrorToPrefix+StringsUtils.SPACE+joined;
		Ln.d("showErrorSendingToAnyNumber: '"+msg+"'");
//		SToast.with(context).show(msg, true);
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
	}
}
