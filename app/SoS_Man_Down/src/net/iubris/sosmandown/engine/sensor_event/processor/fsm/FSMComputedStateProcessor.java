/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.sensor_event.processor.fsm;

import javax.inject.Inject;

import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.engine.computing_state.ComputedState;
import net.iubris.sosmandown.engine.computing_state.ComputedStateContext;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessor;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.model.result.LightComputedResult;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.phase.PhaseStateE;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.utils.Printor;

import org.greenrobot.eventbus.EventBus;

import roboguice.inject.ContextSingleton;
import roboguice.util.Ln;
import android.content.Context;

/**
 *  State Pattern: ComputedStateProcessorFSM is the Context (in state pattern world - not android one), while ComputedState is the State
 */
@ContextSingleton
//@Singleton
public class FSMComputedStateProcessor implements SensorEventProcessor, ComputedStateContext {
	
	private final EventBus eventBus;
	
	private ComputedState computedStateCurrent;
	
	// just for log
	private double accelerationResultant;

	private final FSMComputingStatesLocator fsmComputedStatesLocator;
	
	private final Context context;
	

	@Inject
	public FSMComputedStateProcessor(Context context, FSMComputingStatesLocator fsmComputedStatesLocator, /*ComputedStateInitProvider computedStateInitProvider,*/ EventBus eventBus) {
Printor.whoIAm(this,context);
		this.context = context;
		this.fsmComputedStatesLocator = fsmComputedStatesLocator;
		this.eventBus = eventBus;
	}
	@Override
	public void init() {
		fsmComputedStatesLocator.init(context);
		this.computedStateCurrent = fsmComputedStatesLocator.get(PhaseStateE.INITIALIZING);
	}
	
	@Override
	public ComputedResult elaborate(SensorEventDataHolder sensorEventDataHolder) {
		this.accelerationResultant = sensorEventDataHolder.resultant;
		
		// pass all to handler
		computedStateCurrent.handle(sensorEventDataHolder);
//		printEventuallyOnElaborate(computedStateCurrent, sensorEventDataHolder.resultant);
//		eventuallyCollectingData(sensorEventDataHolder);
//		Ln.d("majorState: "+combinedState.getMajor());
		// here combinedState could has been changed
		ComputedResult computedResult = new ComputedResult(sensorEventDataHolder, computedStateCurrent);
		return computedResult;
	}
	private static void printEventuallyOnElaborate(ComputedState computedState, double result) {
		if (Config.DEV) {
			Ln.d("using MajorState: "+computedState+" (resultant: "+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT, result)+")");
		}
	}
	
	
	/*private List<SensorEventDataHolder> collectedDataFromRelaxToImpact = new ArrayList<>();
	public void startCollectingDataForRelaxToImpactTimeAnalysis() {
		collectedDataFromRelaxToImpact = new ArrayList<>();
	}
	public void eventuallyCollectingData(SensorEventDataHolder object) {
		try {
			collectedDataFromRelaxToImpact.add(object);
		} catch(NullPointerException e) {
			Ln.d("\n\n\n\n\nAAAAAAAAAAAAAAH! collectedData is null!\n\n\n\n\n");
		}
	}
	public void stopCollectingDataForRelaxToImpactTimeAnalysis() {
		collectedDataFromRelaxToImpact = null;
//		isCollectingData = false;
	}
	public List<SensorEventDataHolder> getCollectedDataFromRelaxToImpact() {
		return collectedDataFromRelaxToImpact;
	}*/

	
	@Override
	public void setComputedState(ComputedState computedState) {
		this.computedStateCurrent = computedState;
//		Ln.d("set new computedState:"+computedState.getMajor().name());
		onNewComputedResult(computedState);
	}
	private void onNewComputedResult(ComputedState computedState) {
		LightComputedResult lightComputedResult = new LightComputedResult(computedState.getPhase(), computedState.getMajor(), accelerationResultant);
//		Ln.d("publishing new lightComputedResult: "+lightComputedResult);
		publish(lightComputedResult);
		
		printEventuallyOnNewComputedResult(computedState.getMajor(), accelerationResultant);
	}
	@Override
	public void publish(LightComputedResult lightComputedResult) {
//		Ln.d("pre-publish new lightComputedResult: "+lightComputedResult);
		eventBus.post(lightComputedResult);
//		Ln.d("post-publish new lightComputedResult: "+lightComputedResult);
	}
	private static void printEventuallyOnNewComputedResult(WorkingMajorState state, double result) {
		if (Config.DEV) {
			Ln.d("new MajorState: "+state+" (resultant: "+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT, result)+")");
		}
	}
	
	@Override
	public void resetStateVariables() {
		computedStateCurrent.reset();
	}
	
	public Context getContext() {
		return context;
	}
}
