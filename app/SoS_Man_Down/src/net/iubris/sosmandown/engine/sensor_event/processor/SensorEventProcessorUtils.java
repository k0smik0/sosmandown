/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.sensor_event.processor;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import roboguice.util.Ln;

public class SensorEventProcessorUtils {

	public static double greatRangeLimInf;
	public static double greatRangeLimSup;
	public static double smallRangeLimInf;
	public static double smallRangeLimSup;

	public enum Thresholds {
		INSTANCE;

		private double impact;
		private double gravity;
		private double freeFall;

		public double getImpact() {
			return impact;
		}

		public void setImpact(double impact) {
			this.impact = impact;
		}

		public double getGravity() {
			return gravity;
		}

		public void setGravity(double gravity) {
			this.gravity = gravity;

			// setting here the limits
			greatRangeLimInf = gravity - MARGIN_GREAT;
			Ln.d("setted greatRangeLimInf:"+ SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(greatRangeLimInf));
			greatRangeLimSup = gravity + MARGIN_GREAT;
			Ln.d("setted greatRangeLimSup:"+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(greatRangeLimSup));

			smallRangeLimInf = gravity - MARGIN_SMALL;
			smallRangeLimSup = gravity + MARGIN_SMALL;
		}

		public double getFreeFall() {
			return freeFall;
		}

		public void setFreeFall(double freeFall) {
			this.freeFall = freeFall;
		}
	}

	// public static double IMPACT_RESULT_THRESHOLD;
	// public static double GRAVITY;
	// public static double FREE_FALL_THRESHOLD;

	private static final double MARGIN_SMALL = 0.2;
	private static final double MARGIN_GREAT = 1.5;

	// configuration - start
	// public static double computeAccelerationResultant(EventData
	// eventdataCurrent) {
	// return computeResult(eventdataCurrent.x, eventdataCurrent.y,
	// eventdataCurrent.z);
	// }
	public static double computeAccelerationResultant(float x, float y, float z) {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
	}
	// configuration - end
	
	public static final String DECIMAL_STRING_FORMAT = "%.3f";
	
	// DetectingFall and Fall - start
	public static boolean isResultantInSmallRangeAround1G(double result) {
		boolean isIn = smallRangeLimInf < result && result < smallRangeLimSup;
//		if (isIn) { Ln.d("isResultantInSmallRangeAround1G: is "+String.format(DECIMAL_STRING_FORMAT, smallRangeLimInf)+"<"+String.format(DECIMAL_STRING_FORMAT, result)+"<"+String.format(DECIMAL_STRING_FORMAT, smallRangeLimSup)+":"+isIn); }
		return isIn;
	}

	public static boolean isResultantOutOfGreatRangeAround1G(double result) {
		boolean isOutInf = false;
		boolean isOutSup = false;
		
		if (result < greatRangeLimInf) {
			isOutInf = true;
		} else if (result > greatRangeLimSup) {
			isOutSup = true;
		}
		boolean isOut = isOutInf || isOutSup;
		
//		if (isOut) { Ln.d( result+"<"+greatRangeLimInf+"||"+result+">"+greatRangeLimSup+"?"+isOut); }
		
		return isOut;
	}
	// DetectingFall and Fall - end

	public static final DecimalFormat AccelerationDecimalFormatTwoDigit;
	public static final DecimalFormat AccelerationDecimalFormatOneDigit;
	static {
		// Locale locale = new Locale("en", "US");
		String patternTwoDigit = "00.000";
		String patternOneDigit = "#0.000";

		AccelerationDecimalFormatTwoDigit = (DecimalFormat) NumberFormat.getNumberInstance(/* locale */);
		AccelerationDecimalFormatTwoDigit.applyPattern(patternTwoDigit);
		
		AccelerationDecimalFormatOneDigit = (DecimalFormat) NumberFormat.getNumberInstance(/* locale */);
		AccelerationDecimalFormatOneDigit.applyPattern(patternOneDigit);
	}
}
