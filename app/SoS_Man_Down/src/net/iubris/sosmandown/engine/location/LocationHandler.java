/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.location;

import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation.GeocodingControl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.engine.dialog.Dialoger;
import net.iubris.sosmandown.engine.location.NetworkUtils.NetworkState;
import net.iubris.sosmandown.engine.sms.SmsSender;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.StringsUtils;
import net.iubris.sosmandown.utils.location.LocationProvider;
import roboguice.util.Ln;
import roboguice.util.Strings;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Address;
import android.location.Location;
import android.text.TextUtils;

public class LocationHandler {

	private final LocationProvider locationProvider;
	private final Context context;
	private final SmsSender smsSender;
	private final GeocodingControl geocoding;
		
	private final Dialoger dialoger;
	
	private final Resources resources;
	private final String smsLocationLatLngPrefix;
	private final SharedPreferences sharedPreferences;
	
	
	

	@Inject
	public LocationHandler(LocationProvider locationProvider,
			GeocodingControl geocodingControl,
			SmsSender smsSender,
			Dialoger dialoger,
			Context context,
			SharedPreferences sharedPreferences) {
		this.locationProvider = locationProvider;
		 
		this.geocoding = geocodingControl;
		this.smsSender = smsSender;
		this.dialoger = dialoger;
		this.context = context;
Ln.d("context:"+context);
		this.sharedPreferences = sharedPreferences;
		
		this.resources = context.getResources();
		this.smsLocationLatLngPrefix = resources.getString(R.string.sms__location_prefix);
	}

	public void getAndUseLocation() {
		Location location = locationProvider.getLocation();

//		String msg = StringsUtils.EMPTY;
		if (location!=null) {
			geocoding.reverse(location, smsSenderOnReverseGeocodingListener);
			// TODO remove this below
//			msg = ""+location;
		} else {
			// TODO only for dev
			
			String userName = sharedPreferences.getString("user_name", StringsUtils.EMPTY);
			String alertMessagePart = resources.getString(R.string.sms__message_alert)+StringsUtils.SPACE+userName+StringsUtils.DOT;
			String locatioNullMessagePart = resources.getString(R.string.error__location_null);
			
			String messageWithLocationNull = alertMessagePart+locatioNullMessagePart;
			sendSMS(messageWithLocationNull/*, resources*/);
//			dialoger.show(message);
		}		
//		dialoger.show(msg);
		
		locationProvider.stop();
	}
	
	private final OnReverseGeocodingListener smsSenderOnReverseGeocodingListener = new OnReverseGeocodingListener() {
		@Override
		public void onAddressResolved(Location location, List<Address> addresses) {
			String locationMessage = smsLocationLatLngPrefix+":"+location+StringsUtils.COMMA+resources.getString(R.string.sms__addresses)+StringsUtils.PERIOD+addresses.size();
			
			Ln.d(locationMessage);
//			SToast.with(context).show(locationMessage, true);

			String address = buildAddress(addresses);
			
			String locationAndAddressForSMS = buildSMSLocationAndAddressPart(address, location, resources, context);
			String smsMessage = locationAndAddressForSMS;

			Ln.d(smsMessage);
			
			sendSMS(smsMessage/*, resources*/);
			
			locationProvider.stop();
			geocoding.stop();
		}		
	};
	
	private void sendSMS(String message/*, Resources resources*/) {
		try {
			List<String> targetNumbersResult = smsSender.send(message);
			
			String messageSentNumbersSuffix = resources.getString(R.string.sms__sending_to_prefix)+StringsUtils.SPACE;
			messageSentNumbersSuffix += targetNumbersResult.size()>0 ? 
					Strings.join(StringsUtils.COMMA, targetNumbersResult) : 
					resources.getString(R.string.sms__sending_to_no_one);

			String title = resources.getString(R.string.sms__sending);
			
			String messageIsFakeBecauseInTestOrDev = StringsUtils.EMPTY;
			if (Config.TEST_or_DEV) {
				messageIsFakeBecauseInTestOrDev = resources.getString(R.string.sms__not_really_sent_because_in_test_or_dev);
			}
			String messageComplete = message+messageSentNumbersSuffix+messageIsFakeBecauseInTestOrDev;
			dialoger.showAndDiseapperAfterAMaxWhile(title, messageComplete);
			
		} catch(NullPointerException e) {
			SToast.with(context).showLong( resources.getString(R.string.sms__sending_error)
					+StringsUtils.NEW_LINE
					+resources.getString(R.string.sms__no_address_error_generic) );
		} catch(Exception e) {
			Ln.d(e);
		}
	}
	
	private static String buildAddress(/*Location original,*/ List<Address> addresses) {
		
		List<String> addressElements = buildAddressElements(addresses);
		
		StringBuilder stringBuilder = new StringBuilder(StringsUtils.EMPTY);
		if (addressElements.size()>0) {
			stringBuilder.append(TextUtils.join(", ", addressElements));
		}
		String address = stringBuilder.toString();
//Ln.d("returning address: "+address);
		return address;
	}
	private static List<String> buildAddressElements(List<Address> addresses) {
		List<String> addressElements = new ArrayList<>();
//Ln.d("addresses.size: " + addresses.size());
		for (Address address : addresses) {
			int i = 0;
			for (i=0; i < address.getMaxAddressLineIndex(); i++) {
				addressElements.add(address.getAddressLine(i));
			}
//Ln.d("address: "+i);
		}			
		return addressElements;
	}
	
	private final DecimalFormat locationDecimalFormat = new DecimalFormat("00.000000");
	private final DecimalFormat accuracyDecimalFormat = new DecimalFormat("#0.#");
	private final String locationUrlPrefix = "http://www.google.com/maps/place//@";
	
	private String buildSMSLocationAndAddressPart(String address, Location location, Resources resources, Context context) {
		
//		String smsLocationLatLngPrefix = resources.getString(R.string.sms__location_prefix);
		String smsLocationTimePrefix = resources.getString(R.string.sms__location_time)+StringsUtils.SPACE;
		String smsLocationAccuracyPrefix = resources.getString(R.string.sms__location_accuracy)+StringsUtils.PERIOD;
		
		
		double latitude = location.getLatitude();
		double longitude = location.getLongitude();
		float accuracy = location.getAccuracy();
		long locationTime = location.getTime();
		String time = new SimpleDateFormat("yyyy-MM-dd HH:mm",Locale.getDefault()).format(locationTime);
		
		String geolocationCommonPart = 
			 smsLocationLatLngPrefix+StringsUtils.SPACE+locationUrlPrefix+locationDecimalFormat.format(latitude)+","+locationDecimalFormat.format(longitude)+","
			+smsLocationTimePrefix+time+StringsUtils.COMMA
			+smsLocationAccuracyPrefix+accuracyDecimalFormat.format(accuracy)+"m. ";
		
		String locationAndAddressForSMS = geolocationCommonPart+resources.getString(R.string.sms__address_prefix);
		if (!address.isEmpty()) {
			locationAndAddressForSMS+= address+StringsUtils.DOT+StringsUtils.SPACE;
		} else {
			// TODO vedere se non c'è rete, oppure errore generico
			String noAddressError = StringsUtils.SPACE;
			NetworkState connectivityStatus = NetworkUtils.getConnectivityStatus(context);
			if (connectivityStatus.equals(NetworkState.NOT_CONNECTED)) {
				// error = smsNoAddressErrorNoNetwork;
				noAddressError = resources.getString(R.string.sms__no_address_error_no_network);
			} else {
				// error = smsNoAddressErrorGeneric;
				noAddressError = resources.getString(R.string.sms__no_address_error_generic);
			}
			locationAndAddressForSMS += StringsUtils.SPACE+resources.getString(R.string.sms__no_address_suffix)+noAddressError;
		}
		return locationAndAddressForSMS;
	}
	
}
