/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.looped_layout;

import android.graphics.drawable.Drawable;
//import roboguice.util.Ln;
import android.util.SparseArray;
import android.widget.TextView;

public abstract class LoopedLayout {

	private final SparseArray<EnhancedText> enhancedTextsMap = new SparseArray<>();
	
	private int enhancedTextsNumber;
	
	private int workingValueCurrentIndex = 1;
	private boolean setup;
	
	abstract protected void populateEnhancedTextsMap(SparseArray<EnhancedText> enhancedTextsMap);
	abstract protected TextView getTextView();
	
	/*public LoopedLayout() {
		populateEnhancedTextsMap(enhancedTextsMap);
		enhancedTextsNumber = enhancedTextsMap.size();
	}*/
	
	public void init() {
		if (!setup) {
			populateEnhancedTextsMap(enhancedTextsMap);
			enhancedTextsNumber = enhancedTextsMap.size();
//			resetEnhancedText();
			setup = true;
//			Ln.d("setup with "+enhancedTextsNumber+" ET!");
		}
	}
	
	public void start() {
		applyEnhancedText(1);
	}
	
	public void next() {
		if (workingValueCurrentIndex == enhancedTextsNumber) {
			workingValueCurrentIndex = 1;
		}
		applyEnhancedText(workingValueCurrentIndex);
		workingValueCurrentIndex++;
	}	
	private void applyEnhancedText(int index) {
		EnhancedText enhancedText = enhancedTextsMap.get(index);
//		Ln.d("enhancedText from index:"+index);
//		Ln.d("enhancedText:"+ enhancedText.text);
		applyEnhancedText(enhancedText);
	}
	private void applyEnhancedText(EnhancedText enhancedText) {
		TextView textView = getTextView();
//		Ln.d("textView:"+textView);
//		Ln.d("enhancedText:"+enhancedText);
		textView.setBackground( enhancedText.background );
		textView.setText(enhancedText.text);
		textView.setTextColor(enhancedText.textColor);		
	}

	public void stop() {
		resetEnhancedText();
//		Ln.d("stop");
//		Ln.d("was pressed:"+getTextView().isPressed());
//		getTextView().setPressed(false);
//		Ln.d("is pressed:"+getTextView().isPressed());
	}
	private void resetEnhancedText() {
		applyEnhancedText(0);
		workingValueCurrentIndex = 1;
	}
	
	protected class EnhancedText {
		public final String text;
		public final int textColor;
		public final Drawable background;
		public EnhancedText(String text, int textColor, Drawable background) {
			this.text = text;
			this.textColor = textColor;
			this.background = background;
		}
	}
}
