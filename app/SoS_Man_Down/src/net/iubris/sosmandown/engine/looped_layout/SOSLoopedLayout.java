/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.looped_layout;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import roboguice.RoboGuice;
import roboguice.inject.InjectView;
//import roboguice.util.Ln;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.SparseArray;
import android.widget.Button;
import android.widget.TextView;

public class SOSLoopedLayout extends LoopedLayout {

	@InjectView(R.id.button_sos)
	private Button buttonSOS;
	
	private final Activity activity;
	
	@Inject
	public SOSLoopedLayout(Activity activity) {
//		super();
//		Ln.d("activity:"+activity);
		this.activity = activity;
		RoboGuice.injectMembers(activity, this);
		
//		Ln.d("buttonSOS:"+buttonSOS);
	}
	
	@Override
	protected void populateEnhancedTextsMap(SparseArray<EnhancedText> enhancedTextsMap) {
		Resources resources = activity.getResources();
	
		final String sos = resources.getString(R.string.button_sos_sos);
		final String stop = resources.getString(R.string.button_sos_stop);
		
		enhancedTextsMap.put(0, 
				new EnhancedText(sos, resources.getColor(R.color.accent_color), 
						resources.getDrawable(R.drawable.circular_white_border_black_shadow_big, activity.getTheme())) );
		
		enhancedTextsMap.put(1, 
			new EnhancedText(stop, Color.RED, 
					resources.getDrawable(R.drawable.circular_yellow_border_black_shadown_little, activity.getTheme())) );
		
		enhancedTextsMap.put(2, 
			new EnhancedText(stop, Color.YELLOW, 
					resources.getDrawable(R.drawable.circular_red_border_black_shadown_little, activity.getTheme())) );
		
//		Ln.d("enhancedTextsMap:"+enhancedTextsMap.size());
	}

	@Override
	protected TextView getTextView() {
//		Ln.d("returning:"+buttonSOS);
		return buttonSOS;
	}
}
