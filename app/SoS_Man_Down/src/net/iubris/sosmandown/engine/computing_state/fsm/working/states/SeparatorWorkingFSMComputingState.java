/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states;

import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorStateEnum;

public class SeparatorWorkingFSMComputingState extends AbstractWorkingFSMComputedState implements WorkingFSMComputingState {

	public SeparatorWorkingFSMComputingState() {
		super(null, null, WorkingMajorStateEnum.NULL, WorkingMinorStateEnum._NULL);
	}

	private static final long serialVersionUID = -7765290555711257878L;

	@Override
	public void handle(SensorEventDataHolder sensorEventDataHolder) {}
}
