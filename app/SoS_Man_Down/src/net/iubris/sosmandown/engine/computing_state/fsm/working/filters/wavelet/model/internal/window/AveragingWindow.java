/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window;

public class AveragingWindow extends AbstractWindow {
	public AveragingWindow(int windowLength) {
		super(windowLength);
	}
	public double calculatePower(double input) {
		window[windowIndex] = input * input;
		windowIndex = ++windowIndex % windowLength;
		double result = 0.0;
		for (int i = 0; i < window.length; ++i) {
			result += window[i];
		}
		result = Math.sqrt(result / /*(double)*/ windowLength);
		return result;
	}
}
