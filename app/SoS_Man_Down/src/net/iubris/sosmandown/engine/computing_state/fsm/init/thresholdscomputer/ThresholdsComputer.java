/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer;


import static net.iubris.sosmandown.config.Config.Detection.Impact.SENSITIVITY_MINIMUM;
import static net.iubris.sosmandown.config.Config.Detection.Impact.SENSITIVITY_THRESHOLD_COMPLEMENT;
import static net.iubris.sosmandown.config.Config.Detection.Impact.SENSITIVITY_THRESHOLD_MINIMUM;

import java.util.Vector;

import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import roboguice.util.Ln;


public interface ThresholdsComputer {
	
	ComputedThresholds compute(Vector<Double> gravityAverageSamples);
	
	public static class Util {
		public static double computeImpactResultThreshold(double gravityThreshold, double impactThresholdForGravityComplementaryMultiplier) {
			double multiplier = SENSITIVITY_MINIMUM
					+SENSITIVITY_THRESHOLD_MINIMUM
					+(SENSITIVITY_THRESHOLD_COMPLEMENT-impactThresholdForGravityComplementaryMultiplier)/SENSITIVITY_THRESHOLD_COMPLEMENT;
			double impactResultThreshold = gravityThreshold*multiplier;
			Ln.d("complementaryMultiplier:"
					+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT, impactThresholdForGravityComplementaryMultiplier)
					+", multiplier: "
					+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT, multiplier)
					+", impactResultThreshold:"
					+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT, impactResultThreshold) );
			return impactResultThreshold;
		}
	}
}
