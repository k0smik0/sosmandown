/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer;

import java.util.Vector;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.ui.activities.settings.FragmentSettings;
import roboguice.inject.SharedPreferencesProvider;
import android.content.SharedPreferences;
import android.content.res.Resources;

public class ThresholdsComputerProd extends AbstractThresholdsComputer implements ThresholdsComputer {

	private final SharedPreferencesProvider sharedPreferencesProvider;
	private String impact_sensitivity_threshold__default_value;
	
	@Inject
	public ThresholdsComputerProd(SharedPreferencesProvider sharedPreferencesProvider, Resources resources) {
//		Ln.d(this);
		this.sharedPreferencesProvider = sharedPreferencesProvider;
		impact_sensitivity_threshold__default_value = resources.getString(R.string.impact_sensitivity_threshold__default_value);
	}
	
	@Override
	public ComputedThresholds compute(Vector<Double> gravityAverageSamples/*, Thresholds thresholds*/) {
		
		SharedPreferences sharedPreferences = sharedPreferencesProvider.get();
		double impactThresholdMultiplier = 
				Float.parseFloat(
						sharedPreferences.getString(FragmentSettings.PREFERENCE_IMPACT_SENSITIVITY_THRESHOLD, 
//								StringsUtils.EMPTY+Config.Detection.Impact.getImpactThresholdForGravityMultiplier()
								impact_sensitivity_threshold__default_value
						) );
		ComputedThresholds computedThresholds = realCompute(gravityAverageSamples, impactThresholdMultiplier);
		
		return computedThresholds;
	}
}
