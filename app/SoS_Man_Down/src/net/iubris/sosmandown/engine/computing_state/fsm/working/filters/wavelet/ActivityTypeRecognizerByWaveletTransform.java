/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
/**
 * credits: based on http://mylifewithandroid.blogspot.com/2010/06/shake-walk-run.html
 */
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.delegates.AnalyzerDelegate;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.delegates.AnalyzerDelegateForRunning;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.delegates.AnalyzerDelegateForShaking;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.delegates.AnalyzerDelegateForWalking;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.WaveletProcessedData;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.LevelKeeper;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.AveragingWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.DelayWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.PeakWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.utils.WaveletConstants;
import net.iubris.sosmandown.subscriptions.waveletprocessed.WaveletProcessedDataAndActivityTypePublisher;
import net.iubris.sosmandown.subscriptions.waveletprocessed.WaveletProcessedDataSubscriber;
import net.iubris.sosmandown.ui.activities.main.MainActivity;
import net.iubris.sosmandown.utils.Printor;

import org.greenrobot.eventbus.EventBus;

import android.content.Context;

@Singleton
//@ContextSingleton
public class ActivityTypeRecognizerByWaveletTransform implements WaveletProcessedDataAndActivityTypePublisher {
	
	private static final int MAX_BUFFER_LEN = 40;

	private double gravity;
	
//	private PrintWriter captureFile;
	
	private final AveragingWindow w3power = new AveragingWindow(WaveletConstants.W3_POWER_AVERAGING_WINDOW_LENGTH);
	private final AveragingWindow w4power = new AveragingWindow(WaveletConstants.W4_POWER_AVERAGING_WINDOW_LENGTH);
	private final AveragingWindow w5power = new AveragingWindow(WaveletConstants.W5_POWER_AVERAGING_WINDOW_LENGTH);

	private final PeakWindow w3peakWindow = new PeakWindow(WaveletConstants.W3_PEAK_WINDOW);
	private final PeakWindow w4peakWindow = new PeakWindow(WaveletConstants.W4_PEAK_WINDOW);
	private final PeakWindow w5peakWindow = new PeakWindow(WaveletConstants.W5_PEAK_WINDOW);

	private final DelayWindow w5pwdelayWindow = new DelayWindow(WaveletConstants.W5_PW_DELAY_WINDOW);
	private final LevelKeeper w5pwdelayLevelKeeper = new LevelKeeper(WaveletConstants.W5_PW_DELAY_LEVEL_KEEPER);
	
	private final double sampleBuffer[] = new double[MAX_BUFFER_LEN];
	private int sampleBufferIndex = 0;
	
	private static final long MAX_SAMPLE_COUNTER = 0xFFFFFFF0L;
	private long sampleCounter;
	
	private boolean processingStarted = false;

	
	private final List<AnalyzerDelegate> analyzerDelegates = new ArrayList<>();
	
	private final AnalyzerDelegateForWalking analyzerDelegateForWalking;
	private final AnalyzerDelegateForRunning analyzerDelegateForRunning;
	private final AnalyzerDelegateForShaking analyzerDelegateForShaking;

	private final EventBus eventBus;
	
	private ActivityType activityTypeCurrent = ActivityType.UNKNOWN;
	
	@Inject
	public ActivityTypeRecognizerByWaveletTransform(AnalyzerDelegateForRunning analyzerDelegateForRunning, 
			AnalyzerDelegateForShaking analyzerDelegateForShaking, 
			AnalyzerDelegateForWalking analyzerDelegateForWalking,
			EventBus eventBus, Context context) {
Printor.whoIAm(this,context);
		this.analyzerDelegateForWalking = analyzerDelegateForWalking;
		this.analyzerDelegateForRunning = analyzerDelegateForRunning;
		this.analyzerDelegateForShaking = analyzerDelegateForShaking;
		
		this.eventBus = eventBus;
//		Ln.d("WaveletTransformAnalyzer:"+this.hashCode());
		
		analyzerDelegates.add(analyzerDelegateForShaking);
		analyzerDelegates.add(analyzerDelegateForWalking);
		analyzerDelegates.add(analyzerDelegateForRunning);
		initDelegatesClient();
	}
	private void initDelegatesClient() {
		for (AnalyzerDelegate analyzerDelegate : analyzerDelegates) {
			analyzerDelegate.setDataClient(this);
		}
	}
	
	public void stopProcessing() {
		if (!processingStarted) {
			return;
		}
		
		processingStarted = false;
	}

	public void prepareForProcessing(/*double gravity*/) {
		if (processingStarted) {
			return;
		}
		
		sampleCounter = 0L;
		
		Arrays.fill(sampleBuffer, 0.0);
		sampleBufferIndex = 0;

		w3power.reset();
		w4power.reset();
		w5power.reset();

		w3peakWindow.reset();
		w4peakWindow.reset();
		w5peakWindow.reset();

		w5pwdelayWindow.reset();
		w5pwdelayLevelKeeper.reset();

		for (AnalyzerDelegate analyzerDelegate : analyzerDelegates) {
			analyzerDelegate.init();
		}
		
		for (ActivityType activityType : ActivityType.values()) {
			activityType.resetCounter();
		}

		processingStarted = true;
	}

	// Processes one sample
	public void processSample(double accelerationResultant) {
//		if (values.length < 3) { return; }
		sampleCounter++;
		if (sampleCounter > MAX_SAMPLE_COUNTER) {
			sampleCounter = 0L;
		}
		double ampl = accelerationResultant;
		ampl -= gravity /*9.81*/; // subtract the Earth's gravity accel to decrease the initial ripple in the filters
		sampleBuffer[sampleBufferIndex++] = ampl;
		sampleBufferIndex = sampleBufferIndex % MAX_BUFFER_LEN;
		double w3out = convolution(WaveletConstants.W3, sampleBufferIndex, sampleBuffer);
		double w4out = convolution(WaveletConstants.W4, sampleBufferIndex, sampleBuffer);
		double w5out = convolution(WaveletConstants.W5, sampleBufferIndex, sampleBuffer);
		w3peakWindow.placeValue(w3out);
		w4peakWindow.placeValue(w4out);
		w5peakWindow.placeValue(w5out);
		double w3pw = w3power.calculatePower(w3out);
		double w4pw = w4power.calculatePower(w4out);
		double w5pw = w5power.calculatePower(w5out);
		double delayedw5pw = w5pw;
		// double delayedw5pw = w5pwdelayWindow.placeValue( w5pw );
		double w5pwlevelkept = w5pwdelayLevelKeeper.getLevel(delayedw5pw);

		// Processes the shake movement
		analyzerDelegateForShaking.process(sampleCounter, w5out, w5pw);
		
		// Processes the walking movement
		analyzerDelegateForWalking.process(sampleCounter, w3out, w3pw, w4pw, w5pwlevelkept);
		
		// Processes the running movement
		analyzerDelegateForRunning.process(sampleCounter, w4out, w3pw, w4pw, w5pwlevelkept);
	}
	
	public void enableSendDataForChart() {
		for (AnalyzerDelegate analyzerDelegate : analyzerDelegates) {
			analyzerDelegate.enableSendDataForChart();
		}
	}
	public void disableSendDataForChart() {
		for (AnalyzerDelegate analyzerDelegate : analyzerDelegates) {
			analyzerDelegate.disableSendDataForChart();
		}
	}
	public void onNewDetection(ActivityType activityType) {
//		Ln.d("onNewDetection: new activity: "+activityType.name()+":"+activityType.getCounter());
		publish(activityType);
	}
	public void onContinuedDetection(ActivityType activityType) {
//		Ln.d("onContinuedDetection: continued activity: "+activityType.name()+":"+activityType.getCounter());
		publish(activityType);
	}
	/**
	 * for {@link WaveletProcessedDataAndActivityTypePublisher} - {@link MainActivity}
	 */
	@Override
	public void publish(ActivityType activityType) {
//		Ln.d("posting "+activityType.name());
		eventBus.post(activityType);
	}
	
	public void onNewProcessedData(WaveletProcessedData waveletProcessedData) {
//		Ln.d("new waveletProcessedData: activityType:"+waveletProcessedData.getActivityType().name()+", size:"+waveletProcessedData.getTransformCollectedData().size());
		publish(waveletProcessedData);
	}
	/**
	 * for {@link WaveletProcessedDataSubscriber} - {@link FragmentChartActivityRecognitionByWaveletTransform}
	 */
	@Override
	public void publish(WaveletProcessedData waveletProcessedData) {
		eventBus.post(waveletProcessedData);
	}

	private static double convolution(double filter[], int sampleBufferIndex, double[] sampleBuffer) {
		int counter = sampleBufferIndex - 1;
		double result = 0.0;
		for (int i = 0; i < filter.length; ++i) {
			if (counter < 0) {
				counter = MAX_BUFFER_LEN - 1;
			}
			result += filter[i] * sampleBuffer[counter];
			counter--;
		}
		return result;
	}

	@SuppressWarnings("static-method")
	public ActivityType[] getResults() {
		return ActivityType.values();
	}

	public void setGravity(double gravity) {
		this.gravity = gravity;
	}

	public ActivityType getActivityTypeCurrent() {
		return activityTypeCurrent;
	}
	public void setActivityTypeCurrent(ActivityType activityTypeCurrent) {
		this.activityTypeCurrent = activityTypeCurrent;
	}
	
}
