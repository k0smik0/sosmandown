/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.config.Config.Detection.Detecting;
import net.iubris.sosmandown.engine.computing_state.fsm.FSMComputedState;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.computing_state.fsm.working.actions.StateActionPlaySound;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.DetectionHelper;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.FallDetectionCounter;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorStateEnum;
import roboguice.util.Ln;

@Singleton
public class DetectingFallWorkingFSMComputingState extends AbstractWorkingFSMComputedState implements WorkingFSMComputingState/*, DetectorDelegateClient*/ {
	
	private static final long serialVersionUID = 3247040811426162931L;

//	private final ActivityTypeRecognizerByWaveletTransform activityTypeRecognizerByWaveletTransform;
//	private final PostureRecognizer postureRecognizer;

	private final StateActionPlaySound stateActionPlaySound;

//	private final DetectionsResulter detectionResultDelegate;
	
	private static final int TRANSIENT_SECONDS = 2;
	private long transientEnd;
	
	private long detectingTimeEnd;
	
	private boolean executed;

	private final DetectionHelper detectionHelper;
	
	final long[] subIntervalsArray = new long[Detecting.SUB_INTERVALS+1];

	private int subIntervalCounter;

	@Inject
	public DetectingFallWorkingFSMComputingState(FSMComputedStateProcessor fsmComputedStateProcessor, FSMComputingStatesLocator fsmComputingStatesLocator, 
//			final ActivityTypeRecognizerByWaveletTransform activityTypeRecognizerByWaveletTransform, final PostureRecognizer postureRecognizer, 
//			final DetectionsResulter detectionResulter,
			DetectionHelper detectionDelegate,
			StateActionPlaySound stateActionPlaySound) {
		super(fsmComputedStateProcessor, fsmComputingStatesLocator, WorkingMajorStateEnum.DETECTING_FALL,WorkingMinorStateEnum.DETECTING_FALL_START);
//		this.activityTypeRecognizerByWaveletTransform = activityTypeRecognizerByWaveletTransform;
//		this.postureRecognizer = postureRecognizer;
//		this.detectionResultDelegate = detectionResulter;
		this.detectionHelper = detectionDelegate;
		this.stateActionPlaySound = stateActionPlaySound;
//		this.detectionDelegate = new DetectionDelegate(detectionResultDelegate, activityTypeRecognizerByWaveletTransform, postureRecognizer);
		
		reset();
	}
	
	@Override
	public void handle(SensorEventDataHolder sensorEventDataHolder) {
		
		long now = System.currentTimeMillis();
		
		// transient = 2 seconds after IMPACT
		if (detectionHelper.isStillInTransient(now, transientEnd)) { // still transient
			return;
		}

		// here start real detection		
		
		double resultant = sensorEventDataHolder.resultant;
		
//		activityTypeRecognizerByWaveletTransform.processSample(resultant);
//		postureRecognizer.computePosture(sensorEventDataHolder);
		detectionHelper.collecting(sensorEventDataHolder);
		
		
//		Ln.d("now: "+now+", subIntervalCounter:"+subIntervalCounter);
		if (!detectionHelper.isStillInNthDetectingSubInterval(now, subIntervalsArray[subIntervalCounter])) {
			Ln.d("still to detect, because results: "+detectionHelper.getResults(subIntervalCounter));
			subIntervalCounter++;
			Ln.d("still to detect, so incrementing counter to: "+subIntervalCounter);
			Ln.d("now: "+now+", subIntervalCounter:"+subIntervalCounter);
		} 
		computingForNthInterval(resultant);
		return;
	}
	
	private void computingForNthInterval(double resultant) {
		boolean hasAnyTypeMovement = detectionHelper.hasAnyTypeMovement(resultant, subIntervalCounter);
		if (hasAnyTypeMovement) {
			changeToRELAX();
			return;
		}
		
		// we have no momevements and we are at the end of the detecting time ;/
		if (!hasAnyTypeMovement && subIntervalCounter == Detecting.SUB_INTERVALS) {
			Ln.d("no more detecting, so going to FALL at:" + new Date()+", with resultant:"+resultant);
			changeToFALL();
			return;
		}
	}
	
	
	private void changeToRELAX() {
//		setMinorState(MinorStateE.RELAX_MOVEMENT);
		saveDetection(WorkingMajorStateEnum.RELAX);
		reset();
		FSMComputedState fsmComputedState = fsmComputingStatesLocator.get(WorkingMajorStateEnum.RELAX);
	//	eventProcessorFSM.stopCollectingData();
		changeStateTo(fsmComputedState);		
	}
	private void changeToFALL() {
//		Ln.d("changing to FALL");
		saveDetection(WorkingMajorStateEnum.FALL);
		FallDetectionCounter.DETECTED.incrementCounter();
		reset();
		FSMComputedState fsmComputedState = fsmComputingStatesLocator.get(WorkingMajorStateEnum.FALL);
		fsmComputedState.prepare();
		changeStateTo(fsmComputedState);
	}
	
	private void saveDetection(WorkingMajorStateEnum workingMajorStateEnum) {
//		detectionResultDelegate.saveDetection(workingMajorStateEnum);
		detectionHelper.save(workingMajorStateEnum);
	}

	@Override
	public void prepare() {
		long start = System.currentTimeMillis();
Ln.d("DetectingFall at:" + new Date(start)+" ("+start+")" );
		setupMaxTransientTime(start);
		setupMaxDetectingTime(start);
		
		setupSubIntervalsDetectingTime(start);
		
//		transientDetectorDelegate.setEndTransientTime(transientEnd);
//		collectingDetectorDelegate.setEndDetectingTime(detectingTimeEnd);
//		detectorDelegate = transientDetectorDelegate;
		
		/*activityTypeRecognizerByWaveletTransform.prepareForProcessing();
		postureRecognizer.prepareForProcessing();
		detectionResultDelegate.prepareForProcessing();*/
		detectionHelper.prepare();
		
		
//		waveletTransformAnalyzer.processSample(sensorEventDataHolder.resultant);
	}
	private void setupMaxTransientTime(long now) {
		transientEnd = now+(TRANSIENT_SECONDS*1000);
		Ln.d("transient time end at:"+new Date(transientEnd)+"("+transientEnd+")");
	}
	private void setupMaxDetectingTime(long now) {
		detectingTimeEnd = now+(Detecting.INTERVAL_TIME_MAX*1000);
		Ln.d("detecting time end at:"+new Date(detectingTimeEnd)+"("+detectingTimeEnd+")");
	}
	private void setupSubIntervalsDetectingTime(long now) {
		int subInterval = Detecting.INTERVAL_TIME_MAX*1000/Detecting.SUB_INTERVALS;
		
		for (int i=1;i<=Detecting.SUB_INTERVALS;i++) {
			subIntervalsArray[i] = now+subInterval*i;
//			Ln.d("subIntervalsArray["+i+"] = "+subIntervalsArray[i]);
		}
		subIntervalCounter = 1;
	}

	@Override
	public void reset() {
		Ln.d("reset");
		transientEnd = -1;
		detectingTimeEnd = -1;
//		counterEventsHandled = 0;
//		slightMovementsCounter = 0;
		
		/*activityTypeRecognizerByWaveletTransform.stopProcessing();
//		postureRecognizer.reset();
		detectionResultDelegate.reset();*/
		detectionHelper.reset();
		
//		collectedSize = 0;
//		collected.clear();
		
		subIntervalCounter = 1;
		
		executed = false;
	}
	

	@Override
	public void exec() {
		if (!executed) {
			stateActionPlaySound.setClient(this.getClass().getSimpleName()+"@"+hashCode());
			stateActionPlaySound.setPlayingLoop(true);
//			stateActionPlaySound.doOnState();
//			executor.execute(sleepRunnable);
			executed = true;
		}
	}
	@Override
	public void releaseResources() {
		stateActionPlaySound.releaseResources();
		executed = false;
	}
}
