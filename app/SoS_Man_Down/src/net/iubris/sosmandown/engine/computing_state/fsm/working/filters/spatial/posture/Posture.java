/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture;

import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.Counterable;
import net.iubris.sosmandown.utils.StringsUtils;


public enum Posture implements Counterable {
	FALL {
		@Override
		public void accept(PostureVisitor postureVisitor) {
			postureVisitor.visitFall(this);
		}
	},
	SITTING {
		@Override
		public void accept(PostureVisitor postureVisitor) {
			postureVisitor.visitSitting(this);
		}
	},
	STANDING {
		@Override
		public void accept(PostureVisitor postureVisitor) {
			postureVisitor.visitStanding(this);
		}
	},
	WALKING {
		@Override
		public void accept(PostureVisitor postureVisitor) {
			postureVisitor.visitWalking(this);
		}
	},
	UNKNOWN {
		@Override
		public void accept(PostureVisitor postureVisitor) {
			postureVisitor.visitUnknown(this);
		}
	};

	public void doAction() {
//		Ln.d("posture: "+name());
	}
	
	protected int counter;
	@Override
	public int getCounter() {
		return counter;
	}
	@Override
	public String getCounterAsString() {
		return StringsUtils.EMPTY+counter;
	}
	@Override
	public void resetCounter() {
		counter = 0;
	}
	public void incrementCounter() {
		counter++;
	}
	public abstract void accept(PostureVisitor postureVisitor);
	
	public static interface PostureVisitor {
		void visit(Posture posture);
		void visitFall(Posture posture);
		void visitUnknown(Posture posture);
		void visitStanding(Posture posture);
		void visitSitting(Posture posture);
		void visitWalking(Posture posture);
	}
	public static abstract class DefaultDoNothingPostureVisitor implements PostureVisitor {
		@Override
		public void visitFall(Posture posture) {}
		@Override
		public void visitUnknown(Posture posture) {}
		@Override
		public void visit(Posture posture) {}
		@Override
		public void visitSitting(Posture posture) {}
		@Override
		public void visitStanding(Posture posture) {}
		@Override
		public void visitWalking(Posture posture) {}
	}
}
