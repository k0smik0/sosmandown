/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer;

import java.util.Vector;

import net.iubris.sosmandown.config.Config.Detection.FreeFall;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import roboguice.util.Ln;

abstract public class AbstractThresholdsComputer implements ThresholdsComputer {

	protected static ComputedThresholds realCompute(Vector<Double> gravityAverageSamples, double impactThresholdForGravityMultiplier) {
		double gravity = 0;
		for (Double f : gravityAverageSamples) {
			gravity+=f;
		}
		// we have averaged gravity !
		gravity = 1.0d*gravity/gravityAverageSamples.size();

		double gravityThreshold = gravity;
		
		double freeFallThreshold = gravityThreshold-FreeFall.FREE_FALL_SUBSTRACTER;

		double impactResultThreshold = Util.computeImpactResultThreshold(gravityThreshold,impactThresholdForGravityMultiplier);
		Ln.d("gravityThreshold ["
				+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT,gravityThreshold)
				+"] * "+"impactThresholdForGravityMultiplier ["
				+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT,impactThresholdForGravityMultiplier)
				+"] = "
				+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT,impactResultThreshold) );
		
		
Ln.d("G: "+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(gravityThreshold)
		+", IMPACT_THRESHOLD: "+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(impactResultThreshold)
		+", FREE_FALL_THRESHOLD:"+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(freeFallThreshold));

		ComputedThresholds computedThresholds = new ComputedThresholds(gravityThreshold, freeFallThreshold, impactResultThreshold);
		return computedThresholds;
	}
	
}
