/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.sub_detectors;



public abstract class TransientDetectorDelegate/*<W extends WorkingState>*/ extends AbstractDetectorDelegate {
	protected long transientEnd;

	public TransientDetectorDelegate(DetectorDelegateClient detectorDelegateClient) {
		super(detectorDelegateClient);
	}
	
	@Override
	public boolean hasToRemainHere(long now) {
		// was isStillInTransient(now);
		// transient = ~1 seconds after IMPACT
		boolean beforeEnd = now < transientEnd;
		return beforeEnd;
	}

	public void setEndTransientTime(long transientEnd) {
		this.transientEnd = transientEnd;
	}
}
