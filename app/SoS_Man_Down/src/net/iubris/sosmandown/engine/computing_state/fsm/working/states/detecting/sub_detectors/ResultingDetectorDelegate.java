/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.sub_detectors;

import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import roboguice.util.Ln;

public abstract class ResultingDetectorDelegate extends AbstractDetectorDelegate {
	
	protected WorkingMajorStateEnum nextWorkingMajorState = WorkingMajorStateEnum.NULL;
	protected double resultant;
	protected int subIntervalCounter;

	public ResultingDetectorDelegate(DetectorDelegateClient detectorDelegateClient) {
		super(detectorDelegateClient);
	}
	
//	protected final Map<WorkingMajorStateEnum,PostResultAction> workingMajorStateEnumToPostResultActionMap = new HashMap<>();
	
//	private long transientEnd;
	
	protected long detectingEnd;
	protected boolean detectionEnd;
	
	
	@Override
	protected boolean hasToRemainHere(long now) {
//		boolean spatialResult = bigMovementsCounter >= Detecting.NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE;
//		return spatialResult;
		
		detectionEnd = true;
		if (hasAnyGoodActivity() || hasAnyGoodPosture() || hasAnyGoodRawMovement() ) {
			Ln.d("setting nextWorkingMajorState to relax because some activity/posture/rawMovement has been occurred!");
			nextWorkingMajorState = WorkingMajorStateEnum.RELAX;
		} else if (SensorEventProcessorUtils.isResultantOutOfGreatRangeAround1G(resultant)) {
			Ln.d("setting nextWorkingMajorState to relax because a outOfRange movement has been occurred!");
			// possibly walking or something
			nextWorkingMajorState = WorkingMajorStateEnum.RELAX;
		} else {
			nextWorkingMajorState = WorkingMajorStateEnum.FALL;
//			boolean isTransientEndnow > transientEnd;
//			Ln.d("now > transientEnd")
			if (now > detectingEnd) {
				Ln.d("setting nextWorkingMajorState to fall, and detection ends!");
			} else {
				Ln.d("setting nextWorkingMajorState to fall, but continue to detect!");
				detectionEnd = false;
			}
		}
		// we always return false 
		return false;
	}
	protected abstract boolean hasAnyGoodActivity();
	protected abstract boolean hasAnyGoodPosture();
	protected abstract boolean hasAnyGoodRawMovement();
	
	@Override
	public void act(long now, SensorEventDataHolder sensorEventDataHolder) {
		this.resultant = sensorEventDataHolder.resultant;
		super.act(now, sensorEventDataHolder);
	}

	@Override
	protected void doSomeStuff(SensorEventDataHolder sensorEventDataHolder) {
		this.resultant = sensorEventDataHolder.resultant;
	}

	public void setSubIntervalCounter(int subIntervalCounter) {
		this.subIntervalCounter = subIntervalCounter;
	}
	protected int getSubIntervalCounter() {
		return subIntervalCounter;
	}
	
	/*protected abstract void populateWorkingMajorStateEnumToDetectorMap(Map<WorkingMajorStateEnum,PostResultAction> workingMajorStateEnumToPostResultActionMap);
	public static interface PostResultAction {
		DetectorDelegate getActualDetectorDelegate();
	}*/
	public void setEndDetectingTime(long detectingEnd) {
		this.detectingEnd = detectingEnd;
	}
	@Override
	public void reset() {
		this.subIntervalCounter = 1;
	}
}
