/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal;

public class LevelKeeper {
	private int keepLength;
	private double levelKept;
	private int levelKeepingCounter;

	public LevelKeeper(int keepLength) {
		this.keepLength = keepLength;
		reset();
	}
	public void reset() {
		levelKeepingCounter = 0;
	}

	public double getLevel(double input) {
		double result = input;
		--levelKeepingCounter;
		if ((levelKeepingCounter < 0) || (input > levelKept)) {
			levelKept = input;
			levelKeepingCounter = keepLength;
		} else {
			result = levelKept;
		}
		return result;
	}
}
