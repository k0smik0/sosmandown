/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm;

import net.iubris.sosmandown.engine.computing_state.ComputedState;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.model.state.phase.PhaseState;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorState;

public abstract class AbstractFSMComputingState implements FSMComputedState {

	private static final long serialVersionUID = 7194400976192748518L;

	protected final WorkingMajorState majorState;
	protected WorkingMinorState minorState;
	protected PhaseState runningState;
	
	protected transient final FSMComputedStateProcessor computedStateProcessorFSM;

	protected final FSMComputingStatesLocator fsmComputingStatesLocator;

	public AbstractFSMComputingState(FSMComputedStateProcessor fsmComputedStateProcessor, FSMComputingStatesLocator fsmComputedStatesLocator, WorkingMajorState majorState, WorkingMinorState minorState, PhaseState runningState/*, InitState initState*/) {
		this.computedStateProcessorFSM = fsmComputedStateProcessor;
		this.fsmComputingStatesLocator = fsmComputedStatesLocator;
		this.majorState = majorState;
		this.minorState = minorState;
		this.runningState = runningState;
	}
	
	/*
	 * implements CombinedState - start
	 */
	@Override
	public WorkingMajorState getMajor() {
		return majorState;
	}
	@Override
	public WorkingMinorState getMinor() {
		return minorState;
	}
	protected void setMinorState(WorkingMinorState minorState) {
		this.minorState = minorState;
	}
	@Override
	public PhaseState getPhase() {
		return runningState;
	}
	@Override
	public void exec() {}
	@Override
	public void reset() {}
	@Override
	public void prepare() {}
	@Override
	public void releaseResources() {}
	/*
	 * implements CombinedState - end
	 */

//	@Override
//	protected void changeStateTo(MajorState majorState) {
//		eventProcessorFSM.setCombinedState( FSMCombinedStatesLocator.INSTANCE.get(majorState) );
//	}
	protected void changeStateTo(ComputedState computedState) {
//		Ln.d("changing to: "+computedState.getMajor());
		computedStateProcessorFSM.setComputedState(computedState);
	}
}
