/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm._locator;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.engine.computing_state.fsm.FSMComputedState;
import net.iubris.sosmandown.engine.computing_state.fsm.init.InitFSMComputingState;
import net.iubris.sosmandown.model.state.State;
import net.iubris.sosmandown.model.state.phase.PhaseState;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.utils.Printor;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;
//import roboguice.util.Ln;
import android.content.Context;

@Singleton
//@ContextSingleton
public class FSMComputingStatesLocatorC implements FSMComputingStatesLocator {
	
	private final Map<State,FSMComputedState> workingMajorStateToWorkingFSMComputedStateMap = new HashMap<>();
	private InitFSMComputingState initCombinedState;
	
	@Inject
	public FSMComputingStatesLocatorC(Context context) {
		Printor.whoIAm(this,context);
	}
	
	@Override
	public FSMComputingStatesLocator init(Context context) {
		RoboInjector injector = RoboGuice.getInjector(context);
		initCombinedState = injector.getInstance(InitFSMComputingState.class);
		WorkingMajorStateEnum[] values = WorkingMajorStateEnum.values();
		for (WorkingMajorStateEnum workingMajorStateEnum : values) {
			workingMajorStateToWorkingFSMComputedStateMap.put(workingMajorStateEnum, injector.getInstance(workingMajorStateEnum.getWorkingFSMComputedStateClass()));
		}
		workingMajorStateToWorkingFSMComputedStateMap.remove(WorkingMajorStateEnum.INIT);
		workingMajorStateToWorkingFSMComputedStateMap.remove(WorkingMajorStateEnum.NULL);
		return this;
	}
	
	/*@Override
	public FSMComputedStatesLocator init(Context context, FSMEventProcessor eventProcessorFSM) {
		RoboInjector injector = RoboGuice.getInjector(context);
		
		initCombinedState = new InitFSMComputedState(eventProcessorFSM, this, injector.getInstance(ThresholdsComputer.class) );

		
		ImpactTimeFilter impactTimeFilter = injector.getInstance(ImpactTimeFilter.class);
		workingMajorStateToWorkingFSMComputedStateMap.put(WorkingMajorStateEnum.RELAX, 
				new RelaxWorkingFSMComputedState(eventProcessorFSM, this, impactTimeFilter) );
		
		
		
		workingMajorStateToWorkingFSMComputedStateMap.put(WorkingMajorStateEnum.FREE_FALL, 
				new FreeFallWorkingFSMComputedState(eventProcessorFSM, this) );
		workingMajorStateToWorkingFSMComputedStateMap.put(WorkingMajorStateEnum.GOING_TO_IMPACT, 
				new GoingToImpactWorkingFSMComputedState(eventProcessorFSM, this) );
		
		
		workingMajorStateToWorkingFSMComputedStateMap.put(WorkingMajorStateEnum.IMPACT, 
				new ImpactWorkingFSMComputedState(eventProcessorFSM, this, impactTimeFilter) );

		PostureRecognizer postureRecognizer = injector.getInstance(PostureRecognizer.class);
		ActivityTypeRecognizerByWaveletTransform activityTypeRecognizerByWaveletTransform = injector.getInstance(ActivityTypeRecognizerByWaveletTransform.class);
		DetectingFallWorkingFSMCombinedState detectingFallWorkingFSMCombinedState = 
				new DetectingFallWorkingFSMCombinedState(eventProcessorFSM, 
						this, 
						activityTypeRecognizerByWaveletTransform, 
						postureRecognizer,
						null,
						injector.getInstance(StateActionPlaySound.class));
//		DetectingFallWorkingFSMCombinedState detectingFallWorkingFSMCombinedState = injector.getInstance(DetectingFallWorkingFSMCombinedState.class);
		workingMajorStateToWorkingFSMComputedStateMap.put(WorkingMajorStateEnum.DETECTING_FALL, detectingFallWorkingFSMCombinedState);
				

		PublisherSubscriberDelegate subscriberDelegate = injector.getInstance(PublisherSubscriberDelegate.class);
		
		
		FallWorkingFSMComputedState fallWorkingCombinedState = new FallWorkingFSMComputedState(eventProcessorFSM, 
				this,
				injector.getInstance(StateActionVibrateAndLaunchMainActivity.class),
				injector.getInstance(StateActionPlaySound.class),
//				injector.getInstance(LocationProvider.class), 
				subscriberDelegate);
		workingMajorStateToWorkingFSMComputedStateMap.put(WorkingMajorStateEnum.FALL, fallWorkingCombinedState);
		

		
		return this;
	}*/
	
	@Override
	public FSMComputedState get(WorkingMajorState majorState) {
		return workingMajorStateToWorkingFSMComputedStateMap.get(majorState);
	}
	@Override
	public FSMComputedState get(PhaseState initRunningState) {
//		FSMComputedState combinedState = workingMajorStateToWorkingFSMComputedState.get(initRunningState);
//		Ln.d("getting: combinedState:"+initCombinedState.hashCode()+" major:"+initCombinedState.getMajor()+" runningState:"+initRunningState.getLabel(initCombinedState.getMajor()));
		return initCombinedState;
	}
	@Override
	public void releaseResources() {
		for (FSMComputedState fsmEventState: workingMajorStateToWorkingFSMComputedStateMap.values()) {
			fsmEventState.releaseResources();
		}
	}

/*	@Override
	public void onComputedThresholds(ComputedThresholds computedThresholds) {

//		DetectingFallWorkingFSMCombinedState detectingFallWorkingFSMCombinedState = (DetectingFallWorkingFSMCombinedState) workingMajorStateToWorkingFSMComputedState.get(WorkingMajorStateEnum.DETECTING_FALL);
//		detectingFallWorkingFSMCombinedState.
		// TODO send to ..which?!
	}*/
}
