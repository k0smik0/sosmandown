/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting;


//@Singleton
public enum FallDetectionCounter {
	
	DETECTION,
	DETECTED;
	
	private int counter;
	public int getCounter() {
		return counter;
	}
	public void incrementCounter() {
		counter++;
	}

	public void reset() {
		counter = 0;
	}
}
