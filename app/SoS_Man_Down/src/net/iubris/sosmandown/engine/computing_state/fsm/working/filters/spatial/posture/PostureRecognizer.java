/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture;

import java.util.Arrays;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.subscriptions.PublisherSubscriberDelegate;
import net.iubris.sosmandown.subscriptions.postures.PosturePublisher;
import net.iubris.sosmandown.utils.Printor;
import android.content.Context;

/**
 * 
 * @author "Massimiliano Leone - massimiliano.leone@iubris.net"
 * credits: heavily inspired on https://github.com/BharadwajS/Fall-detection-in-Android/blob/master/Android/SenseFall/SenseFall/src/com/example/sensefall/ReadAccelData.java
 */
@Singleton
//@ContextSingleton
public class PostureRecognizer implements /*PostureObservable,*/ PosturePublisher {
	
	private static final int BUFFER_SIZE = 50;

	private Posture postureCurrent = Posture.UNKNOWN;
	private Posture posturePrevious = Posture.UNKNOWN;
	
	private double[] resultantWindow = new double[50];
	private double sigma = 0.5;
	private double th = 10;
	private double th1 = 5;
	private double th2 = 2;
	/*private double th4 = 2*9.8;
	private boolean probablyFall;
	private double resultantPrevious;*/
	
//	private PostureObserver postureObserver;

	private final PublisherSubscriberDelegate publisherSubscriberDelegate;

	@Inject
	public PostureRecognizer(PublisherSubscriberDelegate publisherSubscriberDelegate, Context context) {
Printor.whoIAm(this,context);
		this.publisherSubscriberDelegate = publisherSubscriberDelegate;
	}
	
	public void setGravity(double gravity) {
		this.th = gravity;
	}
	
	public Posture getPostureCurrent() {
		return postureCurrent;
	}

	public void computePosture(SensorEventDataHolder sensorEventDataHolder) {
		fillWindow(sensorEventDataHolder.resultant);
//		this.resultantPrevious = sensorEventDataHolder.resultant;
        recognizePosture(sensorEventDataHolder.y, sensorEventDataHolder.resultant);
        doOnChangeState();
//        Ln.d(hashCode()+", computePosture: postureCurrent:"+postureCurrent);
	}
	
	public void prepareForProcessing() {
		reset();
	}

	public void reset() {
		postureCurrent = Posture.UNKNOWN;
		posturePrevious = Posture.UNKNOWN;
		for (Posture posture : Posture.values()) {
			posture.resetCounter();
		}
		Arrays.fill(resultantWindow, 0);
	}

	private void fillWindow(double resultant) {
		 for(int i=0;i<=BUFFER_SIZE-2;i++){
	    	resultantWindow[i]=resultantWindow[i+1];
	     }
	     resultantWindow[BUFFER_SIZE-1]=resultant;
	}
	
	private void recognizePosture(double ay, @SuppressWarnings("unused") double resultantCurrent) {
		int zrc=computeZeroCrossingRate(resultantWindow);
		if(zrc==0){
			if (Math.abs(ay)<th1) {
//				postureCurrent=Posture.SITTING.incrementCounter();
				postureCurrent=Posture.SITTING;
//				probablyFall = true;
			} else {
//				postureCurrent=Posture.STANDING.incrementCounter();
				postureCurrent=Posture.STANDING;
//				probablyFall = false;
			}
		} else{
			if (zrc>th2) {
//				postureCurrent=Posture.WALKING.incrementCounter();
				postureCurrent=Posture.WALKING;
//				probablyFall = false;
			} else {
//				postureCurrent=Posture.UNKNOWN.incrementCounter();
				postureCurrent=Posture.UNKNOWN;
//				probablyFall = true;
			}
		}
		
		// TODO 
		/*double diff = resultantCurrent - resultantPrevious;
		if (diff>th4) {
			
		}*/
	}
	private int computeZeroCrossingRate(double[] window) {
		int count=0;
		for(int i=1;i<=BUFFER_SIZE-1;i++){
			if((window[i]-th)<sigma && (window[i-1]-th)>sigma){
				count++;
			}
		}
		return count;
	}
	/*private void fallDetection() {
		int resultantsWindowLength = resultantWindow.length;
		int flag=0;
		int max=1;
		int min=1;
		double[] array1 = Arrays.copyOf(resultantWindow, resultantWindow.length); 
		double[] array2 = new double[resultantWindow.length];
		array2[0]=0;
		for (int i=0;i<resultantWindow.length-1;i++) {
			array2[i+1]=resultantWindow[i];
		}
		double[] arrayDifference = arrayDifference(array1, array2);
		
		int i=0;
		// TODO COMPLETE
//		while( (i<=(resultantsWindowLength-1))&& (flag!=2)) {
//		  if(arrayDifference[i]<0 && arrayDifference[i+1]>0 && flag==0) {
//			 Collections.min(collection)
//		    min=find(min(a_vec(i:i+1)))+i-1;
//		    flag=flag+1;
//		  } 
//		  else if(arrayDifference[i]>0 && arrayDifference[i+1]<0 && flag==1) {
//		    max=find( max(a_vec(i:i+1)) ) +i-1;
//		    flag=flag+1;
//		  }
////		  end
//		  i=i+1;
//		}

		double minmax_attr=resultantWindow[max]-resultantWindow[min];

		if((minmax_attr < 2*9.8) || max<min) {
		    min=0;
		    max=0;
		}
	}
	private double[] arrayDifference(double[] array1, double[] array2) {
		double[] result = new double[array1.length];
		for (int i=0;i<array1.length;i++) {
			result[i]= (array1[i]-array2[i]);
		}
		return result;
	}*/
	
	private void doOnChangeState() {
		// Fall ?!
		if (!posturePrevious.equals(postureCurrent)) {
			/*
			 * if(curr_state2.equals(fall)){ m1_fall.start(); }
			 * if(curr_state2.equals(sitting)){ m2_sit.start(); }
			 * if(curr_state2.equalsIgnoreCase(standing)){ m3_stand.start(); }
			 * if(curr_state2.equalsIgnoreCase(walking)){ m4_walk.start(); }
			 */
			
			postureCurrent.incrementCounter();
			
			postureCurrent.doAction();
			posturePrevious = postureCurrent;
			onNewPosture(postureCurrent);
		}

	}
	
	/*@Override
	public void registerNewPostureObserver(PostureObserver postureObserver) {
		this.postureObserver = postureObserver;
	}
	@Override
	public void notifyNewPosture(Posture posture) {
		postureObserver.onNewPosture(posture);
	}*/
	
	private void onNewPosture(Posture postureCurrent) {
		if (Posture.FALL.equals(postureCurrent) || Posture.UNKNOWN.equals(postureCurrent)) {
			return;
		}
//		Ln.d("publishing "+postureCurrent+":"+postureCurrent.getCounter());
		publish(postureCurrent);		
	}

	@Override
	public void publish(Posture posture) {
		publisherSubscriberDelegate.post(posture);
	}

	@SuppressWarnings("static-method")
	public Posture[] getResults() {
		return Posture.values();
	}
}
