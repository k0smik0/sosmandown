/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model;


public enum WaveletTransformData {

//	INSTANCE;
	RUN,
	SHAKE,
	WALKING;
	
	
//	private ActivityType actualActivity;
//	private int runningCounter;
//	private int shakingCounter;
//	private int walkingCounter;
	
	/*private Queue<List<String>> transformationCollectedData;
	public Queue<List<String>> getTransformationCollectedData() {
		return transformationCollectedData;
	}*/

	/*public ActivityType getActualActivity() {
		return actualActivity;
	}
	public void setActualActivity(ActivityType actualActivity) {
		this.actualActivity = actualActivity;
	}*/
	
//	public int getRunningCounter() {
//		return runningCounter;
//	}
//	public void setRunningCounter(int runningCounter) {
//		this.runningCounter = runningCounter;
//	}
//	public int getShakingCounter() {
//		return shakingCounter;
//	}
//	public void setShakingCounter(int shakingCounter) {
//		this.shakingCounter = shakingCounter;
//	}
//	public int getWalkingCounter() {
//		return walkingCounter;
//	}
//	public void setWalkingCounter(int walkingCounter) {
//		this.walkingCounter = walkingCounter;
//	}	
	

	public void reset() {
//		transformationCollectedData = null;
//		actualActivity = ActivityType.NONE;
//		runningCounter = 0;
//		shakingCounter = 0;
//		walkingCounter = 0;
	}
	
	public void prepareForProcessing() {
//		setActualActivity(ActivityType.NONE.setCounter(0));		
//		transformationCollectedData = null; 
//		transformationCollectedData = new LinkedBlockingQueue<>();
	}
}
