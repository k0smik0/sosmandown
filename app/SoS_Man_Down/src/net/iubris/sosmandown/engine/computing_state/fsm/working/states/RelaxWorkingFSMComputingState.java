/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states;

import javax.inject.Inject;

import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.time.ImpactTimeFilter;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils.Thresholds;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorStateEnum;

public class RelaxWorkingFSMComputingState extends AbstractWorkingFSMComputedState implements WorkingFSMComputingState/*, ActionEventDetectionFallTestSubscriber*/ {

	private static final long serialVersionUID = 7678234426000000949L;

	private final ImpactTimeFilter impactTimeFilter;

//	private final EventBus eventBus;

	@Inject
	public RelaxWorkingFSMComputingState(FSMComputedStateProcessor eventProcessorFSM, FSMComputingStatesLocator fsmComputedStatesLocator, ImpactTimeFilter impactTimeFilter/*, EventBus eventBus*/) {
		super(eventProcessorFSM, fsmComputedStatesLocator, WorkingMajorStateEnum.RELAX, WorkingMinorStateEnum._RELAX);
		this.impactTimeFilter = impactTimeFilter;
//		this.eventBus = eventBus;
	}

	@Override
	public void handle(SensorEventDataHolder sensorEventDataHolder) {
//		Ln.d("RelaxWorkingCombinedState.handle");

		double freeFall = Thresholds.INSTANCE.getFreeFall();
//		Ln.d("resultant:"+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT,sensorEventDataHolder.resultant)+"<?free_fall:"+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT,freeFall));
		if (sensorEventDataHolder.resultant <= freeFall) {
//			minorStateCurrent = calculateMinorStateInFreeFall(result);
//	printEventually(MajorStateE.FREE_FALL, result, 187);
//			majorStatePrevious = State.MajorStateE.FREE_FALL;
			
//			// time-frequency
//			waveletTransformAnalyzer.prepareForProcessing(Thresholds.INSTANCE.getGravity());
//			waveletTransformAnalyzer.processSample(sensorEventDataHolder);
//			
			// time-amplitude
//			eventProcessorFSM.startCollectingDataForRelaxToImpactTimeAnalysis();
//			eventProcessorFSM.eventuallyCollectingData(sensorEventDataHolder);
			impactTimeFilter.setPreImpactStartTime(sensorEventDataHolder.timestamp);
			
			
			FreeFallWorkingFSMComputingState eventState = (FreeFallWorkingFSMComputingState) fsmComputingStatesLocator.get(WorkingMajorStateEnum.FREE_FALL);
			eventState.setAccelerationResultantFromPreviousState(sensorEventDataHolder.resultant);
//			eventState.setRawEventDataFromPreviousEvent( sensorEventDataHolder );
//			eventProcessorFSM.startCollectingDataForRelaxToImpactTimeAnalysis();
			changeStateTo(eventState);
		} // over G: going to impact, or some movements
	}

	@Override
	public void reset() {}
	
	/*
	 * test zone
	 */
	/*@Override
	public void registerSubscription() {
		if (!eventBus.isRegistered(this)) {
			eventBus.register(this);
			Ln.d("register to event subscription");
		}		
	}
	@Override
	public void unregisterSubscription() {
		if (eventBus.isRegistered(this)) {
			eventBus.unregister(this);
		}
	}	
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedLightComputedResult(ActionEventDetectionFallTest n) {
		Ln.d("received "+n);
		DetectingFallWorkingFSMCombinedState eventState = (DetectingFallWorkingFSMCombinedState) fsmComputedStatesLocator.get(WorkingMajorStateEnum.DETECTING_FALL);
		eventState.prepare();
//		eventState.setAccelerationResultantFromPreviousState(sensorEventDataHolder.resultant);
		changeStateTo(eventState);
	}*/
	
}
