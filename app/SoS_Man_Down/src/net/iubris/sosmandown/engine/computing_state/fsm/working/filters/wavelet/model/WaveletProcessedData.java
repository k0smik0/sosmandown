/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model;

import java.io.Serializable;
import java.util.List;

public class WaveletProcessedData implements Serializable {
	
	private static final long serialVersionUID = 6311798413849132392L;
	
	private final ActivityType activityType;
	private final List<Double> data;
	
	public WaveletProcessedData(ActivityType activityType, List<Double> data) {
		this.activityType = activityType;
		this.data = data;
	}	
//	public WaveletProcessedEvent() {}
	
	public ActivityType getActivityType() {
		return activityType;
	}

	public List<Double> getTransformCollectedData() {
		return data;
	}
}
