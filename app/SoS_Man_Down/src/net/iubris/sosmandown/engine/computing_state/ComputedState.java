/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state;

import java.io.Serializable;

import net.iubris.sosmandown.engine.computing_state.fsm.working.states.FallWorkingFSMComputingState;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.phase.PhaseState;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorState;

/**
 * @author "Massimiliano Leone - massimiliano.leone@iubris.net" the class
 *         modeling a possible state for new acceleration event
 */
public interface ComputedState extends Serializable {
	/**
	 * provides MajorState for this EventState
	 * 
	 * @return {@link WorkingMajorState}
	 */
	WorkingMajorState getMajor();

	/**
	 * provides MinorState for this EventState
	 * 
	 * @return {@link WorkingMinorState}
	 */
	WorkingMinorState getMinor();

	/**
	 * provides RunningState for this EventState
	 * 
	 * @return {@link PhaseState}
	 */
	PhaseState getPhase();

	/**
	 * handle the acceleration components, the timestamp and the
	 * accelerationResultant to compute next EventState
	 * 
	 * @param x
	 * @param z
	 * @param y
	 * @param timestamp
	 * @param accelerationResultant
	 */
	void handle(SensorEventDataHolder sensorEventDataHolder);

	/**
	 * reset internal state for EventState (ex: reset internal variables used to
	 * maintain some data, etc)
	 */
	void reset();

	/**
	 * prepare internal state for EventState (ex: init internal variables used
	 * to maintain some data, etc)
	 */
	void prepare();

	/**
	 * exec some action for specified state (ex: see
	 * {@link FallWorkingFSMComputingState#exec()})
	 */
	void exec();

	/**
	 * release hardware resources eventually used by State (ex: see
	 * {@link FallWorkingFSMComputingState#releaseResources()})
	 */
	void releaseResources();
}
