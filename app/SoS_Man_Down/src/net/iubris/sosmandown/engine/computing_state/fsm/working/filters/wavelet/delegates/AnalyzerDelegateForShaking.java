/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.delegates;

import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.DelayWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.PeakWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.utils.WaveletConstants;

public class AnalyzerDelegateForShaking extends AbstractAnalyzerDelegate {
		
	private int shakePeakCounter = 0;
	
	public AnalyzerDelegateForShaking() {
		super(	new DelayWindow(WaveletConstants.W5_DELAY_WINDOW), 
				new PeakWindow(WaveletConstants.DELAYED_W5_PEAK_WINDOW), 
				ActivityType.SHAKE);
	}

	@Override
	public void init() {
		super.init();
		shakePeakCounter = 0;
	}

	@Override
	public void process(long sampleCounter, double... wValues) {
		double w5out = wValues[0];
		double w5pw = wValues[1];
		
		double[] processedByFacade = processFacade(sampleCounter, w5out, wValues);
		double delayedout_w5 = processedByFacade[0];

		eventuallyPublishForChart(/*ActivityType.SHAKE, */new double[]{0,0,0,0,0,0,w5pw,0,0,delayedout_w5});
	}
	@Override
	protected boolean computeIfIsNowActing(double... wValues) {
		double w5pw = wValues[1];
		boolean nowShaking = w5pw > 0.4;
		return nowShaking;
	}
	@Override
	protected void eventuallySubAct() {
		shakePeakCounter = 0;
	}
	@Override
	protected void doOnPeakDetect() {
		shakePeakCounter++;
		if (shakePeakCounter >= 2) {
//			actingCounter++;
//			onNewActivityType(ActivityType.SHAKE.setCounter(actingCounter));
			super.doOnPeakDetect();
		}
	}
}
