/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.delegates;

import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.DelayWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.PeakWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.utils.WaveletConstants;

public class AnalyzerDelegateForWalking extends AbstractAnalyzerDelegate {

//	@Inject
	public AnalyzerDelegateForWalking(/*EventBus eventBus*/) {
		super(new DelayWindow(WaveletConstants.W3_DELAY_WINDOW), new PeakWindow(WaveletConstants.DELAYED_W3_PEAK_WINDOW), ActivityType.WALK);
	}

	@Override
	public void process(long sampleCounter, double... wValues) {
		double w3out = wValues[0];
		double w3pw = wValues[1];
		double w4pw = wValues[2];
		double w5pw = wValues[3];
		
		double[] processedByFacade = processFacade(sampleCounter, w3out, wValues);
		double delayedout_w3 = processedByFacade[0];
		double cw = processedByFacade[1];
		
		eventuallyPublishForChart(/*ActivityType.WALK,*/ new double[]{0,0,0,0,delayedout_w3,cw,0,w3pw,w4pw,w5pw});		
	}
	
	@Override
	protected boolean computeIfIsNowActing(double... wValues) {
		double w3pw = wValues[1];
		double w4pw = wValues[2];
		double w5pw = wValues[3];
		boolean nowWalking = (w5pw < 0.4) && (((w3pw > 0.2) && (w3pw < 0.8)) || ((w4pw > 0.2) && (w4pw < 0.4)));
		return nowWalking;
	}
}
