/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer;

public class ComputedThresholds {
	private final double gravity;
	private final double freeFall;
	private final double impact;
	
	public ComputedThresholds(double gravityThreshold, double freeFall, double impact) {
		this.gravity = gravityThreshold;
		this.freeFall = freeFall;
		this.impact = impact;
	}
	public double getGravity() {
		return gravity;
	}
	public double getFreeFall() {
		return freeFall;
	}
	public double getImpact() {
		return impact;
	}
}
