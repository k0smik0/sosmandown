/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.actions;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.utils.Printor;
import net.iubris.sosmandown.utils.ThreadsUtil;
import roboguice.util.Ln;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

/**
 * singleton because it uses hardware resources (audio manager)
 */
@Singleton
//@ContextSingleton
public class StateActionPlaySound implements StateAction {
	
	protected final Context context;
	private final AudioManager audioManager;
	
	private MediaPlayer soundFall;
	
	private boolean playing;
	
//	private int streamVolumeCurrent;
	// TODO restore
	private final int STREAM_VOLUME_MAX;
	private final int STREAM_VOLUME_DEFAULT;
	
	private boolean playingLoop;
	
	private String client;
	
	private final ThreadsUtil threadsUtil;
	
	@Inject
	public StateActionPlaySound(/*Application application, */AudioManager audioManager, ThreadsUtil threadsUtil, Context context) {
Printor.whoIAm(this,context);
		this.threadsUtil = threadsUtil;
		this.context = context;
		this.audioManager = audioManager;
//		this.playingLoop = playLoop;
		this.STREAM_VOLUME_MAX = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		this.STREAM_VOLUME_DEFAULT = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
	}
	
	public void setClient(String client) {
		this.client = client;
	}
	
	public void setPlayingLoop(boolean playingLoop) {
		this.playingLoop = playingLoop;
	}

	@Override
	public void doOnState() {
		Ln.d("doOnState");

		if (!playing) {
			Ln.d(this+" from "+client+": creating mediaplayer");
			soundFall = MediaPlayer.create(context, R.raw.beep);
			soundFall.setLooping( playingLoop );
			soundFall.setVolume(100, 100);
			Ln.d(this+" from "+client+": playing");
			soundFall.start();
			playing = true;
		} else {
			try { Thread.sleep(250); } catch (InterruptedException e) {}
			
			// TODO restore
			/* streamVolumeCurrent = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
			if (streamVolume<STREAM_VOLUME_MAX) {
				int increased = streamVolume+20;
				audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, increased, 0);
			}*/
		}
	}
	
	public void stop() {
		if (soundFall!=null) {
			soundFall.setLooping(false);
			soundFall.stop();
			Ln.d("stop "+soundFall);
		}
		playing = false;

		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, STREAM_VOLUME_DEFAULT, 0);
	}
	
	@Override
	public void doOnStateThenStop(long afterTimeStop) {
		doOnState();
		sleepRunnable.setStopTime(afterTimeStop);
		threadsUtil.execOnTmp(sleepRunnable);
	}
	private class SleepRunnable implements Runnable {
		private long stopTime;
		void setStopTime(long stopTime) {
			this.stopTime = stopTime;
		}
		@Override
		public void run() {
			try { Thread.sleep(stopTime); } catch (InterruptedException e) { e.printStackTrace(); }
			stop();
			releaseResources();
		}
	}
	/*private final Runnable sleepRunnable = new Runnable() {
		@Override
		public void run() {
			try { Thread.sleep(250); } catch (InterruptedException e) { e.printStackTrace(); }
			stop();
			releaseResources();
		}
	};*/
	private final SleepRunnable sleepRunnable = new SleepRunnable();
//	private final Executor executor = Executors.newFixedThreadPool(1);
	
	@Override
	public void releaseResources() {
		if (soundFall!=null) {
//			if (soundFall.isLooping()) {
//				soundFall.setLooping(false);
//			}
			/*if (soundFall.isPlaying()) {
				soundFall.stop();
			}*/
			Ln.d(hashCode()+" from "+client+": mediaplayer resetting");
			soundFall.reset();
			Ln.d(hashCode()+" from "+client+": mediaplayer releasing");
			soundFall.release();
			
			soundFall = null;
		}
		playing = false;
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, STREAM_VOLUME_DEFAULT, 0);
	}
	
	
	public void test() {
		if (!playing) {
			soundFall = MediaPlayer.create(context, R.raw.beep);
			soundFall.setLooping(playingLoop);
			soundFall.setVolume(100, 100);
			soundFall.start();
		}
		new Thread() {
			@Override
			public void run() {
				Ln.d(this);
				try { Thread.sleep(300); } catch (InterruptedException e) {}
				soundFall.setLooping(false);
				soundFall.pause();
				soundFall.stop();
			}
		}.start();
		
		playing = false;
	}
}
