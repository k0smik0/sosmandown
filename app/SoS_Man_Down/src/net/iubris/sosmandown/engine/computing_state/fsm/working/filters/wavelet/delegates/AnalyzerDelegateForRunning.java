/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.delegates;

import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.DelayWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.PeakWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.utils.WaveletConstants;

public class AnalyzerDelegateForRunning extends AbstractAnalyzerDelegate {
	
	public AnalyzerDelegateForRunning() {
		super(	new DelayWindow(WaveletConstants.W4_DELAY_WINDOW),
				new PeakWindow(WaveletConstants.DELAYED_W4_PEAK_WINDOW), 
				ActivityType.RUN);
	}

	@Override
	public void process(long sampleCounter, double... wValues) {
		
		double w4out = wValues[0];
		double w3pw = wValues[1];
		double w4pw = wValues[2];
		double w5pwlevelkept = wValues[3];
	
		double[] processedByFacade = processFacade(sampleCounter, w4out, wValues);
		double delayedout_w4 = processedByFacade[0];
		double cw = processedByFacade[1];
		
		eventuallyPublishForChart(/*ActivityType.RUN, */new double[]{0,0,0,0,0,delayedout_w4,cw,w3pw,w4pw,w5pwlevelkept});
	}
	@Override
	protected boolean computeIfIsNowActing(double... wValues) {
		double w4pw = wValues[2];
		double w5pwlevelkept = wValues[3];
		boolean nowRunning = (w5pwlevelkept < 0.7) && (w4pw > 0.4);
		return nowRunning;
	}
}
