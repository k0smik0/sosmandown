/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.sub_detectors;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import roboguice.util.Ln;

public abstract class CollectingDetectorDelegate extends AbstractDetectorDelegate {
	protected long detectingTimeEnd;
	
//	private int slightMovementsCounter;

	public CollectingDetectorDelegate(DetectorDelegateClient detectorDelegateClient) {
		super(detectorDelegateClient);
	}
	
	boolean forceStop = false;
//	private long detectingTimeStart;
//	private long detectingIntervalMax;
	
	
	
	@Override
	protected boolean hasToRemainHere(long now) {
		
		int subIntervalCounter = getSubIntervalCounter();
//		Ln.d("searching using "+subIntervalCounter);
		Long partialIntervalLong = partialResultIntervalsMap.get( subIntervalCounter );
		
		if (partialIntervalLong==null) {
			return false;
		}
		
		long partialInterval = partialIntervalLong.longValue();
		boolean lessThan = now < partialInterval;
		if (lessThan) {
			return true;
		}
		
		Ln.d("detecting time for subinterval "+ getSubIntervalCounter()+ " elapsed at: ["+new Date(now)+"("+now+")]"
				+", because "+now+"<"+partialInterval+"="+lessThan);
		return false;
		
//		return useMaxInterval(now);
	}
	
	/*private boolean useMaxInterval(long now) {
		if (forceStop) {
			return false;
		}
		boolean stillInInterval = now < detectingTimeEnd;
		if (!stillInInterval) {
			Ln.d("detecting time elapsed at:"+new Date(now)+"("+now+")");
		}
		return stillInInterval;
	}*/
	
	protected void enableForceStop() {
		this.forceStop = true;
	}
	protected void disableForceStop() {
		this.forceStop = false;
	}
	
	@Override
	protected void doSomeStuff(SensorEventDataHolder sensorEventDataHolder) {		
		// we want collect MAX_SMALL_MOVEMENTS_FOR_FALL_DETECTOR
//		if (slightMovementsCounter<=Detecting.NUM_MAX_SLIGHT_MOVEMENTS_TO_RETRIEVE) {
//			logSlightMovements(slightMovementsCounter);
//			return;
//		}
	}
	@SuppressWarnings("static-method")
	protected boolean areThereSlightMovements(double resultant) {
		if (SensorEventProcessorUtils.isResultantInSmallRangeAround1G(resultant)) {
//			slightMovementsCounter++;
//			logCheckForSlightMovements(resultant);
			return true;
		}
		return false;
	}
	/*private void logSlightMovements(int slightMovementsCounter) {
		if (slightMovementsCounter%32==0) {
			Ln.d("minorState:"+minorState+",smallMovementsCounter:"+slightMovementsCounter+"<="+Detecting.NUM_MAX_SLIGHT_MOVEMENTS_TO_RETRIEVE+", remaining here");
		}
	}*/
	/*private void logCheckForSlightMovements(double resultant) {
		counterLogCheckForSlightMovements++;
		if (counterLogCheckForSlightMovements%20==0) {
			Ln.d("checkForSlightMovements, resultant:"+resultant);
		}
		
	}*/
	
	private final Map<Integer, Long> partialResultIntervalsMap = new HashMap<>();

	private int nthSubIntervalCounter;
	
	/*public final void setNthSubIntervalCounter(int nthSubIntervalCounter) {
		this.nthSubIntervalCounter = nthSubIntervalCounter;
	}*/
	public final void incrementSubIntervalCounter() {
		this.nthSubIntervalCounter++;
	}
	@Override
	public final void reset() {
		this.nthSubIntervalCounter = 1;
	}
	protected int getSubIntervalCounter() {
		return nthSubIntervalCounter;
	}

	@SuppressWarnings("deprecation")
	public final void setTimes(long detectingTimeStart, long detectingTimeEnd, long detectingIntervalMax) {
//		this.detectingTimeStart = detectingTimeStart;
		this.detectingTimeEnd = detectingTimeEnd;
//		this.detectingIntervalMax = detectingIntervalMax;
		
		int subIntervalsNum = Config.Detection.Detecting.SUB_INTERVALS;
		
		long subIntervalOffset = detectingIntervalMax/subIntervalsNum;
		Ln.d("subIntervalOffset:"+subIntervalOffset+" = "+new Date(subIntervalOffset).getSeconds()+"s");
		
		for (int i=1;i<=subIntervalsNum;i++) {
			Long nthInterval = Long.valueOf( detectingTimeStart+(subIntervalOffset*i) );
			Ln.d("putting ("+i+","+nthInterval+")");
			partialResultIntervalsMap.put(i, nthInterval);
		}
		// the last uses the max
//		Ln.d("putting ("+subIntervalsNum+","+detectingIntervalMax+")");
//		partialResultIntervalsMap.put(subIntervalsNum, detectingIntervalMax);
		
		
		/*Map<Integer, Thresholds> partialResultThresholdsMap = Config.Detection.Result.Partial;
		if (partialResultThresholdsMap.size()>0 && subIntervalsNum > 0) {
			int subDetectingInterval = (int) (detectingIntervalMax/subIntervalsNum);
			Ln.d("subInterval:"+subDetectingInterval);
			
			for (int i=1;i<=subIntervalsNum;i++) {
				Thresholds thresholds = partialResultThresholdsMap.get(i);
			}
		}*/
	}
}
