/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.sub_detectors;

import net.iubris.sosmandown.model.result.SensorEventDataHolder;

abstract class AbstractDetectorDelegate implements DetectorDelegate {
		
		protected final DetectorDelegateClient detectorDelegateClient;

		public AbstractDetectorDelegate(DetectorDelegateClient detectorDelegateClient) {
			this.detectorDelegateClient = detectorDelegateClient;
		}
		
		protected abstract boolean hasToRemainHere(long now);
		
		protected abstract void doSomeStuff(SensorEventDataHolder sensorEventDataHolder);
		
		protected abstract void changeDetectorOnClient();
		
		@Override
		public void act(long now, SensorEventDataHolder sensorEventDataHolder) {
			if (hasToRemainHere(now)) {
				doSomeStuff(sensorEventDataHolder);
			} else {
				changeDetectorOnClient();
			}
		}
		
		@Override
		public void reset() {}
	}
