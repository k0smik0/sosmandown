/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states;

import javax.inject.Inject;

import net.iubris.sosmandown.engine.computing_state.fsm.FSMComputedState;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.time.ImpactTimeFilter;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.FallDetectionCounter;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorStateEnum;
import roboguice.util.Ln;

public class ImpactWorkingFSMComputingState extends AbstractWorkingFSMComputedState implements WorkingFSMComputingState {

	private static final long serialVersionUID = -2668170566337448019L;
	
	private final ImpactTimeFilter impactTimeFilter;

//	private final ActivityTypeRecognizerByWaveletTransform waveletTransformAnalyzer;

	@Inject
	public ImpactWorkingFSMComputingState(FSMComputedStateProcessor eventProcessorFSM,
			FSMComputingStatesLocator fsmComputedStatesLocator,
			ImpactTimeFilter impactTimeFilter
			/*,ActivityTypeRecognizerByWaveletTransform waveletTransformAnalyzer*/) {
		super(eventProcessorFSM, fsmComputedStatesLocator, WorkingMajorStateEnum.IMPACT, WorkingMinorStateEnum.IMPACT_FSM);
		this.impactTimeFilter = impactTimeFilter;
//		this.waveletTransformAnalyzer = waveletTransformAnalyzer;
	}

	@Override
	public void handle(SensorEventDataHolder sensorEventDataHolder) {
		// old
		/*
//		eventProcessorFSM.setCombinedState( FSMCombinedStatesLocator.INSTANCE.get(MajorStateE.DETECTING_FALL)  );
		FSMEventState fmsEventState = FSMEventStatesLocator.INSTANCE.get(MajorStateE.DETECTING_FALL);
//		Ln.d("changing to: "+combinedState.getMajor());
		changeStateTo(fmsEventState);
		*/

//		waveletTransformAnalyzer.processSample(sensorEventDataHolder);
		
		impactTimeFilter.setImpactStopTime(sensorEventDataHolder.timestamp);
		float interval = impactTimeFilter.computeImpactIntervalTime();
		// new
		
		FSMComputedState fmsEventState;
		// we consider >1 sec from free fall to impact 
		if ( !impactTimeFilter.isIntervalLowerThanThreshold() ) {
			Ln.d("impactTimeFilter.interval = "+interval+", going to DETECTING_FALL !");
			fmsEventState = fsmComputingStatesLocator.get(WorkingMajorStateEnum.DETECTING_FALL);
			fmsEventState.prepare();
			// time-frequency
//			waveletTransformAnalyzer.prepareForProcessing(/*Thresholds.INSTANCE.getGravity()*/);
//			waveletTransformAnalyzer.processSample(sensorEventDataHolder.resultant);
			
			FallDetectionCounter.DETECTION.incrementCounter();
		} else {
			Ln.d("impactTimeFilter.interval = "+interval+", going to RELAX");
			fmsEventState = fsmComputingStatesLocator.get(WorkingMajorStateEnum.RELAX);
		}
//		eventProcessorFSM.stopCollectingDataForRelaxToImpactTimeAnalysis();
		changeStateTo(fmsEventState);
	}
}
