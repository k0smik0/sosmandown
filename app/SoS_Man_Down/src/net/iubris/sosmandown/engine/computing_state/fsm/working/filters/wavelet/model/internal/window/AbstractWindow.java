/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window;

import java.util.Arrays;

class AbstractWindow {
	protected int windowLength;
	protected int windowIndex;
	protected double window[];

	public AbstractWindow(int windowLength) {
		this.windowLength = windowLength;
		this.window = new double[windowLength];
		reset();
	}
	public void reset() {
		this.windowIndex = 0;
		/*for (int i = 0; i < windowLength; ++i) {
			this.window[i] = 0.0;
		}*/
		Arrays.fill(window, 0.0);
	}
}
