/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.PostureRecognizer;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.ActivityTypeRecognizerByWaveletTransform;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.util.Ln;

@Singleton
public class DetectionHelper {
	
	private final DetectionsResulter detectionResulter;
	private final ActivityTypeRecognizerByWaveletTransform activityTypeRecognizerByWaveletTransform;
	private final PostureRecognizer postureRecognizer;
	
	@Inject
	public DetectionHelper(DetectionsResulter detectionResulter, ActivityTypeRecognizerByWaveletTransform activityTypeRecognizerByWaveletTransform, PostureRecognizer postureRecognizer) {
		this.detectionResulter = detectionResulter;
		this.activityTypeRecognizerByWaveletTransform = activityTypeRecognizerByWaveletTransform;
		this.postureRecognizer = postureRecognizer;
	}
	
	public void prepare() {
		activityTypeRecognizerByWaveletTransform.prepareForProcessing();
		postureRecognizer.prepareForProcessing();
		detectionResulter.prepareForProcessing();
	}
	
	public void reset() {
		activityTypeRecognizerByWaveletTransform.stopProcessing();
		postureRecognizer.reset();
		detectionResulter.reset();
	}
	
	public void save(WorkingMajorStateEnum workingMajorStateEnum) {
		detectionResulter.saveDetection(workingMajorStateEnum);
	}
	
	public void collecting(SensorEventDataHolder sensorEventDataHolder) {
		activityTypeRecognizerByWaveletTransform.processSample(sensorEventDataHolder.resultant);
		postureRecognizer.computePosture(sensorEventDataHolder);
	}

	@SuppressWarnings("static-method")
	public boolean isStillInTransient(long now, long transientEnd) {
		if (now <= transientEnd) {
//			setMinorState(WorkingMinorStateEnum.DETECTING_FALL_TRANSIENT_NOT_ELAPSED);
			// logInTransient(resultant);
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("static-method")
	public boolean isStillInNthDetectingSubInterval(long now, long endNthDetectingSubInterval) {
//		Ln.d("checking "+now+" <= "+endNthDetectingSubInterval);
		if (now <= endNthDetectingSubInterval) {
			return true;
		}
		return false;
	}
	
	public boolean hasAnyTypeMovement(double resultant, int subIntervalCounter ) {		
		if (detectionResulter.hasActivityResultForActivityType(activityTypeRecognizerByWaveletTransform.getResults(), subIntervalCounter) ||
			detectionResulter.hasActivityResultForPosture(postureRecognizer.getResults(), subIntervalCounter) ||
			detectionResulter.hasActivityResultForRawMovements() ) {
			Ln.d("subIntervalCounter:"+subIntervalCounter+", returning true because some activities/postures!");
				return true;
		}
		if (subIntervalCounter==(Config.Detection.Detecting.SUB_INTERVALS-1)) {
			if ( detectionResulter.hasActivityResultForRawMovements() ) {
				Ln.d("subIntervalCounter:"+subIntervalCounter+", returning true because some raw movements!");
				return true;
			}
		}
		if (subIntervalCounter==Config.Detection.Detecting.SUB_INTERVALS) {
			if (SensorEventProcessorUtils.isResultantOutOfGreatRangeAround1G(resultant)) {
				Ln.d("subIntervalCounter:"+subIntervalCounter+", at least returning true because a outOfRange movement has been occurred!");
				// possibly walking or something
	//			nextWorkingMajorState = WorkingMajorStateEnum.RELAX;
				return true;
			}
		}
		// FALL
//		Ln.d("returning false...");
		return false;
	}
	
	public String getResults(Object subIntervalCounter) {
		String s = "";
		s+="a_run:"+detectionResulter.counterActivityRun +StringsUtils.SLASH+ Config.Detection.Result.PartialThresholds.get(subIntervalCounter).minimum_activity_run; 
		s+=", a_walk:"+detectionResulter.counterActivityWalk +StringsUtils.SLASH+ Config.Detection.Result.PartialThresholds.get(subIntervalCounter).minimum_activity_walk;
		s+=", a_shake:"+detectionResulter.counterActivityShake +StringsUtils.SLASH+ Config.Detection.Result.PartialThresholds.get(subIntervalCounter).minimum_activity_shake;
		s+=", p_walk:"+detectionResulter.counterPostureWalking +StringsUtils.SLASH+ Config.Detection.Result.PartialThresholds.get(subIntervalCounter).minimum_posture_walk;
		s+=", b_m:"+detectionResulter.bigMovementsCounter+StringsUtils.SLASH+Config.Detection.Detecting.NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE;
		return s;
	}
}
