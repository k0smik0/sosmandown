/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm._locator;

import net.iubris.sosmandown.engine.computing_state.fsm.FSMComputedState;
import net.iubris.sosmandown.model.state.phase.PhaseState;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import android.content.Context;

public interface FSMComputingStatesLocator {
//	FSMComputedStatesLocator init(Context context, FSMEventProcessor eventProcessorFSM);
	FSMComputingStatesLocator init(Context context);
	FSMComputedState get(WorkingMajorState majorState);
	FSMComputedState get(PhaseState initRunningState);
	void releaseResources();
//	void onComputedThresholds(ComputedThresholds computedThresholds);
}
