/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting;


import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture.DefaultDoNothingPostureVisitor;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture.PostureVisitor;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType.ActivityTypeVisitor;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType.DefaultDoNothingActivityTypeVisitor;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.savers.detections.DetectionsSaverBucket;
import net.iubris.sosmandown.savers.detections.DetectionsSaverBucket.DetectionResult;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.util.Ln;

@Singleton
public class DetectionsResulter {
	
	private final DetectionsSaverBucket detectionSaverBucket;
	
	int counterActivityRun;
	int counterActivityWalk;
	int counterActivityShake;
	
	private final ActivityTypeVisitor activityTypeVisitorForResult = new DefaultDoNothingActivityTypeVisitor() {
		@Override
		public void visitWalk(ActivityType activityType) {
			counterActivityWalk = activityType.getCounter();
		}
		@Override
		public void visitShake(ActivityType activityType) {
			counterActivityShake = activityType.getCounter();
		}
		@Override
		public void visitRun(ActivityType activityType) {
			counterActivityRun = activityType.getCounter();
		}
	};
	
	int counterPostureWalking;
//	private int counterPostureStanding;
//	private int counterPostureSitting;
	private final PostureVisitor postureVisitorForResult = new DefaultDoNothingPostureVisitor() {
		@Override
		public void visitWalking(Posture posture) {
			counterPostureWalking = posture.getCounter();
		}
	};

	int bigMovementsCounter;

	
	/*private final int minimum_activity_walk__max;
	private final int minimum_activity_run__max;
	private final int minimum_activity_shake__max;
	private final int minimum_posture_walk__max;*/

	@Inject
	public DetectionsResulter(DetectionsSaverBucket detectionSaverBucket) {
		this.detectionSaverBucket = detectionSaverBucket;
		
		/*int partialThresholdsSize =  Config.Detection.Result.Partial.size();
		Map<Integer, Thresholds> partialThresholds = Config.Detection.Result.Partial;
		minimum_activity_walk__max = partialThresholds.get(partialThresholdsSize).minimum_activity_walk;
		minimum_activity_run__max = partialThresholds.get(partialThresholdsSize).minimum_activity_run;
		minimum_activity_shake__max = partialThresholds.get(partialThresholdsSize).minimum_activity_shake;
		minimum_posture_walk__max = partialThresholds.get(partialThresholdsSize).minimum_posture_walk;
		Ln.d(minimum_activity_walk__max+" "+minimum_activity_run__max+" "+minimum_activity_shake__max+" "+minimum_posture_walk__max);*/
	}
	
	public void handleRawMovements(double resultant) {
		if (SensorEventProcessorUtils.isResultantOutOfGreatRangeAround1G(resultant)) {
			bigMovementsCounter++;
//			Ln.d("new big movement! counter:"+bigMovementsCounter+"[resultant:"+resultant+"]");
		}
	}
	public void reset() {
//		Ln.d("resetting...");
		counterActivityRun = 0;
		counterActivityWalk = 0;
		counterActivityShake = 0;
		counterPostureWalking = 0;
		bigMovementsCounter = 0;
	}


	public boolean hasActivityResultForActivityType(ActivityType[] activityTypes, int subIntervalCounter) {
		for (ActivityType activityType : activityTypes) {
			activityType.acceptAndSetCounter(activityTypeVisitorForResult);
		}
/*//		if (counterActivityShake >= Config.Detection.Result.MINIMUM_ACTIVITY_SHAKE ||
//			counterActivityWalk >= Config.Detection.Result.MINIMUM_ACTIVITY_WALK ||
//			counterActivityRun >= Config.Detection.Result.MINIMUM_ACTIVITY_RUN) {
		if (counterActivityShake >= minimum_activity_shake__max || 
			counterActivityWalk >= minimum_activity_walk__max ||
			counterActivityRun >= minimum_activity_run__max) {
			Ln.d("returning true because "+
					"counterActivityRun:"+counterActivityRun+"["+minimum_activity_run__max+"], "+
					"counterActivityWalk:"+counterActivityWalk+"["+minimum_activity_walk__max+"], "+
					"counterActivityShake:"+counterActivityShake+"["+minimum_activity_shake__max+"]");
			return true;
		}*/
		
		boolean toReturn = false;
		
		// use counter for map
		int minimum_activity_shake = Config.Detection.Result.PartialThresholds.get(subIntervalCounter).minimum_activity_shake;
		toReturn = counterActivityShake >= minimum_activity_shake;
//Ln.d("computed "+toReturn+" because counterActivityShake:"+counterActivityShake+"["+minimum_activity_shake+"]");
		if (toReturn) { 
			Ln.d("returning true because counterActivityShake: "+counterActivityShake+StringsUtils.SLASH+minimum_activity_shake); 
			return toReturn; 
		}
		
		int minimum_activity_walk = Config.Detection.Result.PartialThresholds.get(subIntervalCounter).minimum_activity_walk;
		toReturn = counterActivityWalk >= minimum_activity_walk;
//Ln.d("computed "+toReturn+" because counterActivityWalk:"+counterActivityWalk+"["+minimum_activity_walk+"]");
		if (toReturn) { 
			Ln.d("returning true because counterActivityWalk: "+counterActivityWalk+StringsUtils.SLASH+minimum_activity_walk);
			return toReturn; 
		}
		
		int minimum_activity_run = Config.Detection.Result.PartialThresholds.get(subIntervalCounter).minimum_activity_run;
		toReturn = counterActivityRun >= minimum_activity_run;
//Ln.d("computed "+toReturn+" because counterActivityrun:"+counterActivityRun+"["+minimum_activity_run+"]");
		if (toReturn) { 
			Ln.d("returning true because counterActivityRun: "+counterActivityRun+StringsUtils.SLASH+minimum_activity_run);
			return toReturn; 
		}
		
//		Ln.d("returning false");
		return false;
	}
	
	public boolean hasActivityResultForPosture(Posture[] postures, int subIntervalCounter) {
		for (Posture posture : postures) {
			posture.accept(postureVisitorForResult);
		}
		int minimum_posture_walk = Config.Detection.Result.PartialThresholds.get(subIntervalCounter).minimum_posture_walk;
		if (counterPostureWalking > minimum_posture_walk) {
Ln.d("returning true because counterPostureWalking:"+counterPostureWalking+StringsUtils.SLASH+minimum_posture_walk);
			return true;
		}
		
//		Ln.d("returning false because counterPostureWalking:"+counterPostureWalking+"["+minimum_posture_walk+"]");
		return false;
	}

	public boolean hasActivityResultForRawMovements() {
		boolean spatialResult = bigMovementsCounter >= Config.Detection.Detecting.NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE;
//Ln.d("spatial result: "+spatialResult+", bigMovementsCounter:"+bigMovementsCounter);
		return spatialResult;
	}

	/*public boolean getCombinationResult() {
		return false;
	}*/
	public void prepareForProcessing() {
		reset();		
	}
	public void saveDetection(WorkingMajorStateEnum workingMajorStateEnum) {
		long timestamp = System.currentTimeMillis();
		DetectionResult detectionResult = new DetectionResult(workingMajorStateEnum, counterActivityRun, counterActivityWalk, counterActivityShake, counterPostureWalking, bigMovementsCounter, timestamp);
		detectionSaverBucket.add(detectionResult);
	}
}
