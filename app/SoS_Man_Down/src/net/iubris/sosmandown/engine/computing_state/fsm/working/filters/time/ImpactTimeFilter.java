/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.time;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.config.Config.Detection.Impact;
import net.iubris.sosmandown.utils.Printor;
import android.content.Context;

@Singleton
//@ContextSingleton
public class ImpactTimeFilter implements IImpactTimeFilter, IImpactTimeFilterStart, IImpactTimeFilterStop {
	
	private final Calendar calendar = Calendar.getInstance();
	
	private long impactStartTime;
	private long impactStopTime;

	private long interval;
	
	/*@Inject
	public ImpactTimeFilter(Application dummyApplication) {
		Ln.d(this);
	}*/
	@Inject
	public ImpactTimeFilter(Context context) {
		Printor.whoIAm(this,context);
	}
	
	@Override
	public void setPreImpactStartTime(long timestamp) {
		this.impactStartTime = timestamp;
	}
	@Override
	public void setImpactStopTime(long timestamp) {
		this.impactStopTime = timestamp;
	}
	@Override
	public float computeImpactIntervalTime() {
		Date dateStart = new Date(impactStartTime);
		Date dateStop = new Date(impactStopTime);
		calendar.setTime(dateStart);
		int secondStart = calendar.get(Calendar.SECOND);
		calendar.setTime(dateStop);
		int secondCurrent = calendar.get(Calendar.SECOND);
		
		long interval = secondCurrent - secondStart;
		this.interval = interval;
		return interval;
	}
	
	@Override
	public boolean isIntervalLowerThanThreshold() {
		return interval<Impact.THRESHOLD_FILTER_IMPACT_TIME;
	}
}
