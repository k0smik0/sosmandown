/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model;

import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.Counterable;
import net.iubris.sosmandown.ui.fragments.SpectrogramView;
import net.iubris.sosmandown.utils.StringsUtils;


public enum ActivityType implements Counterable {

	RUN {
		@Override
		public int[] getColorForSpectrogram() {
			return SpectrogramView.COLOR_RUN_LEAF;
		}
		@Override
		public void acceptAndSetCounter(ActivityTypeVisitor activityTypeVisitor) {
			activityTypeVisitor.visitRun(this);
		}
	},
	SHAKE {
		@Override
		public int[] getColorForSpectrogram() {
			return SpectrogramView.COLOR_SHAKE_FIRE;
		}
		@Override
		public void acceptAndSetCounter(ActivityTypeVisitor activityTypeVisitor) {
			activityTypeVisitor.visitShake(this);
		}
	},
	WALK {
		@Override
		public int[] getColorForSpectrogram() {
			return SpectrogramView.COLOR_WALK_ICE;
		}
		@Override
		public void acceptAndSetCounter(ActivityTypeVisitor activityTypeVisitor) {
			activityTypeVisitor.visitWalk(this);
		}
	},
	UNKNOWN {
		@Override
		public int[] getColorForSpectrogram() {
			return SpectrogramView.COLOR_UNKNOWN_WOOD;
		}
		@Override
		public void acceptAndSetCounter(ActivityTypeVisitor activityTypeVisitor) {}
	};
	
	protected int counter;

	public ActivityType setCounter(int counter) {
		this.counter = counter;
		return this;
	}
	@Override
	public int getCounter() {
		return counter;
	}
	@Override
	public String getCounterAsString() {
		return StringsUtils.EMPTY+counter;
	}
	@Override
	public void resetCounter() {
		setCounter(0);
	}
	/*public void incrementCounter() {
		counter++;
	}*/
	
	public abstract int[] getColorForSpectrogram();
	
	public abstract void acceptAndSetCounter(ActivityTypeVisitor activityTypeVisitor);
	
	public static interface ActivityTypeVisitor {
		void visit(ActivityType activityType);
		void visitWalk(ActivityType activityType);
		void visitShake(ActivityType activityType);
		void visitRun(ActivityType activityType);
	}
	public abstract static class DefaultDoNothingActivityTypeVisitor implements ActivityTypeVisitor {
		@Override
		public void visit(ActivityType activityType) {}
		@Override
		public void visitWalk(ActivityType activityType) {}
		@Override
		public void visitShake(ActivityType activityType) {}
		@Override
		public void visitRun(ActivityType activityType) {}
	}
}
