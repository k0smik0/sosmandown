/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.init;

import java.util.Vector;

import javax.inject.Inject;

import net.iubris.sosmandown.engine.computing_state.ComputedState;
import net.iubris.sosmandown.engine.computing_state.fsm.AbstractFSMComputingState;
import net.iubris.sosmandown.engine.computing_state.fsm.FSMComputedState;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer.ComputedThresholds;
import net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer.ThresholdsComputer;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils.Thresholds;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.model.result.LightComputedResult;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.phase.PhaseStateE;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorStateEnum;
import net.iubris.sosmandown.subscriptions.lightevents.LightComputedResultPublisher;
import net.iubris.sosmandown.subscriptions.thresholds.ThresholdsPublisher;
import net.iubris.sosmandown.ui.fragments.FragmentChartAccelerationNormWithFSMComputedResult;

import org.greenrobot.eventbus.EventBus;

import roboguice.util.Ln;

public class InitFSMComputingState extends AbstractFSMComputingState implements FSMComputedState, ThresholdsPublisher, LightComputedResultPublisher {
	
	private static final long serialVersionUID = -2219626249014049991L;
	
	// TODO restore
	private static final int EVENT_SENDING_FREQUENCY_DIVISOR = 10; // was 10 // was 5
	private static final double MAX_SPURIOUS_GRAVITY = 10;
	private static final double MIN_SPURIOUS_GRAVITY = 9;
	
	private final static int AVERAGE_SAMPLE_NUM = 100;
	private final Vector<Double> gravityAverageSamples = new Vector<>(AVERAGE_SAMPLE_NUM);

	private final Thresholds thresholds = Thresholds.INSTANCE;
	private final ThresholdsComputer thresholdsComputer;
	
	private long eventdataPreviousTimestamp = 0;
	
	private int initializingStateStep;
	
	private double gravityResult;

//	private final ActivityTypeRecognizerByWaveletTransform activityTypeRecognizerByWaveletTransform;
	
	@Inject
	public InitFSMComputingState(FSMComputedStateProcessor eventProcessorFSM, FSMComputingStatesLocator fsmComputedStatesLocator, ThresholdsComputer thresholdsComputer/*, ActivityTypeRecognizerByWaveletTransform activityTypeRecognizerByWaveletTransform*/ ) {
		super(eventProcessorFSM, fsmComputedStatesLocator, WorkingMajorStateEnum.INIT, WorkingMinorStateEnum.ANY, PhaseStateE.INITIALIZING);
		this.thresholdsComputer = thresholdsComputer;
//		this.activityTypeRecognizerByWaveletTransform = activityTypeRecognizerByWaveletTransform;
	}
	
	@Override
	public void handle(SensorEventDataHolder sensorEventDataHolder) {
		if (!isInitPhase(sensorEventDataHolder.resultant)) {
			ComputedState eventStateFromRelax = fsmComputingStatesLocator.get(WorkingMajorStateEnum.RELAX);
//			eventProcessorFSM.setCombinedState( combinedState );
			super.changeStateTo(eventStateFromRelax);
		}
	}
	
	protected boolean isInitPhase(double accelerationResultant) {
		// TODO move init in service/activity, and store averaged gravity on sharedpreferences
		if (!isConfigured(accelerationResultant)) {
			PhaseStateE psI = PhaseStateE.INITIALIZING;
//			Ln.d(initializingStateStep);
			psI.setStep(initializingStateStep);
			super.runningState = psI;
			
//			Toast.makeText(context, "gravity average: "+G, Toast.LENGTH_SHORT).show();
			return true;
		}
		
		// if here, initState = INITIALIZED
		if (isGravityHasToBeComputed()) {
			super.runningState = PhaseStateE.INITIALIZED;
//Ln.d(SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(thresholds.getGravity())+"|"+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(gravityResult)+"|"+super.runningState+"|"+majorState);
			return true;
		}
		
		// if here false, state = STARTED, resultPrevious = Gravity (as default)
		/*if (isNotStarted()) {
//			majorStatePrevious = Init.STARTED;
//			majorStatePrevious = State.MajorE.INIT;
			
//			initState = State.InitStateE.STARTED;
			Ln.d(EventProcessorUtils.GRAVITY+"|"+result+"|"+majorState);
			return true;
		}*/
//		isNotStarted();
		
		// just for initializing - at start, we are in relax
//		if (majorStatePrevious.equals(State.MajorE.STARTED)) {
//		if (initState.equals(InitStateE.STARTED)) {
		if (isNotStarted()) {
//			majorState = State.MajorStateE.RELAX;
			super.runningState = PhaseStateE.STARTING;
			
			Ln.d("Gravity:"+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(thresholds.getGravity())+", "+runningState+":"+majorState+". Houston, we are starting!!!");
			
			// notify observer for bottom bar in MainActivity
			LightComputedResult lightComputedResult = new LightComputedResult(runningState, majorState, getGravityResult());
			publish(lightComputedResult);
			return true;
		}
		
		super.runningState = PhaseStateE.STARTED;		
		return false;
	}
	private boolean isConfigured(double accelerationResultant) {
		if (thresholds.getGravity()!=0) {
			return true;
		}
		
		int size = gravityAverageSamples.size();
//		Ln.d("gravityAverageSamples size (ext): "+size);
		if (size>AVERAGE_SAMPLE_NUM) {
			return true;
		}
		
		// TODO use 1 or 2 - we want calculate average 
		// * when phone is in horizontal position: use "event.values[AXIS_FOR_CONFIGURATION]"
		// * generic: use computeResult(event.x, ecc)
//		calculateGravityAverageAndAdd( event.values[AXIS_FOR_CONFIGURATION] );
		calculateGravityAverageAndAdd(accelerationResultant);
		
		if (size%EVENT_SENDING_FREQUENCY_DIVISOR==0) {
			initializingStateStep = size*100/AVERAGE_SAMPLE_NUM;
			
			// notify observer for bottom bar in MainActivity
			LightComputedResult lightComputedResult = new LightComputedResult(runningState, majorState, getGravityResult());
			publish(lightComputedResult);
		}		
		return false;
	}
	private void calculateGravityAverageAndAdd(double accelerationResultant) {
		if (accelerationResultant>MIN_SPURIOUS_GRAVITY && accelerationResultant<MAX_SPURIOUS_GRAVITY) {
			gravityAverageSamples.add(accelerationResultant);
			gravityResult = accelerationResultant;
		}
	}
	
	
	private boolean isGravityHasToBeComputed() {
		if (thresholds.getGravity()==0) {
			/*if (Config.DEV){
				// always compute in dev mode
				ComputedThresholds computeThresholds = computeThresholds();
				return true;
			} else {
				
			}*/
			ComputedThresholds computedThresholds = thresholdsComputer.compute(gravityAverageSamples);
			
//			Ln.d("computed gravity threshold: "+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(computedThresholds.getGravity()));
//			Ln.d("computed freefall threshold: "+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(computedThresholds.getFreeFall()));
//			Ln.d("computed impact threshold: "+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(computedThresholds.getImpact()));
			
			thresholds.setGravity(computedThresholds.getGravity());
			thresholds.setFreeFall(computedThresholds.getFreeFall());
			thresholds.setImpact(computedThresholds.getImpact());
			
			onComputedThresholds(computedThresholds);
			return true;
		}
		return false;
	}
	/*private ComputedThresholds computeThresholds() {
		double g = 0;
		for (Double f : gravityAverageSamples) {
			g+=f;
		}
		g = 1.0d*g/gravityAverageSamples.size();
		// we have averaged gravity !

		// just for debug
		Ln.d("gravity: "+g);

		
//		 comunica le soglia di FREE_FALL_THRESHOLD e IMPACT_THRESHOLD e GRAVITY
//		 attraverso un broadcast al {@link ChartFragment} ChartFragment, che
//		 ascolta con un broadcastreceiver oneshot: aggiorna il valore
//		 nel grafico e poi deregistra il receiver
		 
		 
		double gravityThreshold = Double.valueOf(formattedGravity); g;
		thresholds.setGravity(gravityThreshold);
		double impactResultThreshold = EventProcessorConstants.GRAVITY_MULTIPLIER_FOR_IMPACT_THRESHOLD*gravityThreshold;
		thresholds.setImpactResult(impactResultThreshold);
		double freeFallThreshold = gravityThreshold-EventProcessorConstants.FREE_FALL_SUBSTRACTER;
		thresholds.setFreeFall(freeFallThreshold);
Ln.d("G: "+gravityThreshold+", IMPACT_THRESHOLD: "+impactResultThreshold+", FREE_FALL_THRESHOLD:"+freeFallThreshold);

//		String thresholds = EventProcessorUtils.FREE_FALL_THRESHOLD+"#"+EventProcessorUtils.IMPACT_RESULT_THRESHOLD;
		ComputedThresholds computedThresholds = new ComputedThresholds(gravityThreshold, freeFallThreshold, impactResultThreshold);
//		sendBroadcastForThreshold(thresholds);
		onComputedThresholds(computedThresholds);
		
		return computedThresholds;
//		Toast.makeText(context, majorState.name(), Toast.LENGTH_SHORT).show();		
	}*/
	
	private boolean isNotStarted() {
//		Ln.d("checking 'isNotStarted'");
		if (eventdataPreviousTimestamp == 0) {	
			eventdataPreviousTimestamp = System.currentTimeMillis()/* event.timestamp*/;		
			gravityResult = thresholds.getGravity();			
//Toast.makeText(context, majorState.name()+" gravity average: "+EventProcessorUtils.GRAVITY, Toast.LENGTH_SHORT).show();
//Ln.d(majorState.name()+", averaged gravity: "+SensorEventProcessorUtils.AccelerationDecimalFormat.format(gravityResult));			
			return true;
		}
		return false;
	}
	
	/*
	 * observer zone - start
	 */
	/*private final SyncObservableDelegate<ConfigurationCombinedStateObserver, ConfigurationCombinedStateNotificationAction> observableDelegate = new SyncObservableDelegate<>();  

	@Override
	public void attachObserver(ConfigurationCombinedStateObserver observer) {
		observableDelegate.attachObserver(observer);
	}
	@Override
	public void detachObserver(ConfigurationCombinedStateObserver observer) {
		observableDelegate.detachObserver(observer);
	}
	@Override
	public void notifyObserver(ConfigurationCombinedStateNotificationAction notificationAction) {
		observableDelegate.notifyObserver(notificationAction);
	}
	public class ConfigurationCombinedStateNotificationAction implements INotificationAction<ConfigurationCombinedStateObserver> {
		private final ConfigurationCombinedState configurationCombinedState;

		public ConfigurationCombinedStateNotificationAction(ConfigurationCombinedState configurationCombinedState) {
			this.configurationCombinedState = configurationCombinedState;
		}
		@Override
		public void execute(ConfigurationCombinedStateObserver observer) {
			observer.onNewConfigurationCombinedState(configurationCombinedState);
		}		
	}*/
	/*
	 * observer zone - end
	 */
	
	private double getGravityResult() {
		return gravityResult;
	}
	
	@Override
	public void publish(LightComputedResult lightComputedResult) {
//		Ln.d("pre-publish new lightComputedResult: " + lightComputedResult);
		computedStateProcessorFSM.publish(lightComputedResult);
//		Ln.d("published new lightComputedResult: " + lightComputedResult);
	}
	
	
//	private void sendBroadcastForThreshold(String thresholds) {
//	Intent intent = new Intent(SensorsService.ACTION);
//	intent.putExtra(EventProcessorConstants.INTENT_EXTRA_THRESHOLDS, thresholds);
//	context.sendBroadcast( intent );
//}
	
	/**
	 * Comunica le soglia di FREE_FALL_THRESHOLD e IMPACT_THRESHOLD e GRAVITY
	 * attraverso un EventBus al {@link FragmentChartAccelerationNormWithFSMComputedResult} ChartFragment, che
	 * ascolta con un receiver oneshot: aggiorna il valore
	 * nel grafico e poi deregistra il receiver<br/>
	 * 
	 * Inoltre, imposta quei valori di soglia negli altri ComputedState
	 */
	@Override
	public void onComputedThresholds(ComputedThresholds computedThresholds) {
		
		final EventBus eventBus = EventBus.getDefault();
//		Ln.d("eventBus:"+eventBus.hashCode());
		eventBus.post(computedThresholds);
		
//		fsmComputedStatesLocator.onComputedThresholds(computedThresholds);
//		activityTypeRecognizerByWaveletTransform.setGravity( computedThresholds.getGravity() );
	}
}
