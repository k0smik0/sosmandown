/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.delegates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.ActivityTypeRecognizerByWaveletTransform;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.WaveletProcessedData;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.DelayWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.internal.window.PeakWindow;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.utils.WaveletConstants;

public abstract class AbstractAnalyzerDelegate implements AnalyzerDelegate {
	
	protected static final boolean collectForDebug = WaveletConstants.COLLECT_FOR_DEBUG;

	private static final int RESET_EVENT_NUM = 30;
	private static final double RESET_EVENT_VALUE = 0;
	
	protected final DelayWindow delayWindow; 
	protected final PeakWindow delayedPeakWindow;
	
	private final ActivityType activityTypeSelf;
//	protected ActivityType activityTypeCurrent = ActivityType.UNKNOWN;
	
	protected long peakIndex = -1L;
	protected boolean currentlyActing = false;
	protected int actingCounter = 1;

	private ActivityTypeRecognizerByWaveletTransform activityTypeRecognizerByWaveletTransform;
	
	public AbstractAnalyzerDelegate(DelayWindow delayWindow, PeakWindow delayedPeakWindow, ActivityType activityTypeSelf) {
		this.delayWindow = delayWindow;
		this.delayedPeakWindow = delayedPeakWindow;
		this.activityTypeSelf = activityTypeSelf;
	}
	
	@Override
	public void init() {
		sendResetValues();
		
		delayWindow.reset();
		delayedPeakWindow.reset();
		
		peakIndex = -1L;
		currentlyActing = false;
		actingCounter = 1;
	}
	protected void sendResetValues() {
//		activityTypeCurrent = ActivityType.UNKNOWN;
		activityTypeRecognizerByWaveletTransform.setActivityTypeCurrent(ActivityType.UNKNOWN);
		double[] zerosArray = new double[10];
		Arrays.fill(zerosArray, RESET_EVENT_VALUE);
		for (int i=0;i<RESET_EVENT_NUM;i++) {
			eventuallyPublishForChart(zerosArray);
		}
	}
	@Override
	public abstract void process(long sampleCounter, double... wValues);
	
	protected double[] processFacade(long sampleCounter, double wOut, double... wValues) {
		boolean nowActing = computeIfIsNowActing(wValues);
		doIfActingDiff(nowActing);
		
		double delayedout = prepareDelayedPeakWindow(wOut);
		double cw = peakDetectAndDo(sampleCounter);
		double[] processedValues = new double[]{delayedout,cw};
		return processedValues;
	}
	
	protected abstract boolean computeIfIsNowActing(double... wValues);
	
	protected void doIfActingDiff(boolean nowActing) {
//		Ln.d("currentlyActing:"+currentlyActing+", nowActing:"+nowActing);
		
		if (currentlyActing != nowActing) {
			currentlyActing = nowActing;
			if (currentlyActing) {
				actingCounter = 1;
				ActivityType activityTypeCurrent = activityTypeSelf;
				activityTypeCurrent.setCounter(actingCounter);
				activityTypeRecognizerByWaveletTransform.setActivityTypeCurrent(activityTypeCurrent);				
				
				eventuallySubAct();
				onNewActivityType();
//				Ln.d("doIfActingDiff:: "+currentlyActing+":currentlyActing"+", nowActing:"+nowActing+", activityTypeCurrent:"+activityTypeCurrent.name()+":"+activityTypeCurrent.getCounter());
			} else {
				ActivityType activityTypeCurrent = ActivityType.UNKNOWN;
				activityTypeRecognizerByWaveletTransform.setActivityTypeCurrent(activityTypeCurrent);
//				Ln.d("doIfActingDiff:: "+currentlyActing+":currentlyActing"+", nowActing:"+nowActing+", activityTypeCurrent:"+activityTypeCurrent.name()+":"+activityTypeCurrent.getCounter());
			}
		} else {
			if (currentlyActing) {
				ActivityType activityTypeCurrent = activityTypeSelf;
				activityTypeRecognizerByWaveletTransform.setActivityTypeCurrent(activityTypeCurrent);
				onContinuedActivityType();
//				Ln.d("doIfActingDiff:: "+currentlyActing+":currentlyActing"+", nowActing:"+nowActing+", activityTypeCurrent:"+activityTypeCurrent.name()+":"+activityTypeCurrent.getCounter());
			}
			// no more
			/*else {
				activityTypeCurrent = ActivityType.UNKNOWN;
//				Ln.d("doIfActingDiff:: "+currentlyActing+":currentlyActing"+", nowActing:"+nowActing+", activityTypeCurrent:"+activityTypeCurrent.name()+":"+activityTypeCurrent.getCounter());
			}*/
		}
	}
	
	protected void eventuallySubAct() {}

	protected double prepareDelayedPeakWindow(double wOut) {
		double delayedout = delayWindow.placeValue(wOut);
		delayedPeakWindow.placeValue(delayedout);
		return delayedout;
	}
	
	protected double peakDetectAndDo(long sampleCounter) {
		double cw = 0.0;
		if (currentlyActing) {
			long peakOffset = peakDetect(delayedPeakWindow, sampleCounter);
			if (peakOffset >= 0L) {
				if (peakOffset != peakIndex) {
					// New peak detected, step the step counter
					peakIndex = peakOffset;
					doOnPeakDetect();
				}
			}	
			cw = 1.0;
		}
		return cw;
	}
	protected void doOnPeakDetect() {
		actingCounter++;
		ActivityType activityTypeCurrent = activityTypeSelf;
		activityTypeCurrent.setCounter(actingCounter);
		activityTypeRecognizerByWaveletTransform.setActivityTypeCurrent(activityTypeCurrent);
		/*if (activityTypeCurrent.equals(ActivityType.SHAKE) || activityTypeCurrent.equals(ActivityType.WALK)) {
			Ln.d(this.getClass().getSimpleName()+", sending peak for "+activityTypeCurrent.name()+":"+activityTypeCurrent.getCounter());
//Ln.d("doOnPeakDetect: activityTypeCurrent:"+activityTypeCurrent.name()+":"+activityTypeCurrent.getCounter());
		}*/
		onContinuedActivityType();
	}
	

	protected static long peakDetect(PeakWindow peakWindow, long sampleCounter) {
		int peakOffset = 0;
		boolean peakSet = false;
		double peakValue = 0.0;
		int bufferIndex = peakWindow.getWindowIndex();
		for (int i = -1; i >= -(peakWindow.getWindowLength()); --i) {
			bufferIndex = peakWindow.adjustIndex(--bufferIndex);
			if (!peakSet || (peakWindow.getValue(bufferIndex) > peakValue)) {
				peakValue = peakWindow.getValue(bufferIndex);
				peakSet = true;
				peakOffset = i;
			}
		}
		// don't accept peaks near to the beginning and end of buffer
		if ((peakOffset <= -(peakWindow.getWindowLength())) || (peakOffset >= -1)) {
			return -1L; // no peak detected
		}
		return sampleCounter + /*(long)*/ peakOffset;
	}
	
	protected static List<Double> buildNthList(double[] args) {
		List<Double> arrayList = new ArrayList<>();
		for (Double a: args) {
			arrayList.add(a);
		}
		return arrayList;
	}
	
	protected void onNewActivityType() {
		updateClientOnNewActivity(activityTypeRecognizerByWaveletTransform.getActivityTypeCurrent());
//		Ln.d("onNewActivityType: updated client with: "+activityTypeCurrent.name());
	}
	protected void onContinuedActivityType() {
		updateClientOnContinuedActivity(activityTypeRecognizerByWaveletTransform.getActivityTypeCurrent());
//		Ln.d("onContinuedActivity: updated client with: "+activityTypeCurrent.name()+","+activityTypeCurrent.getCounter());
	}
	
	protected void eventuallyPublishForChart(/*ActivityType activityType,*/ double[] dataArray) {
		/*if (!activityTypeCurrent.equals(ActivityType.NONE)) {
//			Ln.d("updating chart using:"+activityTypeCurrent.name()+" and values: "
			String s=StringsUtils.EMPTY;
			for (double d : dataArray) {
				s+=","+d;
			}
			s = s.replaceFirst(StringsUtils.COMMA, StringsUtils.EMPTY);
			if (activityTypeCurrent.equals(ActivityType.SHAKE) || activityTypeCurrent.equals(ActivityType.WALK)) {
				Ln.d(this.getClass().getSimpleName()+", sending "+activityTypeCurrent.name()+":"+activityTypeCurrent.getCounter()+" with values:"+s);
			}
		}*/
		List<Double> data = waveletProcessedDataHandler.handle(dataArray);
		WaveletProcessedData waveletProcessedData = new WaveletProcessedData(activityTypeRecognizerByWaveletTransform.getActivityTypeCurrent(), data);
		updateClientForChart(waveletProcessedData);
	}
	private void updateClientForChart(WaveletProcessedData waveletProcessedData) {
		activityTypeRecognizerByWaveletTransform.onNewProcessedData(waveletProcessedData);
	}

	protected void updateClientOnNewActivity(ActivityType activityType) {
		activityTypeRecognizerByWaveletTransform.onNewDetection(activityType);
	}
	protected void updateClientOnContinuedActivity(ActivityType activityType) {
		activityTypeRecognizerByWaveletTransform.onContinuedDetection(activityType);
	}
	
	@Override
	public void setDataClient(ActivityTypeRecognizerByWaveletTransform activityTypeRecognizerByWaveletTransform) {
		this.activityTypeRecognizerByWaveletTransform = activityTypeRecognizerByWaveletTransform;
	}
	
	
	@Override
	public void enableSendDataForChart() {
		this.waveletProcessedDataHandler = waveletProcessedDataHandlerReal;
	}
	@Override
	public void disableSendDataForChart() {
		this.waveletProcessedDataHandler = waveletProcessedDataHandlerDummy;
	}
	private final WaveletProcessedDataHandler waveletProcessedDataHandlerReal = new WaveletProcessedDataHandler() {
		@Override
		public List<Double> handle(double[] data) {
			List<Double> list = buildNthList(data);
			return list;
		}
	};
	private final WaveletProcessedDataHandler waveletProcessedDataHandlerDummy = new WaveletProcessedDataHandler() {
		@Override
		public List<Double> handle(double[] data) { return Collections.emptyList(); }
	};
	private WaveletProcessedDataHandler waveletProcessedDataHandler = waveletProcessedDataHandlerDummy;

	protected interface WaveletProcessedDataHandler {
		List<Double> handle(double[] data);
	}
}
