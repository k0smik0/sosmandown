/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states;

import net.iubris.sosmandown.engine.computing_state.fsm.AbstractFSMComputingState;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.model.state.phase.PhaseStateE;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorState;

public abstract class AbstractWorkingFSMComputedState extends AbstractFSMComputingState implements WorkingFSMComputingState {

	private static final long serialVersionUID = 8580405531113760223L;
	
	public AbstractWorkingFSMComputedState(FSMComputedStateProcessor eventProcessorFSM, FSMComputingStatesLocator fsmComputedStatesLocator, WorkingMajorState majorState, WorkingMinorState minorState) {
		super(eventProcessorFSM, fsmComputedStatesLocator, majorState, minorState, PhaseStateE.STARTED);
	}
}
