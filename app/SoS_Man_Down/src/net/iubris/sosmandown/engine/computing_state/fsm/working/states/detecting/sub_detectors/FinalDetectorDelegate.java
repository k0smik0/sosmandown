/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.sub_detectors;

import java.util.HashMap;
import java.util.Map;

import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import roboguice.util.Ln;

public abstract class FinalDetectorDelegate extends AbstractDetectorDelegate {
	
	protected static interface WorkingStateAction {
		void act(double resultant);
	}
	
	protected final Map<WorkingMajorStateEnum,WorkingStateAction> workingMajorStatesMap = new HashMap<>();
	
	protected abstract void populateMap();

	protected WorkingMajorStateEnum nextMajorState;

	public FinalDetectorDelegate(DetectorDelegateClient detectorDelegateClient) {
		super(detectorDelegateClient);
		populateMap();
	}

	@Override
	public void act(long now, SensorEventDataHolder sensorEventDataHolder) {
		workingMajorStatesMap.get(nextMajorState).act(sensorEventDataHolder.resultant);
	}
	

	/**
	 * never evaluated, so returns false
	 */
	@Override
	protected boolean hasToRemainHere(long now) {
		return false;
	}
	@Override
	protected void doSomeStuff(SensorEventDataHolder sensorEventDataHolder) {}
	@Override
	protected void changeDetectorOnClient() {}
	
	public void setNexmajorState(WorkingMajorStateEnum nextWorkingMajorState) {
		this.nextMajorState = nextWorkingMajorState;
	}
	
	protected static void logActivityTypeBeforeDeclaringFall(ActivityType[] results) {
		String toPrint = "post WaveletProcessing - ActivityTypeS: ";
		for (ActivityType activityType : results) {
			toPrint+=","+activityType.name()+":"+activityType.getCounter();
		}
		toPrint = toPrint.replaceFirst(",", "");
		Ln.d(toPrint);
	}
}
