/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states;


import javax.inject.Inject;

import net.iubris.sosmandown.engine.computing_state.ComputedState;
import net.iubris.sosmandown.engine.computing_state.fsm.FSMComputedState;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorStateEnum;
import roboguice.util.Ln;

import static net.iubris.sosmandown.config.Config.Detection.GoingToImpact.MAX_EVENT_COMPUTED;

public class GoingToImpactWorkingFSMComputingState extends AbstractWorkingFSMComputedState implements WorkingFSMComputingState {

	private static final long serialVersionUID = 5521225514767063201L;
	
	private int eventsComputedInGoingToImpact;
	
//	private double accelerationResultantPrevious;
//	
//	private int goingToImpactPhase1DescentCounter;
//	private int goingToImpactPhase2TransientCounter;
//	private int goingToImpactPhase3RaisingCounter;

	@Inject
	public GoingToImpactWorkingFSMComputingState(FSMComputedStateProcessor eventProcessorFSM, FSMComputingStatesLocator fsmComputedStatesLocator) {
		super(eventProcessorFSM, fsmComputedStatesLocator, WorkingMajorStateEnum.GOING_TO_IMPACT, WorkingMinorStateEnum.RAISING_PHASE_0);
//		this.accelerationResultantPrevious = accelerationResultant;
		eventsComputedInGoingToImpact = 0;
	}
	/*public void setAccelerationResultantPrevious(double accelerationResultantPrevious) {
//		this.accelerationResultantPrevious = accelerationResultantPrevious;
	}*/

	@Override
	public void handle(SensorEventDataHolder sensorEventDataHolder) {
		double impact = SensorEventProcessorUtils.Thresholds.INSTANCE.getImpact();
//		Ln.d("resultant:"+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT,sensorEventDataHolder.resultant)+"<?impact:"+String.format(SensorEventProcessorUtils.DECIMAL_STRING_FORMAT,impact));
		eventsComputedInGoingToImpact++;
//		waveletTransformAnalyzer.processSample(sensorEventDataHolder);
		if (sensorEventDataHolder.resultant < impact) {
//			Ln.d("resultant<IMPACT_THRESHOLD:"+sensorEventDataHolder.resultant);
			// too much time in GOING_TO_IMPACT but no IMPACT reached, so reset to RELAX
			if (eventsComputedInGoingToImpact>=MAX_EVENT_COMPUTED) {
				Ln.d("eventsComputedInGoingToImpact>"+MAX_EVENT_COMPUTED+", so going from IMPACT to RELAX");
				reset();
				ComputedState eventState = fsmComputingStatesLocator.get(WorkingMajorStateEnum.RELAX);
//				eventProcessorFSM.stopCollectingDataForRelaxToImpactTimeAnalysis();
				changeStateTo(eventState);
			}		
			// majorStatePrevious do not change, still in GOING_TO_IMPACT
			/*else {
				setMinorState( calculateMinorStateGoingToImpact(sensorEventDataHolder.resultant) ); // really useful?
//				eventuallyPrintComputedEvents();
			}*/
		}
		// probably IMPACT, since accelerationResultant >= THRESHOLD_IMPACT
		else {
			Ln.d("resultant:"
					+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(sensorEventDataHolder.resultant)
					+">="
					+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(SensorEventProcessorUtils.Thresholds.INSTANCE.getImpact())
					+", changing to IMPACT"
				);
			// state change
			FSMComputedState fsmEventState = fsmComputingStatesLocator.get(WorkingMajorStateEnum.IMPACT);
//			Ln.d("changing to: "+fsmEventState.getMajor()+" because resultant:"+sensorEventDataHolder.resultant);
//			startTransientTimer(); // ?
//			eventProcessorFSM.eventuallyCollectingData(sensorEventDataHolder);
			changeStateTo(fsmEventState);
		}		
	}

	@Override
	public void reset() {
		eventsComputedInGoingToImpact = 0;
//		goingToImpactPhase1DescentCounter = 0;
//		goingToImpactPhase2TransientCounter = 0;
//		goingToImpactPhase3RaisingCounter = 0;
	}
	
	/*private WorkingMinorStateEnum calculateMinorStateGoingToImpact(double accelerationResultant) {
		WorkingMinorStateEnum minorStateCurrent = WorkingMinorStateEnum._NULL;
		// these 3 checks below are just for chart ?
		if (accelerationResultant<accelerationResultantPrevious ) {
			// we are still in free fall
			minorStateCurrent = WorkingMinorStateEnum.RAISING_PHASE_1_DESCENT;
			goingToImpactPhase1DescentCounter++;
		} else if (accelerationResultant == accelerationResultantPrevious ) {
			minorStateCurrent = WorkingMinorStateEnum.RAISING_PHASE_2_TRANSIENT;
			goingToImpactPhase2TransientCounter++;
		} else if (accelerationResultant > accelerationResultantPrevious ) {
			minorStateCurrent = WorkingMinorStateEnum.RAISING_PHASE_3_RAISING;
			goingToImpactPhase3RaisingCounter++;
		}
		accelerationResultantPrevious = accelerationResultant;
		return minorStateCurrent;
	}*/
	
	/*private void eventuallyPrintComputedEvents() {
		if (eventsComputedInGoingToImpact%9==0) {
			Ln.d("eventsComputedInGoingToImpact: "+eventsComputedInGoingToImpact);
		}
	}*/
}
