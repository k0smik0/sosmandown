/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states;

import javax.inject.Inject;

import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorStateEnum;

public class FreeFallWorkingFSMComputingState extends AbstractWorkingFSMComputedState implements WorkingFSMComputingState {
	
	private static final long serialVersionUID = -5981025786701615040L;
	
	private double resultantMinimum = 0;

	// used for minor state
	private double accelerationResultantFromPreviousState;

//	private WaveletTransformAnalyzer waveletTransformAnalyzer;

//	private double xFromPreviousEvent;

//	private SensorEventDataHolder sensorEventDataHolderFromPreviousEvent;
//	private int freeFallPhase1DescentCounter;
//	private int freeFallPhase2TransientCounter;
//	private int freeFallPhase3RaisingCounter;

	@Inject
	public FreeFallWorkingFSMComputingState(FSMComputedStateProcessor eventProcessorFSM, FSMComputingStatesLocator fsmComputedStatesLocator) {
		super(eventProcessorFSM, fsmComputedStatesLocator, WorkingMajorStateEnum.FREE_FALL, WorkingMinorStateEnum.FREE_FALL_PHASE_0);
//		this.accelerationResultantPrevious = accelerationResultant;
//		this.waveletTransformAnalyzer = waveletTransformAnalyzer;
	}
	public void setAccelerationResultantFromPreviousState(double accelerationResultantFromPreviousState) {
		this.accelerationResultantFromPreviousState = accelerationResultantFromPreviousState;
	}
	
	/*public void setRawEventDataFromPreviousEvent(SensorEventDataHolder sensorEventDataHolderFromPreviousEvent) {
		this.sensorEventDataHolderFromPreviousEvent = sensorEventDataHolderFromPreviousEvent;		
	}*/
	
	

	@Override
	public void handle(SensorEventDataHolder sensorEventDataHolder) {
//		Ln.d("FreeFallWorkingCombinedState.handle");
//		waveletTransformAnalyzer.processSample(sensorEventDataHolder);
		if (sensorEventDataHolder.resultant >= SensorEventProcessorUtils.Thresholds.INSTANCE.getFreeFall()) {
			reset();
			// going_to_impact
//			eventProcessorFSM.setCombinedState( new GoingToImpactWorkingCombinedState(eventProcessorFSM, accelerationResultant));
			GoingToImpactWorkingFSMComputingState eventStateGoingToImpact = (GoingToImpactWorkingFSMComputingState) fsmComputingStatesLocator.get(WorkingMajorStateEnum.GOING_TO_IMPACT);
//			eventStateGoingToImpact.setAccelerationResultantPrevious(sensorEventDataHolder.resultant);
			
//			eventProcessorFSM.startCollectingDataForRelaxToImpactTimeAnalysis();
//			eventProcessorFSM.eventuallyCollectingData(sensorEventDataHolder);
			
//			eventProcessorFSM.setCombinedState( combinedState );
//			Ln.d("changing to: "+eventState.getMajor());
			super.changeStateTo(eventStateGoingToImpact);
		} /*else {
			MinorStateE calculatedMinorStateInFreeFall = calculateMinorStateInFreeFall(accelerationResultant);
			minorState = calculatedMinorStateInFreeFall;
		}*/
	}

	@Override
	public void reset() {
//		freeFallPhase1DescentCounter = 0;
//		freeFallPhase2TransientCounter = 0;
//		freeFallPhase3RaisingCounter = 0;
	}
	
	/*private MinorStateE calculateMinorStateInFreeFall(double accelerationResultant) {
		MinorStateE minorStateCurrent = MinorStateE._NULL;
		if (accelerationResultant<accelerationResultantPrevious) {
			// we are still in free fall
			minorStateCurrent = MinorStateE.FREE_FALL_PHASE_1_DESCENT;
			freeFallPhase1DescentCounter++;
		} else if (accelerationResultant == accelerationResultantPrevious) {
			minorStateCurrent = MinorStateE.FREE_FALL_PHASE_2_TRANSIENT;
			freeFallPhase2TransientCounter++;
		} else if (accelerationResultant > accelerationResultantPrevious) {
			minorStateCurrent = MinorStateE.FREE_FALL_PHASE_3_RAISING;
			freeFallPhase3RaisingCounter++;
		}
		accelerationResultantPrevious = accelerationResultant;
		return minorStateCurrent;
	}*/

}
