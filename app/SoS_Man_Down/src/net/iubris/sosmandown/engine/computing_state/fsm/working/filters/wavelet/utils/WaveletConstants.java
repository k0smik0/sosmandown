/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.utils;

public class WaveletConstants {
	
	public static final boolean COLLECT_FOR_DEBUG = true;
	
	public static final int W3_POWER_AVERAGING_WINDOW_LENGTH = 20;
	public static final int W4_POWER_AVERAGING_WINDOW_LENGTH = 20;
	public static final int W5_POWER_AVERAGING_WINDOW_LENGTH = 5;
	
	public static final int W3_PEAK_WINDOW = 10;
	public static final int W4_PEAK_WINDOW = 10;
	public static final int W5_PEAK_WINDOW = 5;
	
	public static final int W3_DELAY_WINDOW = 10;
	public static final int DELAYED_W3_PEAK_WINDOW = 10;
	
	public static final int W4_DELAY_WINDOW = 10;
	public static final int DELAYED_W4_PEAK_WINDOW = 5;
	
	public static final int W5_DELAY_WINDOW = 5;
	public static final int DELAYED_W5_PEAK_WINDOW = 3;
	
	public static final int W5_PW_DELAY_WINDOW = 10;
	public static final int W5_PW_DELAY_LEVEL_KEEPER = 60;

	public static final double W3[] = { 
		-0.000489182807048, // 1
		-0.000858652010111, // 2
		-0.00113583821754, // 3
		-0.00106498662267, // 4
		-0.000358029941802, // 5
		0.00116294815188, // 6
		0.00338024348745, // 7
		0.00573592771904, // 8
		0.00724126323981, // 9
		0.00672663269903, // 10
		0.00332167463362, // 11
		-0.00298492586564, // 12
		-0.0109709108507, // 13
		-0.0182613771714, // 14
		-0.0219854832897, // 15
		-0.0198603493183, // 16
		-0.0112878078472, // 17
		0.00205783949989, // 18
		0.0165239993424, // 19
		0.0276563694057, // 20
		0.0318309886184, // 21
		0.0276563694057, // 22
		0.0165239993424, // 23
		0.00205783949989, // 24
		-0.0112878078472, // 25
		-0.0198603493183, // 26
		-0.0219854832897, // 27
		-0.0182613771714, // 28
		-0.0109709108507, // 29
		-0.00298492586564, // 30
		0.00332167463362, // 31
		0.00672663269903, // 32
		0.00724126323981, // 33
		0.00573592771904, // 34
		0.00338024348745, // 35
		0.00116294815188, // 36
		-0.000358029941802, // 37
		-0.00106498662267, // 38
		-0.00113583821754, // 39
		-0.000858652010111 // 40
	};
	public static final double W4[] = { 
		-0.000978365614096, // 1
		-0.00227167643509, // 2
		-0.000716059883604, // 3
		0.0067604869749, // 4
		0.0144825264796, // 5
		0.00664334926724, // 6
		-0.0219418217014, // 7
		-0.0439709665794, // 8
		-0.0225756156944, // 9
		0.0330479986849, // 10
		0.0636619772368, // 11
		0.0330479986849, // 12
		-0.0225756156944, // 13
		-0.0439709665794, // 14
		-0.0219418217014, // 15
		0.00664334926724, // 16
		0.0144825264796, // 17
		0.0067604869749, // 18
		-0.000716059883604, // 19
		-0.00227167643509, // 20
		-0.000978365614096 // 21
	};
	public static final double W5[] = { 
		-0.00195673122819, // 1
		-0.00143211976721, // 2
		0.0289650529593, // 3
		-0.0438836434028, // 4
		-0.0451512313887, // 5
		0.127323954474, // 6
		-0.0451512313887, // 7
		-0.0438836434028, // 8
		0.0289650529593, // 9
		-0.00143211976721 // 10
	};
}
