/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.actions;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.sosmandown.application.SOSManDownApplication;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.ui.activities.chartdetails.ChartsActivity;
import net.iubris.sosmandown.ui.activities.main.IntentActionDelegate;
import net.iubris.sosmandown.ui.activities.main.MainActivity;
import net.iubris.sosmandown.ui.fragments.FragmentCheckboxForMainActivityCountdown;
import net.iubris.sosmandown.utils.Printor;
import roboguice.inject.SharedPreferencesProvider;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;

/**
 * singleton because it uses hardware resources (audio manager)
 */
@Singleton
//@ContextSingleton
public class StateActionVibrateAndLaunchMainActivity implements StateAction {
	
	private static final long MS_TO_VIBRATE = 500;
	
	private final Vibrator vibrator;
	private final SharedPreferencesProvider sharedPreferencesProvider;
	
	private boolean launchingIntentOneShotInDevOrTestMode;

	private final Context context;
	
	@Inject
	public StateActionVibrateAndLaunchMainActivity(/* AudioManager audioManager, */Vibrator vibrator, SharedPreferencesProvider sharedPreferencesProvider, Context context) {
		Printor.whoIAm(this,context);
		this.context = context;
		//		super(context, audioManager, true);
		this.vibrator = vibrator;
//Ln.d("context: "+context);
		this.sharedPreferencesProvider = sharedPreferencesProvider;
	}

	@Override
	public void doOnState() {
		vibrator.vibrate(MS_TO_VIBRATE);
//		super.doOnState();

		if (Config.TEST_or_DEV) {
			if (!launchingIntentOneShotInDevOrTestMode) {
				if (hasToLaunchMainActivityInTestOrDevMode()) {
					IntentActionDelegate.launchButtonIntent(IntentActionDelegate.INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_START_IN_DEV_MODE, context);
					launchingIntentOneShotInDevOrTestMode = true;
				}
			}
		} else {
			IntentActionDelegate.launchButtonIntent(IntentActionDelegate.INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_START, context);
		}
	}
	private boolean hasToLaunchMainActivityInTestOrDevMode() {
		boolean checkboxChecked = sharedPreferencesProvider.get().getBoolean(FragmentCheckboxForMainActivityCountdown.CHECKBOX_LAUNCH_MAINACTIVITY_COUNTDOWN_ON_FALL, false);
		Class<? extends Activity> currentActivityClass = ((SOSManDownApplication)context.getApplicationContext() ).getCurrentActivity().getClass();
		boolean isInChartActivity = ChartsActivity.class.equals(currentActivityClass);
		return (/*Config.TEST_or_DEV &&*/ isInChartActivity && checkboxChecked);
	}
	/*private void launchCountdownButtonIntent(String intentAction) {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setAction(intentAction);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}*/
	
	@Override
	public void doOnStateThenStop(long afterTimeStop) {}

	public void stop() {
//		super.stop();

		launchingIntentOneShotInDevOrTestMode = false;
		
		if (hasToLaunchMainActivityInTestOrDevMode()) {
			Intent intent = new Intent(context, MainActivity.class);
			intent.setAction(IntentActionDelegate.INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_STOP);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		}
	}
	
	@Override
	public void releaseResources() {
//		super.releaseResources();
		launchingIntentOneShotInDevOrTestMode = false;
	}
}
