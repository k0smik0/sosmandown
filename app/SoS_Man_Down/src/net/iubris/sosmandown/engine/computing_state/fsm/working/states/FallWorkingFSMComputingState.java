/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.working.states;

import java.io.Serializable;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.config.Config.Detection.Fall;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.computing_state.fsm.working.actions.StateActionPlaySound;
import net.iubris.sosmandown.engine.computing_state.fsm.working.actions.StateActionVibrateAndLaunchMainActivity;
import net.iubris.sosmandown.engine.enhancedcountdown.EnhancedCountDownTimer.ActionEventRelaxInterrupt;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.model.state.working.minor.WorkingMinorStateEnum;
import net.iubris.sosmandown.subscriptions.PublisherSubscriberDelegate;
import net.iubris.sosmandown.subscriptions.SOSPrioritySubscriber;
import net.iubris.sosmandown.subscriptions.action_event_relax_interrupt.ActionEventRelaxInterruptSubscriber;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.ThreadsUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.SubscriberExceptionEvent;
import org.greenrobot.eventbus.ThreadMode;

import roboguice.util.Ln;
import android.content.Context;

public class FallWorkingFSMComputingState extends AbstractWorkingFSMComputedState implements WorkingFSMComputingState, ActionEventRelaxInterruptSubscriber, SOSPrioritySubscriber {
	
	private static final long serialVersionUID = -4882394648657613101L;

	private static final int MIN_HANDLED_TO_ACT = 30; // ~8sec
	
	private final StateActionVibrateAndLaunchMainActivity stateActionVibrateAndLaunchMainActivity;
	private final StateActionPlaySound stateActionPlaySound;
	
	private final PublisherSubscriberDelegate subscriberDelegate;
	
	private final ThreadsUtil threadsUtil;
	
	private int handled = 0;
	private int movements = 0;

//	private final float[] locationsDistanceResults = new float[3];

	private boolean sosPriorityValue;

	private boolean executed = false;

	private long start;
	private long end;
	
//	@Inject private SensorAccelerationProvider sensorAccelerationProvider;

//	private ShakeDetector shakeDetector;

	@Inject
	public FallWorkingFSMComputingState(FSMComputedStateProcessor eventProcessorFSM, 
			FSMComputingStatesLocator fsmComputedStatesLocator, 
			StateActionVibrateAndLaunchMainActivity stateActionVibrateAndLaunchMainActivity,
			StateActionPlaySound stateActionPlaySound,
			PublisherSubscriberDelegate subscriberDelegate,
			ThreadsUtil threadsUtil) {
		super(eventProcessorFSM, fsmComputedStatesLocator, WorkingMajorStateEnum.FALL,WorkingMinorStateEnum.FALL_FSM);
		
		this.stateActionVibrateAndLaunchMainActivity = stateActionVibrateAndLaunchMainActivity;
		this.stateActionPlaySound = stateActionPlaySound;
		

		this.subscriberDelegate = subscriberDelegate;
		this.threadsUtil = threadsUtil;
		
//		this.shakeDetector = new ShakeDetector(listener);
//		shakeDetector.start(sensorAccelerationProvider.get());
		
		subscriberDelegate.registerSubscription(this);
		
		this.movements = 0;
	}
	/*private void initLocationsDistanceResults() {
		for(int i=0;i<locationsDistanceResults.length;i++) {
			locationsDistanceResults[i] = -1;
		}
	}*/
	
	@Override
	public void prepare() {
		start = System.currentTimeMillis();
		end = start+(Config.Detection.Fall.MAX_FALL_SECONDS*1000);
	}
	
	@Override
	public void handle(SensorEventDataHolder sensorEventDataHolder) {
		
		/*String msg0 = "FALL!";
		Runnable showToastRunnable0 = buildShowToastRunnable(context0, msg0);
		threadsUtil.execOnUI(showToastRunnable0);*/
		
		long now = System.currentTimeMillis();
		if (now >= end) {
			Context context = computedStateProcessorFSM.getContext().getApplicationContext();
			String msg = context.getResources().getString(R.string.fall_state_elapsed);
			Runnable showToastRunnable = buildShowToastRunnable(msg);
			threadsUtil.execOnUI(showToastRunnable);
			setRelax();
		}

		if (sosPriorityValue) {
			return;
		}		
		
		// prepare
		/*if (handled==0) {
			now = System.currentTimeMillis();
		}*/
		handled++;
		if (handled%9==0 && handled<MIN_HANDLED_TO_ACT) {
			Ln.d("handled events:"+handled);
		}
		
		/*if (locationFirstGood==null) {
			locationFirstGood = locationProvider.getLocation();
		} else {
			Location locationActual = locationProvider.getLocation();
			Location.distanceBetween(locationFirstGood.getLatitude(), locationFirstGood.getLongitude(), locationActual.getLatitude(), locationActual.getAltitude(), locationsDistanceResults);
			float distance = locationsDistanceResults[0];
			if (distance>0) {
				Ln.d("distance between "+locationFirstGood+" and "+locationActual+" is "+distance);
			}
		}*/
		
		// simulate heavy (voluntary?) movements (ex: shake): filtering light movements (breathing?) 
		double resultant = sensorEventDataHolder.resultant;
		if (SensorEventProcessorUtils.isResultantOutOfGreatRangeAround1G(resultant)) {
			movements++;
			if (movements%4==0) { // 4 = Fall.NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE/2
				Ln.d("'out GreatRangeAroundG',[res:"+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(resultant)+"],counter:"+movements);
			}
		}
		if (movements<=Fall.NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE) {
			return;
		}
		Ln.d("'movement>GreatRangeAroundG' > NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE["+Fall.NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE+"], setting relax");
		setRelax();
	}
	
	private static Runnable buildShowToastRunnable(final String message) { 
		return new Runnable() {
			@Override
			public void run() {
				SToast.INSTANCE.showLong(message);
			}
		};
	}
	
	private void setRelax() {
//		Ln.d("changing to RELAX");
		stateActionPlaySound.stop();
		stateActionPlaySound.releaseResources();
		
		stateActionVibrateAndLaunchMainActivity.stop();
		stateActionVibrateAndLaunchMainActivity.releaseResources();
		
		reset();
//		eventProcessorFSM.stopCollectingData();
		changeStateTo( fsmComputingStatesLocator.get(WorkingMajorStateEnum.RELAX) );
	}

	@Override
	public void reset() {
		handled = 0;
		movements = 0;
//		reallyAct = false;
		executed = false;
//		now = -1;
	}
	
	@Override
	public void exec() {
		if (!executed) {
			// this if below acts as a "latency", as "ok, just wait some times before really act!"
			if (handled>MIN_HANDLED_TO_ACT) {
Ln.d("executing stateActionFall");
				stateActionVibrateAndLaunchMainActivity.doOnState(/*reallyAct*/);
				stateActionPlaySound.setClient(this.getClass().getSimpleName()+"@"+hashCode());
				stateActionPlaySound.setPlayingLoop(true);
				stateActionPlaySound.doOnState(/*reallyAct*/);
				executed = true;
			}
		}
	}
	@Override
	public void releaseResources() {
		stateActionVibrateAndLaunchMainActivity.releaseResources();
		subscriberDelegate.unregisterSubscription(this);
	}
	
	/*private Listener listener = new ShakeDetector.Listener() {
		@Override
		public void hearShake() {
			setRelax();
		}		
	};*/
	
	/*@Override
	public void registerSubscription(EventBus eventBus, Subscriber subscriber) {
		if (!eventBus.isRegistered(subscriber)) {
			eventBus.register(subscriber);
			Ln.d("register to event subscription");
		}		
	}
	@Override
	public void unregisterSubscription(EventBus eventBus, Subscriber subscriber) {
		if (eventBus.isRegistered(subscriber)) {
			eventBus.unregister(subscriber);
		}
	}*/	
	
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedActionEventRelaxInterrupt(ActionEventRelaxInterrupt actionEventRelaxInterrupt) {
		Ln.d("received "+actionEventRelaxInterrupt);
		setRelax();		
	}
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedSOSPriority(SOSPriority sosPriority) {
		Ln.d("received "+sosPriority);
		this.sosPriorityValue = sosPriority.value;
	}
	@Subscribe
	@Override
	public void onEvent(SubscriberExceptionEvent subscriberExceptionEvent) {
		Ln.d("subscriberExceptionEvent: event:"+subscriberExceptionEvent.causingEvent+",subscriber:"+subscriberExceptionEvent.causingSubscriber+",eventBus:"+subscriberExceptionEvent.eventBus);
	}
	
	public static class SOSPriority implements Serializable {
		private static final long serialVersionUID = -2566214442818018656L;
		public final boolean value;
		public SOSPriority(boolean value) {
			this.value = value;
		}
		
	}

	/*public void prepareForExec() {
		reallyAct = true;		
	}*/
}
