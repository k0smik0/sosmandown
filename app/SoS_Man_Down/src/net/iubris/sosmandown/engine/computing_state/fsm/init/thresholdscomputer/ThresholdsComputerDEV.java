/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer;

import java.util.Vector;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import roboguice.util.Ln;
import android.content.res.Resources;

public class ThresholdsComputerDEV extends AbstractThresholdsComputer implements ThresholdsComputer {
	
	private String impact_sensitivity_threshold__default_value;

	@Inject
	public ThresholdsComputerDEV(Resources resources) {
		Ln.d(this);
		impact_sensitivity_threshold__default_value = resources.getString(R.string.impact_sensitivity_threshold__default_value);
	}

	@Override
	public ComputedThresholds compute(Vector<Double> gravityAverageSamples) {
		return realCompute(gravityAverageSamples, Double.parseDouble(impact_sensitivity_threshold__default_value) /*Config.Detection.Impact.getImpactThresholdForGravityMultiplier()*/);
	}
}
