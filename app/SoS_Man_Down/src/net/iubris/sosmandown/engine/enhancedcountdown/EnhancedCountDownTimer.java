/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.engine.enhancedcountdown;

import net.iubris.sosmandown.engine.computing_state.fsm.working.states.FallWorkingFSMComputingState;
import net.iubris.sosmandown.engine.enhancedcountdown.EnhancedCountDownTimer.ActionEventRelaxInterrupt;
import net.iubris.sosmandown.subscriptions.Publisher;
import net.iubris.sosmandown.utils.ThreadsUtil;

import org.greenrobot.eventbus.EventBus;

import roboguice.util.Ln;
import android.os.CountDownTimer;

/**
 * An improved {@link CountDownTimer} with callbacking behaviours
 * @author Massimiliano Leone
 */
public class EnhancedCountDownTimer implements Publisher<ActionEventRelaxInterrupt> {

	private final CountDownTimer countDownTimer;
	private final EventBus eventBus;
	
	private final OnStartCallback onStartCallback;
	private final OnCancelCallback onCancelCallback;
	
	private boolean isRunning;
	
	private static final OnStartCallback onStartCallbackDummy = new OnStartCallback() {
		@Override
		public void doOnStart() {}
	};
	private static final OnCancelCallback onCancelCallbackDummy = new OnCancelCallback() {
		@Override
		public void doOnCancel() {}
	};
	
	private final ThreadsUtil threadsUtil;

	public EnhancedCountDownTimer(long millisInFuture, long countDownIntervalBeforeRunTickCallback, OnTickCallback onTickCallback, OnFinishCallback onFinishCallback, EventBus eventBus, ThreadsUtil threadsUtil) {
		this(millisInFuture, countDownIntervalBeforeRunTickCallback, onTickCallback, onFinishCallback, onStartCallbackDummy, onCancelCallbackDummy, eventBus, threadsUtil);
//		this.countDownTimer = buildCountDownTimer(millisInFuture, countDownIntervalBeforeRunTickCallback, onTickCallback, onFinishCallback);
//		this.eventBus = eventBus;
//		
//		this.onStartCallback = null;
	}	
	public EnhancedCountDownTimer(long millisInFuture, long countDownIntervalBeforeRunTickCallback,
			OnTickCallback onTickCallback, OnFinishCallback onFinishCallback,
			OnStartCallback onStartCallback, OnCancelCallback onCancelCallback
			, EventBus eventBus
			, ThreadsUtil threadsUtil) {
		this.countDownTimer = buildCountDownTimer(millisInFuture, countDownIntervalBeforeRunTickCallback, onTickCallback, onFinishCallback);
		this.eventBus = eventBus;
		this.onStartCallback = onStartCallback;
		this.onCancelCallback = onCancelCallback;
		this.threadsUtil = threadsUtil;
	}
//	private final Handler handler = new Handler();
	private CountDownTimer buildCountDownTimer(long millisInFuture, long countDownIntervalBeforeRunTickCallback, final OnTickCallback onTickCallback, final OnFinishCallback onFinishCallback) {
		return new CountDownTimer(millisInFuture, countDownIntervalBeforeRunTickCallback) {
			@Override
			public void onTick(long millisUntilFinished) {
				onTickCallback.doOnTick(millisUntilFinished);
			}
			@Override
			public void onFinish() {
				/*handler.postDelayed(*/threadsUtil.execOnUIWithDelay(new Runnable() {
					@Override
					public void run() {
						/*if (onFinishCallback!=null) {
							onFinishCallback.doOnFinish();
						}*/
						onFinishCallback.doOnFinish();
						isRunning = false;
						sendSOSPriority(false);
						Ln.d("onFinish(ed), isRunning: "+isRunning);
					}
				}, 0);
			}
		};
	}


	public synchronized void cancel() {
		if (isRunning) {
			Ln.d("cancel");
			countDownTimer.cancel();
			
			/*if (onCancelCallback!=null) {
				onCancelCallback.doOnCancel();
			}*/
			onCancelCallback.doOnCancel();
			
//			sosPriority = false;
//			eventBus.post( new FallWorkingFSMComputingState.SOSPriority(false) );
			sendSOSPriority(false);
			isRunning = false;
			Ln.d("cancel(ed), isRunning: "+isRunning);
		}
	}
	/*public synchronized void cancel(OnCancelCallback onCancelCallback) {
		Ln.d("cancel(onCancelCallback)");
		cancel();
		onCancelCallback.doOnCancel();
	}*/
	/**
	 * default: do nothing
	 */
	public synchronized void start() {
		if (isRunning) {
			return;
		}				
		/*if (onStartCallback!=null) {
			onStartCallback.doOnStart();
		}*/
		onStartCallback.doOnStart();
		countDownTimer.start();
		
//		sosPriority = true;
//		eventBus.post( new FallWorkingFSMComputingState.SOSPriority(true) );
		sendSOSPriority(true);
		
		isRunning = true;
		
		Ln.d("start(ed), isRunning: "+isRunning);
	}
	/*public synchronized void start(OnStartCallback onStartCallback) {
		Ln.d("start(onStartCallback)");
		start();
		onStartCallback.doOnStart();
	}*/

	/**
	 * @return true if isRunning after toggle, false otherwise
	 */
	public synchronized boolean toggle() {
		Ln.d("toggle, isRunning: "+isRunning);
		if (isRunning) {
//			Ln.d("isRunning: "+isRunning);
			cancel();
			publish( new ActionEventRelaxInterrupt() );
		} else {
//			Ln.d("isRunning: "+isRunning);
			start();
		}
		return isRunning;
	}
	
	public boolean isRunning() {
		return isRunning;
	}
	
	private void sendSOSPriority(boolean priority) {
		eventBus.post( new FallWorkingFSMComputingState.SOSPriority(priority) );
	}
	
	public static class ActionEventRelaxInterrupt {}

	@Override
	public void publish(ActionEventRelaxInterrupt aer) {
		Ln.d("sending "+aer.getClass().getSimpleName());
		eventBus.post(aer);
	}
}
