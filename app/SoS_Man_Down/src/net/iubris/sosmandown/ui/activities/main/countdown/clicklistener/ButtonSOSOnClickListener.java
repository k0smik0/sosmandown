/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main.countdown.clicklistener;

import javax.inject.Inject;

import net.iubris.sosmandown._di.providers.countdown.EnhancedCountDownTimerProvider;
import net.iubris.sosmandown.engine.enhancedcountdown.EnhancedCountDownTimer;
import roboguice.RoboGuice;
import roboguice.inject.ContextSingleton;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;

@ContextSingleton
public class ButtonSOSOnClickListener implements OnClickListener {

	private final EnhancedCountDownTimer countDownTimer;

	private boolean running;
	
	@Inject
	public ButtonSOSOnClickListener(Activity activity, EnhancedCountDownTimerProvider countDownTimerProvider) {
		RoboGuice.injectMembers(activity, this);
		this.countDownTimer = countDownTimerProvider.get();
		
//		Ln.d("buttonSOS:"+buttonSos);
	}

	@Override
	public void onClick(View v) {
		onClick();
	}
	
	public void onClick() {
		countDownTimer.toggle();
		running = countDownTimer.isRunning();
	}
	
	public boolean isRunningCountdown() {
		return running;
	}
}
