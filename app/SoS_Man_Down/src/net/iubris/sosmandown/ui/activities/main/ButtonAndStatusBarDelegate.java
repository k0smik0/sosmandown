/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main;

import java.util.Map;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.FallDetectionCounter;
import net.iubris.sosmandown.model.result.LightComputedResult;
import net.iubris.sosmandown.model.state.phase.PhaseState;
import net.iubris.sosmandown.ui.activities.main.LightComputedStateWriterHelper.PhaseStateAction;
import net.iubris.sosmandown.ui.activities.main.buttons.ButtonDetectionStartStopOnClickListener;
import net.iubris.sosmandown.utils.Printor;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.RoboGuice;
import roboguice.inject.ContextSingleton;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import roboguice.inject.RoboInjector;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

@ContextSingleton
public class ButtonAndStatusBarDelegate {
	
	@InjectView(R.id.countdown__text)
	private TextView countdownText;
	
	@InjectView(R.id.button_sos)
	private Button buttonSOS;

	@InjectView(R.id.button__detection_start_stop__start)
	private Button buttonDetectionStart;
	@InjectView(R.id.button__detection_start_stop__stop)
	private Button buttonDetectionStop;
	
	@InjectView(R.id.button_chart_activity)
	private Button buttonChartActivity;
	
	@InjectView(R.id.file_saver__button_save_text)
	private TextView fileSaverButtonSaveText;
	@InjectView(R.id.file_saver__button_save)
	private Button fileSaverButtonSave;
	
	@InjectView(R.id.notification_status_bar__container_layout)
	private RelativeLayout notificationStatusBarContainer;
	
//	@InjectView(R.id.notification_status_bar__computed_result__text_prefix)
//	private TextView notificationStatusBarPrefixText;
	// TODO move to delegate
	@InjectView(R.id.notification_status_bar__light_computed_result__text_content)
	private TextView notificationStatusBarLightComputedResultTextContent;
	
	@InjectView(R.id.notification_status_bar__detection_fall_number)
	private TextView notificationStatusBarDetectionFallNumber;
	
	@InjectView(R.id.notification_status_bar__detected_fall_number_prefix_)
	private TextView notificationStatusBarDetectedFallNumberPrefix;
	@InjectView(R.id.notification_status_bar__detected_fall_number)
	private TextView notificationStatusBarDetectedFallNumber;
	@InjectView(R.id.notification_status_bar__detected_fall_number_suffix_)
	private TextView notificationStatusBarDetectedFallNumberSuffix;
	
	@InjectResource(R.string.notification_bar__detected_fall_number_prefix)
	private String notificationStatusBarDetectedFallNumberPrefixString;
	
	
	private final StatusBarCounterActionsDelegate statusBarCounterActionsDelegate;
	private final Activity activity;
	
	private LightComputedStateWriterHelper lightComputedStateWriterHelper;
	
	@Inject
	public ButtonAndStatusBarDelegate(Activity activity, StatusBarCounterActionsDelegate statusBarCounterActionsDelegate, LightComputedStateWriterHelper lightComputedStateWriterHelper) {
Printor.whoIAm(this,activity);
		this.activity = activity;
		
//		statusBarCounterActionsDelegate = injector.getInstance(StatusBarCounterActionsDelegate.class);
		this.statusBarCounterActionsDelegate = statusBarCounterActionsDelegate;
		this.lightComputedStateWriterHelper = lightComputedStateWriterHelper;
		
//		Ln.d(lightComputedStateWriterHelper);
		
		RoboInjector injector = RoboGuice.getInjector(activity);
		injector.injectViewMembers(this);
	}
	
	void restoreBarsStatusOnDetectionStarted() {
//		Ln.d("restoring data and layout");

		// data
		restoreDataOnStatusBar();

		// layout
		countdownText.setVisibility(View.VISIBLE);

		ButtonDetectionStartStopOnClickListener.applyToButtonSOSLayoutParamsWithOthers(buttonSOS);		
//		ButtonDetectionStartStopOnClickListener.applyToButtonDetectionStartStopLayoutAlignLeft(buttonStartStopDetection);
		ButtonDetectionStartStopOnClickListener.applyToButtonsOnDetectionStart(buttonDetectionStart, buttonDetectionStop);
		buttonChartActivity.setVisibility(View.VISIBLE);
		
		fileSaverButtonSaveText.setVisibility(View.VISIBLE);
		fileSaverButtonSave.setVisibility(View.VISIBLE);
		
		notificationStatusBarContainer.setVisibility(View.VISIBLE);
		
		notificationStatusBarLightComputedResultTextContent.invalidate();
		notificationStatusBarContainer.invalidate();
	}
	void restoreDataOnStatusBar() {
		LightComputedResult current = LightComputedResultSaver.INSTANCE.getCurrent();
//		Ln.d("current: "+current);
		Map<PhaseState, PhaseStateAction> phaseStateActionsMap = lightComputedStateWriterHelper.getPhaseStateActionsMap();
		PhaseState phaseState = current.getPhaseState();
//		Ln.d("phaseState:"+phaseState);
		PhaseStateAction phaseStateAction = phaseStateActionsMap.get(phaseState);
		String toPrint = phaseStateAction.getText(current, activity);
		notificationStatusBarLightComputedResultTextContent.setText(toPrint);
		
		notificationStatusBarDetectionFallNumber.setText(StringsUtils.EMPTY+FallDetectionCounter.DETECTION.getCounter());
		
		notificationStatusBarDetectedFallNumber.setText(StringsUtils.EMPTY+FallDetectionCounter.DETECTED.getCounter());
		notificationStatusBarDetectedFallNumberPrefix.setText(notificationStatusBarDetectedFallNumberPrefixString);
		notificationStatusBarDetectedFallNumberSuffix.setText("]");
		
		
		ActivityType[] activityTypes = ActivityType.values();
		for (ActivityType at : activityTypes) {
			try {
				statusBarCounterActionsDelegate.getActivityTypeAction(at).act(at.getCounter());
			} catch (NullPointerException e) {}
		}
		Posture[] postures = Posture.values();
		for (Posture p : postures) {
			try {
				statusBarCounterActionsDelegate.getPostureAction(p).act(p.getCounter());
			} catch (NullPointerException e) {}
		}
	}

	/*public void setLightComputedStateWriterHelper(LightComputedStateWriterHelper lightComputedStateWriterHelper) {
		this.lightComputedStateWriterHelper = lightComputedStateWriterHelper;
	}*/
}
