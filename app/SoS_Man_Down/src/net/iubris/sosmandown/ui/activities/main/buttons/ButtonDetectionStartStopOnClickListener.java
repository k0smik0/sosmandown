/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main.buttons;


import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.engine.computing_state.fsm.working.states.detecting.FallDetectionCounter;
import net.iubris.sosmandown.savers.detections.DetectionsSaver;
import net.iubris.sosmandown.service.SensorsService;
import roboguice.RoboGuice;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ButtonDetectionStartStopOnClickListener implements OnClickListener {
	
	private final Activity activity;
	
	@InjectView(R.id.countdown__text)
	private TextView textviewCountdown;
	
	@InjectView(R.id.button_sos)
	private Button buttonSOS;
	
	@InjectView(R.id.button__detection_start_stop__start)
	private Button buttonDetectionStart;
	@InjectView(R.id.button__detection_start_stop__stop)
	private Button buttonDetectionStop;
	@InjectResource(R.string.button_start_detection)
	private String buttonStartStopDetectionMessageStart;
	@InjectResource(R.string.button_stop_detection)
	private String buttonStartStopDetectionMessageStop;

	@InjectView(R.id.button_chart_activity)
	private Button buttonChartActivity;
	
	@InjectView(R.id.file_saver__button_save_text)
	private TextView fileSaverButtonSaveText;
	@InjectView(R.id.file_saver__button_save)
	private Button fileSaverButtonSave;	
	
	@InjectView(R.id.notification_status_bar__container_layout)
	private RelativeLayout notificationStatusBarContainerLayout;

//	@Inject
//	private EventsSaver eventsSaver;
	
	@Inject
	private DetectionsSaver detectionsSaver;

	@Inject
	public ButtonDetectionStartStopOnClickListener(final Activity activity/*, Button button*/) {
		this.activity = activity;
		RoboGuice.injectMembers(activity, this);
		
//		autoSetText();
	}
	
	/*public void autoSetText() {
		if (SensorsService.isStarted()) {
			if (buttonStartStopDetection!=null) {
				buttonStartStopDetection.setText(buttonStartStopDetectionMessageStop);				
			} else {
				if (buttonStartStopDetection!=null) {
					buttonStartStopDetection.setText(buttonStartStopDetectionMessageStart);
				}
			}
		}
	}*/

	@Override
	public void onClick(View v) {
//		Ln.d("eventsSaver:"+eventsSaver);
		if (!SensorsService.isStarted()) {
			doStart();
		} else {
			doStop();
		}
	}
	
	private void doStart() {
		updateLayoutOnStart();
		
		SensorsService.start(activity);
//		eventsSaver.reset();
		detectionsSaver.reset();
	}
	private void doStop() {
		updateLayoutOnStop();

		SensorsService.stop(activity);
		
		for (FallDetectionCounter fallDetectionCounter : FallDetectionCounter.values() ) {
			fallDetectionCounter.reset();
		}
//		handler.post(runnableStop);
	}
	
	private void updateLayoutOnStart() {
		applyToButtonSOSLayoutParamsWithOthers(buttonSOS);
		
		textviewCountdown.setVisibility(View.VISIBLE);
//		Ln.d(textviewCountdown.getVisibility());
		
//		buttonStartStopDetection.setText(buttonStartStopDetectionMessageStop);
//		applyToButtonDetectionStartStopLayoutAlignLeft(buttonStartStopDetection);
		applyToButtonsOnDetectionStart(buttonDetectionStart, buttonDetectionStop);

//		layoutFileSave.setVisibility(View.VISIBLE);
		// TODO add animation fade-in for View.VISIBLE
		fileSaverButtonSaveText.setVisibility(View.VISIBLE);
		fileSaverButtonSave.setVisibility(View.VISIBLE);
		
//		checkBoxSaveOnFile.setVisibility(View.VISIBLE);
		buttonChartActivity.setVisibility(View.VISIBLE);
		
		notificationStatusBarContainerLayout.setVisibility(View.VISIBLE);
		
		notificationStatusBarContainerLayout.invalidate();
	}
	private void updateLayoutOnStop() {
		textviewCountdown.setVisibility(View.GONE);
		
		applyToButtonSOSLayoutParamsAlone();
		
//		buttonStartStopDetection.setText(buttonStartStopDetectionMessageStart);
//		applyToButtonDetectionStartStopLayoutCenterHorizontal();
		applyToButtonsOnDetectionStop(buttonDetectionStart, buttonDetectionStop);

//		layoutFileSave.setVisibility(View.GONE);
		fileSaverButtonSaveText.setVisibility(View.GONE);
		fileSaverButtonSave.setVisibility(View.GONE);
		
//		checkBoxSaveOnFile.setVisibility(View.GONE);
		buttonChartActivity.setVisibility(View.GONE);
		
		notificationStatusBarContainerLayout.setVisibility(View.GONE);
	}
	
	private void applyToButtonSOSLayoutParamsAlone() {
		LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) buttonSOS.getLayoutParams();
		
//		Ln.d("from params margin.bottom:"+layoutParams.bottomMargin+", margin.top:"+layoutParams.topMargin+", weight:"+layoutParams.weight);
		layoutParams.weight = 0;
		layoutParams.bottomMargin = 60;
		layoutParams.topMargin = 40;
//		Ln.d("to params margin.bottom:"+layoutParams.bottomMargin+", margin.top:"+layoutParams.topMargin+", weight:"+layoutParams.weight);
		buttonSOS.setLayoutParams(layoutParams);
	}
	
	
	public static void applyToButtonSOSLayoutParamsWithOthers(Button buttonSOS) {
		LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) buttonSOS.getLayoutParams();
//		Ln.d("from params margin.bottom:"+layoutParams.bottomMargin+", margin.top:"+layoutParams.topMargin+", weight:"+layoutParams.weight);
		layoutParams.weight = 180;
		layoutParams.bottomMargin = 10;
		layoutParams.topMargin = 0;
//		Ln.d("to params margin.bottom:"+layoutParams.bottomMargin+", margin.top:"+layoutParams.topMargin+", weight:"+layoutParams.weight);
		buttonSOS.setLayoutParams(layoutParams);
	}
	
	/*
	private void applyToButtonDetectionStartStopLayoutCenterHorizontal() {
		RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)buttonStartStopDetection.getLayoutParams();
		layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_START);
		layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
		layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
		buttonStartStopDetection.setLayoutParams(layoutParams);
		buttonStartStopDetection.invalidate();
	}
	
	public static void applyToButtonDetectionStartStopLayoutAlignLeft(Button buttonStartStopDetection) {
		RelativeLayout.LayoutParams buttonDetectionStartStopLayoutAlignLeft = (RelativeLayout.LayoutParams)buttonStartStopDetection.getLayoutParams();
		buttonDetectionStartStopLayoutAlignLeft.removeRule(RelativeLayout.CENTER_HORIZONTAL);
		buttonDetectionStartStopLayoutAlignLeft.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
		buttonDetectionStartStopLayoutAlignLeft.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
		buttonStartStopDetection.setLayoutParams(buttonDetectionStartStopLayoutAlignLeft);
	}*/
	// on start
	public static void applyToButtonsOnDetectionStart(Button buttonDetectionStart, Button buttonDetectionStop) {
		buttonDetectionStart.setVisibility(View.GONE);
		buttonDetectionStop.setVisibility(View.VISIBLE);
	}
	// on stop
	public static void applyToButtonsOnDetectionStop(Button buttonDetectionStart, Button buttonDetectionStop) {
//		Ln.d("on stop...");
		buttonDetectionStart.setVisibility(View.VISIBLE);
		buttonDetectionStop.setVisibility(View.GONE);		
	}
}
