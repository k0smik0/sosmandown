/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.splash;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

/**
 * heavy inspired to RoboSplashActivity (from RoboGuice): as default, show a splash image for 500ms,<br/>
 * then start main activity; time can be modified overriding protected field 'minDisplayMs'<br/><br/>
 * 
 * Launching main activity, an action intent will be passed: ACTION_POST_SPLASH. You could use to know if you're 
 * really in post_boot.
 * @author Massimiliano Leone - k0smik0
 */
public abstract class AbstractUlyssesSplashActivity extends Activity {
	
//	private static final Class<?> thisClass = AbstractUlyssesSplashActivity.class;
//	private static final String TAG = thisClass.getPackage().getName().toUpperCase(Locale.getDefault())+"/"+thisClass.getSimpleName();
	
	public static final String ACTION_POST_SPLASH = "action_post_splash";
	
	/**
	 * milliseconds
	 */
	protected int minDisplayMilliseconds = 500;

	private Application application;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(getLayoutResID());
		super.onCreate(savedInstanceState);
		
		final long start = System.currentTimeMillis();
		application = getApplication();
		
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected void onPreExecute() {
				new Handler().post( new Runnable() {
					@Override
					public void run() {
						doOtherStuffInForeground(application);
					}
				});
			}
			@Override
			protected Void doInBackground(Void... params) {
				doOtherStuffinBackground(application);
				return null;
			}
			@Override
			protected void onPostExecute(Void result) {
				long stop = System.currentTimeMillis();
				long delta = stop - start;
//Ln.d("delta: "+delta);
				long remaining =  (minDisplayMilliseconds-delta);
//Ln.d("remaining: "+remaining);
				
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						startMainActivity();						
					}
				}, remaining);
			}
		}.execute();
	}
	
	protected void startMainActivity() {
		final Intent startIntent = new Intent(this,getActivityToStart());
		startIntent.setAction(AbstractUlyssesSplashActivity.ACTION_POST_SPLASH);
		startIntent.setFlags(getLauncherModeForMainActivity());
		startActivity(startIntent);
		if (!this.isFinishing()) {
			finish();
		}
//		Log.d(TAG,"calling 'overridePendingTransition'");
//		overridePendingTransition(R.anim.fade_in_900ms, R.anim.fade_out_900ms);
	}
	
	/**
	 * use this to start some stuff in background
	 * @param application
	 */
	protected void doOtherStuffinBackground(Application application) {}
	
	/**
	 * use this to start some stuff in foreground
	 * @param application
	 */
	protected void doOtherStuffInForeground(Application application) {}
	
	/**
	 * provide your splash layout 
	 */
	protected abstract int getLayoutResID();
	/**
	 * provide your launcher mode for this activity
	 */
	protected abstract int getLauncherModeForMainActivity();
	/**
	 * provide main activity to start after this
	 * @return
	 */
	protected abstract Class<? extends Activity> getActivityToStart();
}
