/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.info;

import java.util.Locale;

import net.iubris.sosmandown.R;
import roboguice.activity.RoboActivity;
import roboguice.util.Ln;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

public class LicensesActivity extends RoboActivity {

	private static final int[][] CREDITS = new int[][] { 
		{ R.string.credits_1_text, R.string.credits_1_url }, 
		{ R.string.credits_2_text, R.string.credits_2_url },
		{ R.string.credits_3_text, R.string.credits_3_url },
		{ R.string.credits_4_text, R.string.credits_4_url },
		{ R.string.credits_5_text, R.string.credits_5_url },
		{ R.string.credits_6_text, R.string.credits_6_url },
		{ R.string.credits_7_text, R.string.credits_7_url },
		{ R.string.credits_8_text, R.string.credits_8_url },
		{ R.string.credits_9_text, R.string.credits_9_url },
		{ R.string.credits_10_text, R.string.credits_10_url },
		{ R.string.credits_11_text, R.string.credits_11_url },
		{ R.string.credits_12_text, R.string.credits_12_url },
		{ R.string.credits_13_text, R.string.credits_13_url }
	};
	
	private EDialog eDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_licenses);
		
		eDialog = new EDialog(this);
		
		Animation animationScrollBottomToUp1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scroll_bottom_to_up);
		LinearLayout layoutCopyrights = (LinearLayout) findViewById(R.id.licenses__layout_copyright);
		animationScrollBottomToUp1.setDuration(3500);
		layoutCopyrights.startAnimation(animationScrollBottomToUp1);
		
		Animation animationScrollBottomToUp2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scroll_bottom_to_up);
		animationScrollBottomToUp2.setDuration(4500);
		setTextAndAnimate(R.id.licenses__copyright_prefix, R.string.licenses__copyright_prefix_text, animationScrollBottomToUp2);
		
		Button buttonCopyright = ((Button)findViewById(R.id.licenses__copyright_button));
		buttonCopyright.setText(R.string.licenses__copyright_button_text);
//		animationScrollBottomToUp.setStartOffset(2000);
		buttonCopyright.startAnimation(animationScrollBottomToUp2);
		
		
		Animation animationScrollBottomToUp3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scroll_bottom_to_up);
		ScrollView layoutCreditsScroll = (ScrollView) findViewById(R.id.licenses__layout_credits__scroll);
		animationScrollBottomToUp3.setStartOffset(2000);
		layoutCreditsScroll.startAnimation(animationScrollBottomToUp3);
		
		Animation animationScrollBottomToUp4 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scroll_bottom_to_up);
		animationScrollBottomToUp4.setStartOffset(4000);
		setTextAndAnimate(R.id.licenses__credits_prefix, R.string.licenses__credits_prefix_text, animationScrollBottomToUp4);

//		Animation animationScrollBottomToUp5 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scroll_bottom_to_up);
//		animationScrollBottomToUp5.setStartOffset(4000);
		
		LinearLayout layoutCredits = (LinearLayout) findViewById(R.id.licenses__layout_credits);		
		for (int[] creditPair : CREDITS) {
			int creditId = creditPair[0];
			String creditUrl = getString( creditPair[1] );
			
			TextView textView = buildTextView(this);
			layoutCredits.addView(textView);
			setTextLinkAndAnimate(textView, creditId, creditUrl, animationScrollBottomToUp4);
		}
	}
	
	private static TextView buildTextView(Context context) {
		TextView textView = new TextView(context);
//        textView.setText("hallo hallo");
//        textView.setId(5);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		layoutParams.gravity = Gravity.CENTER;
        textView.setLayoutParams(layoutParams);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(18f);
        textView.setTextColor(Color.BLUE);
        
//        textView.setBackgroundResource(R.drawable.border_red);
        
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)textView.getLayoutParams();
        params.setMargins(0, 14, 0, 14); //substitute parameters for left, top, right, bottom
        textView.setLayoutParams(params);
        
        return textView;
	}
	
	private void setTextLinkAndAnimate(final TextView textView, int stringId, final String url, Animation animation) {
//		TextView textviewCredits = (TextView) findViewById(resourcesId);
		textView.setText(stringId);
		textView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				String url = "http://www.example.com";
Ln.d("going to "+url+" from "+textView.getText());
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				LicensesActivity.this.startActivity(i);
			}
		});
		textView.startAnimation(animation);
	}
	
	private void setTextAndAnimate(int resourcesId, int stringId, Animation animation) {
		TextView textviewCredits = (TextView) findViewById(resourcesId);
		textviewCredits.setText(stringId);
		textviewCredits.startAnimation(animation);
	}
	
//	@SuppressWarnings({ "static-method", "resource" })
	public void onClickButtonCopyright(@SuppressWarnings("unused") View view) {
		Locale locale = Locale.getDefault();
		final String language = locale.getLanguage();
		Ln.d(language);
		
		eDialog.show(language);
		
//		new DRoboAsyncTask(thisc).execute(language, eDialog, true);
	}
	
	/*private static class DRoboAsyncTask extends RoboAsyncTask<String> {
		private InputStream inputStream = null;
		private String language;
		private EDialog eDialog;
		private boolean callAgain;
		protected DRoboAsyncTask(Context context) {
			super(context);
		}
		
		public void execute(String language, EDialog eDialog, boolean callAgain) {
			this.language = language;
			this.eDialog = eDialog;
			this.callAgain = callAgain;
			super.execute();
		}
		
		@Override
		public String call() throws Exception {
			Ln.d("language: "+language);
			inputStream = context.getAssets().open(language+File.separator+"gplv3.html");
			String streamToString = streamToString(inputStream);
			if (inputStream!=null) {
				inputStream.close();
			}
			Ln.d(streamToString.length());
			return streamToString;
		}
		@Override
		protected void onSuccess(String htmlContentInStringFormat) throws Exception {
//			aseURL(null, htmlContentInStringFormat, "text/html", "utf-8", null);
			eDialog.show(htmlContentInStringFormat);
		}
		@Override
		protected void onException(Exception e) throws RuntimeException {
			Ln.d(e);
			if (inputStream!=null) {
				try {
					inputStream.close();
				} catch (IOException e1) {
					Ln.d(e1);
				}
			}
			
			if (callAgain) {
				new DRoboAsyncTask(context).execute("en", eDialog, false);
			}
		}
		
		private static String streamToString(InputStream in) throws IOException {
	        if(in == null) {
	            return "";
	        }
	        Writer writer = new StringWriter();
	        char[] buffer = new char[1024];
	        try {
	            Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
	            int n;
	            while ((n = reader.read(buffer)) != -1) {
	                writer.write(buffer, 0, n);
	            }
	        } finally {
	        }
	        return writer.toString();
	    }
	}*/
}
