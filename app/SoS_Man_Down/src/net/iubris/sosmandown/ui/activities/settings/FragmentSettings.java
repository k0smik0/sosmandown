/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.settings;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config;
//import net.iubris.sosmandown.config.Config.Detection.Result.Thresholds;
import net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer.ThresholdsComputer.Util;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils.Thresholds;
import roboguice.fragment.provided.RoboPreferenceFragment;
import roboguice.inject.InjectResource;
import roboguice.util.Ln;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class FragmentSettings extends RoboPreferenceFragment/*Support*/ implements OnSharedPreferenceChangeListener {

	public static final String TAG = "SettingsFragment";
	
	static final String PREFERENCE_KEY_USERNAME = "user_name";
	private static final String PREFERENCE_KEY_CONTACT_1_PHONE_NUMBER = "contact1_phone_number";
	private static final String PREFERENCE_KEY_CONTACT_2_PHONE_NUMBER = "contact2_phone_number";
	private static final String PREFERENCE_KEY_CONTACT_3_PHONE_NUMBER = "contact3_phone_number";
	static final String[] PREFERENCES_PHONE_NUMBERS = new String[]{PREFERENCE_KEY_CONTACT_1_PHONE_NUMBER, PREFERENCE_KEY_CONTACT_2_PHONE_NUMBER, PREFERENCE_KEY_CONTACT_3_PHONE_NUMBER};
	
	private static final String PATTERN_NOT_EMPTY_STRING = "^[a-zA-Z]{3,30}$";
//	private static final String PATTERN_PHONE_NUMBER = "^3[2-9][0-9]{7,8}$";
	static final String PATTERN_PHONE_NUMBER = "^[+]?[0-9]{10,13}$";
	
	public static final String PREFERENCE_IMPACT_SENSITIVITY_THRESHOLD = "impact_sensitivity_threshold__preference";
	public static final String PREFERENCE_IMPACT_SENSITIVITY_THRESHOLD_SLIDER = "impact_sensitivity_threshold__preference_slider";

	private AlertDialog alertDialog;
	
//	@InjectPreference("user_name")
	private EditTextPreference userName;	
	
	@InjectResource(R.string.preference_dialog_warning_no_user_name)
	private String messageEmptyOrWrongUserName;
	@InjectResource(R.string.preference_dialog_warning_no_contacts_phone_number)
	private String a;
	@InjectResource(R.string.preference_dialog_warning_wrong_phone_number)
	private String messageEmptyOrWrongPhoneNumber;
	
//	@InjectResource(R.string.wrongSensivityThreshold)
//	private String wrongSensivityThreshold;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
Ln.d(hashCode());
		addPreferencesFromResource(R.xml.preferences_components);		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = super.onCreateView(inflater, container, savedInstanceState);
	    if(v != null) {
	        ListView lv = (ListView) v.findViewById(android.R.id.list);
	        lv.setPadding(1, 1, 1, 1);
	    }
	    return v;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		userName = (EditTextPreference) findPreference(PREFERENCE_KEY_USERNAME);
		userName.setOnPreferenceChangeListener(onPreferenceChangeListener);
		
		bindListenerToPhoneNumbersPreferences(PREFERENCES_PHONE_NUMBERS);
		
//		EditTextPreference impactThreshold = (EditTextPreference) findPreference(PREFERENCE_IMPACT_SENSITIVITY_THRESHOLD);
//		impactThreshold.setOnPreferenceChangeListener(onPreferenceChangeListener);
		
//		SeekBarPreference seekBarPreference = (SeekBarPreference) findPreference(PREFERENCE_IMPACT_SENSITIVITY_THRESHOLD_SLIDER);
//		seekBarPreference.setOnPreferenceChangeListener(onPreferenceChangeListener);

		
		final Builder alertDialogBuilder = new AlertDialog.Builder( getActivity() );				                
		alertDialogBuilder.setTitle( getString(R.string.preference_dialog_warning_title) );
		
		alertDialog = alertDialogBuilder
			.setPositiveButton(getString(R.string.preference_dialog_button_ok), okAlertDialogListener)
			.setNegativeButton(getString(R.string.preference_dialog_button_cancel), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {/*No Action*/
//							see http://stackoverflow.com/questions/4805896/how-to-open-or-simulate-a-click-on-an-android-preference-created-with-xml-prog
						Ln.d("setting fromDialogChoice to CANCEL");
					}
				})
			.create();
	}
	
	private abstract static class AlertDialogListener implements DialogInterface.OnClickListener {
		protected Preference preferenceToEdit;
		protected String valueToSet;
		
		public void setPreference(Preference preferenceToEdit) {
			this.preferenceToEdit = preferenceToEdit;
		}
		public String getValueToSet() {
			return valueToSet;
		}
		public void setValueToSet(String valueToSet) {
			this.valueToSet = valueToSet;
		}
	}
	private static class OKAlertDialogListener extends AlertDialogListener {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			/*No Action*/
			Ln.d("setting fromDialogChoice to OK");
			SharedPreferences sharedPreferences = preferenceToEdit.getSharedPreferences();
			String key = preferenceToEdit.getKey();
			sharedPreferences.edit().putString(key, getValueToSet()).apply();
			
			String string = sharedPreferences.getString(key, "-");
			Ln.d("new value: "+string);
		}
	}
	private final OKAlertDialogListener okAlertDialogListener = new OKAlertDialogListener();
	
	private void bindListenerToPhoneNumbersPreferences(String... preferenceKeys) {
		for (String preferenceKey: preferenceKeys) {
			EditTextPreference contactPhoneNumber = (EditTextPreference) findPreference(preferenceKey);
			contactPhoneNumber.setOnPreferenceChangeListener(onPreferenceChangeListener);
		}
	}
	
	
	public static boolean isNotNullNorEmpty(String string) {
		return (string!=null && !string.isEmpty());
	}
	
	@Override
	public void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {		
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
		super.onPause();
	}
	
	private void showAlertDialog(String message, Preference preference, String value) {
		okAlertDialogListener.setPreference(preference);
		okAlertDialogListener.setValueToSet(value);
		alertDialog.setMessage(message);
		alertDialog.show();
	}
	
	private final OnPreferenceChangeListener onPreferenceChangeListener = new OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue) {
			
			String value = newValue.toString();
			
			// check username
			if (preference.equals(userName)) {
				if( !value.matches(PATTERN_NOT_EMPTY_STRING) ) {
					FragmentSettings.this.showAlertDialog(messageEmptyOrWrongUserName, preference, value);
//		            return false;
		        }
//				Ln.d("username: "+userName+" valid, returning true");
				return true;
			}
			
			// check phone numbers
			String key = preference.getKey();
			boolean result = true;
			boolean isNumbersValid = true;
			Ln.d("numbersValid:"+isNumbersValid);
			for (String phoneNumberKey: PREFERENCES_PHONE_NUMBERS) {
				if (phoneNumberKey.equals(key)) {
					Ln.d("checking "+phoneNumberKey);
					if ( !PhoneNumberUtils.isGlobalPhoneNumber(value) || !value.matches(PATTERN_PHONE_NUMBER) ) {
						isNumbersValid = false;
						Ln.d("not valid value:"+value);
			        }
					break;
				}
			}
			if (!isNumbersValid){
				Ln.d("showing "+messageEmptyOrWrongPhoneNumber);
				FragmentSettings.this.showAlertDialog(messageEmptyOrWrongPhoneNumber, preference, value);
				
//				result = false;
			}
			
			
			//check sensitivity value
			/*if (key.equals(PREFERENCE_IMPACT_SENSITIVITY_THRESHOLD)) {
				float number = Float.parseFloat(value.replace(StringsUtils.COMMA, StringsUtils.DOT));
				if (number < Config.Detection.Impact.SENSITIVITY_THRESHOLD_MINIMUM || number > Config.Detection.Impact.SENSITIVITY_THRESHOLD_MAXIMUM) {
					FragmentSettings.this.showAlertDialog(wrongSensivityThreshold, preference, value);
					result = false;
				} else {
					result = true;
				}
			}*/
			
			
Ln.d("returning "+result+" for newValue:"+newValue+" for preference:"+preference.getKey());
			
			return result;
		}		
	};
	
	@Override
	public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
		
		
		/*if (key.equals(PREFERENCE_IMPACT_SENSITIVITY_THRESHOLD)) {
			String value = sharedPreferences.getString(key, "default");
			Ln.d("preference "+key+" changed to: " + value);
			float multiplier = 
//					multiplierMax - Float.parseFloat(value);
					Float.parseFloat(value);
//			Ln.d("impact_sensitivity_threshold__multiplier__max_value ["+impact_sensitivity_threshold__multiplier__max_value+"] - impact_sensitivity_threshold__value ["+impact_sensitivity_threshold__value+"] = "+multiplier);
//			Ln.d("Config.Detection.Impact.IMPACT_THRESHOLD_FOR_GRAVITY_MULTIPLIER:"+SensorEventProcessorUtils.AccelerationDecimalFormatOneDigit.format(multiplier));
			Ln.d("new multiplier:"+multiplier);
			Config.Detection.Impact.setImpactThresholdForGravityComplementaryMultiplier(multiplier);
			
			double computeImpactResultThreshold = Util.computeImpactResultThreshold(Thresholds.INSTANCE.getGravity(), multiplier);
			Thresholds.INSTANCE.setImpact(computeImpactResultThreshold);
		}*/
		if (key.equals(PREFERENCE_IMPACT_SENSITIVITY_THRESHOLD_SLIDER)) {
//			float multiplierMax = Float.parseFloat( getActivity().getResources().getString(R.string.impact_sensitivity_threshold_multiplier__max_value) );
			int complementaryMultiplierDefault = getActivity().getResources().getInteger(R.integer.impact_sensitivity_threshold__complement_multiplier__default_value) ;
			int complementaryMultiplier = sharedPreferences.getInt(PREFERENCE_IMPACT_SENSITIVITY_THRESHOLD_SLIDER, complementaryMultiplierDefault);
			Ln.d("preference "+key+" changed to: " + complementaryMultiplier);
//			float multiplier = Integer.parseInt(value);
			Ln.d("new multiplier:"+complementaryMultiplier);
			Config.Detection.Impact.setImpactThresholdForGravityComplementaryMultiplier(complementaryMultiplier);
			
			double computeImpactResultThreshold = Util.computeImpactResultThreshold(Thresholds.INSTANCE.getGravity(), complementaryMultiplier);
			Thresholds.INSTANCE.setImpact(computeImpactResultThreshold);
		}
	}
}
