/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config.Detection.Result.Thresholds;
import android.content.res.Resources;

public class ConfigDelegate {

	private final Resources resources;
	
	public ConfigDelegate(Resources resources) {
		this.resources = resources;
	}

	/*public void setImpactThreshold() {
//		String impact_threshold_multiplier__value_default = resources.getString(R.string.impact_threshold_multiplier__value_default);
		String impact_sensitivity_threshold__value = 
				
				resources.getString(R.string.impact_sensitivity_threshold__default_value);
		String impact_sensitivity_threshold__multiplier__max_value = resources.getString(R.string.impact_sensitivity_threshold_multiplier__max_value);
		float multiplier = Float.parseFloat(impact_sensitivity_threshold__multiplier__max_value) - Float.parseFloat(impact_sensitivity_threshold__value);
		Ln.d("impact_sensitivity_threshold__multiplier__max_value ["+impact_sensitivity_threshold__multiplier__max_value+"] - impact_sensitivity_threshold__value ["+impact_sensitivity_threshold__value+"] = "+multiplier);
		Ln.d("Config.Detection.Impact.IMPACT_THRESHOLD_FOR_GRAVITY_MULTIPLIER:"+SensorEventProcessorUtils.AccelerationDecimalFormatOneDigit.format(multiplier));
		Config.Detection.Impact.setImpactThresholdForGravityMultiplier(multiplier);
	}*/
	
	public void setDetectorParameters() {
		int partialResultsSubInterval = setDetectorThresholds();
		setDetectorSubIntervals(partialResultsSubInterval);
	}
	private int setDetectorThresholds() {
		int activityWalkMax = resources.getInteger(R.integer.minimum_activity_walk_max_threshold);
		int activityRunMax = resources.getInteger(R.integer.minimum_activity_run_max_threshold);
		int activityShakeMax = resources.getInteger(R.integer.minimum_activity_shake_max_threshold);
		int postureWalkMax = resources.getInteger(R.integer.minimum_posture_walk_max_threshold);
		
		int partialResult = resources.getInteger(R.integer.partial_results);
		
		/*for (int i = partialResult; i >= 1; i--) {
			int aw = activityWalk/partialResult*i;
			if (aw==1) {
				aw++;
			}
			int ar = activityRun/partialResult*i;
			if (ar==1) {
				ar++;
			}
			int as = activityShake/partialResult*i;
			if (as==1) {
				as++;
			}
			int pw = postureWalk/partialResult*i;
			if (pw==1) {
				pw++;
			}
			Thresholds thresholds = new net.iubris.sosmandown.config.Config.Detection.Result.Thresholds(aw, ar, as, pw);
//			Ln.d("adding "+i+":"+thresholds);
			net.iubris.sosmandown.config.Config.Detection.Result.PartialThresholds.put(i, thresholds);
		}*/
		
		
		int i = 1;
		int aw = activityWalkMax/partialResult;
		int ar = activityRunMax/partialResult;
		int as = /*(activityShake/partialResult)*2;*/ activityShakeMax;
		int pw = postureWalkMax/partialResult;
		Thresholds thresholds = new net.iubris.sosmandown.config.Config.Detection.Result.Thresholds(aw, ar, as, pw);
//Ln.d("adding "+i+":"+thresholds);
		net.iubris.sosmandown.config.Config.Detection.Result.PartialThresholds.put(1, thresholds);
		for (i=2; i<=partialResult; i++) {
			aw = (activityWalkMax/partialResult*i)-1;
			ar = (activityRunMax/partialResult*i)-1;
			// we want more sensitivityness to shaking
//			as = (activityShakeMax/partialResult+i/i);
			as = activityShakeMax-1;
			pw = (postureWalkMax/partialResult*i)-1;
			
			thresholds = new net.iubris.sosmandown.config.Config.Detection.Result.Thresholds(aw, ar, as, pw);
//Ln.d("adding "+i+":"+thresholds);
			net.iubris.sosmandown.config.Config.Detection.Result.PartialThresholds.put(i, thresholds);
		}
		
		return partialResult;
	}
	private static void setDetectorSubIntervals(int partialResultsSubInterval) {
		net.iubris.sosmandown.config.Config.Detection.Detecting.SUB_INTERVALS = partialResultsSubInterval;		
	}
}
