/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main.google;

import java.util.Random;

import net.iubris.sosmandown.config.Config;
import roboguice.util.Ln;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.drive.Drive;

public class GoogleApiClientDelegate implements GoogleApiClientProvider {
	
	private static final Random RANDOM_CLIENT_ID_GENERATOR = new Random( Long.valueOf(Config.APP_NAME_INTERNAL/*+"_"+androidID*/, Character.MAX_RADIX) );

	private final FragmentActivity activity;
	
	private GoogleApiClient googleApiClient;

	public GoogleApiClientDelegate(FragmentActivity activity) {
		this.activity = activity;
	}
	

	@Override
	public void stop() {
		if (googleApiClient!=null) {
			googleApiClient.stopAutoManage(activity);
		}
	}

	@Override
	public void disconnect() {
		if (googleApiClient!=null) {
			googleApiClient.disconnect();
		}
	}

	@Override
	public GoogleApiClient buildGoogleApiClient(ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
		if (googleApiClient == null) {
			int clientId = Math.abs( RANDOM_CLIENT_ID_GENERATOR.nextInt() );
			Ln.d("using clientId: "+clientId);
			Builder builder = new GoogleApiClient.Builder(activity);
			Ln.d("builder:"+builder);
            googleApiClient = builder 
            		.enableAutoManage(activity, clientId, onConnectionFailedListener)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addScope(Drive.SCOPE_APPFOLDER) // required for App Folder sample
                    .addConnectionCallbacks(connectionCallbacks)
                    .addOnConnectionFailedListener(onConnectionFailedListener)
                    .build();
        }		
		return googleApiClient;
	}
	public void disconnectGoogleApiClient() {
		if (googleApiClient!=null) {
			googleApiClient.stopAutoManage(activity);
			googleApiClient.disconnect();
		}
	}

	@Override
	public GoogleApiClient getGoogleApiClient() {
		return googleApiClient;
	}
}
