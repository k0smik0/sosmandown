/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main;


import java.util.Date;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.application.SOSManDownApplication;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.model.result.LightComputedResult;
import net.iubris.sosmandown.model.state.phase.PhaseState;
import net.iubris.sosmandown.savers.detections.DetectionsSaverDialoger;
import net.iubris.sosmandown.service.SensorsService;
import net.iubris.sosmandown.subscriptions.PublisherSubscriberDelegate;
import net.iubris.sosmandown.subscriptions.lightevents.LightComputedResultSubscriber;
import net.iubris.sosmandown.subscriptions.postures.PostureSubscriber;
import net.iubris.sosmandown.subscriptions.waveletprocessed.ActivityTypeSubscriber;
import net.iubris.sosmandown.ui.activities.info.LicensesActivity;
import net.iubris.sosmandown.ui.activities.main.IntentActionDelegate.IntentAction;
import net.iubris.sosmandown.ui.activities.main.MenuDelegate.MenuAction;
import net.iubris.sosmandown.ui.activities.main.buttons.ButtonChartActivityOnClickListener;
import net.iubris.sosmandown.ui.activities.main.buttons.ButtonDetectionStartStopOnClickListener;
import net.iubris.sosmandown.ui.activities.main.countdown.clicklistener.ButtonSOSOnClickListener;
import net.iubris.sosmandown.ui.activities.main.google.GoogleApiClientDelegate;
import net.iubris.sosmandown.ui.activities.main.google.GoogleApiClientDelegateProvider;
import net.iubris.sosmandown.ui.activities.main.permissions.PermissionsDispatcher;
import net.iubris.sosmandown.ui.activities.settings.SettingsActivity;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.StringsUtils;
import net.iubris.sosmandown.utils.ThreadsUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.SubscriberExceptionEvent;
import org.greenrobot.eventbus.ThreadMode;

import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import roboguice.util.Strings;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

//import com.android.debug.hv.ViewServer;

public class MainActivity extends RoboActionBarActivity 
implements LightComputedResultSubscriber, ActivityTypeSubscriber, PostureSubscriber, GoogleApiClientDelegateProvider {
	
	
	// here for init - they need Activity tough it is used in Service, so....
//	@Inject
//	private EventsSaver eventsSaver;
	@Inject
	private DetectionsSaverDialoger detectionsSaverDialoger;
//	@Inject
//	private DetectionsSaver detectionsSaver;

	@InjectView(R.id.button__detection_start_stop__start)
	private Button buttonDetectionStart;
	@InjectView(R.id.button__detection_start_stop__stop)
	private Button buttonDetectionStop;
	@Inject
	private ButtonDetectionStartStopOnClickListener buttonStartStopOnClickListener;
	
	@InjectView(R.id.button_sos)
	private Button buttonSOS;
	@Inject
	private ButtonSOSOnClickListener buttonSOSOnClickListener;
	

	@InjectView(R.id.file_saver__button_save_text)
	private TextView fileSaverButtonSaveText;
	@InjectView(R.id.file_saver__button_save)
	private Button buttonFileSaverSave;
	
	@InjectView(R.id.button_chart_activity)
	private Button buttonChartActivity;
	
	
	@InjectView(R.id.notification_status_bar__light_computed_result__text_content)
	private TextView notificationStatusBarLightComputedResultTextContent;


	@Inject
	private StatusBarCounterActionsDelegate statusBarCounterActionsDelegate;
		
	private IntentActionDelegate intentActionDelegate;
	
	@Inject
	private PublisherSubscriberDelegate subscriberDelegate;
	
	
//	@Inject
	private GoogleApiClientDelegate googleApiClientDelegate;
	private PermissionsDispatcher permissionsDispatcher;
	private boolean permissionGranted;
	
	@Inject
	private LightComputedStateWriterHelper lightComputedStateWriterHelper;
	
	@Inject
	private ButtonAndStatusBarDelegate buttonAndStatusBarDelegate;
	
	@Inject 
	private ThreadsUtil threadsUtil;

	TutorialDispatcher tutorialDispatcher;
	
	
	
//	private final Map<WorkingMajorState,String> workingMajorStateStringsMap = new HashMap<>();
//	private final Map<PhaseState,PhaseStateAction> phaseStateActionsMap = new HashMap<>();
	
	
	/*private static final String TUTORIAL_FIRST_NAME = "tutorial_1";
	private static final String TUTORIAL_SECOND_NAME = "tutorial_2";
	private static final String TUTORIAL_THIRD_NAME = "tutorial_3";
	private TutoShowcase tutorialFirst;
	private TutoShowcase tutorialSecond;
	private TutoShowcase tutorialThird;*/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Ln.d(this+", onCreate - started: "+new Date());
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
//		Debug.startMethodTracing(Config.APP_NAME);

		SOSManDownApplication.setLogLevel();

		SToast.with(this);

//		ViewServer.get(this).addWindow(this);

		ConfigDelegate configDelegate = new ConfigDelegate(getResources());
		configDelegate.setDetectorParameters();
		
		buttonSOS.setOnClickListener(buttonSOSOnClickListener);
		
		buttonDetectionStart.setOnClickListener(buttonStartStopOnClickListener);
		buttonDetectionStop.setOnClickListener( buttonStartStopOnClickListener);
		
		buttonFileSaverSave.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
//				eventsSaver.save();				
				detectionsSaverDialoger.showPromptDialog();
			}
		});
		
		buttonChartActivity.setOnClickListener( new ButtonChartActivityOnClickListener(this) );
		
		intentActionDelegate = new IntentActionDelegate(this, buttonSOS, buttonSOSOnClickListener, buttonDetectionStart);
		
		
//		unlockScreenInDEV();
		unlockScreen();

		googleApiClientDelegate = new GoogleApiClientDelegate(this);
		
		permissionsDispatcher = new PermissionsDispatcher(this);
		boolean waitForRequest = permissionsDispatcher.request();
		Ln.d(waitForRequest);
		this.permissionGranted = !waitForRequest;
		
		tutorialDispatcher = new TutorialDispatcher(this/*, sharedPreferences*/);
//		tutorialDelegate.buildTutorials();
		
		handleIntentClick( getIntent() );
		
		Ln.d("onCreate - completed: "+new Date());
	}
	

//	@SuppressLint("Override")
	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		Ln.d("requestCode:"+requestCode+", permissions:"+Strings.join(StringsUtils.COMMA, permissions));
		if (PermissionsDispatcher.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS == requestCode) {
			this.permissionGranted = permissionsDispatcher.onRequestPermissionsResult(permissions, grantResults);
		} else {
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}
	

	@Override
	protected void onStart() {
//		Ln.d("onStart");
		super.onStart();
		subscriberDelegate.registerSubscription(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
//		sharedPreferences.edit().clear().commit();
		
		if (this.permissionGranted) {
			doOnResume();
		}
	}
	private void doOnResume() {
		Ln.d("doOnResume");
//		SOSManDownApplication.setLogLevel();
		
//		tutorialDispatcher.dismissAllTutorials();
		
//		ViewServer.get(this).setFocusedWindow(this);
		if (!SensorsService.isStarted()) {
			buttonDetectionStart.performClick();
			restoreStatusIfFallDetectionStarted();
			
			/*threadsUtil.execOnUIWithDelay( new Runnable() {
				@Override
				public void run() {
					getSupportActionBar().openOptionsMenu();
				}
			}, 1000);*/
			
			
//			startTutorialForFirstRun();
		} else {
//			buttonStartStopOnClickListener.autoSetText();
//			tutorialDispatcher.dismissAllTutorials();
			threadsUtil.execOnUI( new Runnable() {
				@Override
				public void run() {
					restoreStatusIfFallDetectionStarted();
					
//					startTutorialAgainIfNotCompleted();
				}
			});
//			IntentActionDelegate.launchButtonIntent(IntentActionDelegate.INTENT_ACTION_PERFORM_CLICK_ON_DETECTION_BUTTON_START, this);
		}
		
//		Intent i = new Intent(this, LicensesActivity.class);
//		startActivity(i);
		
		// DEV
//		else { testSettingsActivity(); }
//		else { testChartActivity(); }
//		else { testCreditsActivity(); }
		
		startTutorialForFirstRun();
	}
	
	
	private void restoreStatusIfFallDetectionStarted() {
//		Ln.d("restoring layout");
		// data && layout
		buttonAndStatusBarDelegate.restoreBarsStatusOnDetectionStarted();
	}
	
	private void startTutorialForFirstRun() {
		threadsUtil.execOnUIWithDelay(new Runnable() {
			@Override
			public void run() {
				/*if (!sharedPreferences.contains(TutorialDispatcher.TUTORIAL_FIRST_RUN_COMPLETED)) {
					tutorialDispatcher.resetTutorialFirstTime(MainActivity.this);
					tutorialDispatcher.startTutorialFirstTime();
				}*/
//				openOptionsMenu();
				tutorialDispatcher.startTutorialForMenuFirstRun();
			}
		}, 3500);
	}
	/*private void startTutorialAgainIfNotCompleted() {
		threadsUtil.execOnUIWithDelay(new Runnable() {
			@Override
			public void run() {
				if (!sharedPreferences.contains(TutorialDispatcher.TUTORIAL_FIRST_RUN_COMPLETED)) {
//					tutorialDispatcher.resetTutorialFirstTime(MainActivity.this);
					tutorialDispatcher.startTutorialForHelp();
				}
			}
		}, 2500);
	}*/
	

	// test zone - start
	@SuppressWarnings("unused")
	private void testSettingsActivity() {
		buttonDetectionStart.performClick();
		threadsUtil.execOnUIWithDelay(startSettingsActivityRunnable, 4000);
	}
	private final Runnable startSettingsActivityRunnable = new Runnable() {
		@Override
		public void run() {
			Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
			MainActivity.this.startActivity(intent);
		}
	};
	
	@SuppressWarnings("unused")
	private void testCreditsActivity() {
		Intent intent = new Intent(this, LicensesActivity.class);
		startActivity(intent);
	}
	@SuppressWarnings("unused")
	private void testChartActivity() {
		buttonDetectionStart.performClick();
		threadsUtil.execOnUIWithDelay(startChartActivityRunnable, 4000);
	}
	private final Runnable startChartActivityRunnable = new Runnable() {
		@Override
		public void run() {
			buttonChartActivity.performClick();
		}
	};
	// test zone - end
	
	@Override
	protected void onNewIntent(Intent intent) {
Ln.d("onNewIntent:"+intent);
		if (this.permissionGranted) {
			handleIntentClick(intent);
		}
	}
	private void handleIntentClick(Intent intent) {
		String action = intent.getAction();
		IntentAction intentAction = intentActionDelegate.get(action);
		if (intentAction!=null) {
			intentAction.doOnAction(action);
		}
		
		/*if ("showTutorialForHelp".equalsIgnoreCase( intent.getStringExtra("methodToCall")) ) {
			showTutorialForHelp();
			setIntent(null);
		}*/
	}
	
	
	@Override
	protected void onStop() {
		super.onStop();
		subscriberDelegate.unregisterSubscription(this);
	}
	@Override
	protected void onDestroy() {
		googleApiClientDelegate.disconnectGoogleApiClient();
		subscriberDelegate.unregisterSubscription(this);
		super.onDestroy();
//		ViewServer.get(this).removeWindow(this);
	}
	
	// subscriber for notification Status bar - begin
//	private MajorState currentMajorState;
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedLightComputedResult(LightComputedResult lightComputedResult) {
		PhaseState phaseState = lightComputedResult.getPhaseState();
//		Ln.d("phaseState:"+phaseState);
		lightComputedStateWriterHelper.getPhaseStateActionsMap().get(phaseState).doStuff(lightComputedResult, this);
	}
	
	/*Map<WorkingMajorState, String> getWorkingMajorStateStringsMap() {
		return workingMajorStateStringsMap;
	}*/
	
	
	/*void setRightTextOnDetectionStarted() {
		Ln.d("setting visibility to Fall Detected");
		
		// just a trick: visibility/gone does not work for this view and I have no more time to waste on android trifles
		((TextView)findViewById(R.id.notification_status_bar__detected_fall_number_prefix_)).setText(getResources().getString(R.string.notification_bar__detected_fall_number_prefix));
		((TextView)findViewById(R.id.notification_status_bar__detected_fall_number_suffix_)).setText("]");
	}*/	
	
	/*void updateUIAndSaver(final String toPrint, LightComputedResult lightComputedResult) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
//				textView.setText(toPrint);
				notificationStatusBarLightComputedResultTextContent.setText(toPrint);
			}
		});
		LightComputedResultSaver.INSTANCE.setCurrent(lightComputedResult);
	}*/
	
	
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedActivityTypeOnFallDetection(final ActivityType activityType) {
//		Ln.d("received:"+activityType);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				int counter = activityType.getCounter();
				statusBarCounterActionsDelegate.getActivityTypeAction(activityType).act(counter);
			}
		});
	}
	
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedPostureOnFallDetection(final Posture posture) {
		Ln.d("received:"+posture);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				int counter = posture.getCounter();
				statusBarCounterActionsDelegate.getPostureAction(posture).act(counter);
			}
		});
	}
	
	@Subscribe
	@Override
	public void onEvent(SubscriberExceptionEvent subscriberExceptionEvent) {
		Ln.d("subscriberExceptionEvent: event:"
				+subscriberExceptionEvent.causingEvent+",subscriber:"
				+subscriberExceptionEvent.causingSubscriber+",eventBus:"+subscriberExceptionEvent.eventBus);
	}
	// subscriber for notification Status bar - end
	
	
	// menu section - begin
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Inject
	SharedPreferences sharedPreferences;
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		/*Set<?> all = sharedPreferences.getAll().entrySet();
		for (Object a : all) {
			Entry<String, ?> e = ((Entry<String, ?>)a);
			Ln.d(e.getKey()+" "+e.getValue());
		}*/
		
		MenuAction defaultMenuAction = new MenuAction() {
			@Override
			public boolean act(MainActivity mainActivity) {
				return MainActivity.super.onOptionsItemSelected(item);
			}
		};
		MenuAction menuAction = MenuDelegate.menuArray.get(item.getItemId(), defaultMenuAction);
		return menuAction.act(this);
	}
	// menu section - end
	
	
	public void unlockScreen() {
		Window window = this.getWindow();        
        window.addFlags(LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(LayoutParams.FLAG_TURN_SCREEN_ON);
	}
	/*private void unlockScreenInDEV() {
		if (Config.DEV) {
			unlockScreen();
		}
    }*/

	// googleapi section - begin
	@Override
	public GoogleApiClientDelegate getGoogleApiClientDelegate() {
		return googleApiClientDelegate;
	}
	// googleapi section - end
	
	
	
	/*private void buildTutorials() {
		tutorialThird = TutoShowcase.from(MainActivity.this)
				.setContentView(R.layout.activity_main__tutorial__3_detecion_fall_area)
				.on(R.id.notification_status_bar__activity_recognizer__text_content__walking)
				.addRoundRect(1.5f, -350, -240, 480, 250).withBorder()
				.onClickContentView(R.id.tutorial_button_third, new OnClickListener() {
					@Override
					public void onClick(View v) {
						Ln.d("third next");
						tutorialThird.dismiss();
					}
				});
		
		tutorialSecond = TutoShowcase.from(MainActivity.this)
				.setContentView(R.layout.activity_main__tutorial__2_fall_detected_area)
				.on(R.id.notification_status_bar__light_computed_result__text_content)
				.addRoundRect(0.01f, -150, -180, 500, 10).withBorder()
				.onClickContentView(R.id.tutorial_button_second, new OnClickListener() {
					@Override
					public void onClick(View v) {
						Ln.d("second next");
						tutorialSecond.dismiss();
						tutorialThird.showOnce(TUTORIAL_THIRD_NAME);
					}
				});
		
		tutorialFirst = TutoShowcase
				.from(MainActivity.this)
				// just for develop
				.resetTutorial(TUTORIAL_FIRST_NAME)
				.resetTutorial(TUTORIAL_SECOND_NAME)
				.resetTutorial(TUTORIAL_THIRD_NAME)
				.setContentView(R.layout.activity_main__tutorial__1_button_countdown)
				.on(R.id.button_sos).addCircle(1.0f, 0, -150).withBorder()
				.onClickContentView(R.id.tutorial_button_first, new OnClickListener() {
					@Override
					public void onClick(View v) {
//						Ln.d("tutorial first");
						tutorialFirst.dismiss();
						tutorialSecond.showOnce(TUTORIAL_SECOND_NAME);
					}
				});
	}*/
}
