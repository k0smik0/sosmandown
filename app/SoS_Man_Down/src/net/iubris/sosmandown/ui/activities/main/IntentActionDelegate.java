/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main;

import java.util.HashMap;
import java.util.Map;

import net.iubris.sosmandown.ui.activities.main.countdown.clicklistener.ButtonSOSOnClickListener;

import org.roboguice.shaded.goole.common.base.Predicate;

import android.content.Context;
import android.content.Intent;
import android.widget.Button;

public class IntentActionDelegate {

	public static final String INTENT_PREFIX = "net.iubris.sosmandown";
	public static final String INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_START = INTENT_PREFIX+".INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_START";
	public static final String INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_STOP =  INTENT_PREFIX+".INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_STOP";
	public static final String INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_START_IN_DEV_MODE =  INTENT_PREFIX+".INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_START_IN_DEV_MODE";
	
	public static final String INTENT_ACTION_PERFORM_CLICK_ON_DETECTION_BUTTON_START = INTENT_PREFIX+".INTENT_ACTION_PERFORM_CLICK_ON_DETECTION_BUTTON_START";
	
	private final Map<String,IntentAction> intentClickActionsMap = new HashMap<>();
	
	private final MainActivity activity;
	private final Button buttonSOS;
	private final ButtonSOSOnClickListener buttonSOSOnClickListener;
	
	private final Button buttonDetection;
	
	
	public IntentActionDelegate(MainActivity activity, Button buttonSOS, ButtonSOSOnClickListener buttonSOSOnClickListener, Button buttonDetection) {
		this.activity = activity;
		this.buttonSOS = buttonSOS;
		this.buttonSOSOnClickListener = buttonSOSOnClickListener;
		this.buttonDetection = buttonDetection;
		populateIntentClickActionsMap();
	}

	IntentAction get(String action) {
		return intentClickActionsMap.get(action);
	}
	
	private void populateIntentClickActionsMap() {
		intentClickActionsMap.put(INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_START, new IntentAction() {
			@Override
			public void doOnAction(String action) {
				if (predicateIsNotRunningCountdown.apply(action)) {
					activity.unlockScreen();
					onButtonSOSIntentClick();
				}
			}
		});
		intentClickActionsMap.put(INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_STOP, new IntentAction() {
			@Override
			public void doOnAction(String action) {
				if (predicateIsRunningCountdown.apply(action)) {
					onButtonSOSIntentClick();
				}
			}
		});
		intentClickActionsMap.put(INTENT_ACTION_PERFORM_CLICK_ON_SOS_BUTTON_START_IN_DEV_MODE, new IntentAction() {
			@Override
			public void doOnAction(String action) {
				if (predicateIsRunningCountdown.apply(action)) {
					activity.unlockScreen();
					onButtonSOSIntentClick();
				}
			}
		});
		intentClickActionsMap.put(INTENT_ACTION_PERFORM_CLICK_ON_DETECTION_BUTTON_START, new IntentAction() {
			@Override
			public void doOnAction(String action) {
				activity.unlockScreen();
				onButtonDetectionIntentClick();
			}
		});
	}
	
	private void onButtonSOSIntentClick() {
		buttonSOS.performClick();
		buttonSOS.invalidate();
		activity.setIntent(null);
	}
	private final Predicate<String> predicateIsRunningCountdown = new Predicate<String>() {
		@Override
		public boolean apply(String nullArg) {
			return buttonSOSOnClickListener.isRunningCountdown();
		}
	};
	private final Predicate<String> predicateIsNotRunningCountdown = new Predicate<String>() {
		@Override
		public boolean apply(String nullArg) {
			return !buttonSOSOnClickListener.isRunningCountdown();
		}
	};
	
	private void onButtonDetectionIntentClick() {
		buttonDetection.performClick();
		buttonDetection.invalidate();
		activity.setIntent(null);
	}
	
	interface IntentAction {
		void doOnAction(String action);
	}
	
	
	public static void launchButtonIntent(String intentAction, Context context) {
		Intent intent = new Intent(context, MainActivity.class);
		intent.setAction(intentAction);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
}
