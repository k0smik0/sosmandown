/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.model.result.LightComputedResult;
import net.iubris.sosmandown.model.state.phase.PhaseState;
import net.iubris.sosmandown.model.state.phase.PhaseStateE;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.RoboGuice;
import roboguice.inject.ContextSingleton;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.app.Activity;
import android.widget.TextView;

@ContextSingleton
public class LightComputedStateWriterHelper {
	
	private final Map<PhaseState,PhaseStateAction> phaseStateActionsMap = new HashMap<>();
	
	@InjectView(R.id.notification_status_bar__light_computed_result__text_content)
	private TextView notificationStatusBarLightComputedResultTextContent;
	
	@Inject
	public LightComputedStateWriterHelper(Activity activity) {
		
		RoboGuice.getInjector(activity).injectViewMembers(this);
		
		
		if (phaseStateActionsMap.size()==0) {
			phaseStateActionsMap.put(PhaseStateE.INITIALIZING, new WriteInitTextPhaseStateAction(activity) {
				@Override
				protected TextView getNotificationStatusBarLightComputedResultTextContent() {
					return notificationStatusBarLightComputedResultTextContent;
				}
			});
			phaseStateActionsMap.put(PhaseStateE.INITIALIZED, new DoNothingPhaseStateAction() {});
			phaseStateActionsMap.put(PhaseStateE.STARTING, new DoNothingPhaseStateAction() {
				@Override
				public void doStuff(LightComputedResult lightComputedResult, Activity activity) {
	Ln.d("received:"+lightComputedResult.getPhaseState()+":"+lightComputedResult.getMajorState());
//					mainActivity.setRightTextOnDetectionStarted();
//					setRightTextOnDetectionStarted(activity);
				}
				
				/*void setRightTextOnDetectionStarted(Activity activity) {
					Ln.d("setting visibility to Fall Detected");
					// just a trick: visibility/gone does not work for this view and I have no more time to waste on android trifles
//					((TextView)activity.findViewById(R.id.notification_status_bar__detected_fall_number_prefix_)).setText(activity.getString(R.string.notification_bar__detected_fall_number_prefix));
//					((TextView)activity.findViewById(R.id.notification_status_bar__detected_fall_number_suffix_)).setText("]");
					
//					View statusBar = activity.findViewById(R.id.notification_status_bar__container_layout);
//					statusBar.setVisibility(View.GONE);
//					Ln.d("gone to visible");
//					statusBar.setVisibility(View.VISIBLE);
				}*/			
			});
			phaseStateActionsMap.put(PhaseStateE.STARTED, new WriteTextPhaseStateAction(activity) {
				@Override
				public void doStuff(LightComputedResult lightComputedResult, Activity activity) {
					super.doStuff(lightComputedResult, activity);
//					buttonAndStatusBarDelegate.restoreDataOnStatusBar();
//					Ln.d("data/layout completed");
				}
				@Override
				protected TextView getNotificationStatusBarLightComputedResultTextContent() {
					return notificationStatusBarLightComputedResultTextContent;
				}
			});
			
			/*Set<Entry<PhaseState, PhaseStateAction>> entrySet = phaseStateActionsMap.entrySet();
			for (Entry<PhaseState, PhaseStateAction> entry : entrySet) {
				Ln.d(entry.getKey()+":"+entry.getValue());
			}*/
		}
		
	}
	
	Map<PhaseState, PhaseStateAction> getPhaseStateActionsMap() {
		return phaseStateActionsMap;
	}

	interface PhaseStateAction {
		String getText(LightComputedResult lightComputedResult, Activity activity);
		void doStuff(LightComputedResult lightComputedResult, Activity activity);
	}
	static class DoNothingPhaseStateAction implements PhaseStateAction {
		@Override
		public void doStuff(LightComputedResult lightComputedResult, Activity activity) {}
		@Override
		public String getText(LightComputedResult lightComputedResult, Activity activity) { return StringsUtils.EMPTY; }
	}
	static abstract class AbstractWriteTextPhaseStateAction implements PhaseStateAction {
		
		private static enum MapHolder {
			INSTANCE;
			
			private final Map<WorkingMajorState,String> workingMajorStateStringsMap = new HashMap<>();			
			private boolean initialized;
			
			boolean isInitialized() {
				return initialized;
			}
			public void init(Activity activity) {
				workingMajorStateStringsMap.put(WorkingMajorStateEnum.INIT, activity.getString(R.string.state_init));
				workingMajorStateStringsMap.put(WorkingMajorStateEnum.RELAX, activity.getString(R.string.state_relax));
				workingMajorStateStringsMap.put(WorkingMajorStateEnum.FREE_FALL, activity.getString(R.string.state_free_fall));
				workingMajorStateStringsMap.put(WorkingMajorStateEnum.GOING_TO_IMPACT, activity.getString(R.string.state_going_to_impact));
				workingMajorStateStringsMap.put(WorkingMajorStateEnum.IMPACT, activity.getString(R.string.state_impact));
				workingMajorStateStringsMap.put(WorkingMajorStateEnum.DETECTING_FALL, activity.getString(R.string.state_detecting_fall));
				workingMajorStateStringsMap.put(WorkingMajorStateEnum.FALL, activity.getString(R.string.state_fall));
				
				this.initialized = true;				
			}
		}		

//		protected final TextView notificationStatusBarLightComputedResultTextContent;

		protected AbstractWriteTextPhaseStateAction(Activity activity) {
//			notificationStatusBarLightComputedResultTextContent = (TextView) activity.findViewById(R.id.notification_status_bar__light_computed_result__text_content);
//			Ln.d("notificationStatusBarLightComputedResultTextContent:"+notificationStatusBarLightComputedResultTextContent);
			if (!MapHolder.INSTANCE.isInitialized()) {
				MapHolder.INSTANCE.init(activity);
			}
		}
		abstract protected TextView getNotificationStatusBarLightComputedResultTextContent()/* {
			return notificationStatusBarLightComputedResultTextContent;
		}*/;
		protected static String getRawText(LightComputedResult lightComputedResult, /*MainActivity mainActivity,*/ String suffix) {
			WorkingMajorState majorState = lightComputedResult.getMajorState();
			
			String majorStateTranslated = MapHolder.INSTANCE.workingMajorStateStringsMap.get(majorState);
			
			double accelerationResultant = lightComputedResult.getAccelerationResultant();
			String accelerationResultantPrefixed =  getAccelerationResultantPrefixed(accelerationResultant);
			
			String toPrint = majorStateTranslated+suffix+accelerationResultantPrefixed;
			return toPrint;
		}
		
		private static String getAccelerationResultantPrefixed(double accelerationResultant) {
			String acceleration = " (G:"+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(accelerationResultant)+")";
			return acceleration;
		}

		protected void updateUIAndSaver(final String toPrint, LightComputedResult lightComputedResult, Activity activity) {
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
//					textView.setText(toPrint);
					getNotificationStatusBarLightComputedResultTextContent().setText(toPrint);
				}
			});
			LightComputedResultSaver.INSTANCE.setCurrent(lightComputedResult);
		}		
	}
	
	static abstract class WriteInitTextPhaseStateAction extends AbstractWriteTextPhaseStateAction implements PhaseStateAction {
		
		public WriteInitTextPhaseStateAction(Activity activity) {
			super(activity);
		}
		@Override
		public String getText(LightComputedResult lightComputedResult, Activity activity) {
			PhaseState phaseState = lightComputedResult.getPhaseState();
			String suffix = StringsUtils.SPACE+phaseState.getStep()+"% ";
			String toPrint = getRawText(lightComputedResult, /*activity,*/ suffix);
			return toPrint;
		}
		@Override
		public void doStuff(LightComputedResult lightComputedResult, Activity activity) {
			String toPrint = getText(lightComputedResult, activity);
Ln.d(toPrint);
			updateUIAndSaver(toPrint, lightComputedResult, activity);
		}
	}
	
	static abstract class WriteTextPhaseStateAction extends AbstractWriteTextPhaseStateAction  implements PhaseStateAction {		
		public WriteTextPhaseStateAction(Activity activity) {
			super(activity);
		}
		@Override
		public String getText(LightComputedResult lightComputedResult, Activity activity) {
			String toPrint = getRawText(lightComputedResult, /*activity,*/ StringsUtils.EMPTY);
			return toPrint;
		}
		@Override
		public void doStuff(LightComputedResult lightComputedResult, Activity activity) {
			final String toPrint = getText(lightComputedResult, activity);
//Ln.d("received:"+toPrint);			
			updateUIAndSaver(toPrint, lightComputedResult, activity);
		}		
	}
}
