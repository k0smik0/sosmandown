/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.chartdetails;

import java.util.Date;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.engine.computing_state.ComputedState;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.WaveletProcessedData;
import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.model.result.LightComputedResult;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.subscriptions.PublisherSubscriberDelegate;
import net.iubris.sosmandown.subscriptions.computed_result.ComputedResultSubscriber;
import net.iubris.sosmandown.subscriptions.postures.PostureSubscriber;
import net.iubris.sosmandown.subscriptions.waveletprocessed.WaveletProcessedDataSubscriber;
import net.iubris.sosmandown.ui.fragments.FragmentChartAccelerationNormWithFSMComputedResult;
import net.iubris.sosmandown.ui.fragments.FragmentChartPostureRecognition;
import net.iubris.sosmandown.ui.fragments.FragmentCheckboxForMainActivityCountdown;
import net.iubris.sosmandown.ui.fragments.FragmentEventDetails;
import net.iubris.sosmandown.ui.fragments.FragmentSpectrogramActivityTypeRecognition;
import net.iubris.sosmandown.ui.utils.SToast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.SubscriberExceptionEvent;
import org.greenrobot.eventbus.ThreadMode;

import roboguice.activity.RoboActionBarActivity;
import roboguice.util.Ln;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class ChartsActivity extends RoboActionBarActivity implements ComputedResultSubscriber, WaveletProcessedDataSubscriber, PostureSubscriber {
	
	private static final int MAX_CHARTED_VALUES_COUNT_IN_MAIN_CHART = 7000;
	
	private FragmentChartAccelerationNormWithFSMComputedResult fragmentComputedResultStateChart;
	private FragmentSpectrogramActivityTypeRecognition fragmentSpectrogram;
	private FragmentChartPostureRecognition fragmentChartPostureRecognition;
	private FragmentEventDetails fragmentEventDetails;
	private FragmentCheckboxForMainActivityCountdown fragmentCheckboxCountdown;
	
	@Inject
	private PublisherSubscriberDelegate publisherSubscriberDelegate;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_charts);
		
		initFragments();
		
		SToast.with(this);
        
        // TODO remove
        Ln.d( new Date() );
	}
	
	private void initFragments() {
		final FragmentManager supportFragmentManager = getSupportFragmentManager();
		
		fragmentComputedResultStateChart = initFragment(supportFragmentManager,fragmentComputedResultStateChart, FragmentChartAccelerationNormWithFSMComputedResult.class, FragmentChartAccelerationNormWithFSMComputedResult.TAG);
//		fragmentChartActivityRecognition = initFragment(supportFragmentManager, fragmentChartActivityRecognition, FragmentChartActivityRecognitionByWaveletTransform.class, FragmentChartActivityRecognitionByWaveletTransform.TAG);
		fragmentSpectrogram = initFragment(supportFragmentManager, fragmentSpectrogram, FragmentSpectrogramActivityTypeRecognition.class, FragmentSpectrogramActivityTypeRecognition.TAG);
		fragmentChartPostureRecognition = initFragment(supportFragmentManager, fragmentChartPostureRecognition, FragmentChartPostureRecognition.class, FragmentChartPostureRecognition.TAG);
		fragmentEventDetails = initFragment(supportFragmentManager, fragmentEventDetails, FragmentEventDetails.class, FragmentEventDetails.TAG);
		if (Config.TEST_or_DEV) {
			fragmentCheckboxCountdown = initFragment(supportFragmentManager, fragmentCheckboxCountdown, FragmentCheckboxForMainActivityCountdown.class, FragmentCheckboxForMainActivityCountdown.TAG);
		}
        
        FragmentTransaction transaction = supportFragmentManager.beginTransaction();
        addFragment(transaction, fragmentComputedResultStateChart, R.id.frame__fragment_chart__computed_result, FragmentChartAccelerationNormWithFSMComputedResult.TAG);
        // TODO restore
//        addFragment(transaction, fragmentChartActivityRecognition, R.id.frame__fragment_chart__activity_recognizer, FragmentChartActivityRecognitionByWaveletTransform.TAG);
        addFragment(transaction, fragmentSpectrogram, R.id.frame__fragment_chart__activity_recognizer_spectrogram, FragmentSpectrogramActivityTypeRecognition.TAG); 
        addFragment(transaction, fragmentChartPostureRecognition, R.id.frame__fragment_chart__posture_recognizer, FragmentChartPostureRecognition.TAG);
        addFragment(transaction, fragmentEventDetails, R.id.frame__fragment__details, FragmentEventDetails.TAG);
        if (Config.TEST_or_DEV) {
        	addFragment(transaction, fragmentCheckboxCountdown, R.id.frame__fragment__checkbox_countdown, FragmentCheckboxForMainActivityCountdown.TAG);
        }
    	transaction.commit();
	}
	
	private boolean isAnyFragmentNull() {
		boolean anyMandatoryFragmentNull = false;
		if (fragmentComputedResultStateChart==null || 
//			fragmentChartActivityRecognition==null ||
			fragmentSpectrogram == null ||
			fragmentChartPostureRecognition==null ||
			fragmentEventDetails==null) {
			anyMandatoryFragmentNull = true;
		}
		if (Config.TEST_or_DEV && fragmentCheckboxCountdown==null) {
			anyMandatoryFragmentNull = true;
		}
		return anyMandatoryFragmentNull;
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (isAnyFragmentNull()) {
			initFragments();
		}
		
		publisherSubscriberDelegate.registerSubscription(this);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
	}
	
	
	@Override
	protected void onStop() {
//		SensorsServiceUtils.unregisterReceiver(eventsReceiver, this);
//		EventBus.getDefault().unregister(this);
		publisherSubscriberDelegate.unregisterSubscription(this);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
		
		if (computedResult!=null) {
			ComputedState eventState = computedResult.getEventState();
			LightComputedResult lightComputedResult = new LightComputedResult(eventState.getPhase(), 
					eventState.getMajor(), 
					computedResult.getSensorEventDataHolder().resultant);
			publisherSubscriberDelegate.post(lightComputedResult);
		}
		super.onStop();
	}
	
	
	private WorkingMajorState majorStatePrevious = WorkingMajorStateEnum.RELAX;
	private ComputedResult computedResult;
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedComputedResult(final ComputedResult computedResult) {
		int chartedValuesCount = fragmentComputedResultStateChart.getChartedValuesCount();
		if (chartedValuesCount>MAX_CHARTED_VALUES_COUNT_IN_MAIN_CHART) {
			resetCharts();
			return;
		}
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				fragmentComputedResultStateChart.onDataReceived(computedResult);
				fragmentEventDetails.onEventReceived(computedResult);
				
				WorkingMajorState majorStateCurrent = computedResult.getEventState().getMajor();
				/*if (WorkingMajorStateEnum.RELAX.equals(majorStateCurrent) && WorkingMajorStateEnum.FALL.equals(majorStatePrevious)) {
					fragmentChartActivityRecognition.reset();
				}				
				majorStatePrevious = majorStateCurrent;*/
				
				if (!majorStatePrevious.equals(majorStateCurrent)) {
					if (WorkingMajorStateEnum.DETECTING_FALL.equals(majorStateCurrent)) {
//						Ln.d("would be reset activityType chart...");
//						fragmentChartActivityRecognition.reset();
						majorStatePrevious = majorStateCurrent;
					}
				}
				if (WorkingMajorStateEnum.RELAX.equals(majorStateCurrent)) {
					majorStatePrevious = majorStateCurrent;
				}
				
				
				ChartsActivity.this.computedResult = computedResult;
//				Ln.d("stored computedResult on instance field: "+comu);
			}
		});
	}
	
	
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedWaveletProcessedData(final WaveletProcessedData waveletProcessedData) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
//				fragmentProcessEventStateChart.onProcessEventReceived(processEvent);
				// TODO restore
//				fragmentChartActivityRecognition.onDataReceived(waveletProcessedData);
				
//				Ln.d("received: "+waveletProcessedData);
				
				fragmentSpectrogram.onDataReceived(waveletProcessedData);
			}
		});
	}
	
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	@Override
	public void onReceivedPostureOnFallDetection(final Posture posture) {
//		Ln.d("received: "+posture+":"+posture.getCounter());
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				fragmentChartPostureRecognition.onDataReceived(posture);
			}
		});
	}
	
	@Subscribe
	@Override
	public void onEvent(SubscriberExceptionEvent subscriberExceptionEvent) {
		Ln.d("subscriberExceptionEvent: event:"+subscriberExceptionEvent.causingEvent+",subscriber:"+subscriberExceptionEvent.causingSubscriber+",eventBus:"+subscriberExceptionEvent.eventBus);
	}
	
	public void resetCharts() {
		Intent intent = new Intent(this, ChartsActivity.class);
		startActivity(intent);
		finish();
	}
	
	@SuppressWarnings("unchecked")
	private static <F extends Fragment> F initFragment(FragmentManager fragmentManager, Fragment fragmentToAssign, Class<F> fragmentClass, String fragmentTag) {
		if (fragmentToAssign==null) {
        	fragmentToAssign = fragmentManager.findFragmentByTag(fragmentTag);
        }
		if (fragmentToAssign!=null) {
			return (F) fragmentToAssign;
		}
		try {
			fragmentToAssign = fragmentClass.newInstance();
			return (F) fragmentToAssign;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
        return null;
	}
	private static void addFragment(FragmentTransaction fragmentTransaction, Fragment fragment, int fragmentId, String fragmentTag) {
		if (!fragment.isAdded()) {
        	fragmentTransaction.add(fragmentId , fragment, fragmentTag);
        }
	}

	/*private ChartRunningState chartRunningState = ChartRunningState.PLAY;
	public ChartRunningState getChartRunningState() {
		return chartRunningState;		
	}
	public void setChartRunningState(ChartRunningState chartRunningState) {
		this.chartRunningState = chartRunningState;
	}
	public enum ChartRunningState {
		STOP,
		PAUSE,
		PLAY
	}*/
}
