/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.info;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

@ContentView(R.layout.activity_info)
public class InfoActivity extends RoboActionBarActivity {
	
	@InjectView(R.id.info__version)
	private TextView textviewVersion;
	
//	@InjectView(R.id.info__button_licenses)
//	private Button buttonLicenses;
	
//	@InjectView(R.id.info__button_feedback)
//	private Button buttonFeedback;
	@InjectResource(R.string.feedback__mail_subject)
	private String mailSubject;
	@InjectResource(R.string.feedback__mail_body)
	private String mailBody;
	@InjectResource(R.string.feedback__mail_sending)
	private String mailSending;
	
//	@InjectView(R.id.info__copyright_button)
//	private Button buttonCopyright;
//	private EasyLicensesDialog easyLicensesDialog;
	
//	private static final long MAX_TIME_TO_EPOCH = 2147483648L;
//	private static final int MAX_DAY_TIME = 3; // = 24/24+60/60+60/60;
	
	/*private static final String GOOGLE_PLUS_URL = "https://plus.google.com/b/113141749716320691899";
	private static final int PLUS_ONE_REQUEST_CODE = 0;
	@InjectView(R.id.plus_one_button)
	private PlusOneButton plusOneButton;*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String text = Config.BUILD_VERSION;
		if (Config.TEST_or_DEV) {
			text+=" ["+Config.BUILD_TIMESTAMP+"]";
		}
		textviewVersion.setText(text);
		
		/*buttonFeedback.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sendFeedbacksMail();
			}
		});*/
		
		/*buttonLicenses.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
//				SToast.with(InfoActivity.this).show("");
				Intent intent = new Intent(InfoActivity.this, CreditsActivity.class);
				startActivity(intent);
			}
		});*/
		
		
		/*easyLicensesDialog = new EasyLicensesDialog(InfoActivity.this);
		easyLicensesDialog.setTitle("License");
		easyLicensesDialog.setCancelable(true);
		buttonCopyright.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				easyLicensesDialog.show();
			}
		});*/
	}
	
	public void emailRaffaele(View view) {
		sendEmail("raffone@gmail.com", "info SOSManDownApp", StringsUtils.EMPTY);
	}
	public void emailMassimiliano(View view) {
		sendEmail("massimiliano.leone@iubris.net", "info SOSManDownApp", StringsUtils.EMPTY);
	}
	private void sendEmail(String address, String subject, String body) {
		Intent intent = new Intent(Intent.ACTION_SENDTO);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.setData(Uri.parse("mailto:" + address));
		startActivity(intent);
	}
	
	@SuppressWarnings("unused")
	public void webMassimiliano(View view) {
		Intent webIntent = new Intent(Intent.ACTION_VIEW);
		webIntent.setData(Uri.parse("http://www.iubris.net"));
		startActivity(webIntent);
	}
	
	@SuppressWarnings("unused")
	public void sendFeedbacksMail(View view) {
		String recepientEmail = "sosmandownapp@gmail.com";
		sendEmail(recepientEmail, mailSubject, mailBody);
	}
	
	@SuppressWarnings("unused")
	public void licenses(View view) {
		Intent intent = new Intent(InfoActivity.this, LicensesActivity.class);
		startActivity(intent);
	}
	
	
	
	
	

}
