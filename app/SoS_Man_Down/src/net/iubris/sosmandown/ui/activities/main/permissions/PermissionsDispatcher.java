/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main.permissions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.ui.activities.main.IntentActionDelegate;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.util.Ln;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class PermissionsDispatcher {
	
	public final static int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
	private final Activity activity;
//	private boolean granted;
	
	
	private final static Map<String,String> permissionToDescriptionMap = new HashMap<>();
	static {
		permissionToDescriptionMap.put(Manifest.permission.ACCESS_FINE_LOCATION, "GPS");
		permissionToDescriptionMap.put(Manifest.permission.ACCESS_COARSE_LOCATION, "NETWORK_POSITION");
		permissionToDescriptionMap.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, "EXTERNAL_STORAGE");
		permissionToDescriptionMap.put(Manifest.permission.INTERNET, "INTERNET");
		permissionToDescriptionMap.put(Manifest.permission.ACCESS_NETWORK_STATE, "NETWORK_STATE");
//		permissionToDescriptionMap.put(Manifest.permission.ACCESS_WIFI_STATE, "WIFI_STATE");
		permissionToDescriptionMap.put(Manifest.permission.SEND_SMS, "SMS");
		permissionToDescriptionMap.put(Manifest.permission.VIBRATE, "VIBRATE");
		permissionToDescriptionMap.put(Manifest.permission.SEND_SMS, "SMS");
	}
	
	public PermissionsDispatcher(Activity activity) {
		this.activity = activity;
	}

//	CountDownLatch latch = null;
	public boolean request() {
//		final AtomicBoolean waitForRequest = new AtomicBoolean(false);
		boolean waitForRequest = false;
		final List<String> permissionsList = new ArrayList<>();
		final List<String> permissionsNeeded = new ArrayList<>();
	    
	    for (Map.Entry<String,String> entry: permissionToDescriptionMap.entrySet()) {
	    	if (!isPermissionGrantedOrAdd(entry.getKey(), permissionsList)) {
	    		String value = entry.getValue();
Ln.d("adding "+value);
	    		permissionsNeeded.add(value);
	    	}
	    }
	    
	 
	    int permissionsNeededSize = permissionsNeeded.size();
	    if (permissionsList.size() > 0 && permissionsNeededSize > 0) {
//	    	latch = new CountDownLatch(permissionsNeededSize);
            // Need Rationale
	    	String prefix = activity.getResources().getString(R.string.permissions__request);
            String message = prefix+StringsUtils.SPACE+ permissionsNeeded.get(0);
            for (int i = 1; i < permissionsNeeded.size(); i++) {
                message = message + ", " + permissionsNeeded.get(i);
            }
//Ln.d(message);
            showMessageOKCancel(message,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        	ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
//                        	waitForRequest.set(true);
                        }
                    });
//            waitForRequest = true;
        }
	    if (permissionsNeededSize>0) {
	    	waitForRequest = true;
	    }
	    /*if (permissionsList.size()>0) {
	    	ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
	    	waitForRequest = true;
	    }*/
        return waitForRequest;
	}	
	private boolean isPermissionGrantedOrAdd(String permission, List<String> permissionsList) {
//		Ln.d("internet: "+ContextCompat.checkSelfPermission(activity, Manifest.permission.INTERNET));
//		Ln.d("checking: "+permission);
//		if ( ContextCompat.checkSelfPermission(activity, Manifest.permission.INTERNET)==0 ) {
//			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.INTERNET}, REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
//		}
	    if ( ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
Ln.d("adding: "+permission);
	        permissionsList.add(permission);
	        // Check for Rationale Option
	        if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
	            return false;
	        }
	    }
	    return true;
	}
	private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
	    new AlertDialog.Builder(activity)
	            .setMessage(message)
	            .setPositiveButton(activity.getResources().getString(R.string.button_ok), okListener)
	            .setNegativeButton(activity.getResources().getString(R.string.button_cancel), null)
	            .create()
	            .show();
	}
	
	
	public boolean onRequestPermissionsResult(/*int requestCode,*/ String[] permissions, int[] grantResults) {
        Map<String, Integer> perms = new HashMap<>();
        // Initial
        perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.INTERNET, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.ACCESS_NETWORK_STATE, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.SEND_SMS, PackageManager.PERMISSION_GRANTED);
        perms.put(Manifest.permission.VIBRATE, PackageManager.PERMISSION_GRANTED);
        // Fill with results
        for (int i = 0; i < permissions.length; i++) {
            perms.put(permissions[i], grantResults[i]);
        }
        // Check for all permissions
        boolean granted = true;
        for (Entry<String, Integer> entry: perms.entrySet()) {
        	Integer result = entry.getValue();
        	if (result != PackageManager.PERMISSION_GRANTED) {
        		granted = false;
Ln.d("no granted for: "+entry.getKey());
        	}
        	/*if (latch!=null) {
        		latch.countDown();
        	}*/
        }
        if (!granted) {
        	SToast.with(activity).showLong(activity.getResources().getString(R.string.permissions__not_granted));
        } else {
        	IntentActionDelegate.launchButtonIntent(IntentActionDelegate.INTENT_ACTION_PERFORM_CLICK_ON_DETECTION_BUTTON_START, activity);
        }
//        this.granted = granted;
        Ln.d("returning granted:"+granted);
        return granted;
        
        /*if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                && perms.get(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            // All Permissions Granted
            // use location
        } else {
            // Permission Denied
        	SToast.with(activity).show("Some Permission is Denied");
        }*/
	}
	
	/*public boolean isGranted() {
		return granted;
	}*/
	
	
}
