/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main;

import net.iubris.sosmandown.R;
import roboguice.util.Ln;
import android.view.View;
import android.view.View.OnClickListener;

import com.github.florent37.tutoshowcase.TutoShowcase;

//@ContextSingleton
public class TutorialDispatcher {
	
//	private MainActivity mainActivity;
	
	/*private static final String TUTORIAL_FIRST_NAME_FIRST_RUN = "tutorial_1_first_run";
	private static final String TUTORIAL_SECOND_NAME_FIRST_RUN = "tutorial_2_first_run";
	private static final String TUTORIAL_THIRD_NAME_FIRST_RUN = "tutorial_3_first_run";
	private final TutoShowcase tutorialFirstForFirstRun;
	private final TutoShowcase tutorialSecondForFirstRun;
	private final TutoShowcase tutorialThirdForFirstRun;*/
	
	private final TutoShowcase tutorialFirstForHelp;
	private final TutoShowcase tutorialSecondForHelp;
	private final TutoShowcase tutorialThirdForHelp;
	private final TutoShowcase tutorialMenuForHelp;
	
	static final String TUTORIAL_FIRST_RUN_COMPLETED = "tutorial_first_run_completed";
	private static final String TUTORIAL_HELP = "tutorial__first_run__help"; 

//	@Inject
	public TutorialDispatcher(final MainActivity mainActivity/*, final SharedPreferences sharedPreferences*/) {
//		this.mainActivity = mainActivity;
	/*}

	void buildTutorials() {*/
		/*OnClickListener onClickListenerThirdForFirstRun = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Ln.d("tutorial third");
				tutorialThirdForFirstRun.dismiss();
				tutorialThirdForFirstRun.doNotShowAnymore(TUTORIAL_THIRD_NAME_FIRST_RUN);
				
				sharedPreferences.edit().putString(TUTORIAL_FIRST_RUN_COMPLETED, TUTORIAL_FIRST_NAME_FIRST_RUN).apply();
			}
		};
		tutorialThirdForFirstRun = TutoShowcase.from(mainActivity)
			.setContentView(R.layout.activity_main__tutorial__3_detecion_fall_area)
//			.onClickContentView(R.layout.activity_main__tutorial__3_detecion_fall_area, onClickListenerThirdForFirstRun)
			.on(R.id.notification_status_bar__activity_recognizer__text_content__walking)
			.addRoundRect(1.5f, -350, -200, 480, 250).withBorder()
			.onClickContentView(R.id.tutorial_button_third, onClickListenerThirdForFirstRun);
		
		final OnClickListener onClickListenerSecondForFirstRun = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Ln.d("tutorial second");
				tutorialSecondForFirstRun.dismiss();
				tutorialSecondForFirstRun.doNotShowAnymore(TUTORIAL_SECOND_NAME_FIRST_RUN);
				tutorialThirdForFirstRun.showOnce(TUTORIAL_THIRD_NAME_FIRST_RUN);
//				tutorialThirdForFirstRun.show();
			}
		};		
		tutorialSecondForFirstRun = TutoShowcase.from(mainActivity)
			.setContentView(R.layout.activity_main__tutorial__2_fall_detected_area)
//			.onClickContentView(R.layout.activity_main__tutorial__2_fall_detected_area, onClickListenerSecondForFirstRun)
			.on(R.id.notification_status_bar__light_computed_result__text_content)
//			.addRoundRect()
			.addRoundRect(1.01f, 0, 550, 530, 50).withBorder()
			.onClickContentView(R.id.tutorial_button_second, onClickListenerSecondForFirstRun);
		
		final OnClickListener onClickListenerFirstForFirstRun = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Ln.d("tutorial first");
				tutorialFirstForFirstRun.dismiss();
				tutorialFirstForFirstRun.doNotShowAnymore(TUTORIAL_FIRST_NAME_FIRST_RUN);
				tutorialSecondForFirstRun.showOnce(TUTORIAL_SECOND_NAME_FIRST_RUN);
//				tutorialSecondForFirstRun.show();
//				buildSecondForFirstRun(mainActivity).showOnce(TUTORIAL_SECOND_NAME_FIRST_RUN);
			}
		};		
		tutorialFirstForFirstRun = TutoShowcase.from(mainActivity)
			// just for develop
//			.resetTutorial(TUTORIAL_FIRST_NAME)
//			.resetTutorial(TUTORIAL_SECOND_NAME)
//			.resetTutorial(TUTORIAL_THIRD_NAME)
			// end for develop
			.setContentView(R.layout.activity_main__tutorial__1_button_countdown)
//			.onClickContentView(R.layout.activity_main__tutorial__1_button_countdown, onClickListenerFirstForFirstRun)
			
			.on(R.id.button_sos).addCircle(1.0f, 0, -150).withBorder()
			.onClickContentView(R.id.tutorial_button_first, onClickListenerFirstForFirstRun);*/
		
		
		
		tutorialThirdForHelp = TutoShowcase.from(mainActivity)
			.setContentView(R.layout.activity_main__tutorial__3_detecion_fall_area)
			.on(R.id.notification_status_bar__activity_recognizer__text_content__walking)
			.addRoundRect(1.5f, -350, -240, 480, 250).withBorder()
			.onClickContentView(R.id.tutorial_button_third, new OnClickListener() {
				@Override
				public void onClick(View v) {
					Ln.d("help: tutorial third");
					tutorialThirdForHelp.dismiss();
				}
			});
		
		tutorialSecondForHelp = TutoShowcase.from(mainActivity)
			.setContentView(R.layout.activity_main__tutorial__2_fall_detected_area)
			.on(R.id.notification_status_bar__light_computed_result__text_content)
			.addRoundRect(0.01f, -150, -180, 530, 10).withBorder()
			.onClickContentView(R.id.tutorial_button_second, new OnClickListener() {
				@Override
				public void onClick(View v) {
					Ln.d("help: tutorial second");
					tutorialSecondForHelp.dismiss();
					tutorialThirdForHelp.show();
				}
			});
		
		tutorialFirstForHelp = TutoShowcase.from(mainActivity)
			// just for develop
//			.resetTutorial(TUTORIAL_FIRST_NAME)
//			.resetTutorial(TUTORIAL_SECOND_NAME)
//			.resetTutorial(TUTORIAL_THIRD_NAME)
			.setContentView(R.layout.activity_main__tutorial__1_button_countdown)
			.on(R.id.button_sos).addCircle(1.0f, 0, -150).withBorder()
			.onClickContentView(R.id.tutorial_button_first, new OnClickListener() {
				@Override
				public void onClick(View v) {
					Ln.d("help: tutorial first");
					tutorialFirstForHelp.dismiss();
					tutorialSecondForHelp.show();
				}
			});
		
		tutorialMenuForHelp = TutoShowcase.from(mainActivity)
			.setContentView(R.layout.activity_main__tutorial__menu)
			.on(R.id.action_bar).addCircle(0.3f, 400, -20).withBorder()
			.onClickContentView(R.id.tutorial_button_ok, new OnClickListener() {
				@Override
				public void onClick(View v) {
					Ln.d("help: tutorial menu");
					tutorialMenuForHelp.dismiss();
//					tutorialSecondForHelp.show();
				}
			});
	}
	/*private TutoShowcase buildSecondForFirstRun(MainActivity mainActivity) {
		View view = mainActivity.findViewById(R.id.notification_status_bar__light_computed_result__text_content);
		return TutoShowcase.from(mainActivity)
		.setContentView(R.layout.activity_main__tutorial__2_fall_detected_area)
//		.onClickContentView(R.layout.activity_main__tutorial__2_fall_detected_area, onClickListenerSecondForFirstRun)
//		.on(R.id.notification_status_bar__light_computed_result__text_content)
		.on(view)
		.addRoundRect()
//		.addRoundRect(0.01f, 0, 750, 530, 50).withBorder()
		.onClickContentView(R.id.tutorial_button_second, new OnClickListener() {
			@Override
			public void onClick(View v) {
				tutorialSecondForFirstRun.dismiss();
				tutorialSecondForFirstRun.doNotShowAnymore(TUTORIAL_SECOND_NAME_FIRST_RUN);
				tutorialThirdForFirstRun.showOnce(TUTORIAL_THIRD_NAME_FIRST_RUN);				
			}
		});
	}*/
	
	/*void resetTutorialFirstTime(MainActivity mainActivity) {
		TutoShowcase.from(mainActivity)
		.resetTutorial(TUTORIAL_FIRST_NAME_FIRST_RUN)
		.resetTutorial(TUTORIAL_SECOND_NAME_FIRST_RUN)
		.resetTutorial(TUTORIAL_THIRD_NAME_FIRST_RUN);
	}*/
	/*void dismissAllTutorials() {
		tutorialFirstForFirstRun.dismiss();
		tutorialSecondForFirstRun.dismiss();
		tutorialThirdForFirstRun.dismiss();
	}*/
	
	void startTutorialForMenuFirstRun() {
//		tutorialFirstForFirstRun.showOnce(TUTORIAL_FIRST_NAME_FIRST_RUN);
		tutorialMenuForHelp.showOnce(TUTORIAL_HELP);
//		tutorialMenuForHelp.show();
//		tutorialFirstForFirstRun.show();
	}
	void startTutorialForHelp() {
		/*tutorialFirst.resetTutorial(TUTORIAL_FIRST_NAME);
		tutorialSecond.resetTutorial(TUTORIAL_SECOND_NAME);
		tutorialThird.resetTutorial(TUTORIAL_THIRD_NAME);*/
//		Ln.d("starting first once...");
		tutorialFirstForHelp.show();
	}
}
