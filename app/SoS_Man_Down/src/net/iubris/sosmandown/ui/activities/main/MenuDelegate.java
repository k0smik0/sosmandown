/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.service.SensorsService;
import net.iubris.sosmandown.ui.activities.info.InfoActivity;
import net.iubris.sosmandown.ui.activities.settings.SettingsActivity;
import roboguice.util.Ln;
import android.content.Intent;
import android.util.SparseArray;

public class MenuDelegate {

	public static final SparseArray<MenuAction> menuArray = new SparseArray<>();
	
	/*public static final MenuAction defaultAction = new MenuAction() {
		@Override
		public boolean act(MainActivity mainActivity) {
			return mainActivity.super.onOptionsItemSelected(item);
		}
	};*/
	
	static {
		menuArray.put(R.id.action_settings, new MenuAction() {
			@Override
			public boolean act(MainActivity mainActivity) {
				mainActivity.startActivity( new Intent(mainActivity, SettingsActivity.class) );
				return true;
			}
		});
		
		menuArray.put(R.id.action_help, new MenuAction() {
			@Override
			public boolean act(MainActivity mainActivity) {
				Ln.d("launching tutorial...");
				mainActivity.tutorialDispatcher.startTutorialForHelp();
				return true;
			}
		});
		menuArray.put(R.id.action_info, new MenuAction() {
			@Override
			public boolean act(MainActivity mainActivity) {
				mainActivity.startActivity( new Intent(mainActivity, InfoActivity.class) );
				return true;
			}
		});
		menuArray.put(R.id.action_exit , new MenuAction() {
			@Override
			public boolean act(MainActivity mainActivity) {
				SensorsService.stop(mainActivity);
//				Debug.stopMethodTracing();
				mainActivity.finish();
				return true;
			}
		});
		
//		Debug.stopMethodTracing();
	}
	public static MenuAction getAction(int menuItemId) {
		return menuArray.get(menuItemId);
	}
	public interface MenuAction {
		boolean act(MainActivity mainActivity);
	}
}
