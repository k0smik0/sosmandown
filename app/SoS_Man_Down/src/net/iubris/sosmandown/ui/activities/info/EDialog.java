/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.info;

import java.io.File;

import net.iubris.sosmandown.R;
import roboguice.util.Ln;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class EDialog extends AlertDialog {
	
	private static final String HTTP_PROTOCOL = "http://";
	private static final String HTTPS_PROTOCOL = "https://";
	private static final String OK = "OK";
	
	private static final String ASSETS_PROTOCOL = "file:///android_asset";

	public Context context;
	AlertDialog.Builder alertDialogBuilder;
	
	private final OnClickListener onClickListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int id) {
			dialog.cancel();
		}
	};
	private final WebViewClient webViewClient = new WebViewClient() {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url==null) {
				return false;
			}
			if (url.startsWith(HTTP_PROTOCOL) || url.startsWith(HTTPS_PROTOCOL)) {
				view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				return true;
			}
			return false;
		}
		@Override
	    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
Ln.d("errorCode:"+errorCode+", description:"+description+", failingUrl:"+failingUrl);
			String url = buildUrl(LANGUAGE_DEFAULT);
			view.loadUrl(url);
	    }
	};
	private static final String LANGUAGE_DEFAULT = "en";
	private String language;
	private static final String licenseFile = "gplv3.html";

	public EDialog(Context context) {
		super(context);
		this.context = context;
		this.alertDialogBuilder = new AlertDialog.Builder(context);
		
		this.alertDialogBuilder.setPositiveButton(OK, onClickListener);
		String title = context.getString(R.string.license);
		this.alertDialogBuilder.setTitle(title);
		this.alertDialogBuilder.setCancelable(true);
	}

	public void show(String language) {
		this.language = language;
		show();
	}
	
	@Deprecated
	@Override
	public void show() {
		
		WebView webView = buildWebView();
		
		this.alertDialogBuilder.setView(webView);
		this.alertDialogBuilder.show();
	}
	
	/*private int getScale(	Context context){
	    Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay(); 
	    int width = display.getWidth(); 
	    Double val = Double.valueOf(width)/Double.valueOf(PIC_WIDTH);
	    val = val * 100d;
	    Ln.d("val:"+val);
	    return val.intValue();
	}*/
	
	private String buildUrl(String language) {
		return ASSETS_PROTOCOL+File.separator+language+File.separator+licenseFile;
	}
	
	private WebView buildWebView() {
		WebView webView = new WebView(context);
		
		WebSettings settings = webView.getSettings();
		
		settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

		// fit to page
//		settings.setUseWideViewPort(true);
//		settings.setLoadWithOverviewMode(true);
		settings.setBuiltInZoomControls(true);
	    settings.setSupportZoom(true);
//		webView.setInitialScale(1);
		
		webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

		
		webView.setWebViewClient(webViewClient);
		webView.loadUrl( buildUrl(language) );
		
		return webView;
	}
}
