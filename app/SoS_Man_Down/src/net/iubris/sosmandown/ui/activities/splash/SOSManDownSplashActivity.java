/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.splash;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.ui.activities.main.MainActivity;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager.LayoutParams;

public class SOSManDownSplashActivity extends AbstractUlyssesSplashActivity {
	
//	private static final Class<?> thisClass = SOSManDownSplashActivity.class;
//	private static final String TAG = thisClass.getPackage().getName().toUpperCase(Locale.getDefault())+"/"+thisClass.getSimpleName();
	
	private static final int APPNAME_FADE_IN_TIME = 800;
////	private ImageView imageviewAnimation;
////	private TextView textviewAppname;
//	private long start;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Log.d(TAG,"onCreate");
		super.minDisplayMilliseconds = APPNAME_FADE_IN_TIME;
//		start = System.currentTimeMillis();
		super.onCreate(savedInstanceState);		
		
////	    textviewAppname = (TextView) findViewById(R.id.textview_splash_appname);
////	    imageviewAnimation = (ImageView)findViewById(R.id.imageviewAnimation);
	    
////	    imageviewAnimation.setBackgroundResource(R.drawable.wine_progress_anim);
	    unlockScreen();
	}
	
	@Override
	public void onWindowFocusChanged (boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        /*if (imageviewAnimation!=null) {
        	AnimationDrawable frameAnimation = (AnimationDrawable) imageviewAnimation.getBackground();
        	if (frameAnimation!=null) {
	        	if(hasFocus) {
	                frameAnimation.start();
	                if (textviewAppname!=null) {
	                	textviewAppname.animate().setDuration(APPNAME_FADE_IN_TIME).alphaBy(0).alpha(1);
	                }
	            } else {
	                frameAnimation.stop();
	            }
        	}
        }*/
	}
	
	@Override
	protected void startMainActivity() {
		super.startMainActivity();
		overridePendingTransition(R.anim.fade_out_900ms, R.anim.fade_in_300ms);
//		long stop = System.currentTimeMillis();
		
//		long delta = stop-start;
//		Ln.d("delta: "+delta/1000f);
	}

	@Override
	protected int getLayoutResID() {
		return R.layout.activity_splash;
	}

	@Override
	protected int getLauncherModeForMainActivity() {
		return Intent.FLAG_ACTIVITY_NEW_TASK;
	}

	@Override
	protected Class<? extends Activity> getActivityToStart() {
		return MainActivity.class;
	}
	
	@Override
	protected void doOtherStuffInForeground(Application application) {
//		Log.d(TAG,"in foreground");
	}
	@Override
	protected void doOtherStuffinBackground(Application application) {
//		Log.d(TAG,"in background");
	}
	
	private void unlockScreen() {
		Window window = this.getWindow();
		window.addFlags(LayoutParams.FLAG_DISMISS_KEYGUARD);
		window.addFlags(LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		window.addFlags(LayoutParams.FLAG_TURN_SCREEN_ON);
	}
}
