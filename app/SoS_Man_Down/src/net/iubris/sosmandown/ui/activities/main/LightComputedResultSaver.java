/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main;

import net.iubris.sosmandown.model.result.LightComputedResult;
import net.iubris.sosmandown.model.state.phase.PhaseStateE;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;

enum LightComputedResultSaver {
	INSTANCE;
	private LightComputedResult lightComputedResult = new LightComputedResult(PhaseStateE.INITIALIZING, WorkingMajorStateEnum.INIT, 0);

	public void setCurrent(LightComputedResult lightComputedResult) {
		this.lightComputedResult = lightComputedResult;
	}
	public LightComputedResult getCurrent() {
		return lightComputedResult;
	}
}
