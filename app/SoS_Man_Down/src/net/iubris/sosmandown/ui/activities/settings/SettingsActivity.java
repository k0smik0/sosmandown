/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.settings;

import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectResource;
import roboguice.util.Ln;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;

public class SettingsActivity extends RoboActionBarActivity {

	private static final int MAX_NUMBERS_WRONG = 3;

	private static final int MAX_NUMBERS_EMPTY = 3;

	@InjectResource(R.string.preference_dialog_warning_no_preferences)
	private String messageNoOnePreferencesSet;
	
	@InjectResource(R.string.preference_dialog_warning_no_user_name)
	private String messageNoUsername;
	
	@InjectResource(R.string.preference_dialog_warning_no_contacts_phone_number)
	private String messageNoContactsPhoneNumberSet;
	
	@InjectResource(R.string.preference_dialog_warning_wrong_phone_number)
	private String messageEmptyOrWrongPhoneNumber;
	
	private FragmentSettings settingsFragment;

	private AlertDialog alertDialog;
	
	@Inject
	private SharedPreferences sharedPreferences;
	
//	@InjectPreference(FragmentSettings.PREFERENCE_KEY_USERNAME)
//	private String preferenceUsername;
	

	// using fragment
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_settings);
		
		// Display the fragment as the main content.
		FragmentManager fragmentManager = getFragmentManager();
		settingsFragment = (FragmentSettings) fragmentManager.findFragmentByTag(FragmentSettings.TAG);
		
		if (settingsFragment==null) {
			settingsFragment = new FragmentSettings();
		}
		
		fragmentManager.beginTransaction().replace(R.id.preference_container, new FragmentSettings(), FragmentSettings.TAG).commit();
		
		final Builder alertDialogBuilder = new AlertDialog.Builder( this );
		alertDialogBuilder.setTitle(getString(R.string.preference_dialog_warning_title));
		alertDialog = alertDialogBuilder
			.setPositiveButton(getString(R.string.preference_dialog_button_ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {/*No Action*/
//							SettingsActivity.this.onBackPressed();
						SettingsActivity.this.realOnBackPressed();
					}
				})
			.setNegativeButton(getString(R.string.preference_dialog_button_cancel), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {/*No Action*/
//							Preference preference = null;
//							see http://stackoverflow.com/questions/4805896/how-to-open-or-simulate-a-click-on-an-android-preference-created-with-xml-prog
					}
				})
			.create();
	}
	
	public void showAlertDialog(String message) {
		alertDialog.setMessage(message);
		alertDialog.show();	
	}
	
	@Override
	public void onBackPressed() {
		Map<String, ?> preferencesMap  = sharedPreferences.getAll();
		for (Entry<String, ?> entry : preferencesMap.entrySet()) {
			Ln.d("key:"+entry.getKey()+",value:"+entry.getValue());
		}
		
		
		boolean isUsernameEmpty = false;
		int numberEmpty = 0;
		int numberWrong = 0;
		
		String preferenceUsernameValue = sharedPreferences.getString(FragmentSettings.PREFERENCE_KEY_USERNAME, StringsUtils.EMPTY);
		if (preferenceUsernameValue.isEmpty()) {
			isUsernameEmpty = true;
			Ln.d("username empty");
//			showAlertDialog(messageNoOnePreferencesSet);
//		}
//		if (isAnyEmpty) {
//			
//			showAlertDialog(messageNoOnePreferencesSet);
		} else {
			
			for (String phoneNumberKey: FragmentSettings.PREFERENCES_PHONE_NUMBERS) {
				String preferencePhoneNumberValue = sharedPreferences.getString(phoneNumberKey, StringsUtils.EMPTY);
				if (preferencePhoneNumberValue.isEmpty()) {
					numberEmpty++;
					Ln.d("phone empty from "+phoneNumberKey);
				}/* else {
					numberEmpty = false;
				}*/
				if ( !PhoneNumberUtils.isGlobalPhoneNumber(preferencePhoneNumberValue) || 
						!preferencePhoneNumberValue.matches(FragmentSettings.PATTERN_PHONE_NUMBER) ) {
					if (!PhoneNumberUtils.isGlobalPhoneNumber(preferencePhoneNumberValue)) {
						Ln.d(preferencePhoneNumberValue+" is not global phone number");
					}
					if (!preferencePhoneNumberValue.matches(FragmentSettings.PATTERN_PHONE_NUMBER)) {
						Ln.d(preferencePhoneNumberValue+" does not match pattern "+FragmentSettings.PATTERN_PHONE_NUMBER);
					}
					numberWrong++;
				}
			}
			Ln.d("empty numbers: "+numberEmpty+", wrong numbers:"+numberWrong);
			
			/*if (isAnyNumberEmpty) {
				showAlertDialog(messageNoContactsPhoneNumberSet);
			} else if (isAnyNumberWrong>0) {
				showAlertDialog(messageEmptyOrWrongPhoneNumber);
			}*/
		}
		
		if (isUsernameEmpty) {
			Ln.d("showing "+messageNoUsername);
			showAlertDialog(messageNoUsername);
		} else if (numberEmpty==MAX_NUMBERS_EMPTY) {
			Ln.d("showing '"+messageNoContactsPhoneNumberSet+"'");
			showAlertDialog(messageNoContactsPhoneNumberSet);
		} else if (numberWrong==MAX_NUMBERS_WRONG) {
			Ln.d("wrong numbers:"+numberWrong+" (MAX:"+MAX_NUMBERS_WRONG+")");
			Ln.d("showing '"+messageEmptyOrWrongPhoneNumber+"'");
			showAlertDialog(messageEmptyOrWrongPhoneNumber);
		} else {
//			super.onBackPressed();
			realOnBackPressed();
		}
//		
		/*if (preferencesMap.isEmpty()) {
			showAlertDialog(messageNoOnePreferencesSet);
		} else if (FragmentSettings.isNotNullNorEmpty(preferencesMap.get(FragmentSettings.PREFERENCE_KEY_USERNAME))) {
			showAlertDialog(messageNoUsername);
		} else if (!preferencesMap.isEmpty() && 
				!FragmentSettings.isNotNullNorEmpty(preferencesMap.get(FragmentSettings.PREFERENCE_KEY_USERNAME))) {
			showAlertDialog(messageNoContactsPhoneNumberSet);
		} else {
//			super.onBackPressed();
			realOnBackPressed();
		}*/
	}
	
	private void realOnBackPressed() {
		super.onBackPressed();
	}
}
