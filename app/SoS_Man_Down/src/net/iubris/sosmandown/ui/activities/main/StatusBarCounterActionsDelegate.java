/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.activities.main;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.RoboGuice;
import roboguice.inject.ContextSingleton;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.widget.TextView;

@ContextSingleton
public class StatusBarCounterActionsDelegate {
	
	@InjectView(R.id.notification_status_bar__activity_recognizer__text_content__walking_counter)
	private TextView activityWalkingCounter;
	@InjectView(R.id.notification_status_bar__activity_recognizer__text_content__running_counter)
	private TextView activityRunningCounter;
	@InjectView(R.id.notification_status_bar__activity_recognizer__text_content__shaking_counter)
	private TextView activityShakingCounter;
	
	@InjectView(R.id.notification_status_bar__posture_recognizer__text_content__sitting_counter)
	private TextView postureSittingCounter;
	@InjectView(R.id.notification_status_bar__posture_recognizer__text_content__standing_counter)
	private TextView postureStandingCounter;
	@InjectView(R.id.notification_status_bar__posture_recognizer__text_content__walking_counter)
	private TextView postureWalkingCounter;

	private final Map<ActivityType, Action> activityTypeActionsMap = new HashMap<>();
	private final Map<Posture, Action> postureActionsMap = new HashMap<>();
	
	@Inject
	public StatusBarCounterActionsDelegate(Activity activity) {
		RoboGuice.injectMembers(activity, this);
		
		populateActivityTypeActionsMap();
		populatePostureActionsMap();
	}
	
	private void populateActivityTypeActionsMap() {
		if (activityTypeActionsMap.size()>0) {
			return;
		}
		activityTypeActionsMap.put(ActivityType.RUN, new Action() {
			@Override
			public void act(int counter) {
				activityRunningCounter.setText(StringsUtils.EMPTY+counter);
			}
		});
		activityTypeActionsMap.put(ActivityType.WALK, new Action() {
			@Override
			public void act(int counter) {
				activityWalkingCounter.setText(StringsUtils.EMPTY+counter);
			}
		});
		activityTypeActionsMap.put(ActivityType.SHAKE, new Action() {
			@Override
			public void act(int counter) {
				activityShakingCounter.setText(StringsUtils.EMPTY+counter);
			}
		});
		activityTypeActionsMap.put(ActivityType.UNKNOWN, new Action() {
			@Override
			public void act(int counter) {}
		});
	}
	private void populatePostureActionsMap() {
		postureActionsMap.put(Posture.SITTING, new Action() {
			@Override
			public void act(int counter) {
				postureSittingCounter.setText(StringsUtils.EMPTY+counter);
			}
		});
		postureActionsMap.put(Posture.STANDING, new Action() {
			@Override
			public void act(int counter) {
				postureStandingCounter.setText(StringsUtils.EMPTY+counter);
			}
		});
		postureActionsMap.put(Posture.WALKING, new Action() {
			@Override
			public void act(int counter) {
				postureWalkingCounter.setText(StringsUtils.EMPTY+counter);
			}
		});
		/*postureActionsMap.put(Posture.FALL, new Action() {
			@Override
			public void act(int counter) {}
		});
		postureActionsMap.put(Posture.UNKNOWN, new Action() {
			@Override
			public void act(int counter) {}
		});*/
	}
	
	public Action getActivityTypeAction(ActivityType activityType) {
		return activityTypeActionsMap.get(activityType);
	}
	
	interface Action {
		void act(int counter);
	}

	public Action getPostureAction(Posture posture) {
		return postureActionsMap.get(posture);
	}
}
