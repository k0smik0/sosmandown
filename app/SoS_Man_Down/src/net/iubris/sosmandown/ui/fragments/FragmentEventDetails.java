/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.fragments;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.engine.computing_state.ComputedState;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils.Thresholds;
import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * @author "Massimiliano Leone - massimiliano.leone@iubris.net"
 * 
 * A fragment showing details about acceleration event (x,y,z,resultant,time, state according to model in {@link ComputedState} 
 */
public class FragmentEventDetails extends RoboFragment {

	public static final String TAG = FragmentEventDetails.class.getSimpleName();

	private static final int TO_SKIP_MAX = 4;
	
	@InjectView(R.id.result)
	private TextView textviewResult;
	@InjectView(R.id.event_x)
	private TextView textviewX;
	@InjectView(R.id.event_y)
	private TextView textviewY;
	@InjectView(R.id.event_z)
	private TextView textviewZ;
	@InjectView(R.id.state)
	private TextView textviewState;
	@InjectView(R.id.event_date)
	private TextView textviewDate;
	
	@InjectResource(R.color.accent_color)
	private int colorAccent;
	@InjectResource(R.color.primary_color)
	private int colorPrimary;
	@InjectResource(R.color.red)
	private int colorRed;
	
	@SuppressLint("SimpleDateFormat")
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.sss");


	private int counterSkip;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment__event_details, container, false);
		return rootView;
	}
	
	public void onEventReceived(ComputedResult resultEvent) {
		writeEventDetails(resultEvent);
	}
	
	
	private void writeEventDetails(ComputedResult resultEvent) {
		if (counterSkip<TO_SKIP_MAX) {
			counterSkip++;
			return;
		}
		counterSkip = 0;
		
		SensorEventDataHolder sensorEventDataHolder = resultEvent.getSensorEventDataHolder();
		float x = sensorEventDataHolder.x;
		textviewX.setText(StringsUtils.EMPTY+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(x));
					
		float y = sensorEventDataHolder.y;
		textviewY.setText(StringsUtils.EMPTY+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(y));
					
		float z = sensorEventDataHolder.z;
		textviewZ.setText(StringsUtils.EMPTY+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(z));
		
		WorkingMajorState major = resultEvent.getEventState().getMajor();
		textviewState.setText(major.name());
		if (major.equals(WorkingMajorStateEnum.IMPACT) || major.equals(WorkingMajorStateEnum.FALL)) {
			textviewState.setTextColor(colorAccent);
		} else {
			textviewState.setTextColor(colorPrimary);
		}

		double result = sensorEventDataHolder.resultant;
		textviewResult.setText(StringsUtils.EMPTY+SensorEventProcessorUtils.AccelerationDecimalFormatTwoDigit.format(result));
		if (result >= Thresholds.INSTANCE.getImpact()) {
			textviewResult.setTextColor(colorRed);
		} else {
			textviewResult.setTextColor(colorPrimary);
		}
		
		long timestampInMS = sensorEventDataHolder.timestamp;
		textviewDate.setText(StringsUtils.EMPTY+sdf.format( new Date(timestampInMS) ));
	}
	
}
