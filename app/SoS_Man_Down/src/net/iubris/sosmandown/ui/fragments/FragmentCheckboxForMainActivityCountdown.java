/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.fragments;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import roboguice.inject.SharedPreferencesProvider;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class FragmentCheckboxForMainActivityCountdown extends RoboFragment {

	public static final String TAG = FragmentCheckboxForMainActivityCountdown.class.getSimpleName();
	
	public static final String CHECKBOX_LAUNCH_MAINACTIVITY_COUNTDOWN_ON_FALL = "CHECKBOX_LAUNCH_MAINACTIVITY_COUNTDOWN_ON_FALL";	
	
	@InjectView(R.id.checkbox_launch_mainactivity_countdown_on_fall)
	private CheckBox checkBox;
	
	/*@InjectResource(R.string.button_run_charts_pause)
	private String buttonRunChartsPause;
	@InjectResource(R.string.button_run_charts_play)
	private String buttonRunChartsPlay;*/
	
	@Inject
	private SharedPreferencesProvider sharedPreferencesProvider;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment__chart__countdown_checkbox, container, false);
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				sharedPreferencesProvider.get().edit().putBoolean(CHECKBOX_LAUNCH_MAINACTIVITY_COUNTDOWN_ON_FALL, isChecked).commit();
			}
		});
		// as init
		boolean checkedFromPreference = sharedPreferencesProvider.get().getBoolean(FragmentCheckboxForMainActivityCountdown.CHECKBOX_LAUNCH_MAINACTIVITY_COUNTDOWN_ON_FALL, false);
		checkBox.setChecked(checkedFromPreference);
		
		
		/*buttonProcessEventChartStop.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO complete
//				ChartsActivity activityChartsWithDetails = (ChartsActivity)getActivity();
//				ChartRunningState chartRunningState = activityChartsWithDetails.getChartRunningState();
//				if (chartRunningState.equals(ChartRunningState.PLAY)) {
////					chartRunningState = ChartRunningState.PAUSE_BUT_DRAW_SEPARATOR;
//					activityChartsWithDetails.setChartRunningState(ChartRunningState.PAUSE);
//					buttonProcessEventChartStop.setText( buttonRunChartsPause );
//				}
//				if (chartRunningState.equals(ChartRunningState.STOP)) {
////					chartRunningState = ChartRunningState.PLAY;
//					activityChartsWithDetails.setChartRunningState(ChartRunningState.PLAY);
//					buttonProcessEventChartStop.setText( buttonRunChartsPlay );
//				}
			}
		});*/
		
		/*buttonTestDetection.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				eventBus.post( new ActionEventDetectionFallTest() );
			}
		});*/
	}
	
	public static class ActionEventDetectionFallTest {}
}
