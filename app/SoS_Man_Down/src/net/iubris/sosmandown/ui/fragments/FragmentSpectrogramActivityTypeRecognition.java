/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.fragments;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.ActivityTypeRecognizerByWaveletTransform;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.WaveletProcessedData;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType.ActivityTypeVisitor;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType.DefaultDoNothingActivityTypeVisitor;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class FragmentSpectrogramActivityTypeRecognition extends RoboFragment {

	public static final String TAG = FragmentSpectrogramActivityTypeRecognition.class.getSimpleName();
	
	@InjectView(R.id.activity_recognizer_spectogram_view)
	private SpectrogramView frequencyView;
	
	@InjectView(R.id.fragment_chart__activity_recognizer__counter__activity_run)
	private TextView counterRunning;
	@InjectView(R.id.fragment_chart__activity_recognizer__counter__activity_walk)
	private TextView counterWalking;
	@InjectView(R.id.fragment_chart__activity_recognizer__counter__activity_shake)
	private TextView counterShaking;

	@Inject
	private ActivityTypeRecognizerByWaveletTransform waveletTransformAnalyzer;

	private FragmentActivity activity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate( R.layout.fragment__chart__activity_recognizer__spectrogram , container, false);
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		activity = getActivity();
		
//		counterRunning = (TextView) activity.findViewById(R.id.fragment_chart__activity_recognizer__counter__activity_run);
//		counterWalking = (TextView) activity.findViewById(R.id.fragment_chart__activity_recognizer__counter__activity_walk);
//		counterShaking = (TextView) activity.findViewById(R.id.fragment_chart__activity_recognizer__counter__activity_shake);
//		Ln.d("counterRunning:"+counterRunning);
//		Ln.d("counterWalking:"+counterWalking);
//		Ln.d("counterShaking:"+counterShaking);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		waveletTransformAnalyzer.enableSendDataForChart();
	}
	
	@Override
	public void onStop() {
		waveletTransformAnalyzer.disableSendDataForChart();
		super.onStop();
	}
	
	public void onDataReceived(WaveletProcessedData waveletProcessedData) {
//		Ln.d("setting "+waveletProcessedData.getActivityType());
		frequencyView.setData(waveletProcessedData);
		activity.runOnUiThread(new Runnable() {
	         @Override
	         public void run() {
	        	frequencyView.invalidate();
	         }
		});
		ActivityType activityType = waveletProcessedData.getActivityType();
		activityType.acceptAndSetCounter(activityTypeVisitorForLegend);
	}
	private final ActivityTypeVisitor activityTypeVisitorForLegend = new DefaultDoNothingActivityTypeVisitor() {
		@Override
		public void visitRun(ActivityType activityType) {
			counterRunning.setText(activityType.getCounterAsString());
		}
		@Override
		public void visitWalk(ActivityType activityType) {
			counterWalking.setText(activityType.getCounterAsString());
		}
		@Override
		public void visitShake(ActivityType activityType) {
			counterShaking.setText(activityType.getCounterAsString());
		}
	};
}
