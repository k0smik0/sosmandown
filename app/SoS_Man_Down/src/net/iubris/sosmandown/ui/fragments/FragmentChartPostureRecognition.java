/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.fragments;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture.DefaultDoNothingPostureVisitor;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.spatial.posture.Posture.PostureVisitor;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.inject.InjectView;
//import roboguice.util.Ln;
import android.app.Activity;
import android.graphics.Color;
import android.widget.TextView;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

/**
 * @author "Massimiliano Leone - massimiliano.leone@iubris.net"
 * 
 * Chart fragment posture recognition chart 
 */
public class FragmentChartPostureRecognition extends AbstractFragmentChart<Posture> {
	
	public static final String TAG = FragmentChartPostureRecognition.class.getSimpleName();

	private static int X_VISIBLE_MIN = 10;
	private static int X_VISIBLE_MAX = 500;
	
	@InjectView(R.id.fragment_chart__posture_recognizer__counter__posture_walking)
	private TextView counterWalking;
	@InjectView(R.id.fragment_chart__posture_recognizer__counter__posture_sitting)
	private TextView counterSitting;
	@InjectView(R.id.fragment_chart__posture_recognizer__counter__posture_standing)
	private TextView counterStanding;
	
	
	private final Map<Posture,EntryComputerAction> entryComputerActionMap = new EnumMap<>(Posture.class);
	
	private LineDataSet lineDataSetSitting;
	private static final int COLOR_SITTING = Color.parseColor("#FFBB00"); // orange
	private LineDataSet lineDataSetStanding;
	private static final int COLOR_STANDING = Color.parseColor("#0000DD"); // blue
	private LineDataSet lineDataSetWalking;
	private static final int COLOR_WALKING = Color.parseColor("#00CC00"); // dark green
	private LineDataSet lineDataSetUnknown;
	private static final int COLOR_UNKNOWN = Color.GRAY;

	private static final String legendSitting = "sitting";
	private static final String legendStanding = "standing";
	private static final String legendWalking = "walking";
	private static final String legendUnknown = "unknown";

	public FragmentChartPostureRecognition() {
		super(X_VISIBLE_MIN, X_VISIBLE_MAX, 10);
		populateDrawerActionMap();
	}
	private void populateDrawerActionMap() {
		entryComputerActionMap.put(Posture.UNKNOWN, new UnknownPostureEntryComputerAction());
		entryComputerActionMap.put(Posture.SITTING, new SittingPostureEntryComputerAction());
		entryComputerActionMap.put(Posture.STANDING, new StandingPostureEntryComputerAction());
		entryComputerActionMap.put(Posture.WALKING, new WalkingPostureEntryComputerAction());
//		entryComputerActionMap.put(Posture.FALL, new PostureEntryComputerAction(OFFSET_FALL));
	}
	private abstract class AbstractPostureEntryComputerAction implements EntryComputerAction {
		protected static final int REPEATER = 5; 
		@Override
		public void addLineEntry(int incrementalEventForXCoordinate, float value) {
			
			LineDataSet lineDataSet = getLineDataSet();
			
			for (int i=0;i<=REPEATER;i++) {
				lineData.addXValue(StringsUtils.EMPTY+incrementalEventForXCoordinate);
				float entryValue = getEntryValue(value, i);
				AbstractFragmentChart.addLineEntry(entryValue, lineDataSet, incrementalEventForXCoordinate);
//Ln.d(this.getClass().getSimpleName()+", added: ("+incrementalEventForXCoordinate+","+entryValue+")");
				incrementalEventForXCoordinate++;
			}
			incrementalEventForXCoordinate++;
//			Ln.d("setting "+incrementalEventForXCoordinate+" to "+incrementalEventForBaseXCoordinate);
			FragmentChartPostureRecognition.this.incrementalEventForBaseXCoordinate = incrementalEventForXCoordinate;
		}
		protected abstract LineDataSet getLineDataSet();
		protected abstract float getEntryValue(float value, int index);
		protected abstract int getColorForLineDataSet();
	}
	private class SittingPostureEntryComputerAction extends AbstractPostureEntryComputerAction implements EntryComputerAction {
		private static final float OFFSET = 0.3f;
		@Override
		protected float getEntryValue(float value, int index) {
			return value+OFFSET;
		}
		@Override
		protected LineDataSet getLineDataSet() {
			return lineDataSetSitting;
		}
		@Override
		protected int getColorForLineDataSet() {
			return COLOR_SITTING; 
		}
	}
	private class StandingPostureEntryComputerAction extends AbstractPostureEntryComputerAction implements EntryComputerAction {
		@Override
		public void addLineEntry(int incrementalEventForXCoordinate, float value) {
			
			LineDataSet lineDataSet = getLineDataSet();

			lineData.addXValue(StringsUtils.EMPTY+incrementalEventForXCoordinate);
			for (int i=0;i<=REPEATER;i++) {
				float entryValue = getEntryValue(value, i);
				AbstractFragmentChart.addLineEntry(entryValue, lineDataSet, incrementalEventForXCoordinate);
//Ln.d(this.getClass().getSimpleName()+", added: ("+incrementalEventForXCoordinate+","+entryValue+")");
			}
			
			incrementalEventForXCoordinate++;
			
//			Ln.d("setting "+incrementalEventForXCoordinate+" to "+incrementalEventForBaseXCoordinate);
			FragmentChartPostureRecognition.this.incrementalEventForBaseXCoordinate = incrementalEventForXCoordinate;
		}
		@Override
		protected LineDataSet getLineDataSet() {
			return lineDataSetStanding;

		}
		@Override
		protected float getEntryValue(float value, int index) {
			return (value+index)/REPEATER;
		}
		@Override
		protected int getColorForLineDataSet() {
			return COLOR_STANDING;
		}
	}
	private class WalkingPostureEntryComputerAction extends AbstractPostureEntryComputerAction implements EntryComputerAction {
		@Override
		protected float getEntryValue(float value, int index) {
			return (value+index)/REPEATER;
		}
		@Override
		protected LineDataSet getLineDataSet() {
			return lineDataSetWalking;
		}
		@Override
		protected int getColorForLineDataSet() {
			return COLOR_WALKING;
		}
	}
	private class UnknownPostureEntryComputerAction extends AbstractPostureEntryComputerAction {
		@Override
		protected float getEntryValue(float value, int index) {
			return value;
		}
		@Override
		protected LineDataSet getLineDataSet() {
			return lineDataSetUnknown;
		}
		@Override
		protected int getColorForLineDataSet() {
			return COLOR_UNKNOWN;
		}
	}
	
	@Override
	protected int getLayoutResourceId() {
		return R.layout.fragment__chart__posture_recognizer;
	}
	@Override
	protected int getLinechartResourceId() {
		return R.id.posture_recognizer_chart;
	}
	
	@Override
	protected void drawEventLines(Posture posture, int incrementalEventForBaseXCoordinate) {
		EntryComputerAction entryComputerAction = entryComputerActionMap.get(posture);
		posture.accept(postureVisitorForLegend);
		entryComputerAction.addLineEntry(incrementalEventForBaseXCoordinate, 0);
		this.incrementalEventForBaseXCoordinate++;
	}
	private final PostureVisitor postureVisitorForLegend = new DefaultDoNothingPostureVisitor() {
		@Override
		public void visitSitting(Posture posture) {
			counterSitting.setText(posture.getCounterAsString());
		}
		@Override
		public void visitStanding(Posture posture) {
			counterStanding.setText(posture.getCounterAsString());
		}
		@Override
		public void visitWalking(Posture posture) {
			counterWalking.setText(posture.getCounterAsString());
		}
	};
	
	@Override
	protected int getToX(int xValCount, int xVisibleMax) {
		int	toX = 0;
		int diff = xValCount - xVisibleMax;
		if (diff>xVisibleMax) {
			toX = diff;
		}
		return toX;
	}
	
	private final List<LineDataSet> lineDataSets = new ArrayList<>();
	@Override
	protected void prepareChart(Activity activity, int linechartResourceId) {
		super.prepareChart(activity, linechartResourceId);
		
		YAxis axisLeft = chart.getAxisLeft();
		axisLeft.setDrawAxisLine(false);
		axisLeft.setShowOnlyMinMax(true);
		axisLeft.setEnabled(false);
		
//		lineDataSet = getLineDataSetOrBuildAndAddToLineData(5, COLOR_UNKNOWN, "posture");
//		lineDataSet.setDrawFilled(false);
		
		// dark green
		lineDataSetWalking = getLineDataSetOrBuildAndAddToLineData(0, COLOR_WALKING, legendWalking);
		lineDataSetWalking.setDrawFilled(false);
		lineDataSetWalking.setCircleColor(COLOR_WALKING);
		lineDataSets.add(lineDataSetWalking);
		
		lineDataSetStanding = getLineDataSetOrBuildAndAddToLineData(1, COLOR_STANDING, legendStanding);
		lineDataSetStanding.setDrawFilled(false);
		lineDataSetStanding.setCircleColor(COLOR_STANDING);
		lineDataSets.add(lineDataSetStanding);
		
		// orange
		lineDataSetSitting = getLineDataSetOrBuildAndAddToLineData(2, COLOR_SITTING, legendSitting);
		lineDataSetSitting.setDrawFilled(false);
		lineDataSetSitting.setCircleColor(COLOR_SITTING);
		lineDataSets.add(lineDataSetSitting);
		
		lineDataSetUnknown = getLineDataSetOrBuildAndAddToLineData(3, COLOR_UNKNOWN, legendUnknown);
		lineDataSetUnknown.setDrawFilled(false);
		lineDataSetUnknown.setCircleColor(COLOR_UNKNOWN);
		lineDataSets.add(lineDataSetUnknown);
		

		for (LineDataSet lineDataSet: lineDataSets) {
			lineDataSet.setCircleRadius(2);
			lineDataSet.setCircleHoleRadius(0);
			
			lineDataSet.setDrawCircles(true);
			lineDataSet.setLineWidth(0.000f);
			lineDataSet.enableDashedLine(0.1f, 1f, 0);
		}
		
		chart.getLegend().setEnabled(false);
	}
	
	
	@Override
	protected float getMinimumVerticalAxis() {
		return 0f;
	}
	@Override
	protected float getMaximumVerticalAxis() {
		return 1.5f;
	}
	private final OnChartValueSelectedListener onChartValueSelectedListener = new AbstractOnChartValueSelectedListener() {
		@Override
		protected String getAMessage(Posture posture) {
			return "act: "+posture.name();
		}
	};
	@Override
	protected OnChartValueSelectedListener getOnChartValueSelectedListener() {
		return onChartValueSelectedListener;
	}
	
	@Override
	public void reset() {
//		Ln.d("reset");
		incrementalEventForBaseXCoordinate = 0;
//		processed = 0;
		lineData.clearValues();
		lineDataSetSitting.clear();
		lineDataSetStanding.clear();
		lineDataSetWalking.clear();
		lineDataSetUnknown.clear();
		chart.clear();
		chart.notifyDataSetChanged();
		chart.postInvalidate();
	}
}
