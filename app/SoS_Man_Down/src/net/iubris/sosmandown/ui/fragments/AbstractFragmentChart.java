/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.ui.activities.chartdetails.ChartsActivity;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.StringsUtils;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectResource;
import roboguice.util.Ln;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

public abstract class AbstractFragmentChart<D> extends RoboFragment {
	

	@InjectResource(R.string.chart_waiting_data)
	private String waitingForData;
	
	protected final LineData lineData;
	
//	private static final float X_RANGE_MAX_DEFAULT = 200;
	private final int xVisibleRangeMax;
	private final int xVisibleRangeMin;
//	private final float xVisibleMin;
	private final int yVisibleRangeMax;
	
	protected LineChart chart;
	protected int incrementalEventForBaseXCoordinate = 0;

//	private final Map<ChartRunningState,ChartRunningStateAction<D>> chartRunningStateActionMap = new EnumMap<>(ChartRunningState.class);

	
	/*protected interface ChartRunningStateAction<D> {
		void act(ChartsActivity activityChartsWithDetails, D data);
	}*/

	public AbstractFragmentChart(int xVisibleRangeMin, int xVisibleRangeMax, int yVisibleRangeMax) {
		this.xVisibleRangeMin = xVisibleRangeMin;
		this.xVisibleRangeMax = xVisibleRangeMax;
		this.yVisibleRangeMax = yVisibleRangeMax;
		
		this.lineData = new LineData();
//		populateChartRunningStateActionMap(chartRunningStateActionMap);
	}
//	protected abstract void populateChartRunningStateActionMap(Map<ChartRunningState, ChartRunningStateAction<D>> chartRunningStateActionMap);
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		incrementalEventForBaseXCoordinate = 0;
		View rootView = inflater.inflate( getLayoutResourceId() , container, false);
		return rootView;
	}
	protected abstract int getLayoutResourceId();
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		onActivityCreatedInternal();
		
		prepareChart(getActivity(), getLinechartResourceId());
	}
	protected void onActivityCreatedInternal() {}
	protected abstract int getLinechartResourceId();
	protected void prepareChart(Activity activity, int linechartResourceId) {
		chart = (LineChart)activity.findViewById(linechartResourceId);
//		chart.setOnChartValueSelectedListener(this);
		chart.setOnChartValueSelectedListener(getOnChartValueSelectedListener());
        chart.setNoDataTextDescription(waitingForData);
        // enable touch gestures
        chart.setTouchEnabled(true);
        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setDrawGridBackground(false);
        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(true);
        // set an alternative background color
        chart.setBackgroundColor(Color.WHITE);
//        chart.setHighlightEnabled(true);
        chart.setHighlightPerTapEnabled(true);
        
        chart.setDescription(StringsUtils.EMPTY);
        
        chart.setHardwareAccelerationEnabled(true);
//        Ln.d("isHardwareAccelerated:"+chart.isHardwareAccelerated());
        
        chart.setVisibleXRangeMaximum(xVisibleRangeMax);
        chart.setVisibleYRangeMaximum(yVisibleRangeMax,AxisDependency.LEFT);
        
        chart.setData(lineData);
        
        lineData.setValueTextColor(Color.BLACK);
        
        // get the legend (only possible after setting data)
        Legend legend = chart.getLegend();
        // modify the legend ...
        legend.setPosition(LegendPosition.BELOW_CHART_LEFT);
        legend.setForm(LegendForm.LINE);
        legend.setTextColor(Color.BLACK);

        XAxis xl = chart.getXAxis();
////        xl.setTypeface(tf);
        xl.setTextColor(Color.BLACK);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        xl.setLabelsToSkip(100); // was 30k
        xl.setSpaceBetweenLabels(50); // was 50
        xl.setEnabled(true);

//        YAxis 
        YAxis verticalAxisLeft = chart.getAxisLeft();
        verticalAxisLeft.setTextColor(Color.BLACK);
        verticalAxisLeft.setAxisMaxValue(getMaximumVerticalAxis());
        verticalAxisLeft.setAxisMinValue(getMinimumVerticalAxis());
//        verticalAxisLeft.setStartAtZero(true);
        verticalAxisLeft.setDrawGridLines(true);
        verticalAxisLeft.setShowOnlyMinMax(false);
        

        YAxis verticalAxisRight = chart.getAxisRight();
//        verticalAxisRight.setDrawGridLines(true);
        verticalAxisRight.setEnabled(false);
        
        
        
        // old
//        LimitLine llGravity = new LimitLine((int)EventProcessorUtils.Thresholds.INSTANCE.getGravity(), "gravity");
//        llGravity.setTextSize(14f);
//        llGravity.setLineColor(Line.RESULT.getColor());
//        llGravity.enableDashedLine(1, 2, 0);
//        verticalAxis.addLimitLine(llGravity);
        
        // time
//        LimitLine llTime = new LimitLine(TIME_Y_VALUE, limitlineTime);
//        llTime.setTextSize(lineTextSize);
//        llTime.setLineColor(Line.TIME.getColor());
//        llTime.enableDashedLine(1, 10, 45);
//        verticalAxisLeft.addLimitLine(llTime);
	}

	protected abstract float getMinimumVerticalAxis();
	protected abstract float getMaximumVerticalAxis();
	protected LineDataSet getLineDataSetOrBuildAndAddToLineData(int index, int color, String name) {
		ILineDataSet set = lineData.getDataSetByIndex(index);
		if (set==null) {
			set = createLineDataSet(color, name);
			set.setVisible(true);
			lineData.addDataSet(set);
		}
		return (LineDataSet)set;
	}
	// OnChartValueSelectedListener area - start
	protected abstract class AbstractOnChartValueSelectedListener implements OnChartValueSelectedListener {
		@Override
		public void onNothingSelected() {}
		@SuppressWarnings("unchecked")
		@Override
		public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
			D data = (D)e.getData();
			if (data!=null) {
				String aMsg = getAMessage(data);
//				String aMsg = StringsUtils.EMPTY;
				String eMsg = StringsUtils.COMMA+e.toString()+": x:"+e.getXIndex()+",v:"+e.getVal();
				String hMsg = StringsUtils.COMMA+"H: x:"+h.getXIndex()+"ds"+h.getDataSetIndex()+"s:"+h.getStackIndex();
				SToast.with(getActivity()).showLong(aMsg+eMsg+hMsg);
			}			
		}
		@SuppressWarnings("unused")
		protected String getAMessage(D data) {
			return StringsUtils.EMPTY;
		}
	}
	 
	protected abstract OnChartValueSelectedListener getOnChartValueSelectedListener();
	// OnChartValueSelectedListener area - end
	
	
	public int getChartedValuesCount() {
		return incrementalEventForBaseXCoordinate;
	}
	
	public void onDataReceived(D data) {
		ChartsActivity activityChartsWithDetails = (ChartsActivity)getActivity();
		if (activityChartsWithDetails==null) {
			return;
		}
//		ChartRunningState chartRunningState = activityChartsWithDetails.getChartRunningState();
		/*if (!ChartRunningState.PAUSE.equals(chartRunningState)) {
			incrementalEventForXCoordinate++;
		}
		if (ChartRunningState.PLAY.equals(chartRunningState)) {
			drawResultEventLinesAndUpdate(computedResult, incrementalEventForXCoordinate);
		}
		if (ChartRunningState.PAUSE_BUT_DRAW_SEPARATOR.equals(chartRunningState)) {
			drawResultEventLinesAndUpdate(buildResultEventSeparator(computedResult.getSensorEventDataHolder().timestamp), incrementalEventForXCoordinate);
			chartRunningState = ChartRunningState.PAUSE;
		}*/
		
		/*ChartRunningStateAction<D> chartRunningStateAction = chartRunningStateActionMap.get(chartRunningState);
		try {
			chartRunningStateAction.act(activityChartsWithDetails, data);
		} catch(NullPointerException e) {
			Ln.d("chartRunningStateAction null!");
		}*/
		
//		Ln.d("drawing: "+data.getClass().getSimpleName());
		drawEventLinesAndUpdate(data/*, incrementalEventForXCoordinate*/);
	}
	
	protected void drawEventLinesAndUpdate(D data/*, int incrementalEventForXCoordinate*/) {
//		incrementalEventForXCoordinate++;
		drawEventLines(data, incrementalEventForBaseXCoordinate);
		incrementalEventForBaseXCoordinate++;
		updateChart();
	}
	protected abstract void drawEventLines(D event, int incrementalEventForBaseXCoordinate);
	
	protected static float addLineEntry(float value, LineDataSet set, int incrementalEventForXCoordinate) {
		Entry entry = new Entry(value, incrementalEventForXCoordinate);
		set.addEntry( entry );
		set.notifyDataSetChanged();
		return value;
	}
	protected static LineDataSet createLineDataSet(int color, String name) {
        LineDataSet set = new LineDataSet(new ArrayList<Entry>(), name);
        set.setAxisDependency(AxisDependency.LEFT);
        set.setColor(color);
//        set.setCircleColor(color-1);
        set.setLineWidth(2f);
//        set.setCircleSize(0f);
        set.setCircleRadius(0f);
//        set.setFillAlpha(200);
//        set.setFillColor(color);
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.BLACK);
//        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        return set;
    }
	protected void updateChart() {
		int xValCount = lineData.getXValCount();
        chart.notifyDataSetChanged();
        chart.setVisibleXRangeMaximum(xVisibleRangeMax);
        int toX = getToX(xValCount, xVisibleRangeMin);
        chart.moveViewToX( toX );
	}
	protected abstract int getToX(int xValCount, int xVisibleMax);
	
	protected void clearChart() {
		chart.clear();
	}
	public void reset() {
		Ln.d("reset");
		incrementalEventForBaseXCoordinate = 0;
		lineData.clearValues();
		List<ILineDataSet> dataSets = lineData.getDataSets();
		for (ILineDataSet iLineDataSet : dataSets) {
			iLineDataSet.clear();
		}
		chart.notifyDataSetChanged();
		chart.postInvalidate();
	}
	
	protected interface EntryComputerAction {
		void addLineEntry(int incrementalEventForXCoordinate, float d);
	}
}
