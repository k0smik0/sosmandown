/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
/**
 * Spectrogram Android application
 * Copyright (c) 2013 Guillaume Adam  http://www.galmiza.net/

 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the use of this software.
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:

 * 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

package net.iubris.sosmandown.ui.fragments;

import java.util.List;

import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.ActivityType;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.wavelet.model.WaveletProcessedData;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * credits: from https://bitbucket.org/galmiza/spectrogram-android
 * 
 * Class associated with the spectrogram view
 * Handles events:
 *  onSizeChanged, onTouchEvent, onDraw
 */
public class SpectrogramView extends View {
    
    private static final int FFT_RESOLUTION = 10;
	private static final int SAMPLING_RATE = 250; // 250Hz
	private static final float MULTIPLIER = 2;
	
	private static final int STROKE_WIDTH = 10;
	private static final int COLOR_BACKGROUND = Color.parseColor("#FF330000");
	private static final String LEGEND = "A"; 
	// Attributes
//    private Activity activity;
    private final Paint paint;
    private Bitmap bitmap;
    private Canvas canvas;
    private int position;
    private final int samplingRate = SAMPLING_RATE;
    private int width, height;
    private final float[] magnitudes = new float[FFT_RESOLUTION];
////    private int[] colorRainbow = new int[] {    0xFFFFFFFF, 0xFFFF00FF, 0xFFFF0000, 0xFFFFFF00, 0xFF00FF00, 0xFF00FFFF, 0xFF0000FF, 0xFF000000 };
//    public static int[] COLOR_SHAKE_FIRE = new int[] {    0xFF888800, 0xFFFFFF00, 0xFFFF0000, 0XFFBB0000, 0XFF330000 };
//    public static int[] COLOR_WALK_ICE = new int[] { 	 0xFF880088, 0xFF00FFFF, 0xFF0000FF, 0XFF0000BB, 0XFF000033 };
//    public static int[] COLOR_RUN_LEAF = new int[] { 	 0xFF888800, 0xFF00FFFF, 0xFF00FF00, 0XFF00BB00, 0XFF003300 };
////    private int[] colorGrey = new int[] { 	0xFFFFFFFF, 0XFFCCCCCC, 0xFF999999 };
//    public static int[] COLOR_UNKNOWN_WOOD = new int[] { 	0xEE330000, 0XFF330000 };
    
  
  public static int[] COLOR_RUN_LEAF = new int[] { 	 0xFFFFFFAA, 0xFF00EE00, 0xFF99FF33, 0XFF003300 };
  public static int[] COLOR_SHAKE_FIRE = new int[] { 0xFFFFFF66, 0xFFFF0000, 0xFFFF9900, 0XFF003300 };
  public static int[] COLOR_WALK_ICE = new int[] { 	 0xFFAAEEFF, 0xFF0000FF, 0xFF0099FF, 0XFF000033 };
  public static int[] COLOR_UNKNOWN_WOOD = new int[] { 	0xEE330000, 0XFF330000 };
    
    // Window
    float minX, minY, maxX, maxY;

	private ActivityType activityType = ActivityType.UNKNOWN;
	private List<Double> transformCollectedData;
    
    public SpectrogramView(Context context) {
        super(context);
        paint = new Paint();
//        activity = (Activity) Misc.getAttribute("activity");
        paint.setColor(COLOR_BACKGROUND);
        paint.setStrokeWidth(STROKE_WIDTH);
//        initView();
        setWillNotDraw(false);
    }
    public SpectrogramView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
//        activity = (Activity) Misc.getAttribute("activity");
        paint.setColor(COLOR_BACKGROUND);
        paint.setStrokeWidth(STROKE_WIDTH);
//        initView();
        setWillNotDraw(false);
    }
    
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        width = w;
        height = h;
        if (bitmap!=null) {
        	bitmap.recycle();
        }
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
    }
    
    /**
     * Simple sets
     */
    /*public void setFFTResolution(int res) {
        magnitudes = new float[res];
    }*/
    /*public void setSamplingRate(int sampling) {
        samplingRate = sampling;
    }*/
    private void setMagnitudes(float[] magnitudes) {
//        System.arraycopy(m, 0, magnitudes, 0, m.length);
        System.arraycopy(magnitudes, 0, this.magnitudes, 0, magnitudes.length);
        
//        Ln.d("new magnitudes.length:"+this.magnitudes.length);
    }
    
    public void setData(WaveletProcessedData waveletProcessedData) {
    	transformCollectedData = waveletProcessedData.getTransformCollectedData();
		if (transformCollectedData==null) {
			return;
		}
		
		int size = transformCollectedData.size();
		if (size==0) {
			return;
		}
				
		
		this.activityType = waveletProcessedData.getActivityType();
		
//		Ln.d("received list of "+size+" for activityType:"+activityType.name());
		
		float[] floatsArray = new float[size];
		int i=0;
//		String toPrint = StringsUtils.EMPTY;
		for (Double d : transformCollectedData) {
			floatsArray[i]= d.floatValue()*MULTIPLIER;
//			toPrint+=StringsUtils.COMMA+floatsArray[i];
			i++;
		}
//		toPrint = toPrint.replaceFirst(StringsUtils.COMMA, StringsUtils.EMPTY);
		/*if (activityType.equals(ActivityType.SHAKE)) {
			Ln.d("received list of "+size+" for activityType:"+activityType.name()+":"+activityType.getCounter()+toPrint);
		}*/
		
		setMagnitudes(floatsArray);
    }
    
    /**
     * Called whenever a redraw is needed
     * Renders spectrogram and scale on the right
     * Frequency scale can be linear or logarithmic
     */
    @Override
    public void onDraw(Canvas canvas) {
    	super.onDraw(canvas);
    	int[] colors = activityType.getColorForSpectrogram();
//		Ln.d(activityType.name());    	
    	drawValues(canvas, colors);
    }
    
    private void drawValues(Canvas canvas, int[] colors) {
        int wColor = 10; // default: 10
        int wFrequency = 30; // default: 30
        int rWidth = width-wColor-wFrequency;
        
        // Get scale preferences
//		String defFrequency = activity.getString(R.string.preferences_frequency_scale_default_value);
//    	boolean logFrequency = !Misc.getPreference("frequency_scale", defFrequency).equals(defFrequency);
    	
    	boolean logarithmicFrequency = false; // -> linear
        
        // Update buffer bitmap
    	paint.setColor(COLOR_BACKGROUND);
        canvas.drawLine(position%rWidth, 0, position%rWidth, height, paint);
        for (int i=0; i<height; i++) {
        	float value = getValueFromRelativePosition((float)(height-i)/height, 1, samplingRate/2, logarithmicFrequency);
        	float valueResampled = value / samplingRate/2;
//        	value /= samplingRate/2;
//Ln.d("value resampled:"+value);
//        	int magnitudesIndex = (int) (value*magnitudes.length/2);
//        	float magnitude = magnitudes[magnitudesIndex];
//        	double log10 = Math.log10(magnitude);
//            float db = (float) Math.max(0,-20*log10);
//        	float db = magnitude;
        	
//            float db = (float) (log10);
        	float db = valueResampled/2;
        	float dbUnit = db*9f;
//            int c = getInterpolatedColor(colors, db*0.009f);

        	
            int color = getInterpolatedColor(colors, dbUnit);
//            int c = getInterpolatedColor(colors, db.floatValue()*9f);
            
            /*if (activityType!=ActivityTypeCounter.NONE) {
            	Ln.d("activityType:"+activityType.name()
            			+", value:"+value+", valueResampled:"+valueResampled
            			+", dbUnit:"+dbUnit+", color:"+color);
            }*/
//		+", j:"+value+", magnitudesIndex:"+magnitudesIndex+ ", magnitude:"+magnitude+", db:"+db+", c:"+c);
            paint.setColor(color);
            int x = position%rWidth;
            int y = i;
//            Ln.d("drawing P("+x+","+y+")");
            this.canvas.drawPoint(x, y, paint);
            this.canvas.drawPoint(x, y, paint); // make color brighter
            //this.canvas.drawPoint(pos%rWidth, height-i, paint); // make color even brighter
        }
        
        // Draw bitmap
        boolean positionLessThanRWidth = position<rWidth;
//Ln.d("positionLessThanRWidth:"+positionLessThanRWidth);
        if (positionLessThanRWidth) {
            canvas.drawBitmap(bitmap, wColor, 0, paint);
        } else {
            canvas.drawBitmap(bitmap, (float) wColor - position%rWidth, 0, paint);
            canvas.drawBitmap(bitmap, (float) wColor + (rWidth - position%rWidth), 0, paint);
        }

        // Draw color scale
        paint.setColor(COLOR_BACKGROUND);
        canvas.drawRect(0, 0, wColor, height, paint);
        for (int i=0; i<height; i++) {
            int c = getInterpolatedColor(colors, ((float) i)/height);
            paint.setColor(c);
            canvas.drawLine(0, i, wColor-5, i, paint);
        }
        
        // Draw frequency scale
        float ratio = 0.7f*getResources().getDisplayMetrics().density;
        paint.setTextSize(12f*ratio);
        paint.setColor(COLOR_BACKGROUND);
        canvas.drawRect(rWidth + wColor, 0, width, height, paint);
        paint.setColor(Color.MAGENTA);
        canvas.drawText(LEGEND, rWidth + wColor, 12*ratio, paint);
        if (logarithmicFrequency) {
        	for (int i=1; i<5; i++) {
	    		float y = getRelativePosition((float) Math.pow(10,i), 1, samplingRate/2, logarithmicFrequency);
	    		canvas.drawText("1e"+i, rWidth + wColor, (1f-y)*height, paint);
    		}
        } else {
	        for (int i=0; i<(samplingRate-500)/2; i+=1000) {
	            canvas.drawText(" "+i/1000, rWidth + wColor, height*(1f-(float) i/(samplingRate/2)), paint);
	        }
        }
        
//        position++;
        position+=5;
//        Ln.d("pos:"+pos);
    }

    
    /**
     * Converts relative position of a value within given boundaries
     * Log=true for logarithmic scale
     */
    private static float getRelativePosition(float value, float minValue, float maxValue, boolean log) {
    	if (log) { 
    		return ((float) Math.log10(1+value-minValue) / (float) Math.log10(1+maxValue-minValue)); 
    	}
    	return (value-minValue)/(maxValue-minValue);
    }
    
    /**
     * Returns a value from its relative position within given boundaries
     * Log=true for logarithmic scale
     */
    private float getValueFromRelativePosition(float position, float minValue, float maxValue, boolean log) {
    	if (log) { 
    		return (float) (Math.pow(10, position*Math.log10(1+maxValue-minValue))+minValue-1); 
    	}
    	return minValue + position*(maxValue-minValue);
    }
    
    /**
     * Calculate rainbow colors
     */
    private static int ave(int s, int d, float p) {
        return s + Math.round(p * (d - s));
    }
    
    private static int getInterpolatedColor(int[] colors, float unit) {
        if (unit <= 0) return colors[0];
        if (unit >= 1) return colors[colors.length - 1];
        
        float p = unit * (colors.length - 1);
        int i = (int) p;
        p -= i;

        // now p is just the fractional part [0...1) and i is the index
        int c0 = colors[i];
        int c1 = colors[i + 1];
        int a = ave(Color.alpha(c0), Color.alpha(c1), p);
        int r = ave(Color.red(c0), Color.red(c1), p);
        int g = ave(Color.green(c0), Color.green(c1), p);
        int b = ave(Color.blue(c0), Color.blue(c1), p);

        return Color.argb(a, r, g, b);
    }

}
