/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer.ComputedThresholds;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessorUtils.Thresholds;
import net.iubris.sosmandown.model.result.ComputedResult;
import net.iubris.sosmandown.model.result.SensorEventDataHolder;
import net.iubris.sosmandown.model.state.working.major.WorkingMajorState;
import net.iubris.sosmandown.model.state.working.major.majors.impl.WorkingMajorStateEnum;
import net.iubris.sosmandown.subscriptions.thresholds.ThresholdsSubscriber;
import net.iubris.sosmandown.ui.activities.chartdetails.ChartsActivity;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.StringsUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import roboguice.inject.InjectResource;
import roboguice.util.Ln;
import android.app.Activity;
import android.graphics.Color;

import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

/**
 * 
 * @author "Massimiliano Leone - massimiliano.leone@iubris.net"
 * 
 * Chart fragment providing a realtime timeseries showing x,y,z,resultant acceleration components, 
 * and relative State according to FiniteStateMachine model from {@link WorkingMajorStateEnum}  
 *
 */
public class FragmentChartAccelerationNormWithFSMComputedResult extends 
//RoboFragment 
AbstractFragmentChart<ComputedResult>
implements /*OnChartValueSelectedListener,*/ ThresholdsSubscriber {

	public static final String TAG = FragmentChartAccelerationNormWithFSMComputedResult.class.getSimpleName();
	
	
	@InjectResource(R.string.chart_legend__description)
	private String legendDescription;
	@InjectResource(R.string.chart_legend__x)
	private String legendX;
	@InjectResource(R.string.chart_legend__y)
	private String legendY;
	@InjectResource(R.string.chart_legend__z)
	private String legendZ;
	@InjectResource(R.string.chart_legend__result)
	private String legendResult;
	@InjectResource(R.string.chart_legend__state)
	private String legendState;
	
	@InjectResource(R.string.chart_limitline_threshold_gravity)
	private String limitlineThresholdGravityLabel;
	@InjectResource(R.string.chart_limitline_threshold_freefall)
	private String limitlineThresholdFreefallLabel;
	@InjectResource(R.string.chart_limitline_threshold_impact)
	private String limitlineThresholdImpactLabel;
	
	// time
	/*private static final String limitlineTime = "time";	
	private String chartTime = Line.TIME.name();
	private static final int TIME_Y_VALUE = 2;*/

	private static final float lineTextSize = 11f;

	/*private static final float EVENT_VALUE_FOR_X_Y_Z_SEPARATOR = -40;
	private static final double EVENT_VALUE_FOR_RESULTANT_SEPARATOR = 40;*/

	private static final int X_VISIBLE_RANGE_MIN = 50;
	private static final int X_VISIBLE_RANGE_MAX = 150;


	@Inject
	private EventBus eventBus;

	private final Map<WorkingMajorState, String> stateNameMap;
	@InjectResource(R.string.chart_state_relax)
	private String stateRelax;
//	@InjectResource(R.string.chart_state_going_to_impact)
//	private String stateGoingToImpact;
	@InjectResource(R.string.chart_state_impact)
	private String stateDetectingImpact;
	@InjectResource(R.string.chart_state_detecting_fall)
	private String stateDetectingFall;
	@InjectResource(R.string.chart_state_fall)
	private String stateFall;

	private ComputedThresholds computedThresholds;

	private LineDataSet lineDataSetAccelerationX;
	private LineDataSet lineDataSetAccelerationY;
	private LineDataSet lineDataSetAccelerationZ;
	private LineDataSet lineDataSetAccelerationResultant;
	private LineDataSet lineDataSetStatus;
	
	public FragmentChartAccelerationNormWithFSMComputedResult() {
		super(X_VISIBLE_RANGE_MIN, X_VISIBLE_RANGE_MAX, 50);
		this.stateNameMap = new HashMap<>();
	}
	/*protected void populateChartRunningStateActionMap(Map<ChartRunningState, ChartRunningStateAction<ComputedResult>> chartRunningStateActionMap) {
		chartRunningStateActionMap.put(ChartRunningState.PLAY, new ChartRunningStateAction<ComputedResult>() {
			@Override
			public void act(ChartsActivity activityChartsWithDetails, ComputedResult computedResult) {
				incrementalEventForXCoordinate++;
				drawEventLinesAndUpdate(computedResult, incrementalEventForXCoordinate);
			}
		});
		chartRunningStateActionMap.put(ChartRunningState.STOP, new ChartRunningStateAction<ComputedResult>() {
			@Override
			public void act(ChartsActivity activityChartsWithDetails, ComputedResult computedResult) {
				incrementalEventForXCoordinate++;
				drawEventLinesAndUpdate(buildResultEventSeparator(computedResult.getSensorEventDataHolder().timestamp), incrementalEventForXCoordinate);
				activityChartsWithDetails.setChartRunningState(ChartRunningState.STOP);
			}
		});
	}
	private ComputedResult buildResultEventSeparator(long timestamp) {
		return new ComputedResult(
				new SensorEventDataHolder(EVENT_VALUE_FOR_X_Y_Z_SEPARATOR,
					EVENT_VALUE_FOR_X_Y_Z_SEPARATOR,
					EVENT_VALUE_FOR_X_Y_Z_SEPARATOR,
					EVENT_VALUE_FOR_RESULTANT_SEPARATOR,
					timestamp),
				new SeparatorWorkingFSMComputedState() );
	}*/

	@Override
	protected int getLinechartResourceId() {
		return R.id.computed_result_chart;
	}
	@Override
	protected void prepareChart(Activity activity, int linechartResourceId) {
		super.prepareChart(activity, linechartResourceId);
		
		chart.getAxisLeft().setYOffset(-5);
		XAxis xl = chart.getXAxis();
		xl.setLabelsToSkip(30000);
		
        prepareChartInternal();
	}
	@Override
	protected float getMinimumVerticalAxis() {
		return -2f;
	}
	@Override
	protected float getMaximumVerticalAxis() {
		return 35f;
	}
	private OnChartValueSelectedListener onChartValueSelectedListener = new AbstractOnChartValueSelectedListener() {
		@Override
		public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
			if (e!=null) {
//				SToast.with(getActivity()).show(e.toString()+" - "+dataSetIndex+" - "+h.getXIndex(),true);
				
//				String aMsg = "act: "+waveletProcessedEvent.getActivityType().name()+",count:"+waveletProcessedEvent.getActivityType().getCounter();
				String eMsg = StringsUtils.COMMA+e.toString()+": x:"+e.getXIndex()+",v:"+e.getVal();
				String hMsg = StringsUtils.COMMA+"H: x:"+h.getXIndex()+"ds"+h.getDataSetIndex()+"s:"+h.getStackIndex();
				SToast.with(getActivity()).showLong(eMsg+hMsg);
			}
		}
	};
	@Override
	protected OnChartValueSelectedListener getOnChartValueSelectedListener() {
		return onChartValueSelectedListener;
	}
	private void prepareChartInternal() {
		int meanful_threshold = WorkingMajorStateEnum.getLogicZero()+1;
        WorkingMajorState[] majorStates =  WorkingMajorStateEnum.values();
        YAxis verticalAxisLeft = chart.getAxisLeft();
        for (WorkingMajorState major : majorStates) {
			int weight = major.getWeight();
			if (weight<meanful_threshold) {
				continue;
			}
			String stateLabel = StringsUtils.EMPTY;
			try {
				stateLabel = stateNameMap.get(major);
			} catch(NullPointerException e) {
				stateLabel = major.name().replace(StringsUtils.UNDERSCORE, StringsUtils.SPACE);
			}			
			LimitLine limitlineMajorstate = new LimitLine(weight, stateLabel);
			limitlineMajorstate.setTextSize(lineTextSize);
			limitlineMajorstate.setLineColor(Line.STATE.getColor());
			limitlineMajorstate.enableDashedLine(1, 1, 0);
			verticalAxisLeft.addLimitLine(limitlineMajorstate);
		}
        
        lineDataSetAccelerationX = getLineDataSetOrBuildAndAddToLineData(Line.X.ordinal(), Line.X.getColor(), legendX);
        
		lineDataSetAccelerationY = getLineDataSetOrBuildAndAddToLineData(Line.Y.ordinal(), Line.Y.getColor(), legendY);
        lineDataSetAccelerationZ = getLineDataSetOrBuildAndAddToLineData(Line.Z.ordinal(), Line.Z.getColor(), legendZ);
        lineDataSetAccelerationResultant = getLineDataSetOrBuildAndAddToLineData(Line.RESULT.ordinal(), Line.RESULT.getColor(), legendResult);
        lineDataSetStatus = getLineDataSetOrBuildAndAddToLineData(Line.STATE.ordinal(), Line.STATE.getColor(), legendState);
        lineDataSetStatus.setDrawFilled(false);
//        lineDataSetStatus.setFillAlpha(40);
	}
	
	@Override
	protected int getLayoutResourceId() {
		return R.layout.fragment__chart__acceleration_norm_with_computed_result;
	}
	@Override
	protected void onActivityCreatedInternal() {
		stateNameMap.put(WorkingMajorStateEnum.RELAX, stateRelax);
//		stateNameMap.put(WorkingMajorStateEnum.GOING_TO_IMPACT, stateGoingToImpact);
		stateNameMap.put(WorkingMajorStateEnum.IMPACT, stateDetectingImpact);
		stateNameMap.put(WorkingMajorStateEnum.DETECTING_FALL, stateDetectingFall);
		stateNameMap.put(WorkingMajorStateEnum.FALL, stateFall);
	}
	
	
	/*@Override
	public void onDataReceived(ComputedResult data) {
		WorkingMajorState major = data.getEventState().getMajor();
		super.onDataReceived(data);
	}*/
	
	@Override
	protected void drawEventLines(ComputedResult computedResult, int incrementalEventForXCoordinate) {
//		Ln.d("incrementalEventForXCoordinate:"+incrementalEventForXCoordinate);
		lineData.addXValue( StringsUtils.EMPTY+incrementalEventForXCoordinate );
		
		// old
//		LineDataSet lineDataSetAccelerationX = getLineDataSetOrBuildAndAddToLineData(0, Line.X.getColor(), legendX);
//		LineDataSet lineDataSetAccelerationY = getLineDataSetOrBuildAndAddToLineData(1, Line.Y.getColor(), legendY);
//        LineDataSet lineDataSetAccelerationZ = getLineDataSetOrBuildAndAddToLineData(2, Line.Z.getColor(), legendZ);
//        LineDataSet lineDataSetAccelerationResultant = getLineDataSetOrBuildAndAddToLineData(3, Line.RESULT.getColor(), legendResult);
//        LineDataSet lineDataSetStatus = getLineDataSetOrBuildAndAddToLineData(4, Line.STATE.getColor(), legendState);
//        lineDataSetStatus.setDrawFilled(true);
//        lineDataSetStatus.setFillAlpha(40);

		// old
        // timestamp
//            LineDataSet setTime = getLineDataSetOrBuildAndAddToLineData(lineData, 5, Line.TIME.getColor(), chartTime);
//            setTime.enableDashedHighlightLine(0.5f, 15, 45);
////            setT.enableDashedLine(1, 15, 45);
//            setTime.setFillAlpha(10);
////            setT.setLineWidth(0.01f);
////            setT.setDrawCircles(true);
////            setT.setCircleSize(5f);
////            setT.setCircleColor(Line.TIME.getColor());
//            
		
		SensorEventDataHolder sensorEventDataHolder = computedResult.getSensorEventDataHolder();
		
		addLineEntry(sensorEventDataHolder.x, lineDataSetAccelerationX, incrementalEventForXCoordinate);
		addLineEntry(sensorEventDataHolder.y, lineDataSetAccelerationY, incrementalEventForXCoordinate);
		addLineEntry(sensorEventDataHolder.z, lineDataSetAccelerationZ, incrementalEventForXCoordinate);
		addLineEntry((float)sensorEventDataHolder.resultant, lineDataSetAccelerationResultant, incrementalEventForXCoordinate);
		
		WorkingMajorState major = computedResult.getEventState().getMajor();
//		inspectState(major);
		Integer eventStateWeight = major.getWeight();
		addLineEntry(eventStateWeight, lineDataSetStatus, incrementalEventForXCoordinate);
		
		// old
		// timestamp
//		if (incrementalEventForXCoordinate%150==0) {
//			addLineEntry(TIME_Y_VALUE, setTime, incrementalEventForXCoordinate, event);
//		}
		
//		incrementalEventForXCoordinate++;
		
		checkIfReset();
    }
	
	private WorkingMajorState majorStateCurrent = WorkingMajorStateEnum.NULL;
	/*private final WorkingMajorStateRelaxVisitor workingMajorStateDetectingFallVisitor = new WorkingMajorStateRelaxVisitor() {
		@Override
		public void visit(WorkingMajorStateRelax workingMajorStateRelax) {
			majorStateCurrent = WorkingMajorStateEnum.RELAX;
		}
	};*/
	
	@Override
	public void onDataReceived(ComputedResult data) {
		super.onDataReceived(data);
		majorStateCurrent = data.getEventState().getMajor();
	}
	
	private /*synchronized*/ boolean checkIfReset() {
		if ((majorStateCurrent.equals(WorkingMajorStateEnum.RELAX) 
				&& incrementalEventForBaseXCoordinate>Config.Charting.MAX_VALUES_TO_CHART_IN_RELAX)
			|| incrementalEventForBaseXCoordinate>Config.Charting.MAX_VALUES_TO_CHART) {
Ln.d("incrementalEventForXCoordinate:"+incrementalEventForBaseXCoordinate+", resetting charts");
			ChartsActivity activity = (ChartsActivity) getActivity();
			this.incrementalEventForBaseXCoordinate = 0;
			activity.resetCharts();
			return true;
		}
		return false;
	}
	
	/*
	// reaching max 1024 values in relax state, we clear chart...
//	protected static final long X_COORDINATE_MAX = 1024L - 1;
	private final WorkingMajorStateRelaxVisitor workingMajorStateRelaxVisitor = new WorkingMajorStateRelaxVisitor() {
		@Override
		public void visit(WorkingMajorStateRelax workingMajorStateRelax) {
			if ( (incrementalEventForXCoordinate & X_COORDINATE_MAX)==0 ) {
				clearChart();
				incrementalEventForXCoordinate = 0;
			}
		}
	};
	private void inspectState(WorkingMajorState workingMajorState) {
		workingMajorState.accept(workingMajorStateRelaxVisitor);
	}*/
	
	
	// for time
	/*private Entry addLineEntry(float value, LineDataSet set, int incrementalEventForXCoordinate, ResultEvent event) {
		Entry entry = new Entry(value, incrementalEventForXCoordinate, event);
		set.addEntry( entry );
		return entry;
	}*/
	
	@Override
	protected int getToX(int xValCount, int xVisible) {
		int	toX = xValCount - xVisible;
		return toX;
//		return xValCount;
	}
	
	@Override
	public void onResume() {
//		Ln.d("onResume");
		super.onResume();
		
//		Ln.d("computedThresholds:"+computedThresholds);
		if (computedThresholds!=null) {
			drawAxis(computedThresholds);
		} else {
			Thresholds thresholds = Thresholds.INSTANCE;
			this.computedThresholds = new ComputedThresholds(thresholds.getGravity(), thresholds.getFreeFall(), thresholds.getImpact());
			drawAxis(computedThresholds);
//			double gravityThresholds = computedThresholds.getGravity();
//			double freeFallThresholds = computedThresholds.getFreeFall();
//			double impactThresholds = computedThresholds.getImpact();			
//			setThresholdLimitLines(gravityThresholds, freeFallThresholds, impactThresholds);
			
			registerForThresholds();
		}
	}	
	@Override
	public void onDestroyView() {
		unregisterForThresholds();
		super.onDestroyView();
	}	
	@Override
	public void registerForThresholds() {
		if (!eventBus.isRegistered(this)) {
			eventBus.register(this);
		}
	}
	@Override
	public void unregisterForThresholds() {
		if (eventBus.isRegistered(this)) {
			eventBus.unregister(this);
		}
	}
	@Override
	@Subscribe(threadMode=ThreadMode.BACKGROUND)
	public void onThresholdsReceived(ComputedThresholds computedThresholds) {
		this.computedThresholds = computedThresholds;
		Ln.d("received: "+computedThresholds);
		drawAxis(computedThresholds);
		unregisterForThresholds();
	}	
	private void drawAxis(ComputedThresholds computedThresholds) {
		double gravityThresholds = computedThresholds.getGravity();
		double freeFallThresholds = computedThresholds.getFreeFall();
		double impactThresholds = computedThresholds.getImpact();
		
		setThresholdLimitLines(gravityThresholds, freeFallThresholds, impactThresholds);
	}
	private void setThresholdLimitLines(double gravityThresholds, double freefallThreshold, double impactThreshold) {
		List<LimitLine> limitLines = new ArrayList<>();
		Ln.d(gravityThresholds+" "+freefallThreshold+" "+impactThreshold);
		if (gravityThresholds>0) {
			float gravity = (float) gravityThresholds;
			Ln.d(gravity);
			limitLines.add( new LimitLine(gravity, limitlineThresholdGravityLabel) );
		}
//		limitLines.add( new LimitLine((int)(Float.parseFloat(freefallThreshold)), limitlineThresholdFreefallLabel) );
//		limitLines.add( new LimitLine((int)(Float.parseFloat(impactThreshold)), limitlineThresholdImpactLabel) );
		for (LimitLine limitLine : limitLines) {
			limitLine.setTextSize(lineTextSize);
			limitLine.setTextColor(Line.THRESHOLD.getColor());
	        limitLine.setLineColor(Line.THRESHOLD.getColor());
	        limitLine.enableDashedLine(1, 5, 0);
	        limitLine.setXOffset(250);
	        limitLine.setYOffset(0.1f);

//	        YAxis axis = chart.getAxisRight();
	        YAxis axis = chart.getAxisLeft();
	        axis.addLimitLine(limitLine);
	        
	        Ln.d("added:"+limitLine.getLabel());
		}
	}
	
	
	private enum Line {
		X {
			@Override
			int getColor() { return Color.argb(20, 255, 200, 0); } // yellow, alpha=20
		},
		Y {
			@Override
			int getColor() { return Color.argb(20, 0, 255, 0); } // green, alpha=20
		},
		Z {
			@Override
			int getColor() { return Color.argb(20, 0, 0, 255); } // blue, alpha=20
		},
		RESULT {
			@Override
			int getColor() { return Color.argb(120, 255, 0, 0); } // red, alpha=120
		},
		STATE {
			@Override
			int getColor() { return Color.GRAY; }
		},
		THRESHOLD {
			@Override
			int getColor() { return Color.argb(200, 180, 180, 180); } // light grey, alpha=200
		}
		/*,
		TIME {
			@Override
			int getColor() { return Color.BLACKGRAY; }
		}*/;
		abstract int getColor();
	}
}
