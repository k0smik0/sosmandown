/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.utils;

import net.iubris.sosmandown.utils.StringsUtils;
import android.widget.TextView;

public class ViewUtils {

	public static void setText(TextView textView, String text) {
		textView.setText(text);
	}
	public static void setText(TextView textView, int integer) {
		textView.setText(StringsUtils.EMPTY+integer);
	}
}
