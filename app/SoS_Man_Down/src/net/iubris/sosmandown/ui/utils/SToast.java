/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.ui.utils;

import android.content.Context;
import frenchtoast.FrenchToast;

public enum SToast {
	
	INSTANCE;
	
	public void showShort(String msg) {
//		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
		FrenchToast.with(context).showText(msg);
	}
	
	public void showLong(String msg/*, boolean longToast*/) {
//		Ln.d("context:"+context);
		/*if (longToast) {
//			Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
//			FrenchToast.with(context).showLayout(layoutResId)
			FrenchToast.with(context).longLength().showText(msg);
		} else {
//			Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
			FrenchToast.with(context).shortLength().showText(msg);
		}*/
		
		FrenchToast.with(context).longLength().showText(msg);
	}
	
	private Context context;

	public static SToast with(Context context) {
		return INSTANCE.setContext(context);
	}

	private SToast setContext(Context context) {
		this.context = context;
		return INSTANCE;
	}
}
