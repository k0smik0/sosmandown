/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.utils;

import static net.iubris.sosmandown.utils.StringsUtils.SPACE;
import static net.iubris.sosmandown.utils.StringsUtils.UNDERSCORE;
import static net.iubris.sosmandown.utils.StringsUtils.capitalize;
import android.os.Build;

public class DeviceUtils {

	public static String getDeviceName() {
	    String manufacturer = Build.MANUFACTURER;
	    String model = Build.MODEL;
	    String name;
	    if (model.startsWith(manufacturer)) {
	        name = capitalize(model);
	    } else {
	        name = capitalize(manufacturer) + UNDERSCORE + model;
	    }
	    name.replaceAll(SPACE, UNDERSCORE);
	    return name;
	}	
}
