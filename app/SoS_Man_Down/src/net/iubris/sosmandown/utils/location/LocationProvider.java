/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.utils.location;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.iubris.polaris.locator.core.Locator;
import net.iubris.polaris.locator.core.exceptions.LocationNullException;
import net.iubris.polaris.locator.core.updater.OnLocationUpdatedCallback;
import net.iubris.polaris.locator.utils.LocationStrategiesUtils;
import android.location.Location;

@Singleton
//@ContextSingleton
public class LocationProvider {

	private final Locator locator;
	
	private final static int MAXIMUM_TIME = 5;
	private final static int MAXIMUM_DISTANCE = 20;
	
	private Location location;
	
	private final OnLocationUpdatedCallback onLocationUpdatedCallback = new OnLocationUpdatedCallback() {
		@Override
		public void onLocationUpdated(Location location) {
			if (LocationStrategiesUtils.isLocationBetter(location, LocationProvider.this.location, MAXIMUM_TIME, MAXIMUM_DISTANCE)) {
				LocationProvider.this.location = location;
//				Ln.d("new location: "+location);
			}
		}
	};

//	private final String errorLocationNull;

	@Inject
	public LocationProvider(Locator locator/*, ContextProvider contextProvider*//*, Application dummyApplication*/) {
		this.locator = locator;
//		this.errorLocationNull = context.getResources().getString( R.string.error__location_null );
	}
	
	public void start() throws LocationNullException {
		locator.startLocationUpdates(onLocationUpdatedCallback);
		this.location = locator.getLocation();
	}
	public void stop() {
		locator.stopLocationUpdates();
	}
	
	public Location getLocation() {
		return location;
	}
}
