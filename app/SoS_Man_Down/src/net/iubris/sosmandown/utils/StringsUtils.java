/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.utils;

public class StringsUtils {
	public static final String EMPTY = "";
	
	public static final String UNDERSCORE = "_";

	public static final String SPACE = " ";

	public static final String COMMA = ",";

	public static final String PIPE = "|" ;

	public static final String DOT = ".";

	public static final String PERIOD = ":";
	
	public static final String SLASH = "/";
	
	public static final String NEW_LINE = "\n";
	
	public static String capitalize(String s) {
	    if (s == null || s.length() == 0) {
	        return StringsUtils.EMPTY;
	    }
	    char first = s.charAt(0);
	    if (Character.isUpperCase(first)) {
	        return s;
	    } 
	        
	    return Character.toUpperCase(first) + s.substring(1);
	}
}
