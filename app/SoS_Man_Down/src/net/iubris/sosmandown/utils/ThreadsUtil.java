/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import roboguice.util.Ln;
import android.content.Context;
import android.os.Handler;

@Singleton
public class ThreadsUtil {

	private final ExecutorService threadPoolWorker;
	private final ExecutorService threadPoolTmp;
	private final Handler handler;

	@Inject
	public ThreadsUtil(Context context) {
Printor.whoIAm(this,context);
//		this.application = application;
		threadPoolWorker = Executors.newFixedThreadPool(1);
		threadPoolTmp = Executors.newFixedThreadPool(1);
		handler = new Handler();
		
		
	}
	
	public void execOnWorker(Runnable runnable) {
		threadPoolWorker.execute(runnable);
	}
	
	public void execOnTmp(Runnable runnable) {
		Ln.d("executing "+runnable+" on tmp thread");
		threadPoolTmp.execute(runnable);
	}
	
	public void execOnUI(Runnable runnable) {
		handler.post(runnable);
	}
	public void execOnUIWithDelay(Runnable runnable, long delay) {
		handler.postDelayed(runnable, delay);
	}
	public void removeCallbackFromUIThread(Runnable runnable) {
		handler.removeCallbacks(runnable);
	}
	
}
