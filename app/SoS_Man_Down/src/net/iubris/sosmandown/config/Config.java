/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown.config;

import java.util.HashMap;
import java.util.Map;

import roboguice.util.Ln;
import android.hardware.SensorManager;

public class Config {
	
	public static final String APP_NAME_INTERNAL = "SOSManDown";

	public static class Countdown {
		/**
		 *  in seconds
		 */
		public static final int TIMEOUT = 30; // was 30
	}
	public static class LocationRetriever {
		/**
		 * in minutes
		 */
		public static final int THRESHOLD_TIME_MINIMUM = 5;
		/**
		 * in meters
		 */
		public static final int THRESHOLD_DISTANCE_MAXIMUM = 20;
	}
	public static class Dialog {
		/**
		 * in seconds
		 */
		public static final long MAX_DIALOG_APPEAR = 20*1000;
	}
	public static class Detection {
		/**
		 * each 250ms <-> 4 events/min
		 */
		public static final int SENSOR_SENSITIVITY = SensorManager.SENSOR_DELAY_UI;
		public static class FreeFall {
			/**
			 * -> free fall threshold will be: g=~9.8-3 =~ 4.8
			 */
			public static final double FREE_FALL_SUBSTRACTER = 3;
		}
		public static class GoingToImpact {
			/**
			 * this is the first threshold:<br/>
			 * the more higher, the more engine behaviour trend to go to impact
			 */
			public static final int MAX_EVENT_COMPUTED = 30; // was 20
		}	
		public static class Impact {
			/**
			 * in seconds
			 */
			public static final float THRESHOLD_FILTER_IMPACT_TIME = 0.1f; // (was 1) sec
			
			// minimum and maximu are used in complement to 1
			public static final float SENSITIVITY_THRESHOLD_MINIMUM = 0.1f;
			public static final float SENSITIVITY_THRESHOLD_MAXIMUM = 1.0f;
			public static final int SENSITIVITY_THRESHOLD_COMPLEMENT = 10;
			public static final float SENSITIVITY_MINIMUM = 1.6f;
			
			// (was 2.7) -> impact threshold will be: ~9.8*2.3 =~ 22.54
			/**
			 *  so: impact-freefall will be greater then about 2g (=~24.4)
			 */
			private static double IMPACT_THRESHOLD_FOR_GRAVITY_COMPLEMENTARY_MULTIPLIER; // 1.7 from config.xml // was 1.9, 2.7, 2.3
			public static void setImpactThresholdForGravityComplementaryMultiplier(int impact_threshold_for_gravity_complementary_multiplier) {
				Ln.d("setting impact_threshold_for_gravity_complementary_multiplier:"+impact_threshold_for_gravity_complementary_multiplier);
				IMPACT_THRESHOLD_FOR_GRAVITY_COMPLEMENTARY_MULTIPLIER = impact_threshold_for_gravity_complementary_multiplier;
			}
			public static double getImpactThresholdForGravityComplementaryMultiplier() {
				Ln.d("getting impact_threshold_for_gravity_complementary_multiplier: "+IMPACT_THRESHOLD_FOR_GRAVITY_COMPLEMENTARY_MULTIPLIER);
				return IMPACT_THRESHOLD_FOR_GRAVITY_COMPLEMENTARY_MULTIPLIER;
			}
			
			// credits: from https://github.com/BharadwajS/Fall-detection-in-Android/blob/master/Fall%20detection.pdf
		}
		public static class Detecting {
			/**
			 * transient period, in seconds
			 */
			public static final int TRANSIENT_SECONDS = 2;
			/**
			 * detecting INTERVAL, in seconds
			 */
			public static final int INTERVAL_TIME_MAX = 30; // seconds
			public static int SUB_INTERVALS = 3; // TODO retrieve from config.xml
			public static void setSubInterval(int subInterval) {
				SUB_INTERVALS = subInterval;
			}
			public static final int NUM_MAX_SLIGHT_MOVEMENTS_TO_RETRIEVE = 64; // was 20
			public static final int NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE = 2*Fall.NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE;
		}
		public static class Fall {
			public static final int NUM_MIN_BIG_MOVEMENTS_TO_RETRIEVE = 16;
			public static final int MAX_FALL_SECONDS = 35;
		}
		public static class Result {
			public static Map<Integer,Thresholds> PartialThresholds = new HashMap<>();
			public static class Thresholds {
				public final int minimum_activity_walk;  // 6 4 2
				public final int minimum_activity_run;   // 3 2 1
				public final int minimum_activity_shake; // 4 2 2
				public final int minimum_posture_walk;   // 3 2 1
				public Thresholds(int minimum_activity_walk, int minimum_activity_run, int minimum_activity_shake, int minimum_posture_walk) {
					this.minimum_activity_walk = minimum_activity_walk;
					this.minimum_activity_run = minimum_activity_run;
					this.minimum_activity_shake = minimum_activity_shake;
					this.minimum_posture_walk = minimum_posture_walk;
				}
				@Override
				public String toString() {
					return "[aw: "+minimum_activity_walk+", ar:"+minimum_activity_run+", as:"+minimum_activity_shake+", pw:"+minimum_posture_walk+"]";
				}
			}
		}
	}
	
	public static class Charting {
		public static final int MAX_VALUES_TO_CHART = 15000;
		public static final int MAX_VALUES_TO_CHART_IN_RELAX = 3000;
	}

	public static class Saver {
		public static class Detection {
			// id = 0B9oS3H2pbafWclpQWVpRLWNKNzQ
			// collecting_data/detections/news
			// https://drive.google.com/open?id=0B9oS3H2pbafWV3U0V2p0amt3NzQ
			public static final String DRIVE_DETECTIONS_FOLDER_DRIVE_ID_CLEAR = "0B9oS3H2pbafWV3U0V2p0amt3NzQ";
			public static final String DRIVE_DETECTIONS_FOLDER_DRIVE_ID_ENCODED = "DriveId:CAESHDBCOW9TM0gycGJhZldWM1UwVjJwMGFtdDNOelEYACAAKAA=";
			
		}
		public static class Events {
			/**
			 *  4 events/min
			 */
			public static final int EVENTS_TO_SAVE_FOR_EXTERNAL_ANALYSIS_MAX_NUM = 5000;
			
			// is https://drive.google.com/folderview?id=0B9oS3H2pbafWcGV4emtUV3hUeFU&usp=sharing
			// to obtain this below: new DriveId(DRIVE_FOLDER_RESOURCE_ID,0,0,0).encodeToString();
			// where DRIVE_FOLDER_RESOURCE_ID is part right of id= from url
			
			// id = 0B9oS3H2pbafWMU1uZWNoRzRQNXM
			// collecting_data/events/news
			// https://drive.google.com/open?id=0B9oS3H2pbafWMU1uZWNoRzRQNXM
			public static final String DRIVE_EVENTS_FOLDER_DRIVE_ID_CLEAR = "0B9oS3H2pbafWMU1uZWNoRzRQNXM"; 
			public static final String DRIVE_EVENTS_FOLDER_DRIVE_ID_ENCODED = "DriveId:CAESABiqXSDO0tihmVQoAA==";
		}   
	}
	
	public static final boolean DEV = false;
	public static final boolean TEST = true;
	public static final boolean TEST_or_DEV = (TEST || DEV);
	public static final boolean LOG_IN_TEST = true;
	
	
	public static String BUILD_VERSION;
	public static String BUILD_TIMESTAMP;
}
