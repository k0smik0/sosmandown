/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown._di.providers.location;

import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.SmartLocation.GeocodingControl;
import io.nlopez.smartlocation.geocoding.providers.DefaultAndroidGeocodingProvider;

import javax.inject.Inject;
import javax.inject.Provider;

import net.iubris.sosmandown.engine.location.AndroidGeocodingService;
import android.app.IntentService;

public class GeocodingControlProvider implements Provider<GeocodingControl> {

	private final GeocodingControl geocodingControl;

	@Inject
	public GeocodingControlProvider(SmartLocation smartLocation) {
		this.geocodingControl = smartLocation.geocoding().provider( new DefaultAndroidGeocodingProvider() {
			@Override
			public Class<? extends IntentService> getGeocodingServiceClass() {
				return AndroidGeocodingService.class;
			}
		});
	}

	@Override
	public GeocodingControl get() {
		return geocodingControl;
	}
}
