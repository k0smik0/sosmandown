/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown._di.providers.location;

import javax.inject.Inject;
import javax.inject.Provider;

import net.iubris.kusor2.KLocatorSLL;
import net.iubris.sosmandown.config.Config.LocationRetriever;
import net.iubris.sosmandown.utils.Printor;
import roboguice.inject.ContextSingleton;
import android.content.Context;

@ContextSingleton
public class KLocatorProvider implements Provider<KLocatorSLL> {

	private final KLocatorSLL kLocatorSLL;

	@Inject
	public KLocatorProvider(Context context) {
Printor.whoIAm(this,context);
		kLocatorSLL = new KLocatorSLL(context, LocationRetriever.THRESHOLD_TIME_MINIMUM, LocationRetriever.THRESHOLD_DISTANCE_MAXIMUM);
	}

	@Override
	public KLocatorSLL get() {
		return kLocatorSLL;
	}

}
