/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown._di.providers.countdown;

import javax.inject.Inject;
import javax.inject.Provider;

import net.iubris.polaris.locator.core.exceptions.LocationNullException;
import net.iubris.sosmandown.R;
import net.iubris.sosmandown.config.Config.Countdown;
import net.iubris.sosmandown.engine.enhancedcountdown.EnhancedCountDownTimer;
import net.iubris.sosmandown.engine.enhancedcountdown.OnCancelCallback;
import net.iubris.sosmandown.engine.enhancedcountdown.OnFinishCallback;
import net.iubris.sosmandown.engine.enhancedcountdown.OnStartCallback;
import net.iubris.sosmandown.engine.enhancedcountdown.OnTickCallback;
import net.iubris.sosmandown.engine.location.LocationHandler;
import net.iubris.sosmandown.engine.looped_layout.SOSLoopedLayout;
import net.iubris.sosmandown.ui.utils.SToast;
import net.iubris.sosmandown.utils.Printor;
import net.iubris.sosmandown.utils.StringsUtils;
import net.iubris.sosmandown.utils.ThreadsUtil;
import net.iubris.sosmandown.utils.location.LocationProvider;

import org.greenrobot.eventbus.EventBus;

import roboguice.RoboGuice;
import roboguice.inject.ContextSingleton;
import roboguice.inject.InjectView;
import roboguice.util.Ln;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

@ContextSingleton
public class EnhancedCountDownTimerProvider implements Provider<EnhancedCountDownTimer> {

	private static final int COUNTDOWN = (Countdown.TIMEOUT*1000); // we increment by 1 because first is swallowed
	private static final int COUNT_DOWN_INTERVAL_BEFORE_RUN_TICK_CALLBACK_IN_MILLISECONDS = 1000;
	
//	private final Activity activity;
	
	private final EnhancedCountDownTimer countDownTimer;
	
	@InjectView(R.id.countdown__text)
	private TextView textviewCountdown;
	
//	@InjectView(R.id.button_sos)
//	private Button sosButton;
	
	private final Runnable runnableLocationRetriever;
	private final Runnable runnableStart;
	
//	@InjectView(R.id.countdown__switcher)
//	private SOSLoopedTextView sosSwitcher;
	
	private final SOSLoopedLayout sosLoopedLayout;
	
	private final ThreadsUtil threadsUtil;
	
//	private final TickTask tickTask;
	
	@Inject
	public EnhancedCountDownTimerProvider(final Activity activity, 
			final SOSLoopedLayout sosLoopedLayout, 
			final LocationProvider locationProvider, LocationHandler locationHandler, EventBus eventBus, ThreadsUtil threadsUtil) {
		Printor.whoIAm(this,activity);
		this.threadsUtil = threadsUtil;
		//		this.activity = activity;
		RoboGuice.injectMembers(activity, this);
//		Button b = (Button) activity.findViewById(R.id.button_sos);
//		Ln.d("buttonSOS:"+b);
		
		
		this.sosLoopedLayout = sosLoopedLayout;
		this.sosLoopedLayout.init();
		
		final String msgErrorLocationNull = activity.getString(R.string.error__location_null);
		this.runnableLocationRetriever = new Runnable() {
			@Override
			public void run() {
				try {
					locationProvider.start();
					Ln.d("locationProvider started");
				} catch (LocationNullException e) {
					SToast.with(activity).showLong(msgErrorLocationNull);
				}
			}
		};
		final String text = StringsUtils.EMPTY+(COUNTDOWN/1000);
		runnableStart = new Runnable() {
			@Override
			public void run() {
				Ln.d("textviewCountdown:"+textviewCountdown.getVisibility());
				textviewCountdown.setVisibility(View.VISIBLE);
				textviewCountdown.setText(text);
//				sosSwitcher.next();
				
				sosLoopedLayout.start();
				
				Ln.d("text: " + text);
			}
		};
		
		this.countDownTimer = new EnhancedCountDownTimer(COUNTDOWN+1, COUNT_DOWN_INTERVAL_BEFORE_RUN_TICK_CALLBACK_IN_MILLISECONDS,
			/*buildOnTickCallback(sosLoopedLayout),
			buildOnFinishCallback(sosLoopedLayout, locationHandler),
			buildStartCountdownWithTextAndStartLocationProviderCallback(sosLoopedLayout, locationProvider),
			buildStopCountdownAndStopLocationProviderCallback(sosLoopedLayout, locationProvider),*/
			buildOnTickCallback(),
			buildOnFinishCallback(locationHandler),
			buildStartCountdownWithTextAndStartLocationProviderCallback(/*locationProvider*/),
			buildStopCountdownAndStopLocationProviderCallback(locationProvider),
			eventBus,
			threadsUtil
		);
//		tickTask = new TickTask(context);
	}
	
	@Override
	public EnhancedCountDownTimer get() {
//		this.sosLoopedLayout.init();
		return countDownTimer;
	}
	
//	private final Handler handler = new Handler();
	
	private OnStartCallback buildStartCountdownWithTextAndStartLocationProviderCallback(/*/*final SOSLoopedLayout sosLoopedLayout,*/ /*final LocationProvider locationProvider*/) {
		return new OnStartCallback() {
			
//			private final String msgErrorLocationNull = activity.getString(R.string.error__location_null);
			@Override
			public void doOnStart() {
Ln.d("doOnStart");
				/*final String text = StringsUtils.EMPTY+(COUNTDOWN/1000);
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Ln.d("textviewCountdown:"+textviewCountdown);
						textviewCountdown.setText(text);
//						sosSwitcher.next();
						imageSwitcher.next();
//						sosLoopedLayout.start();
						Ln.d("text: " + text);
					}
				});*/
				/*handler.post*/threadsUtil.execOnUI(runnableStart);
								
				/*handler.post*/threadsUtil.execOnUI(runnableLocationRetriever);				
			}
		};
	}
	
	private OnCancelCallback buildStopCountdownAndStopLocationProviderCallback(/*final SOSLoopedLayout sosLoopedLayout,*/ final LocationProvider locationProvider) {
		return new OnCancelCallback() {
			@Override
			public void doOnCancel() {
				Ln.d("doOnCancel");
				textviewCountdown.setText(StringsUtils.EMPTY);
//					imageSwitcher.reset();
//				sosSwitcher.stop();
				sosLoopedLayout.stop();
				Ln.d("stopped sosLoopedLayout");
				
				Ln.d("stopping locationProvider in doOnCancel");
				locationProvider.stop();
			}
		};
	}
	
	/*RoboAsyncTask<Void> task = new RoboAsyncTask<Void>(context) {
		@Override
		public Void call() throws Exception {
			textviewCountdown.setText(text);
			Ln.d("text: "+text);
//			imageSwitcher.next();
			return null;
		}
		@Override
		protected void onSuccess(Void t) throws Exception {
			
		}
	};*/
	/*class TickTask extends RoboAsyncTask<Void> {
		private String text;
		protected TickTask(Context context) {
			super(context);
		}
		@Override
		public Void call() throws Exception {
			Ln.d("text: "+text);
			return null;
		}
		@Override
		protected void onSuccess(Void t) throws Exception {
			textviewCountdown.setText(text);
			imageSwitcher.next();
		}
		public void execute(String text) {
			this.text = text;
			super.execute();
		}
	}*/
	
	
	private OnTickCallback buildOnTickCallback(/*final SOSLoopedLayout sosLoopedLayout*/) {
		return new OnTickCallback() {
			@Override
			public void doOnTick(long millisUntilFinishedAsMaximumTimeout) {
				final String text = StringsUtils.EMPTY + ((millisUntilFinishedAsMaximumTimeout / 1000));
				final Runnable runnable = new Runnable() {
					@Override
					public void run() {
						textviewCountdown.setText(text);
//						sosSwitcher.next();
						sosLoopedLayout.next();
						Ln.d("text: "+text);
					}
				};				
//				activity.runOnUiThread( runnable );
				/*handler.post*/threadsUtil.execOnUI(runnable);				
//				tickTask.execute(text);
			}
		};
	}
	private OnFinishCallback buildOnFinishCallback(/*final SOSLoopedLayout sosLoopedLayout,*/ final LocationHandler locationHandler) {
		return new OnFinishCallback() {
			@Override
			public void doOnFinish() {
				textviewCountdown.setText("1");
//					Ln.d("text: 1");
//					imageSwitcher.reset(); // do not use
//				sosSwitcher.stop();
				sosLoopedLayout.stop();
				/*new Handler().post( new Runnable() {
					@Override
					public void run() {
						imageSwitcher.stop();
					}
				});*/

				// TODO sms sending is inside locationHandler: move out ?
				locationHandler.getAndUseLocation();
				Ln.d("sent sms?");
				textviewCountdown.setText(StringsUtils.EMPTY);
			}
		};
	}
}
