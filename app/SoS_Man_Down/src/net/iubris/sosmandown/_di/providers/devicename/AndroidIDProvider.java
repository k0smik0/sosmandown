/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown._di.providers.devicename;

import javax.inject.Inject;
import javax.inject.Provider;

import net.iubris.sosmandown.utils.Printor;
import roboguice.inject.ContextSingleton;
import android.content.Context;
import android.provider.Settings.Secure;

@ContextSingleton
public class AndroidIDProvider implements Provider<String> {

	private final String androidID;
	
	@Inject
	public AndroidIDProvider(Context context) {
Printor.whoIAm(this,context);
		this.androidID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
	}
	
	@Override
	public String get() {
		return androidID;
	}
}
