/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown._di.providers.fsm;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocatorC;
import net.iubris.sosmandown.utils.Printor;
import android.content.Context;

@Singleton
public class FSMComputedStatesLocatorProvider implements Provider<FSMComputingStatesLocator> {

	private final FSMComputingStatesLocatorC fsmComputedStatesLocator;
//	private final FSMComputedStatesLocator FSMComputedStatesLocatorE.INSTANCE;
	
	@Inject
	public FSMComputedStatesLocatorProvider(/*Application application,*/ FSMComputingStatesLocatorC fsmComputedStatesLocatorC, Context context) {
Printor.whoIAm(this,context);
		this.fsmComputedStatesLocator = fsmComputedStatesLocatorC;
	}

	@Override
	public FSMComputingStatesLocator get() {
		return fsmComputedStatesLocator;
	}
}
