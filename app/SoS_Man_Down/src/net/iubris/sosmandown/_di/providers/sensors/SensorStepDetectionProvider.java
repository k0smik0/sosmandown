/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown._di.providers.sensors;

import javax.inject.Inject;
import javax.inject.Provider;

import net.iubris.sosmandown.utils.Printor;
import roboguice.inject.ContextSingleton;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

@ContextSingleton
//@Singleton
public class SensorStepDetectionProvider implements Provider<Sensor> {

	private final Sensor sensorStepDetector;

	@Inject
	public SensorStepDetectionProvider(SensorManager sensorManager, Context context) {
Printor.whoIAm(this,context);
		sensorStepDetector = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
		
		// TODO remove
//		Ln.d(sensorStepDetector+" isWakeUpSensor: "+sensorStepDetector.isWakeUpSensor() );
	}
	
	@Override
	public Sensor get() {
		return sensorStepDetector;
	}

}
