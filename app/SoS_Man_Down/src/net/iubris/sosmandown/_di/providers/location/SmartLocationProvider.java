/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown._di.providers.location;

import io.nlopez.smartlocation.SmartLocation;

import javax.inject.Inject;
import javax.inject.Provider;

import net.iubris.sosmandown.config.Config;
import android.content.Context;

public class SmartLocationProvider implements Provider<SmartLocation> {

	private final SmartLocation smartLocation;

	@Inject
	public SmartLocationProvider(Context context) {
		this.smartLocation = new SmartLocation.Builder(context)
			.preInitialize(true)
			.logging(Config.LOG_IN_TEST)
			.build();
	}

	@Override
	public SmartLocation get() {
		return smartLocation;
	}
}
