/*******************************************************************************
 * Copyright (c) 2016 Massimiliano Leone - massimiliano.leone@iubris.net.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Project and development:
 *     Massimiliano Leone - massimiliano.leone@iubris.net - project, API, implementation
 ******************************************************************************/
package net.iubris.sosmandown._di.roboguice.module;

import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.SmartLocation.GeocodingControl;
import net.iubris.polaris.locator.core.Locator;
import net.iubris.sosmandown._di.annotations.AndroidID;
import net.iubris.sosmandown._di.annotations.DeviceName;
import net.iubris.sosmandown._di.annotations.SensorAcceleration;
import net.iubris.sosmandown._di.providers.countdown.EnhancedCountDownTimerProvider;
import net.iubris.sosmandown._di.providers.devicename.AndroidIDProvider;
import net.iubris.sosmandown._di.providers.fsm.FSMComputedStatesLocatorProvider;
import net.iubris.sosmandown._di.providers.location.GeocodingControlProvider;
import net.iubris.sosmandown._di.providers.location.KLocatorProvider;
import net.iubris.sosmandown._di.providers.location.SmartLocationProvider;
import net.iubris.sosmandown._di.providers.sensors.SensorAccelerationProvider;
import net.iubris.sosmandown.config.Config;
import net.iubris.sosmandown.engine.computing_state.fsm._locator.FSMComputingStatesLocator;
import net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer.ThresholdsComputer;
import net.iubris.sosmandown.engine.computing_state.fsm.init.thresholdscomputer.ThresholdsComputerProd;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.time.IImpactTimeFilter;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.time.IImpactTimeFilterStart;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.time.IImpactTimeFilterStop;
import net.iubris.sosmandown.engine.computing_state.fsm.working.filters.time.ImpactTimeFilter;
import net.iubris.sosmandown.engine.enhancedcountdown.EnhancedCountDownTimer;
import net.iubris.sosmandown.engine.sensor_event.processor.SensorEventProcessor;
import net.iubris.sosmandown.engine.sensor_event.processor.fsm.FSMComputedStateProcessor;
import net.iubris.sosmandown.savers.detections.DetectionsSaver;
import net.iubris.sosmandown.savers.detections.csv.drive.GoogleDriveCSVFileDetectionsSaver;
import net.iubris.sosmandown.service.listeners.SensorAccelerationListenerEventSavingProcessorDelegate;
import net.iubris.sosmandown.service.listeners.SensorAccelerationListenerEventSavingProcessorDelegateDev;
import net.iubris.sosmandown.service.listeners.SensorAccelerationListenerEventSavingProcessorDelegateProd;
import net.iubris.sosmandown.utils.DeviceUtils;

import org.greenrobot.eventbus.EventBus;

import android.hardware.Sensor;

import com.google.inject.AbstractModule;

public class SOSManDownModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(EnhancedCountDownTimer.class).toProvider(EnhancedCountDownTimerProvider.class);
		
		bind(Locator.class).toProvider(KLocatorProvider.class);
		bind(SmartLocation.class).toProvider(SmartLocationProvider.class).asEagerSingleton();
		bind(GeocodingControl.class).toProvider(GeocodingControlProvider.class).asEagerSingleton();
		
//		bind(StateActionFall.class).asEagerSingleton();
		
		bind(FSMComputingStatesLocator.class).toProvider(FSMComputedStatesLocatorProvider.class);
		
		bind(Sensor.class).annotatedWith(SensorAcceleration.class).toProvider(SensorAccelerationProvider.class);
//		bind(Sensor.class).annotatedWith(SensorSignificantMotion.class).toProvider(SensorSignificantMotionProvider.class);
//		bind(Sensor.class).annotatedWith(SensorStepDetection.class).toProvider(SensorStepDetectionProvider.class);
		
		bind(SensorEventProcessor.class).to(FSMComputedStateProcessor.class);
		
		bind(IImpactTimeFilterStart.class).to(ImpactTimeFilter.class);
		bind(IImpactTimeFilterStop.class).to(ImpactTimeFilter.class);
		bind(IImpactTimeFilter.class).to(ImpactTimeFilter.class);
		
		 // not used - if so, restore Singleton in EventsSaverBucket}
//		bind(EventsSaver.class).to(GoogleDriveCSVFileEventsSaver.class);
		
		bind(DetectionsSaver.class).to(GoogleDriveCSVFileDetectionsSaver.class);

		bindConstant().annotatedWith(DeviceName.class).to(DEVICE_NAME);
		bind(String.class).annotatedWith(AndroidID.class).toProvider(AndroidIDProvider.class);
		if (Config.TEST_or_DEV) {
			bind(SensorAccelerationListenerEventSavingProcessorDelegate.class).to(SensorAccelerationListenerEventSavingProcessorDelegateDev.class);
//			bind(ThresholdsComputer.class).to(ThresholdsComputerDEV.class);
		} else {
			bind(SensorAccelerationListenerEventSavingProcessorDelegate.class).to(SensorAccelerationListenerEventSavingProcessorDelegateProd.class);
//			bind(ThresholdsComputer.class).to(ThresholdsComputerProd.class);
		}
		bind(ThresholdsComputer.class).to(ThresholdsComputerProd.class);
		
		bind(EventBus.class).toInstance(EventBus.getDefault());
	}
	
	private final static String DEVICE_NAME = DeviceUtils.getDeviceName();
}
