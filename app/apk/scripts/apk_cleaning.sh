#!/bin/bash

[ $# -lt 1 ] && exit 1

DIR=$1
APK=SOSManDown.apk

zip -d $DIR/$APK META-INF/\*

mv $DIR/$APK $DIR/"$(basename $APK .apk)__cleaned.apk"
