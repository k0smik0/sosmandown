#!/bin/bash

function doStuff() {
   local label=$1
   local script="$2"
   local dir_now=$3
   echo "#### $label: start ####"
   bash $script $dir_now
   echo "#### $label: done ####"
}

DIR_SOURCE="../SoS_Man_Down"

grep debuggable $DIR_SOURCE/AndroidManifest.xml | grep -q true && echo "debug=true in manifest - rebuild apk!" && exit 1

if [ $# -eq 1 ]; then
   local_apk=$1
   build_apk=$DIR_SOURCE/bin/SOSManDown.apk
   diff $local_apk $build_apk
   if [ $? -ne 0 ]; then
      echo "$local_apk and $build_apk differ, exiting..."
      exit 1
   else
      APK=$1
   fi
else 
   echo "$(basename $0) FILE_TO_SIGN.apk"
   exit 1
fi


DIR_NOW=$(date "+%Y%m%d%H%M%S")

echo "using $DIR_NOW"

[ ! -d $DIR_NOW ] && mkdir $DIR_NOW

cp -av $APK $DIR_NOW/SOSManDown.apk

#echo "#### cleaning: start"
#bash ./scripts/remove_metainf.sh $DIR_NOW
#echo "#### cleaning: done"

#echo "#### aligning: start"
#bash ./scripts/zipalign.sh $DIR_NOW
#echo "#### aligning: done"

#echo "signing:"
#bash ./scripts/sign_release.sh $DIR_NOW
#echo "done"

doStuff cleaning ./scripts/apk_cleaning.sh $DIR_NOW
echo -e "\n"
doStuff signing ./scripts/apk_signing_release.sh $DIR_NOW
echo -e "\n"
doStuff aligning ./scripts/apk_zipaligning.sh $DIR_NOW
echo -e "\n"

mv -v $DIR_NOW/SOSManDown__final.apk $DIR_NOW/SOSManDown_${DIR_NOW}.apk