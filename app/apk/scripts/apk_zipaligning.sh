#!/bin/bash

[ $# -lt 1 ] && exit 1

DIR=$1


APK_CLEANED_SIGNED="SOSManDown__cleaned_signed.apk"

APK_FINAL="SOSManDown__final.apk"

#ALIGNED=$2


ZIP_ALIGN_DIR=$ANDROID_HOME/build-tools/23.0.2/

#VERBOSE="-v"
OPTIONS="-p 4"

$ZIP_ALIGN_DIR/zipalign $VERBOSE $OPTIONS $DIR/$APK_CLEANED_SIGNED $DIR/$APK_FINAL