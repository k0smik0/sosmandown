#!/bin/bash

[ $# -lt 1 ] && exit 1

DIR=$1

PASS="sosmandownkey0"
KEYSTORE="keystore/sosmandown.keystore"
#APK="$DIR/SOSManDown__cleaned_aligned.apk"
ALIAS="sosmandown"

APK_CLEANED="$DIR/SOSManDown__cleaned.apk"
APK_SIGNED="$DIR/SOSManDown__cleaned_signed.apk"

#VERBOSE="-verbose"
jarsigner $VERBOSE -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE -storepass $PASS $APK_CLEANED $ALIAS
#apksigner sign --ks $KEYSTORE $APK_CLEANED_ALIGNED
mv $APK_CLEANED $APK_SIGNED
