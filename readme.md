
# SoSManDown App

Welcome!  

Here you can find technical docs and source code for SoSManDown, an Android app providing an aid in falling down.   
It uses a deterministic algorithm to recognize if smartphone owner is probably fall down, and then an help request with geolocation/address is sent via sms to some configured contacts.    

The engine behind the scenes works on acceleration detection, and it analyze particular patterns in order to recognize for freefall-impact sequence; some filter is applied to raw detection:

 * time high pass filter: the acceleration changes potentially recognized as freefall-impact must be occur in a > 1.5 sec ( -> too fast impact are discarded)
 *  after a good impact detection: two sub-engines try to recognize some user activity and posture (walking, running, shaking the phone; sitting, standing) for next 30 seconds: first one using wavelet w3 transform on acceleration signal, second one using zero crossing rate on acceleration norm. If any activity is found, the freefall-impact sequence can not be accounted as an assumption for a falling down
 * instead, if no activity are found, a fall is probably occurred, and a bu tton countdown (with a beep sound) is started: the user can stop the alarm within other 30 seconds, then the help sms will be sent.

Main screen (an android activity) provides the countdown button and a summary of detected falls and some data for current fall detection; another screen provides multiple chart, showing acceleration detection, computed states (relax, free fall, impact, detection fall), activity and posture detection from sub-engines.


### update:
the project is actually no more developed, because I have not time to apply on: the last update branch is "feature/subwindow_detector__saver_refactoring", still not merged to master, develop/tough, and last apk builded is 1.3.2 in 2017.

## some screenshots:
<div float="left">
<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_splash.png" target="_blank"><div>activity splash</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_splash.png" width="200px"></a></span>

<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main_init__en.png" target="_blank"><div>activity main init</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main_init__en.png" width="200px"></a></span>

<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main_started__en.png" target="_blank"><div>activity main started</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main_started__en.png" width="200px"></a></span>

<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main_relax__en.png" target="_blank"><div>activity main relax</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main_relax__en.png" width="200px"></a></span>


<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_charts__relax__en.png" target="_blank"><div>activity charts  relax</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_charts__relax__en.png" width="200px"></a></span>

<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_charts__detecting_fall_1__en.png" target="_blank"><div>activity charts  detecting fall 1</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_charts__detecting_fall_1__en.png" width="200px"></a></span>

<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_charts__detecting_fall_2__en.png" target="_blank"><div>activity charts  detecting fall 2</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_charts__detecting_fall_2__en.png" width="200px"></a></span>

<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_charts__detecting_fall_relax__en.png" target="_blank"><div>activity charts  detecting fall relax</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_charts__detecting_fall_relax__en.png" width="200px"></a></span>


<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main_countdown_5__en.png" target="_blank"><div>activity main countdown 5</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main_countdown_5__en.png" width="200px"></a></span>


<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main__relax_with_some_counter__en.png" target="_blank"><div>activity main  relax with some counter</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_main__relax_with_some_counter__en.png" width="200px"></a></span>


<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_settings__en.png" target="_blank"><div>activity settings</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_settings__en.png" width="200px"></a></span>

<span float="left" clear="none"><a href="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_settings__user_not_configured__en.png" target="_blank"><div>activity settings  user not configured</div><img src="https://gitlab.com/k0smik0/sosmandown/raw/feature/subwindow_detector__saver_refactoring/app/images/screenshots/en//activity_settings__user_not_configured__en.png" width="200px"></a></span>

</div>
<br/><br/><br/><br/>





#### credits:
 * for activity recognizer, wavelet based: [http://mylifewithandroid.blogspot.com/2010/06/shake-walk-run.html](http://mylifewithandroid.blogspot.com/2010/06/shake-walk-run.html)
 * for posture recognizer, zero crossing based: [https://github.com/BharadwajS/Fall-detection-in-Android](https://github.com/BharadwajS/Fall-detection-in-Android)
 * for acceleration, computed states, posture chart: mpandroidchart [https://github.com/PhilJay/MPAndroidChart](https://github.com/PhilJay/MPAndroidChart)
 * for activity recognizer chart: [https://bitbucket.org/galmiza/spectrogram-android](https://bitbucket.org/galmiza/spectrogram-android)
 * for fall image: [https://pixabay.com/en/falling-tripping-down-stairs-99175/](https://pixabay.com/en/falling-tripping-down-stairs-99175/)
 * for showcase: [https://github.com/florent37/TutoShowcase](https://github.com/florent37/TutoShowcase)
 * for buttons: [http://angrytools.com/android/button/](http://angrytools.com/android/button/)
